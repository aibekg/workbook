import Toast from 'react-native-toast-message';
import {
  ApolloClient,
  ApolloLink,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client';
import {setContext} from '@apollo/client/link/context';
import {onError} from '@apollo/client/link/error';
import {GRAPHQL_ENDPOINT} from '@env';

import {selectAccessToken} from '@store/auth/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';

let reduxStore: any;
export const setReduxStore = (store: any) => {
  reduxStore = store;
};

const errorLink = onError(({graphQLErrors, networkError}) => {
  if (graphQLErrors && __DEV__) {
    if (graphQLErrors.length > 0) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: graphQLErrors[0].message || 'Please try again later',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
    graphQLErrors.forEach(({message, extensions}) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Stack: ${JSON.stringify(
          extensions,
        )}`,
      ),
    );
  }
  if (networkError && __DEV__) {
    console.log(`[Network error]: ${networkError.message}`);
  }
});

const apolloLink = ApolloLink.from([
  errorLink,
  setContext(async (_, prev) => {
    const state = reduxStore.getState();
    const accessToken = selectAccessToken(state);
    if (accessToken) {
      return {
        ...prev,
        headers: {
          ...prev.headers,
          authorization: `Bearer ${accessToken}`,
        },
      };
    }

    return prev;
  }),
  createHttpLink({uri: GRAPHQL_ENDPOINT}),
]);

const getApolloClient = () =>
  new ApolloClient({
    link: apolloLink,
    cache: new InMemoryCache(),
  });

export const apolloClient = getApolloClient();
