import {gql} from '@apollo/client';
import * as ApolloReactCommon from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends {[key: string]: unknown}> = {[K in keyof T]: T[K]};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
};

export type AuthUser = {
  token?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

export type Body = {
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
};

export type CarrierCompany = {
  dot?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
};

export type CarrierDriver = {
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  licenceNumber?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
};

export type CarrierDriverInput = {
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  licenceNumber?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
};

export type Color = {
  id: Scalars['Int'];
  value: Scalars['String'];
};

export type Comodity = {
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

export type ComodityType = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

export type Condition = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

export type Customer = {
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  warehouseCompany?: Maybe<WarehouseCompany>;
};

export type Damage = {
  affectedParts?: Maybe<Array<Maybe<Scalars['String']>>>;
  damageType?: Maybe<Scalars['String']>;
  detail?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  shape?: Maybe<Scalars['String']>;
  svg?: Maybe<Scalars['String']>;
  unit?: Maybe<Unit>;
};

export type DamageInput = {
  affectedParts?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  damageType?: InputMaybe<Scalars['String']>;
  detail?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  svg?: InputMaybe<Scalars['String']>;
};

export type Document = {
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

export type Environment = {
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  location: Location;
  temperatura: Scalars['Int'];
  unitId: Scalars['String'];
  weather: Scalars['String'];
};

export type EnvironmentInput = {
  unitId: Scalars['String'];
  weather: Scalars['String'];
};

export type EquipmentInput = {
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
};

export type ExteriorInput = {
  id?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  side?: InputMaybe<Scalars['String']>;
};

export type History = {
  actionId?: Maybe<Scalars['String']>;
  actionType?: Maybe<Scalars['String']>;
  createdAt: Scalars['Date'];
  device?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  unitId?: Maybe<Scalars['String']>;
  userId: Scalars['String'];
};

export type InBoundUnits = {
  data?: Maybe<Array<Maybe<Unit>>>;
  hiddenData?: Maybe<Array<Maybe<Unit>>>;
  status?: Maybe<Scalars['String']>;
};

export type InBoundUnitsDataArgs = {
  search?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type InBoundUnitsHiddenDataArgs = {
  search?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};

export type InteriorInput = {
  id?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  side?: InputMaybe<Scalars['String']>;
};

export type Keys = {
  id: Scalars['String'];
  manualKeyCount?: Maybe<Scalars['Int']>;
  photo?: Maybe<Scalars['String']>;
  remoteKeyCount?: Maybe<Scalars['Int']>;
};

export type Location = {
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
};

export type Mutation = {
  addEnvironment?: Maybe<Environment>;
  addNote?: Maybe<Note>;
  deleteNote: Scalars['Boolean'];
  login?: Maybe<AuthUser>;
  receiveUnit: Unit;
  saveImages: Scalars['String'];
  updateCarrierDriver: CarrierDriver;
  updateInBoundWarehouseAreaId: Scalars['Boolean'];
  updateUnit: Unit;
  updateUnitCarrierDriver?: Maybe<Unit>;
  updateUnitDocument?: Maybe<Unit>;
  updateUnitEquipment?: Maybe<UnitEquipment>;
  updateUnitEquipmentDamage?: Maybe<UnitEquipment>;
  updateUnitExterior?: Maybe<UnitExterior>;
  updateUnitExteriorDamage?: Maybe<UnitExterior>;
  updateUnitInterior?: Maybe<UnitInterior>;
  updateUnitInteriorDamage?: Maybe<UnitInterior>;
  updateUnitKeys?: Maybe<Unit>;
  updateUserProfile: User;
  userRegister: Scalars['Boolean'];
};

export type MutationAddEnvironmentArgs = {
  frontIp?: InputMaybe<Scalars['String']>;
  input: EnvironmentInput;
};

export type MutationAddNoteArgs = {
  input: NoteInput;
};

export type MutationDeleteNoteArgs = {
  id: Scalars['String'];
};

export type MutationLoginArgs = {
  login: Scalars['String'];
  password: Scalars['String'];
};

export type MutationReceiveUnitArgs = {
  bodyId: Scalars['Int'];
  colorId: Scalars['Int'];
  conditionId: Scalars['Int'];
  id: Scalars['String'];
};

export type MutationSaveImagesArgs = {
  imgBase64: Scalars['String'];
};

export type MutationUpdateCarrierDriverArgs = {
  input: CarrierDriverInput;
  photo?: InputMaybe<Scalars['String']>;
};

export type MutationUpdateInBoundWarehouseAreaIdArgs = {
  inBoundId: Scalars['String'];
  lotNumber: Scalars['String'];
  unitId: Scalars['String'];
  warehouseAreaId: Scalars['String'];
};

export type MutationUpdateUnitArgs = {
  input: UnitInput;
  photo?: InputMaybe<Scalars['String']>;
};

export type MutationUpdateUnitCarrierDriverArgs = {
  carrirerDriverId: Scalars['String'];
  unitId: Scalars['String'];
};

export type MutationUpdateUnitDocumentArgs = {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  photo: Scalars['String'];
  type: Scalars['String'];
  unitId: Scalars['String'];
};

export type MutationUpdateUnitEquipmentArgs = {
  equipmentInput: EquipmentInput;
  unitId: Scalars['String'];
};

export type MutationUpdateUnitEquipmentDamageArgs = {
  damageInput: Array<InputMaybe<DamageInput>>;
  equipmentId: Scalars['String'];
  unitId: Scalars['String'];
};

export type MutationUpdateUnitExteriorArgs = {
  exteriorInput: ExteriorInput;
  unitId: Scalars['String'];
};

export type MutationUpdateUnitExteriorDamageArgs = {
  damageInput: Array<InputMaybe<DamageInput>>;
  exteriorId: Scalars['String'];
  unitId: Scalars['String'];
};

export type MutationUpdateUnitInteriorArgs = {
  interiorInput: InteriorInput;
  unitId: Scalars['String'];
};

export type MutationUpdateUnitInteriorDamageArgs = {
  damageInput: Array<InputMaybe<DamageInput>>;
  interiorId: Scalars['String'];
  unitId: Scalars['String'];
};

export type MutationUpdateUnitKeysArgs = {
  manualKeyCount: Scalars['Int'];
  photo: Scalars['String'];
  remoteKeyCount: Scalars['Int'];
  unitId: Scalars['String'];
};

export type MutationUpdateUserProfileArgs = {
  input: UserInput;
  photo?: InputMaybe<Scalars['String']>;
};

export type MutationUserRegisterArgs = {
  input: UserInput;
};

export type Note = {
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  text: Scalars['String'];
  unit: Unit;
  unitId: Scalars['String'];
  user: User;
  userId: Scalars['String'];
};

export type NoteInput = {
  text: Scalars['String'];
  unitId: Scalars['String'];
};

export type Query = {
  carrierCompanies?: Maybe<Array<Maybe<CarrierCompany>>>;
  carrierDrivers?: Maybe<Array<Maybe<CarrierDriver>>>;
  currentUser?: Maybe<User>;
  environmentById?: Maybe<Environment>;
  historyByActionId?: Maybe<Array<Maybe<History>>>;
  historyByUnitId?: Maybe<Array<Maybe<History>>>;
  historyByUserId?: Maybe<Array<Maybe<History>>>;
  inBoundUnits?: Maybe<Array<Maybe<InBoundUnits>>>;
  listCustomers?: Maybe<Array<Maybe<Customer>>>;
  listEquipments?: Maybe<Array<Maybe<UnitEquipment>>>;
  noteById?: Maybe<Note>;
  unitBodies?: Maybe<Array<Maybe<Body>>>;
  unitById?: Maybe<Unit>;
  unitColors?: Maybe<Array<Maybe<Color>>>;
  unitComodities?: Maybe<Array<Maybe<Comodity>>>;
  unitEquipmentNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  unitEquipments?: Maybe<Array<Maybe<UnitEquipment>>>;
  unitExteriors?: Maybe<Array<Maybe<UnitExterior>>>;
  unitInteriors?: Maybe<Array<Maybe<UnitInterior>>>;
  unitManufacturers?: Maybe<Array<Maybe<Scalars['String']>>>;
  unitModels?: Maybe<Array<Maybe<Scalars['String']>>>;
  unitTypes?: Maybe<Array<Maybe<ComodityType>>>;
  userLocation?: Maybe<Location>;
  userParam?: Maybe<Scalars['String']>;
  vinData?: Maybe<VinDataResult>;
  warehouseAreaByWarehouseId?: Maybe<Array<Maybe<WarehouseArea>>>;
};

export type QueryCarrierCompaniesArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
};

export type QueryCarrierDriversArgs = {
  carrierCompanyId: Scalars['String'];
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
};

export type QueryEnvironmentByIdArgs = {
  id?: InputMaybe<Scalars['String']>;
};

export type QueryHistoryByActionIdArgs = {
  actionId?: InputMaybe<Scalars['String']>;
};

export type QueryHistoryByUnitIdArgs = {
  unitId?: InputMaybe<Scalars['String']>;
};

export type QueryHistoryByUserIdArgs = {
  userId?: InputMaybe<Scalars['String']>;
};

export type QueryNoteByIdArgs = {
  id?: InputMaybe<Scalars['String']>;
};

export type QueryUnitByIdArgs = {
  id?: InputMaybe<Scalars['String']>;
};

export type QueryUnitEquipmentsArgs = {
  unitId: Scalars['String'];
};

export type QueryUnitExteriorsArgs = {
  unitId: Scalars['String'];
};

export type QueryUnitInteriorsArgs = {
  unitId: Scalars['String'];
};

export type QueryUnitManufacturersArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
};

export type QueryUnitModelsArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  manufacturer: Scalars['String'];
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
};

export type QueryUnitTypesArgs = {
  comodityId: Scalars['Int'];
};

export type QueryUserLocationArgs = {
  frontIp?: InputMaybe<Scalars['String']>;
};

export type QueryVinDataArgs = {
  photo?: InputMaybe<Scalars['String']>;
  vin: Scalars['String'];
};

export type QueryWarehouseAreaByWarehouseIdArgs = {
  warehouseId?: InputMaybe<Scalars['String']>;
};

export type Unit = {
  body?: Maybe<Body>;
  carrierDriver?: Maybe<CarrierDriver>;
  color?: Maybe<Color>;
  comodity?: Maybe<Comodity>;
  comodityType?: Maybe<ComodityType>;
  condition?: Maybe<Condition>;
  createdAt?: Maybe<Scalars['Date']>;
  customer?: Maybe<Customer>;
  documents?: Maybe<Array<Maybe<Document>>>;
  equipments?: Maybe<Array<Maybe<UnitEquipment>>>;
  exteriors?: Maybe<Array<Maybe<UnitExterior>>>;
  id: Scalars['String'];
  inBoundId?: Maybe<Scalars['String']>;
  interiors?: Maybe<Array<Maybe<UnitInterior>>>;
  isForklift?: Maybe<Scalars['Boolean']>;
  keys?: Maybe<Keys>;
  manufacturer?: Maybe<Scalars['String']>;
  model?: Maybe<Scalars['String']>;
  notes?: Maybe<Array<Maybe<Note>>>;
  photo?: Maybe<Array<Maybe<Scalars['String']>>>;
  poNumber?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['Date']>;
  user?: Maybe<User>;
  vinNumber: Scalars['String'];
  vinPhoto?: Maybe<Scalars['String']>;
  wareHouseAreaLot?: Maybe<WarehouseAreaLot>;
  wareHouseAreaLotId?: Maybe<Scalars['String']>;
  year?: Maybe<Scalars['Int']>;
};

export type UnitEquipment = {
  damages?: Maybe<Array<Maybe<Damage>>>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
};

export type UnitExterior = {
  damages?: Maybe<Array<Maybe<Damage>>>;
  id?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  side?: Maybe<Scalars['String']>;
  svg?: Maybe<Scalars['String']>;
};

export type UnitInput = {
  bodyId?: InputMaybe<Scalars['Int']>;
  colorId?: InputMaybe<Scalars['Int']>;
  comodityId?: InputMaybe<Scalars['Int']>;
  comodityTypeId?: InputMaybe<Scalars['Int']>;
  customerId?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  manufacturer?: InputMaybe<Scalars['String']>;
  model?: InputMaybe<Scalars['String']>;
  poNumber?: InputMaybe<Scalars['Int']>;
  vinNumber?: InputMaybe<Scalars['String']>;
  wareHouseAreaLotId?: InputMaybe<Scalars['String']>;
  year?: InputMaybe<Scalars['Int']>;
};

export type UnitInterior = {
  damages?: Maybe<Array<Maybe<Damage>>>;
  id?: Maybe<Scalars['String']>;
  photo?: Maybe<Scalars['String']>;
  side?: Maybe<Scalars['String']>;
  svg?: Maybe<Scalars['String']>;
};

export type User = {
  email: Scalars['String'];
  id: Scalars['String'];
  isVerified: Scalars['Boolean'];
  lastLoginDate?: Maybe<Scalars['Date']>;
  login: Scalars['String'];
  photo?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Maybe<Scalars['String']>>>;
  userCreateDate: Scalars['Date'];
  userFirstName: Scalars['String'];
  userLastName: Scalars['String'];
  warehouse?: Maybe<Warehouse>;
  warehouseId?: Maybe<Scalars['String']>;
};

export type UserInput = {
  email?: InputMaybe<Scalars['String']>;
  isVerified?: InputMaybe<Scalars['Boolean']>;
  login?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  photo?: InputMaybe<Scalars['String']>;
  position?: InputMaybe<Scalars['String']>;
  userFirstName?: InputMaybe<Scalars['String']>;
  userLastName?: InputMaybe<Scalars['String']>;
  warehouseId?: InputMaybe<Scalars['String']>;
};

export type VinData = {
  assemblyPlant?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  details?: Maybe<Scalars['String']>;
  manufacturer?: Maybe<Scalars['String']>;
  model?: Maybe<Scalars['String']>;
  securityCode?: Maybe<Scalars['String']>;
  serialNumber?: Maybe<Scalars['String']>;
  year?: Maybe<Scalars['Int']>;
};

export type VinDataResult = {
  unit?: Maybe<Unit>;
  vinData?: Maybe<VinData>;
};

export type Warehouse = {
  active?: Maybe<Scalars['Boolean']>;
  createdAt: Scalars['Date'];
  id: Scalars['String'];
  name: Scalars['String'];
  warehouseCompany?: Maybe<WarehouseCompany>;
};

export type WarehouseArea = {
  active?: Maybe<Scalars['Boolean']>;
  color?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  warehouse?: Maybe<Warehouse>;
};

export type WarehouseAreaLot = {
  active?: Maybe<Scalars['Boolean']>;
  coordinates?: Maybe<Array<Maybe<Scalars['Int']>>>;
  id?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['String']>;
  warehouseArea?: Maybe<WarehouseArea>;
};

export type WarehouseCompany = {
  active?: Maybe<Scalars['Boolean']>;
  createdAt: Scalars['Date'];
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
};

export type LoginMutationVariables = Exact<{
  login: Scalars['String'];
  password: Scalars['String'];
}>;

export type LoginMutation = {
  login?: {
    token?: string | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      isVerified: boolean;
      photo?: string | null;
      userCreateDate: any;
      warehouseId?: string | null;
      roles?: Array<string | null> | null;
      login: string;
      warehouse?: {name: string} | null;
    } | null;
  } | null;
};

export type UserRegisterMutationVariables = Exact<{
  input: UserInput;
}>;

export type UserRegisterMutation = {userRegister: boolean};

export type UserItemFragment = {
  id: string;
  userFirstName: string;
  userLastName: string;
  email: string;
  isVerified: boolean;
  photo?: string | null;
  userCreateDate: any;
  warehouseId?: string | null;
  roles?: Array<string | null> | null;
  login: string;
  warehouse?: {name: string} | null;
};

export type UnitItemFragment = {
  id: string;
  wareHouseAreaLotId?: string | null;
  inBoundId?: string | null;
  manufacturer?: string | null;
  model?: string | null;
  poNumber?: number | null;
  status?: string | null;
  vinNumber: string;
  vinPhoto?: string | null;
  year?: number | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  photo?: Array<string | null> | null;
  body?: {id: number; name?: string | null} | null;
  condition?: {
    id?: number | null;
    name?: string | null;
    value?: string | null;
  } | null;
  color?: {id: number; value: string} | null;
  customer?: {id: string; name: string; email?: string | null} | null;
  user?: {
    id: string;
    userFirstName: string;
    userLastName: string;
    email: string;
    photo?: string | null;
  } | null;
};

export type CarrierDriverItemFragment = {
  email?: string | null;
  id: string;
  licenceNumber?: string | null;
  name?: string | null;
  phone?: string | null;
  photo?: string | null;
};

export type InBoundUnitItemFragment = {
  id: string;
  wareHouseAreaLotId?: string | null;
  inBoundId?: string | null;
  manufacturer?: string | null;
  model?: string | null;
  poNumber?: number | null;
  status?: string | null;
  vinNumber: string;
  vinPhoto?: string | null;
  year?: number | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  photo?: Array<string | null> | null;
  carrierDriver?: {
    email?: string | null;
    id: string;
    name?: string | null;
    phone?: string | null;
    photo?: string | null;
    licenceNumber?: string | null;
  } | null;
  user?: {
    id: string;
    userFirstName: string;
    userLastName: string;
    email: string;
    photo?: string | null;
  } | null;
  body?: {id: number; name?: string | null} | null;
  condition?: {
    id?: number | null;
    name?: string | null;
    value?: string | null;
  } | null;
  color?: {id: number; value: string} | null;
  customer?: {id: string; name: string; email?: string | null} | null;
};

export type UnitDetailFragment = {
  isForklift?: boolean | null;
  wareHouseAreaLotId?: string | null;
  id: string;
  inBoundId?: string | null;
  manufacturer?: string | null;
  model?: string | null;
  poNumber?: number | null;
  status?: string | null;
  vinNumber: string;
  vinPhoto?: string | null;
  year?: number | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  photo?: Array<string | null> | null;
  comodity?: {id?: number | null; name?: string | null} | null;
  comodityType?: {id?: number | null; name?: string | null} | null;
  keys?: {
    id: string;
    remoteKeyCount?: number | null;
    manualKeyCount?: number | null;
    photo?: string | null;
  } | null;
  documents?: Array<{
    description?: string | null;
    id: string;
    type?: string | null;
    photo?: string | null;
    name?: string | null;
  } | null> | null;
  carrierDriver?: {
    email?: string | null;
    id: string;
    name?: string | null;
    phone?: string | null;
    photo?: string | null;
    licenceNumber?: string | null;
  } | null;
  customer?: {id: string; email?: string | null; name: string} | null;
  user?: {
    id: string;
    userFirstName: string;
    userLastName: string;
    email: string;
    photo?: string | null;
  } | null;
  wareHouseAreaLot?: {
    active?: boolean | null;
    coordinates?: Array<number | null> | null;
    id?: string | null;
    warehouseArea?: {
      active?: boolean | null;
      color?: string | null;
      id: string;
      name: string;
      warehouse?: {
        active?: boolean | null;
        createdAt: any;
        id: string;
        name: string;
        warehouseCompany?: {
          active?: boolean | null;
          createdAt: any;
          email?: string | null;
          id: string;
          name: string;
          phone?: string | null;
        } | null;
      } | null;
    } | null;
  } | null;
  exteriors?: Array<{
    id?: string | null;
    side?: string | null;
    photo?: string | null;
    damages?: Array<{
      id?: string | null;
      damageType?: string | null;
      detail?: string | null;
    } | null> | null;
  } | null> | null;
  interiors?: Array<{
    id?: string | null;
    side?: string | null;
    photo?: string | null;
    damages?: Array<{
      id?: string | null;
      damageType?: string | null;
      detail?: string | null;
    } | null> | null;
  } | null> | null;
  equipments?: Array<{
    id?: string | null;
    name?: string | null;
    description?: string | null;
    photo?: string | null;
    damages?: Array<{
      id?: string | null;
      damageType?: string | null;
      detail?: string | null;
    } | null> | null;
  } | null> | null;
  body?: {id: number; name?: string | null} | null;
  condition?: {
    id?: number | null;
    name?: string | null;
    value?: string | null;
  } | null;
  color?: {id: number; value: string} | null;
};

export type UnitEnvironmentFragment = {
  createdAt: any;
  id: string;
  temperatura: number;
  unitId: string;
  weather: string;
};

export type SaveImagesMutationVariables = Exact<{
  image: Scalars['String'];
}>;

export type SaveImagesMutation = {saveImages: string};

export type UpdateUnitMutationVariables = Exact<{
  input: UnitInput;
}>;

export type UpdateUnitMutation = {
  updateUnit: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  };
};

export type ReceiveUnitMutationVariables = Exact<{
  bodyId: Scalars['Int'];
  colorId: Scalars['Int'];
  conditionId: Scalars['Int'];
  id: Scalars['String'];
}>;

export type ReceiveUnitMutation = {
  receiveUnit: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  };
};

export type UpdateCarrierDriverMutationVariables = Exact<{
  input: CarrierDriverInput;
  photo?: InputMaybe<Scalars['String']>;
}>;

export type UpdateCarrierDriverMutation = {
  updateCarrierDriver: {
    email?: string | null;
    id: string;
    licenceNumber?: string | null;
    name?: string | null;
    phone?: string | null;
    photo?: string | null;
  };
};

export type UpdateUnitCarrierDriverMutationVariables = Exact<{
  unitId: Scalars['String'];
  carrirerDriverId: Scalars['String'];
}>;

export type UpdateUnitCarrierDriverMutation = {
  updateUnitCarrierDriver?: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  } | null;
};

export type UpdateUnitKeysMutationVariables = Exact<{
  manualKeyCount: Scalars['Int'];
  remoteKeyCount: Scalars['Int'];
  photo: Scalars['String'];
  unitId: Scalars['String'];
}>;

export type UpdateUnitKeysMutation = {
  updateUnitKeys?: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  } | null;
};

export type UpdateUnitDocumentMutationVariables = Exact<{
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  photo: Scalars['String'];
  type: Scalars['String'];
  unitId: Scalars['String'];
}>;

export type UpdateUnitDocumentMutation = {
  updateUnitDocument?: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  } | null;
};

export type AddUnitEnvironmentMutationVariables = Exact<{
  frontIp?: InputMaybe<Scalars['String']>;
  input: EnvironmentInput;
}>;

export type AddUnitEnvironmentMutation = {
  addEnvironment?: {
    createdAt: any;
    id: string;
    temperatura: number;
    unitId: string;
    weather: string;
  } | null;
};

export type VinDataQueryVariables = Exact<{
  vin: Scalars['String'];
  photo?: InputMaybe<Scalars['String']>;
}>;

export type VinDataQuery = {
  vinData?: {
    vinData?: {
      assemblyPlant?: string | null;
      country?: string | null;
      details?: string | null;
      manufacturer?: string | null;
      securityCode?: string | null;
      serialNumber?: string | null;
      year?: number | null;
      model?: string | null;
      body?: string | null;
    } | null;
    unit?: {
      isForklift?: boolean | null;
      wareHouseAreaLotId?: string | null;
      id: string;
      inBoundId?: string | null;
      manufacturer?: string | null;
      model?: string | null;
      poNumber?: number | null;
      status?: string | null;
      vinNumber: string;
      vinPhoto?: string | null;
      year?: number | null;
      createdAt?: any | null;
      updatedAt?: any | null;
      photo?: Array<string | null> | null;
      comodity?: {id?: number | null; name?: string | null} | null;
      comodityType?: {id?: number | null; name?: string | null} | null;
      keys?: {
        id: string;
        remoteKeyCount?: number | null;
        manualKeyCount?: number | null;
        photo?: string | null;
      } | null;
      documents?: Array<{
        description?: string | null;
        id: string;
        type?: string | null;
        photo?: string | null;
        name?: string | null;
      } | null> | null;
      carrierDriver?: {
        email?: string | null;
        id: string;
        name?: string | null;
        phone?: string | null;
        photo?: string | null;
        licenceNumber?: string | null;
      } | null;
      customer?: {id: string; email?: string | null; name: string} | null;
      user?: {
        id: string;
        userFirstName: string;
        userLastName: string;
        email: string;
        photo?: string | null;
      } | null;
      wareHouseAreaLot?: {
        active?: boolean | null;
        coordinates?: Array<number | null> | null;
        id?: string | null;
        warehouseArea?: {
          active?: boolean | null;
          color?: string | null;
          id: string;
          name: string;
          warehouse?: {
            active?: boolean | null;
            createdAt: any;
            id: string;
            name: string;
            warehouseCompany?: {
              active?: boolean | null;
              createdAt: any;
              email?: string | null;
              id: string;
              name: string;
              phone?: string | null;
            } | null;
          } | null;
        } | null;
      } | null;
      exteriors?: Array<{
        id?: string | null;
        side?: string | null;
        photo?: string | null;
        damages?: Array<{
          id?: string | null;
          damageType?: string | null;
          detail?: string | null;
        } | null> | null;
      } | null> | null;
      interiors?: Array<{
        id?: string | null;
        side?: string | null;
        photo?: string | null;
        damages?: Array<{
          id?: string | null;
          damageType?: string | null;
          detail?: string | null;
        } | null> | null;
      } | null> | null;
      equipments?: Array<{
        id?: string | null;
        name?: string | null;
        description?: string | null;
        photo?: string | null;
        damages?: Array<{
          id?: string | null;
          damageType?: string | null;
          detail?: string | null;
        } | null> | null;
      } | null> | null;
      body?: {id: number; name?: string | null} | null;
      condition?: {
        id?: number | null;
        name?: string | null;
        value?: string | null;
      } | null;
      color?: {id: number; value: string} | null;
    } | null;
  } | null;
};

export type UnitByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type UnitByIdQuery = {
  unitById?: {
    isForklift?: boolean | null;
    wareHouseAreaLotId?: string | null;
    id: string;
    inBoundId?: string | null;
    manufacturer?: string | null;
    model?: string | null;
    poNumber?: number | null;
    status?: string | null;
    vinNumber: string;
    vinPhoto?: string | null;
    year?: number | null;
    createdAt?: any | null;
    updatedAt?: any | null;
    photo?: Array<string | null> | null;
    comodity?: {id?: number | null; name?: string | null} | null;
    comodityType?: {id?: number | null; name?: string | null} | null;
    keys?: {
      id: string;
      remoteKeyCount?: number | null;
      manualKeyCount?: number | null;
      photo?: string | null;
    } | null;
    documents?: Array<{
      description?: string | null;
      id: string;
      type?: string | null;
      photo?: string | null;
      name?: string | null;
    } | null> | null;
    carrierDriver?: {
      email?: string | null;
      id: string;
      name?: string | null;
      phone?: string | null;
      photo?: string | null;
      licenceNumber?: string | null;
    } | null;
    customer?: {id: string; email?: string | null; name: string} | null;
    user?: {
      id: string;
      userFirstName: string;
      userLastName: string;
      email: string;
      photo?: string | null;
    } | null;
    wareHouseAreaLot?: {
      active?: boolean | null;
      coordinates?: Array<number | null> | null;
      id?: string | null;
      warehouseArea?: {
        active?: boolean | null;
        color?: string | null;
        id: string;
        name: string;
        warehouse?: {
          active?: boolean | null;
          createdAt: any;
          id: string;
          name: string;
          warehouseCompany?: {
            active?: boolean | null;
            createdAt: any;
            email?: string | null;
            id: string;
            name: string;
            phone?: string | null;
          } | null;
        } | null;
      } | null;
    } | null;
    exteriors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    interiors?: Array<{
      id?: string | null;
      side?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    equipments?: Array<{
      id?: string | null;
      name?: string | null;
      description?: string | null;
      photo?: string | null;
      damages?: Array<{
        id?: string | null;
        damageType?: string | null;
        detail?: string | null;
      } | null> | null;
    } | null> | null;
    body?: {id: number; name?: string | null} | null;
    condition?: {
      id?: number | null;
      name?: string | null;
      value?: string | null;
    } | null;
    color?: {id: number; value: string} | null;
  } | null;
};

export type UnitColorsQueryVariables = Exact<{[key: string]: never}>;

export type UnitColorsQuery = {
  unitColors?: Array<{id: number; value: string} | null> | null;
};

export type UnitComoditiesQueryVariables = Exact<{[key: string]: never}>;

export type UnitComoditiesQuery = {
  unitComodities?: Array<{
    id?: number | null;
    name?: string | null;
    description?: string | null;
  } | null> | null;
};

export type UnitTypesQueryVariables = Exact<{
  comodityId: Scalars['Int'];
}>;

export type UnitTypesQuery = {
  unitTypes?: Array<{id?: number | null; name?: string | null} | null> | null;
};

export type UnitBodiesQueryVariables = Exact<{[key: string]: never}>;

export type UnitBodiesQuery = {
  unitBodies?: Array<{
    id: number;
    name?: string | null;
    description?: string | null;
    photo?: string | null;
  } | null> | null;
};

export type UnitManufacturersQueryVariables = Exact<{
  search?: InputMaybe<Scalars['String']>;
  offset?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
}>;

export type UnitManufacturersQuery = {
  unitManufacturers?: Array<string | null> | null;
};

export type UnitModelsQueryVariables = Exact<{
  manufacturer: Scalars['String'];
  search?: InputMaybe<Scalars['String']>;
  offset?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
}>;

export type UnitModelsQuery = {unitModels?: Array<string | null> | null};

export type CarrierCompaniesQueryVariables = Exact<{
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
}>;

export type CarrierCompaniesQuery = {
  carrierCompanies?: Array<{
    dot?: string | null;
    email?: string | null;
    id: string;
    name?: string | null;
    phone?: string | null;
  } | null> | null;
};

export type CarrierDriversQueryVariables = Exact<{
  carrierCompanyId: Scalars['String'];
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
}>;

export type CarrierDriversQuery = {
  carrierDrivers?: Array<{
    email?: string | null;
    id: string;
    name?: string | null;
    phone?: string | null;
    photo?: string | null;
    licenceNumber?: string | null;
  } | null> | null;
};

export type UnitExteriorsQueryVariables = Exact<{
  unitId: Scalars['String'];
}>;

export type UnitExteriorsQuery = {
  unitExteriors?: Array<{
    id?: string | null;
    side?: string | null;
    svg?: string | null;
    photo?: string | null;
    damages?: Array<{
      affectedParts?: Array<string | null> | null;
      damageType?: string | null;
      detail?: string | null;
      shape?: string | null;
      id?: string | null;
      photo?: string | null;
      svg?: string | null;
    } | null> | null;
  } | null> | null;
};

export type UpdateUnitExteriorMutationVariables = Exact<{
  unitId: Scalars['String'];
  exteriorInput: ExteriorInput;
}>;

export type UpdateUnitExteriorMutation = {
  updateUnitExterior?: {
    id?: string | null;
    side?: string | null;
    svg?: string | null;
    photo?: string | null;
    damages?: Array<{
      affectedParts?: Array<string | null> | null;
      damageType?: string | null;
      detail?: string | null;
      shape?: string | null;
      id?: string | null;
      photo?: string | null;
      svg?: string | null;
    } | null> | null;
  } | null;
};

export type UpdateUnitExteriorDamagesMutationVariables = Exact<{
  unitId: Scalars['String'];
  exteriorId: Scalars['String'];
  damageInput: Array<InputMaybe<DamageInput>> | InputMaybe<DamageInput>;
}>;

export type UpdateUnitExteriorDamagesMutation = {
  updateUnitExteriorDamage?: {
    id?: string | null;
    side?: string | null;
    svg?: string | null;
    photo?: string | null;
    damages?: Array<{
      affectedParts?: Array<string | null> | null;
      damageType?: string | null;
      detail?: string | null;
      shape?: string | null;
      id?: string | null;
      photo?: string | null;
      svg?: string | null;
    } | null> | null;
  } | null;
};

export type WarehouseAreaQueryVariables = Exact<{
  warehouseId?: InputMaybe<Scalars['String']>;
}>;

export type WarehouseAreaQuery = {
  warehouseAreaByWarehouseId?: Array<{
    id: string;
    active?: boolean | null;
    color?: string | null;
    name: string;
    warehouse?: {id: string; active?: boolean | null} | null;
  } | null> | null;
};

export type UpdateWarehouseLotMutationVariables = Exact<{
  inBoundId: Scalars['String'];
  unitId: Scalars['String'];
  warehouseAreaId: Scalars['String'];
  lotNumber: Scalars['String'];
}>;

export type UpdateWarehouseLotMutation = {
  updateInBoundWarehouseAreaId: boolean;
};

export type InBoundUnitsQueryVariables = Exact<{
  dataStatus?: InputMaybe<Scalars['String']>;
  hiddenDataStatus?: InputMaybe<Scalars['String']>;
  dataSearch?: InputMaybe<Scalars['String']>;
  hiddenDataSearch?: InputMaybe<Scalars['String']>;
}>;

export type InBoundUnitsQuery = {
  inBoundUnits?: Array<{
    status?: string | null;
    data?: Array<{
      id: string;
      wareHouseAreaLotId?: string | null;
      inBoundId?: string | null;
      manufacturer?: string | null;
      model?: string | null;
      poNumber?: number | null;
      status?: string | null;
      vinNumber: string;
      vinPhoto?: string | null;
      year?: number | null;
      createdAt?: any | null;
      updatedAt?: any | null;
      photo?: Array<string | null> | null;
      carrierDriver?: {
        email?: string | null;
        id: string;
        name?: string | null;
        phone?: string | null;
        photo?: string | null;
        licenceNumber?: string | null;
      } | null;
      user?: {
        id: string;
        userFirstName: string;
        userLastName: string;
        email: string;
        photo?: string | null;
      } | null;
      body?: {id: number; name?: string | null} | null;
      condition?: {
        id?: number | null;
        name?: string | null;
        value?: string | null;
      } | null;
      color?: {id: number; value: string} | null;
      customer?: {id: string; name: string; email?: string | null} | null;
    } | null> | null;
    hiddenData?: Array<{
      id: string;
      wareHouseAreaLotId?: string | null;
      inBoundId?: string | null;
      manufacturer?: string | null;
      model?: string | null;
      poNumber?: number | null;
      status?: string | null;
      vinNumber: string;
      vinPhoto?: string | null;
      year?: number | null;
      createdAt?: any | null;
      updatedAt?: any | null;
      photo?: Array<string | null> | null;
      carrierDriver?: {
        email?: string | null;
        id: string;
        name?: string | null;
        phone?: string | null;
        photo?: string | null;
        licenceNumber?: string | null;
      } | null;
      user?: {
        id: string;
        userFirstName: string;
        userLastName: string;
        email: string;
        photo?: string | null;
      } | null;
      body?: {id: number; name?: string | null} | null;
      condition?: {
        id?: number | null;
        name?: string | null;
        value?: string | null;
      } | null;
      color?: {id: number; value: string} | null;
      customer?: {id: string; name: string; email?: string | null} | null;
    } | null> | null;
  } | null> | null;
};

export type UpdateUserProfileMutationVariables = Exact<{
  input: UserInput;
  photo?: InputMaybe<Scalars['String']>;
}>;

export type UpdateUserProfileMutation = {
  updateUserProfile: {
    id: string;
    userFirstName: string;
    userLastName: string;
    email: string;
    isVerified: boolean;
    photo?: string | null;
    userCreateDate: any;
    warehouseId?: string | null;
    roles?: Array<string | null> | null;
    login: string;
    warehouse?: {name: string} | null;
  };
};

export type CurrentUserQueryVariables = Exact<{[key: string]: never}>;

export type CurrentUserQuery = {
  currentUser?: {
    id: string;
    userFirstName: string;
    userLastName: string;
    email: string;
    isVerified: boolean;
    photo?: string | null;
    userCreateDate: any;
    warehouseId?: string | null;
    roles?: Array<string | null> | null;
    login: string;
    warehouse?: {name: string} | null;
  } | null;
};

export const UserItemFragmentDoc = gql`
  fragment UserItem on User {
    id
    userFirstName
    userLastName
    email
    isVerified
    photo
    userCreateDate
    warehouseId
    warehouse {
      name
    }
    roles
    login
  }
`;
export const CarrierDriverItemFragmentDoc = gql`
  fragment CarrierDriverItem on CarrierDriver {
    email
    id
    licenceNumber
    name
    phone
    photo
  }
`;
export const UnitItemFragmentDoc = gql`
  fragment UnitItem on Unit {
    id
    wareHouseAreaLotId
    inBoundId
    manufacturer
    model
    poNumber
    status
    vinNumber
    vinPhoto
    year
    createdAt
    updatedAt
    photo
    body {
      id
      name
    }
    condition {
      id
      name
      value
    }
    color {
      id
      value
    }
    customer {
      id
      name
      email
    }
    user {
      id
      userFirstName
      userLastName
      email
      photo
    }
  }
`;
export const InBoundUnitItemFragmentDoc = gql`
  fragment InBoundUnitItem on Unit {
    ...UnitItem
    carrierDriver {
      email
      id
      name
      phone
      photo
      licenceNumber
    }
    user {
      id
      userFirstName
      userLastName
      email
      photo
    }
  }
  ${UnitItemFragmentDoc}
`;
export const UnitDetailFragmentDoc = gql`
  fragment UnitDetail on Unit {
    ...UnitItem
    isForklift
    comodity {
      id
      name
    }
    comodityType {
      id
      name
    }
    keys {
      id
      remoteKeyCount
      manualKeyCount
      photo
    }
    documents {
      description
      id
      type
      photo
      name
    }
    carrierDriver {
      email
      id
      name
      phone
      photo
      licenceNumber
    }
    customer {
      id
      email
    }
    user {
      id
      userFirstName
      userLastName
      email
      photo
    }
    wareHouseAreaLotId
    wareHouseAreaLot {
      active
      coordinates
      id
      warehouseArea {
        active
        color
        id
        name
        warehouse {
          active
          createdAt
          id
          name
          warehouseCompany {
            active
            createdAt
            email
            id
            name
            phone
          }
        }
      }
    }
    exteriors {
      id
      side
      photo
      damages {
        id
        damageType
        detail
      }
    }
    interiors {
      id
      side
      photo
      damages {
        id
        damageType
        detail
      }
    }
    equipments {
      id
      name
      description
      photo
      damages {
        id
        damageType
        detail
      }
    }
  }
  ${UnitItemFragmentDoc}
`;
export const UnitEnvironmentFragmentDoc = gql`
  fragment UnitEnvironment on Environment {
    createdAt
    id
    temperatura
    unitId
    weather
  }
`;
export const LoginDocument = gql`
  mutation login($login: String!, $password: String!) {
    login(login: $login, password: $password) {
      token
      user {
        ...UserItem
      }
    }
  }
  ${UserItemFragmentDoc}
`;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<
  LoginMutation,
  LoginMutationVariables
>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      login: // value for 'login'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    LoginMutation,
    LoginMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(
    LoginDocument,
    options,
  );
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult =
  ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const UserRegisterDocument = gql`
  mutation userRegister($input: UserInput!) {
    userRegister(input: $input)
  }
`;
export type UserRegisterMutationFn = ApolloReactCommon.MutationFunction<
  UserRegisterMutation,
  UserRegisterMutationVariables
>;

/**
 * __useUserRegisterMutation__
 *
 * To run a mutation, you first call `useUserRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userRegisterMutation, { data, loading, error }] = useUserRegisterMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUserRegisterMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UserRegisterMutation,
    UserRegisterMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UserRegisterMutation,
    UserRegisterMutationVariables
  >(UserRegisterDocument, options);
}
export type UserRegisterMutationHookResult = ReturnType<
  typeof useUserRegisterMutation
>;
export type UserRegisterMutationResult =
  ApolloReactCommon.MutationResult<UserRegisterMutation>;
export type UserRegisterMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UserRegisterMutation,
  UserRegisterMutationVariables
>;
export const SaveImagesDocument = gql`
  mutation saveImages($image: String!) {
    saveImages(imgBase64: $image)
  }
`;
export type SaveImagesMutationFn = ApolloReactCommon.MutationFunction<
  SaveImagesMutation,
  SaveImagesMutationVariables
>;

/**
 * __useSaveImagesMutation__
 *
 * To run a mutation, you first call `useSaveImagesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSaveImagesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [saveImagesMutation, { data, loading, error }] = useSaveImagesMutation({
 *   variables: {
 *      image: // value for 'image'
 *   },
 * });
 */
export function useSaveImagesMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    SaveImagesMutation,
    SaveImagesMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    SaveImagesMutation,
    SaveImagesMutationVariables
  >(SaveImagesDocument, options);
}
export type SaveImagesMutationHookResult = ReturnType<
  typeof useSaveImagesMutation
>;
export type SaveImagesMutationResult =
  ApolloReactCommon.MutationResult<SaveImagesMutation>;
export type SaveImagesMutationOptions = ApolloReactCommon.BaseMutationOptions<
  SaveImagesMutation,
  SaveImagesMutationVariables
>;
export const UpdateUnitDocument = gql`
  mutation updateUnit($input: UnitInput!) {
    updateUnit(input: $input) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;
export type UpdateUnitMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUnitMutation,
  UpdateUnitMutationVariables
>;

/**
 * __useUpdateUnitMutation__
 *
 * To run a mutation, you first call `useUpdateUnitMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitMutation, { data, loading, error }] = useUpdateUnitMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUnitMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitMutation,
    UpdateUnitMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitMutation,
    UpdateUnitMutationVariables
  >(UpdateUnitDocument, options);
}
export type UpdateUnitMutationHookResult = ReturnType<
  typeof useUpdateUnitMutation
>;
export type UpdateUnitMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitMutation>;
export type UpdateUnitMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateUnitMutation,
  UpdateUnitMutationVariables
>;
export const ReceiveUnitDocument = gql`
  mutation receiveUnit(
    $bodyId: Int!
    $colorId: Int!
    $conditionId: Int!
    $id: String!
  ) {
    receiveUnit(
      bodyId: $bodyId
      colorId: $colorId
      conditionId: $conditionId
      id: $id
    ) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;
export type ReceiveUnitMutationFn = ApolloReactCommon.MutationFunction<
  ReceiveUnitMutation,
  ReceiveUnitMutationVariables
>;

/**
 * __useReceiveUnitMutation__
 *
 * To run a mutation, you first call `useReceiveUnitMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useReceiveUnitMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [receiveUnitMutation, { data, loading, error }] = useReceiveUnitMutation({
 *   variables: {
 *      bodyId: // value for 'bodyId'
 *      colorId: // value for 'colorId'
 *      conditionId: // value for 'conditionId'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useReceiveUnitMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    ReceiveUnitMutation,
    ReceiveUnitMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    ReceiveUnitMutation,
    ReceiveUnitMutationVariables
  >(ReceiveUnitDocument, options);
}
export type ReceiveUnitMutationHookResult = ReturnType<
  typeof useReceiveUnitMutation
>;
export type ReceiveUnitMutationResult =
  ApolloReactCommon.MutationResult<ReceiveUnitMutation>;
export type ReceiveUnitMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ReceiveUnitMutation,
  ReceiveUnitMutationVariables
>;
export const UpdateCarrierDriverDocument = gql`
  mutation updateCarrierDriver($input: CarrierDriverInput!, $photo: String) {
    updateCarrierDriver(input: $input, photo: $photo) {
      ...CarrierDriverItem
    }
  }
  ${CarrierDriverItemFragmentDoc}
`;
export type UpdateCarrierDriverMutationFn = ApolloReactCommon.MutationFunction<
  UpdateCarrierDriverMutation,
  UpdateCarrierDriverMutationVariables
>;

/**
 * __useUpdateCarrierDriverMutation__
 *
 * To run a mutation, you first call `useUpdateCarrierDriverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCarrierDriverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCarrierDriverMutation, { data, loading, error }] = useUpdateCarrierDriverMutation({
 *   variables: {
 *      input: // value for 'input'
 *      photo: // value for 'photo'
 *   },
 * });
 */
export function useUpdateCarrierDriverMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateCarrierDriverMutation,
    UpdateCarrierDriverMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateCarrierDriverMutation,
    UpdateCarrierDriverMutationVariables
  >(UpdateCarrierDriverDocument, options);
}
export type UpdateCarrierDriverMutationHookResult = ReturnType<
  typeof useUpdateCarrierDriverMutation
>;
export type UpdateCarrierDriverMutationResult =
  ApolloReactCommon.MutationResult<UpdateCarrierDriverMutation>;
export type UpdateCarrierDriverMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateCarrierDriverMutation,
    UpdateCarrierDriverMutationVariables
  >;
export const UpdateUnitCarrierDriverDocument = gql`
  mutation updateUnitCarrierDriver(
    $unitId: String!
    $carrirerDriverId: String!
  ) {
    updateUnitCarrierDriver(
      unitId: $unitId
      carrirerDriverId: $carrirerDriverId
    ) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;
export type UpdateUnitCarrierDriverMutationFn =
  ApolloReactCommon.MutationFunction<
    UpdateUnitCarrierDriverMutation,
    UpdateUnitCarrierDriverMutationVariables
  >;

/**
 * __useUpdateUnitCarrierDriverMutation__
 *
 * To run a mutation, you first call `useUpdateUnitCarrierDriverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitCarrierDriverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitCarrierDriverMutation, { data, loading, error }] = useUpdateUnitCarrierDriverMutation({
 *   variables: {
 *      unitId: // value for 'unitId'
 *      carrirerDriverId: // value for 'carrirerDriverId'
 *   },
 * });
 */
export function useUpdateUnitCarrierDriverMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitCarrierDriverMutation,
    UpdateUnitCarrierDriverMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitCarrierDriverMutation,
    UpdateUnitCarrierDriverMutationVariables
  >(UpdateUnitCarrierDriverDocument, options);
}
export type UpdateUnitCarrierDriverMutationHookResult = ReturnType<
  typeof useUpdateUnitCarrierDriverMutation
>;
export type UpdateUnitCarrierDriverMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitCarrierDriverMutation>;
export type UpdateUnitCarrierDriverMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUnitCarrierDriverMutation,
    UpdateUnitCarrierDriverMutationVariables
  >;
export const UpdateUnitKeysDocument = gql`
  mutation updateUnitKeys(
    $manualKeyCount: Int!
    $remoteKeyCount: Int!
    $photo: String!
    $unitId: String!
  ) {
    updateUnitKeys(
      manualKeyCount: $manualKeyCount
      remoteKeyCount: $remoteKeyCount
      photo: $photo
      unitId: $unitId
    ) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;
export type UpdateUnitKeysMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUnitKeysMutation,
  UpdateUnitKeysMutationVariables
>;

/**
 * __useUpdateUnitKeysMutation__
 *
 * To run a mutation, you first call `useUpdateUnitKeysMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitKeysMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitKeysMutation, { data, loading, error }] = useUpdateUnitKeysMutation({
 *   variables: {
 *      manualKeyCount: // value for 'manualKeyCount'
 *      remoteKeyCount: // value for 'remoteKeyCount'
 *      photo: // value for 'photo'
 *      unitId: // value for 'unitId'
 *   },
 * });
 */
export function useUpdateUnitKeysMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitKeysMutation,
    UpdateUnitKeysMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitKeysMutation,
    UpdateUnitKeysMutationVariables
  >(UpdateUnitKeysDocument, options);
}
export type UpdateUnitKeysMutationHookResult = ReturnType<
  typeof useUpdateUnitKeysMutation
>;
export type UpdateUnitKeysMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitKeysMutation>;
export type UpdateUnitKeysMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUnitKeysMutation,
    UpdateUnitKeysMutationVariables
  >;
export const UpdateUnitDocumentDocument = gql`
  mutation updateUnitDocument(
    $description: String
    $name: String!
    $photo: String!
    $type: String!
    $unitId: String!
  ) {
    updateUnitDocument(
      description: $description
      name: $name
      photo: $photo
      type: $type
      unitId: $unitId
    ) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;
export type UpdateUnitDocumentMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUnitDocumentMutation,
  UpdateUnitDocumentMutationVariables
>;

/**
 * __useUpdateUnitDocumentMutation__
 *
 * To run a mutation, you first call `useUpdateUnitDocumentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitDocumentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitDocumentMutation, { data, loading, error }] = useUpdateUnitDocumentMutation({
 *   variables: {
 *      description: // value for 'description'
 *      name: // value for 'name'
 *      photo: // value for 'photo'
 *      type: // value for 'type'
 *      unitId: // value for 'unitId'
 *   },
 * });
 */
export function useUpdateUnitDocumentMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitDocumentMutation,
    UpdateUnitDocumentMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitDocumentMutation,
    UpdateUnitDocumentMutationVariables
  >(UpdateUnitDocumentDocument, options);
}
export type UpdateUnitDocumentMutationHookResult = ReturnType<
  typeof useUpdateUnitDocumentMutation
>;
export type UpdateUnitDocumentMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitDocumentMutation>;
export type UpdateUnitDocumentMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUnitDocumentMutation,
    UpdateUnitDocumentMutationVariables
  >;
export const AddUnitEnvironmentDocument = gql`
  mutation addUnitEnvironment($frontIp: String, $input: EnvironmentInput!) {
    addEnvironment(input: $input, frontIp: $frontIp) {
      ...UnitEnvironment
    }
  }
  ${UnitEnvironmentFragmentDoc}
`;
export type AddUnitEnvironmentMutationFn = ApolloReactCommon.MutationFunction<
  AddUnitEnvironmentMutation,
  AddUnitEnvironmentMutationVariables
>;

/**
 * __useAddUnitEnvironmentMutation__
 *
 * To run a mutation, you first call `useAddUnitEnvironmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddUnitEnvironmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addUnitEnvironmentMutation, { data, loading, error }] = useAddUnitEnvironmentMutation({
 *   variables: {
 *      frontIp: // value for 'frontIp'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddUnitEnvironmentMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    AddUnitEnvironmentMutation,
    AddUnitEnvironmentMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    AddUnitEnvironmentMutation,
    AddUnitEnvironmentMutationVariables
  >(AddUnitEnvironmentDocument, options);
}
export type AddUnitEnvironmentMutationHookResult = ReturnType<
  typeof useAddUnitEnvironmentMutation
>;
export type AddUnitEnvironmentMutationResult =
  ApolloReactCommon.MutationResult<AddUnitEnvironmentMutation>;
export type AddUnitEnvironmentMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    AddUnitEnvironmentMutation,
    AddUnitEnvironmentMutationVariables
  >;
export const VinDataDocument = gql`
  query vinData($vin: String!, $photo: String) {
    vinData(vin: $vin, photo: $photo) {
      vinData {
        assemblyPlant
        country
        details
        manufacturer
        securityCode
        serialNumber
        year
        model
        body
      }
      unit {
        ...UnitDetail
      }
    }
  }
  ${UnitDetailFragmentDoc}
`;

/**
 * __useVinDataQuery__
 *
 * To run a query within a React component, call `useVinDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useVinDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useVinDataQuery({
 *   variables: {
 *      vin: // value for 'vin'
 *      photo: // value for 'photo'
 *   },
 * });
 */
export function useVinDataQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    VinDataQuery,
    VinDataQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<VinDataQuery, VinDataQueryVariables>(
    VinDataDocument,
    options,
  );
}
export function useVinDataLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    VinDataQuery,
    VinDataQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<VinDataQuery, VinDataQueryVariables>(
    VinDataDocument,
    options,
  );
}
export type VinDataQueryHookResult = ReturnType<typeof useVinDataQuery>;
export type VinDataLazyQueryHookResult = ReturnType<typeof useVinDataLazyQuery>;
export type VinDataQueryResult = ApolloReactCommon.QueryResult<
  VinDataQuery,
  VinDataQueryVariables
>;
export const UnitByIdDocument = gql`
  query unitById($id: String!) {
    unitById(id: $id) {
      ...UnitDetail
    }
  }
  ${UnitDetailFragmentDoc}
`;

/**
 * __useUnitByIdQuery__
 *
 * To run a query within a React component, call `useUnitByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUnitByIdQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    UnitByIdQuery,
    UnitByIdQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<UnitByIdQuery, UnitByIdQueryVariables>(
    UnitByIdDocument,
    options,
  );
}
export function useUnitByIdLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitByIdQuery,
    UnitByIdQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<UnitByIdQuery, UnitByIdQueryVariables>(
    UnitByIdDocument,
    options,
  );
}
export type UnitByIdQueryHookResult = ReturnType<typeof useUnitByIdQuery>;
export type UnitByIdLazyQueryHookResult = ReturnType<
  typeof useUnitByIdLazyQuery
>;
export type UnitByIdQueryResult = ApolloReactCommon.QueryResult<
  UnitByIdQuery,
  UnitByIdQueryVariables
>;
export const UnitColorsDocument = gql`
  query unitColors {
    unitColors {
      id
      value
    }
  }
`;

/**
 * __useUnitColorsQuery__
 *
 * To run a query within a React component, call `useUnitColorsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitColorsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitColorsQuery({
 *   variables: {
 *   },
 * });
 */
export function useUnitColorsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    UnitColorsQuery,
    UnitColorsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<UnitColorsQuery, UnitColorsQueryVariables>(
    UnitColorsDocument,
    options,
  );
}
export function useUnitColorsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitColorsQuery,
    UnitColorsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitColorsQuery,
    UnitColorsQueryVariables
  >(UnitColorsDocument, options);
}
export type UnitColorsQueryHookResult = ReturnType<typeof useUnitColorsQuery>;
export type UnitColorsLazyQueryHookResult = ReturnType<
  typeof useUnitColorsLazyQuery
>;
export type UnitColorsQueryResult = ApolloReactCommon.QueryResult<
  UnitColorsQuery,
  UnitColorsQueryVariables
>;
export const UnitComoditiesDocument = gql`
  query unitComodities {
    unitComodities {
      id
      name
      description
    }
  }
`;

/**
 * __useUnitComoditiesQuery__
 *
 * To run a query within a React component, call `useUnitComoditiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitComoditiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitComoditiesQuery({
 *   variables: {
 *   },
 * });
 */
export function useUnitComoditiesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    UnitComoditiesQuery,
    UnitComoditiesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    UnitComoditiesQuery,
    UnitComoditiesQueryVariables
  >(UnitComoditiesDocument, options);
}
export function useUnitComoditiesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitComoditiesQuery,
    UnitComoditiesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitComoditiesQuery,
    UnitComoditiesQueryVariables
  >(UnitComoditiesDocument, options);
}
export type UnitComoditiesQueryHookResult = ReturnType<
  typeof useUnitComoditiesQuery
>;
export type UnitComoditiesLazyQueryHookResult = ReturnType<
  typeof useUnitComoditiesLazyQuery
>;
export type UnitComoditiesQueryResult = ApolloReactCommon.QueryResult<
  UnitComoditiesQuery,
  UnitComoditiesQueryVariables
>;
export const UnitTypesDocument = gql`
  query unitTypes($comodityId: Int!) {
    unitTypes(comodityId: $comodityId) {
      id
      name
    }
  }
`;

/**
 * __useUnitTypesQuery__
 *
 * To run a query within a React component, call `useUnitTypesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitTypesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitTypesQuery({
 *   variables: {
 *      comodityId: // value for 'comodityId'
 *   },
 * });
 */
export function useUnitTypesQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    UnitTypesQuery,
    UnitTypesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<UnitTypesQuery, UnitTypesQueryVariables>(
    UnitTypesDocument,
    options,
  );
}
export function useUnitTypesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitTypesQuery,
    UnitTypesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<UnitTypesQuery, UnitTypesQueryVariables>(
    UnitTypesDocument,
    options,
  );
}
export type UnitTypesQueryHookResult = ReturnType<typeof useUnitTypesQuery>;
export type UnitTypesLazyQueryHookResult = ReturnType<
  typeof useUnitTypesLazyQuery
>;
export type UnitTypesQueryResult = ApolloReactCommon.QueryResult<
  UnitTypesQuery,
  UnitTypesQueryVariables
>;
export const UnitBodiesDocument = gql`
  query unitBodies {
    unitBodies {
      id
      name
      description
      photo
    }
  }
`;

/**
 * __useUnitBodiesQuery__
 *
 * To run a query within a React component, call `useUnitBodiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitBodiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitBodiesQuery({
 *   variables: {
 *   },
 * });
 */
export function useUnitBodiesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    UnitBodiesQuery,
    UnitBodiesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<UnitBodiesQuery, UnitBodiesQueryVariables>(
    UnitBodiesDocument,
    options,
  );
}
export function useUnitBodiesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitBodiesQuery,
    UnitBodiesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitBodiesQuery,
    UnitBodiesQueryVariables
  >(UnitBodiesDocument, options);
}
export type UnitBodiesQueryHookResult = ReturnType<typeof useUnitBodiesQuery>;
export type UnitBodiesLazyQueryHookResult = ReturnType<
  typeof useUnitBodiesLazyQuery
>;
export type UnitBodiesQueryResult = ApolloReactCommon.QueryResult<
  UnitBodiesQuery,
  UnitBodiesQueryVariables
>;
export const UnitManufacturersDocument = gql`
  query unitManufacturers($search: String, $offset: Int, $limit: Int) {
    unitManufacturers(offset: $offset, limit: $limit, search: $search)
  }
`;

/**
 * __useUnitManufacturersQuery__
 *
 * To run a query within a React component, call `useUnitManufacturersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitManufacturersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitManufacturersQuery({
 *   variables: {
 *      search: // value for 'search'
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useUnitManufacturersQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    UnitManufacturersQuery,
    UnitManufacturersQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    UnitManufacturersQuery,
    UnitManufacturersQueryVariables
  >(UnitManufacturersDocument, options);
}
export function useUnitManufacturersLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitManufacturersQuery,
    UnitManufacturersQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitManufacturersQuery,
    UnitManufacturersQueryVariables
  >(UnitManufacturersDocument, options);
}
export type UnitManufacturersQueryHookResult = ReturnType<
  typeof useUnitManufacturersQuery
>;
export type UnitManufacturersLazyQueryHookResult = ReturnType<
  typeof useUnitManufacturersLazyQuery
>;
export type UnitManufacturersQueryResult = ApolloReactCommon.QueryResult<
  UnitManufacturersQuery,
  UnitManufacturersQueryVariables
>;
export const UnitModelsDocument = gql`
  query unitModels(
    $manufacturer: String!
    $search: String
    $offset: Int
    $limit: Int
  ) {
    unitModels(
      manufacturer: $manufacturer
      offset: $offset
      limit: $limit
      search: $search
    )
  }
`;

/**
 * __useUnitModelsQuery__
 *
 * To run a query within a React component, call `useUnitModelsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitModelsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitModelsQuery({
 *   variables: {
 *      manufacturer: // value for 'manufacturer'
 *      search: // value for 'search'
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useUnitModelsQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    UnitModelsQuery,
    UnitModelsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<UnitModelsQuery, UnitModelsQueryVariables>(
    UnitModelsDocument,
    options,
  );
}
export function useUnitModelsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitModelsQuery,
    UnitModelsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitModelsQuery,
    UnitModelsQueryVariables
  >(UnitModelsDocument, options);
}
export type UnitModelsQueryHookResult = ReturnType<typeof useUnitModelsQuery>;
export type UnitModelsLazyQueryHookResult = ReturnType<
  typeof useUnitModelsLazyQuery
>;
export type UnitModelsQueryResult = ApolloReactCommon.QueryResult<
  UnitModelsQuery,
  UnitModelsQueryVariables
>;
export const CarrierCompaniesDocument = gql`
  query carrierCompanies($limit: Int, $offset: Int, $search: String) {
    carrierCompanies(limit: $limit, offset: $offset, search: $search) {
      dot
      email
      id
      name
      phone
    }
  }
`;

/**
 * __useCarrierCompaniesQuery__
 *
 * To run a query within a React component, call `useCarrierCompaniesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCarrierCompaniesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCarrierCompaniesQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      search: // value for 'search'
 *   },
 * });
 */
export function useCarrierCompaniesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CarrierCompaniesQuery,
    CarrierCompaniesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    CarrierCompaniesQuery,
    CarrierCompaniesQueryVariables
  >(CarrierCompaniesDocument, options);
}
export function useCarrierCompaniesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CarrierCompaniesQuery,
    CarrierCompaniesQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    CarrierCompaniesQuery,
    CarrierCompaniesQueryVariables
  >(CarrierCompaniesDocument, options);
}
export type CarrierCompaniesQueryHookResult = ReturnType<
  typeof useCarrierCompaniesQuery
>;
export type CarrierCompaniesLazyQueryHookResult = ReturnType<
  typeof useCarrierCompaniesLazyQuery
>;
export type CarrierCompaniesQueryResult = ApolloReactCommon.QueryResult<
  CarrierCompaniesQuery,
  CarrierCompaniesQueryVariables
>;
export const CarrierDriversDocument = gql`
  query carrierDrivers(
    $carrierCompanyId: String!
    $limit: Int
    $offset: Int
    $search: String
  ) {
    carrierDrivers(
      carrierCompanyId: $carrierCompanyId
      search: $search
      limit: $limit
      offset: $offset
    ) {
      email
      id
      name
      phone
      photo
      licenceNumber
    }
  }
`;

/**
 * __useCarrierDriversQuery__
 *
 * To run a query within a React component, call `useCarrierDriversQuery` and pass it any options that fit your needs.
 * When your component renders, `useCarrierDriversQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCarrierDriversQuery({
 *   variables: {
 *      carrierCompanyId: // value for 'carrierCompanyId'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      search: // value for 'search'
 *   },
 * });
 */
export function useCarrierDriversQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    CarrierDriversQuery,
    CarrierDriversQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    CarrierDriversQuery,
    CarrierDriversQueryVariables
  >(CarrierDriversDocument, options);
}
export function useCarrierDriversLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CarrierDriversQuery,
    CarrierDriversQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    CarrierDriversQuery,
    CarrierDriversQueryVariables
  >(CarrierDriversDocument, options);
}
export type CarrierDriversQueryHookResult = ReturnType<
  typeof useCarrierDriversQuery
>;
export type CarrierDriversLazyQueryHookResult = ReturnType<
  typeof useCarrierDriversLazyQuery
>;
export type CarrierDriversQueryResult = ApolloReactCommon.QueryResult<
  CarrierDriversQuery,
  CarrierDriversQueryVariables
>;
export const UnitExteriorsDocument = gql`
  query unitExteriors($unitId: String!) {
    unitExteriors(unitId: $unitId) {
      id
      side
      svg
      photo
      damages {
        affectedParts
        damageType
        detail
        shape
        id
        photo
        svg
      }
    }
  }
`;

/**
 * __useUnitExteriorsQuery__
 *
 * To run a query within a React component, call `useUnitExteriorsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUnitExteriorsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUnitExteriorsQuery({
 *   variables: {
 *      unitId: // value for 'unitId'
 *   },
 * });
 */
export function useUnitExteriorsQuery(
  baseOptions: ApolloReactHooks.QueryHookOptions<
    UnitExteriorsQuery,
    UnitExteriorsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    UnitExteriorsQuery,
    UnitExteriorsQueryVariables
  >(UnitExteriorsDocument, options);
}
export function useUnitExteriorsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    UnitExteriorsQuery,
    UnitExteriorsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    UnitExteriorsQuery,
    UnitExteriorsQueryVariables
  >(UnitExteriorsDocument, options);
}
export type UnitExteriorsQueryHookResult = ReturnType<
  typeof useUnitExteriorsQuery
>;
export type UnitExteriorsLazyQueryHookResult = ReturnType<
  typeof useUnitExteriorsLazyQuery
>;
export type UnitExteriorsQueryResult = ApolloReactCommon.QueryResult<
  UnitExteriorsQuery,
  UnitExteriorsQueryVariables
>;
export const UpdateUnitExteriorDocument = gql`
  mutation updateUnitExterior(
    $unitId: String!
    $exteriorInput: ExteriorInput!
  ) {
    updateUnitExterior(exteriorInput: $exteriorInput, unitId: $unitId) {
      id
      side
      svg
      photo
      damages {
        affectedParts
        damageType
        detail
        shape
        id
        photo
        svg
      }
    }
  }
`;
export type UpdateUnitExteriorMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUnitExteriorMutation,
  UpdateUnitExteriorMutationVariables
>;

/**
 * __useUpdateUnitExteriorMutation__
 *
 * To run a mutation, you first call `useUpdateUnitExteriorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitExteriorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitExteriorMutation, { data, loading, error }] = useUpdateUnitExteriorMutation({
 *   variables: {
 *      unitId: // value for 'unitId'
 *      exteriorInput: // value for 'exteriorInput'
 *   },
 * });
 */
export function useUpdateUnitExteriorMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitExteriorMutation,
    UpdateUnitExteriorMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitExteriorMutation,
    UpdateUnitExteriorMutationVariables
  >(UpdateUnitExteriorDocument, options);
}
export type UpdateUnitExteriorMutationHookResult = ReturnType<
  typeof useUpdateUnitExteriorMutation
>;
export type UpdateUnitExteriorMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitExteriorMutation>;
export type UpdateUnitExteriorMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUnitExteriorMutation,
    UpdateUnitExteriorMutationVariables
  >;
export const UpdateUnitExteriorDamagesDocument = gql`
  mutation updateUnitExteriorDamages(
    $unitId: String!
    $exteriorId: String!
    $damageInput: [DamageInput]!
  ) {
    updateUnitExteriorDamage(
      damageInput: $damageInput
      unitId: $unitId
      exteriorId: $exteriorId
    ) {
      id
      side
      svg
      photo
      damages {
        affectedParts
        damageType
        detail
        shape
        id
        photo
        svg
      }
    }
  }
`;
export type UpdateUnitExteriorDamagesMutationFn =
  ApolloReactCommon.MutationFunction<
    UpdateUnitExteriorDamagesMutation,
    UpdateUnitExteriorDamagesMutationVariables
  >;

/**
 * __useUpdateUnitExteriorDamagesMutation__
 *
 * To run a mutation, you first call `useUpdateUnitExteriorDamagesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUnitExteriorDamagesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUnitExteriorDamagesMutation, { data, loading, error }] = useUpdateUnitExteriorDamagesMutation({
 *   variables: {
 *      unitId: // value for 'unitId'
 *      exteriorId: // value for 'exteriorId'
 *      damageInput: // value for 'damageInput'
 *   },
 * });
 */
export function useUpdateUnitExteriorDamagesMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUnitExteriorDamagesMutation,
    UpdateUnitExteriorDamagesMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUnitExteriorDamagesMutation,
    UpdateUnitExteriorDamagesMutationVariables
  >(UpdateUnitExteriorDamagesDocument, options);
}
export type UpdateUnitExteriorDamagesMutationHookResult = ReturnType<
  typeof useUpdateUnitExteriorDamagesMutation
>;
export type UpdateUnitExteriorDamagesMutationResult =
  ApolloReactCommon.MutationResult<UpdateUnitExteriorDamagesMutation>;
export type UpdateUnitExteriorDamagesMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUnitExteriorDamagesMutation,
    UpdateUnitExteriorDamagesMutationVariables
  >;
export const WarehouseAreaDocument = gql`
  query warehouseArea($warehouseId: String) {
    warehouseAreaByWarehouseId(warehouseId: $warehouseId) {
      id
      active
      color
      name
      warehouse {
        id
        active
      }
    }
  }
`;

/**
 * __useWarehouseAreaQuery__
 *
 * To run a query within a React component, call `useWarehouseAreaQuery` and pass it any options that fit your needs.
 * When your component renders, `useWarehouseAreaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWarehouseAreaQuery({
 *   variables: {
 *      warehouseId: // value for 'warehouseId'
 *   },
 * });
 */
export function useWarehouseAreaQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    WarehouseAreaQuery,
    WarehouseAreaQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    WarehouseAreaQuery,
    WarehouseAreaQueryVariables
  >(WarehouseAreaDocument, options);
}
export function useWarehouseAreaLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    WarehouseAreaQuery,
    WarehouseAreaQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    WarehouseAreaQuery,
    WarehouseAreaQueryVariables
  >(WarehouseAreaDocument, options);
}
export type WarehouseAreaQueryHookResult = ReturnType<
  typeof useWarehouseAreaQuery
>;
export type WarehouseAreaLazyQueryHookResult = ReturnType<
  typeof useWarehouseAreaLazyQuery
>;
export type WarehouseAreaQueryResult = ApolloReactCommon.QueryResult<
  WarehouseAreaQuery,
  WarehouseAreaQueryVariables
>;
export const UpdateWarehouseLotDocument = gql`
  mutation updateWarehouseLot(
    $inBoundId: String!
    $unitId: String!
    $warehouseAreaId: String!
    $lotNumber: String!
  ) {
    updateInBoundWarehouseAreaId(
      inBoundId: $inBoundId
      warehouseAreaId: $warehouseAreaId
      unitId: $unitId
      lotNumber: $lotNumber
    )
  }
`;
export type UpdateWarehouseLotMutationFn = ApolloReactCommon.MutationFunction<
  UpdateWarehouseLotMutation,
  UpdateWarehouseLotMutationVariables
>;

/**
 * __useUpdateWarehouseLotMutation__
 *
 * To run a mutation, you first call `useUpdateWarehouseLotMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateWarehouseLotMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateWarehouseLotMutation, { data, loading, error }] = useUpdateWarehouseLotMutation({
 *   variables: {
 *      inBoundId: // value for 'inBoundId'
 *      unitId: // value for 'unitId'
 *      warehouseAreaId: // value for 'warehouseAreaId'
 *      lotNumber: // value for 'lotNumber'
 *   },
 * });
 */
export function useUpdateWarehouseLotMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateWarehouseLotMutation,
    UpdateWarehouseLotMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateWarehouseLotMutation,
    UpdateWarehouseLotMutationVariables
  >(UpdateWarehouseLotDocument, options);
}
export type UpdateWarehouseLotMutationHookResult = ReturnType<
  typeof useUpdateWarehouseLotMutation
>;
export type UpdateWarehouseLotMutationResult =
  ApolloReactCommon.MutationResult<UpdateWarehouseLotMutation>;
export type UpdateWarehouseLotMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateWarehouseLotMutation,
    UpdateWarehouseLotMutationVariables
  >;
export const InBoundUnitsDocument = gql`
  query inBoundUnits(
    $dataStatus: String
    $hiddenDataStatus: String
    $dataSearch: String
    $hiddenDataSearch: String
  ) {
    inBoundUnits {
      data(status: $dataStatus, search: $dataSearch) {
        ...InBoundUnitItem
      }
      hiddenData(status: $hiddenDataStatus, search: $hiddenDataSearch) {
        ...InBoundUnitItem
      }
      status
    }
  }
  ${InBoundUnitItemFragmentDoc}
`;

/**
 * __useInBoundUnitsQuery__
 *
 * To run a query within a React component, call `useInBoundUnitsQuery` and pass it any options that fit your needs.
 * When your component renders, `useInBoundUnitsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInBoundUnitsQuery({
 *   variables: {
 *      dataStatus: // value for 'dataStatus'
 *      hiddenDataStatus: // value for 'hiddenDataStatus'
 *      dataSearch: // value for 'dataSearch'
 *      hiddenDataSearch: // value for 'hiddenDataSearch'
 *   },
 * });
 */
export function useInBoundUnitsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    InBoundUnitsQuery,
    InBoundUnitsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<
    InBoundUnitsQuery,
    InBoundUnitsQueryVariables
  >(InBoundUnitsDocument, options);
}
export function useInBoundUnitsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    InBoundUnitsQuery,
    InBoundUnitsQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    InBoundUnitsQuery,
    InBoundUnitsQueryVariables
  >(InBoundUnitsDocument, options);
}
export type InBoundUnitsQueryHookResult = ReturnType<
  typeof useInBoundUnitsQuery
>;
export type InBoundUnitsLazyQueryHookResult = ReturnType<
  typeof useInBoundUnitsLazyQuery
>;
export type InBoundUnitsQueryResult = ApolloReactCommon.QueryResult<
  InBoundUnitsQuery,
  InBoundUnitsQueryVariables
>;
export const UpdateUserProfileDocument = gql`
  mutation updateUserProfile($input: UserInput!, $photo: String) {
    updateUserProfile(input: $input, photo: $photo) {
      ...UserItem
    }
  }
  ${UserItemFragmentDoc}
`;
export type UpdateUserProfileMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUserProfileMutation,
  UpdateUserProfileMutationVariables
>;

/**
 * __useUpdateUserProfileMutation__
 *
 * To run a mutation, you first call `useUpdateUserProfileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserProfileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserProfileMutation, { data, loading, error }] = useUpdateUserProfileMutation({
 *   variables: {
 *      input: // value for 'input'
 *      photo: // value for 'photo'
 *   },
 * });
 */
export function useUpdateUserProfileMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUserProfileMutation,
    UpdateUserProfileMutationVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useMutation<
    UpdateUserProfileMutation,
    UpdateUserProfileMutationVariables
  >(UpdateUserProfileDocument, options);
}
export type UpdateUserProfileMutationHookResult = ReturnType<
  typeof useUpdateUserProfileMutation
>;
export type UpdateUserProfileMutationResult =
  ApolloReactCommon.MutationResult<UpdateUserProfileMutation>;
export type UpdateUserProfileMutationOptions =
  ApolloReactCommon.BaseMutationOptions<
    UpdateUserProfileMutation,
    UpdateUserProfileMutationVariables
  >;
export const CurrentUserDocument = gql`
  query currentUser {
    currentUser {
      ...UserItem
    }
  }
  ${UserItemFragmentDoc}
`;

/**
 * __useCurrentUserQuery__
 *
 * To run a query within a React component, call `useCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentUserQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    CurrentUserQuery,
    CurrentUserQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useQuery<CurrentUserQuery, CurrentUserQueryVariables>(
    CurrentUserDocument,
    options,
  );
}
export function useCurrentUserLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    CurrentUserQuery,
    CurrentUserQueryVariables
  >,
) {
  const options = {...defaultOptions, ...baseOptions};
  return ApolloReactHooks.useLazyQuery<
    CurrentUserQuery,
    CurrentUserQueryVariables
  >(CurrentUserDocument, options);
}
export type CurrentUserQueryHookResult = ReturnType<typeof useCurrentUserQuery>;
export type CurrentUserLazyQueryHookResult = ReturnType<
  typeof useCurrentUserLazyQuery
>;
export type CurrentUserQueryResult = ApolloReactCommon.QueryResult<
  CurrentUserQuery,
  CurrentUserQueryVariables
>;
