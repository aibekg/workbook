import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {runOnJS} from 'react-native-reanimated';
import {
  Camera,
  useCameraDevices,
  useFrameProcessor,
} from 'react-native-vision-camera';
import {useNavigation} from '@react-navigation/native';
import {BarcodeFormat, scanBarcodes} from 'vision-camera-code-scanner';

import Button from '@components/Button';
import {useActions} from '@hooks/useActions';
import {useSaveImages} from '@hooks/useSaveImages';
import {useVinData} from '@hooks/useVinData';
import CameraButtonSvg from '@icons/camera-button.svg';
import XCircleSvg from '@icons/x-circle.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import {
  convertBlobToBase64,
  getBlobFromFileURI,
} from '@utils/imagePickerHelpers';

import {Unit} from '../../generated/graphql';

import {styles} from './styles';

const CodeScanner = () => {
  const {setNewUnitDataFiled, clearUnitDataFiled} = useActions();

  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.CodeScanner>>();

  const [hasPermission, setHasPermission] = React.useState(false);
  const [askedPermission, setAsskedPermission] = useState(false);
  const [barcodes, setBarcodes] = useState<any>([]);
  const [imgPath, setImgPath] = useState<string | null>(null);
  const [vinPhotoUpload, setVinPhoto] = useState<string | null>(null);
  const [isVinNotRecognized, setIsVinNotRecognized] = useState(false);

  const devices = useCameraDevices();
  const camera = useRef<Camera>(null);
  const frameProcessor = useFrameProcessor(frame => {
    'worklet';
    const detectedBarcodes = scanBarcodes(
      frame,
      [BarcodeFormat.CODE_39, BarcodeFormat.QR_CODE, BarcodeFormat.CODABAR],
      {
        checkInverted: true,
      },
    );
    runOnJS(setBarcodes)(detectedBarcodes);
  }, []);

  useEffect(() => {
    if (!hasPermission && askedPermission) {
      navigation.replace(ERootStackRoutes.EnterVin, {vinPhoto: vinPhotoUpload});
    }
  }, [hasPermission, askedPermission, navigation, vinPhotoUpload]);

  useEffect(() => {
    (async () => {
      try {
        const status = await Camera.requestCameraPermission();
        setHasPermission(status === 'authorized');
        setAsskedPermission(true);
      } catch (e) {}
    })();
  }, []);

  const {getVinData} = useVinData();
  const {saveImages} = useSaveImages();

  const handleTakeAPicture = useCallback(async () => {
    try {
      const photo = await camera.current?.takePhoto({
        flash: 'auto',
      });
      if (photo) {
        setImgPath(`file://${photo.path}`);
        let vinPhoto: null | string = null;
        try {
          const blob = await getBlobFromFileURI(photo.path);
          const base66Photo = await convertBlobToBase64(blob);
          if (base66Photo) {
            vinPhoto = await saveImages({image: base66Photo as string});
            setVinPhoto(vinPhoto);
          }
        } catch (e) {}
        const barcode = barcodes[0];
        if (!barcode) {
          setIsVinNotRecognized(true);
        } else {
          const vinNumber = barcodes[0].rawValue;
          const vinDataResponse = await getVinData({
            vin: vinNumber,
            photo: vinPhoto,
          });
          clearUnitDataFiled();
          setNewUnitDataFiled({
            type: 'vin',
            value: vinNumber || null,
          });
          if (vinDataResponse?.unit) {
            setNewUnitDataFiled({
              type: 'year',
              value: vinDataResponse?.unit?.year || null,
            });
            setNewUnitDataFiled({
              type: 'manufacturer',
              value: vinDataResponse?.unit?.manufacturer || null,
            });
            setNewUnitDataFiled({
              type: 'model',
              value: vinDataResponse?.unit?.model || null,
            });
            setNewUnitDataFiled({
              type: 'bodyId',
              value: vinDataResponse?.unit?.body?.id || null,
            });
            setNewUnitDataFiled({
              type: 'colorId',
              value: vinDataResponse?.unit?.color?.id || null,
            });
            setTimeout(() => {
              if (vinDataResponse?.unit?.id) {
                navigation.navigate({
                  name: ERootStackRoutes.ReceiveUnitConfirm,
                  params: {
                    unit: vinDataResponse?.unit as Unit,
                  },
                });
              }
            }, 600);
          } else {
            if (vinDataResponse?.vinData) {
              if (vinDataResponse?.vinData?.manufacturer) {
                setNewUnitDataFiled({
                  type: 'manufacturer',
                  value: vinDataResponse?.vinData?.manufacturer || null,
                });
              }
              if (vinDataResponse?.vinData?.year) {
                setNewUnitDataFiled({
                  type: 'year',
                  value: vinDataResponse?.vinData?.year || null,
                });
              }
            }
            setTimeout(() => {
              navigation.navigate({
                name: ERootStackRoutes.AddUnitCommodity,
                params: undefined,
              });
            }, 600);
          }
          setImgPath(null);
        }
      } else {
        setIsVinNotRecognized(true);
      }
    } catch (e) {
      setIsVinNotRecognized(true);
    }
  }, [
    saveImages,
    clearUnitDataFiled,
    setNewUnitDataFiled,
    getVinData,
    barcodes,
    navigation,
  ]);

  const handleCancel = useCallback(() => {
    navigation.goBack();
    setIsVinNotRecognized(false);
    setImgPath(null);
    clearUnitDataFiled();
  }, [clearUnitDataFiled, navigation]);

  const handleEnterManual = useCallback(() => {
    navigation.replace(ERootStackRoutes.EnterVin, {
      vinPhoto: vinPhotoUpload,
    });
    setIsVinNotRecognized(false);
    setImgPath(null);
    clearUnitDataFiled();
  }, [clearUnitDataFiled, navigation, vinPhotoUpload]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>
          {isVinNotRecognized
            ? 'Scan Vin'
            : 'Take a photo of Vin# Plate or other ID'}
        </Text>
        <TouchableOpacity onPress={handleCancel}>
          <XCircleSvg fill="#FFFFFF" />
        </TouchableOpacity>
      </View>
      <View style={styles.topTransparent} />
      <View style={styles.bottomTransparent} />
      {imgPath ? (
        <Image source={{uri: imgPath}} style={styles.camera} />
      ) : !devices.back ? (
        <View style={styles.camera} />
      ) : (
        <Camera
          ref={camera}
          style={styles.camera}
          device={devices.back}
          orientation="portrait"
          isActive={true}
          photo={true}
          frameProcessor={frameProcessor}
          frameProcessorFps={5}
        />
      )}
      {isVinNotRecognized ? (
        <Button
          buttonStyle={styles.btn}
          text="Skip and Enter Manually"
          onPress={handleEnterManual}
        />
      ) : (
        <TouchableOpacity onPress={handleTakeAPicture} style={styles.cameraBtn}>
          <CameraButtonSvg />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CodeScanner;
