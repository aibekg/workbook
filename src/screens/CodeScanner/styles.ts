import {Dimensions, StyleSheet} from 'react-native';

import {bottomSpace, statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(45,45,45)',
    position: 'relative',
  },
  camera: {
    flex: 1,
    width: '100%',
    backgroundColor: 'black',
    // height: Dimensions.get('window').width - 100,
  },
  barcodeTextURL: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  header: {
    position: 'absolute',
    top: statusBarHeight + 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 16,
    zIndex: 2,
  },
  text: {
    flex: 1,
    color: '#FFFFFF',
    textTransform: 'uppercase',
    fontWeight: '700',
    fontSize: 12,
    marginRight: 20,
  },
  cameraBtn: {
    position: 'absolute',
    bottom: bottomSpace,
    zIndex: 2,
  },
  btn: {
    left: 56,
    right: 56,
    backgroundColor: '#000000',
    position: 'absolute',
    bottom: bottomSpace + 16,
    zIndex: 10,
  },
  topTransparent: {
    height: Dimensions.get('window').height / 3.5,
    width: '100%',
    position: 'absolute',
    top: 0,
    backgroundColor: 'rgba(19,19,19,0.91)',
    zIndex: 1,
  },
  bottomTransparent: {
    height: Dimensions.get('window').height / 3.5,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'rgba(19,19,19,0.91)',
    zIndex: 1,
  },
});
