import React, {useCallback, useEffect} from 'react';
import {Platform, ScrollView, StatusBar, View} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useNavigation, useRoute} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useWarehouseAreas} from '@hooks/useWarehouseArea';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import Header from '@screens/Location/components/Header';
import Item from '@screens/Location/components/Item';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {WarehouseArea} from '../../../generated/graphql';
import {styles} from '../styles';

const WarehouseAreaScreen = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.LocationArea>>();

  const route = useRoute<TNavigationRouteProp<ERootStackRoutes.LocationArea>>();

  const {warehouseAreas, selectedWarehouseArea, setSelectedWarehouseArea} =
    useWarehouseAreas();

  useEffect(() => {
    if (route?.params?.unit?.wareHouseAreaLot?.warehouseArea) {
      setSelectedWarehouseArea(
        route?.params?.unit?.wareHouseAreaLot?.warehouseArea as WarehouseArea,
      );
    }
  }, [
    setSelectedWarehouseArea,
    route?.params?.unit?.wareHouseAreaLot?.warehouseArea,
  ]);

  const handlePress = useCallback(() => {
    if (selectedWarehouseArea) {
      navigation.navigate({
        name: ERootStackRoutes.LocationLot,
        params: {
          warehouseArea: selectedWarehouseArea,
          unit: route?.params?.unit,
        },
      });
    }
  }, [navigation, route?.params?.unit, selectedWarehouseArea]);

  const onSelectArea = useCallback(
    (areaId: string) => {
      const selectedArea = warehouseAreas.find(item => item.id === areaId);
      if (selectedArea) {
        setSelectedWarehouseArea(selectedArea);
      }
    },
    [setSelectedWarehouseArea, warehouseAreas],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="WAREHOUSE AREA" />
      <ScrollView
        style={styles.paddedList}
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.paddingContent}
        showsVerticalScrollIndicator={false}>
        {warehouseAreas.map(item => (
          <Item
            key={item.id}
            selected={selectedWarehouseArea?.id}
            color={item.color}
            title={item.name}
            value={item.id}
            onPress={onSelectArea}
          />
        ))}
      </ScrollView>
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitYear> />
          <Button
            disabled={!selectedWarehouseArea}
            hideWhenDisabled={true}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      )}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default WarehouseAreaScreen;
