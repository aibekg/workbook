export type TItemProps = {
  title: string;
  value: string;
  selected?: string | null;
  color?: string | null;
  onPress?: (item: string) => void;
};
