import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingVertical: 20,
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  selectedContainer: {
    borderRadius: 12,
    width: '100%',
    backgroundColor: '#1E1F24',
    paddingVertical: 20,
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  title: {
    fontSize: 28,
    color: '#212121',
  },
  selectedTitle: {
    fontSize: 28,
    color: '#FFFFFF',
  },
  circle: {
    width: 32,
    height: 32,
    borderRadius: 16,
    borderColor: '#90909040',
    marginLeft: 16,
  },
  markerIcon: {
    width: 32,
    height: 32,
    marginLeft: 16,
  },
  opacity: {
    opacity: 0.5,
  },
});
