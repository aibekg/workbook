import React, {FC, useCallback, useMemo} from 'react';
import {Keyboard, Text, TouchableOpacity, View} from 'react-native';

import Marker from '@icons/marker.svg';

import {styles} from './styles';
import {TItemProps} from './types';

const Item: FC<TItemProps> = React.memo(
  ({title, value, color, onPress, selected}) => {
    const handlePress = useCallback(() => {
      Keyboard.dismiss();
      onPress && onPress(value);
    }, [onPress, value]);

    const isSelected = useMemo(
      () => !!selected && selected === value,
      [selected, value],
    );

    const isNotSelected = useMemo(
      () => !!selected && !(selected === value),
      [selected, value],
    );

    return (
      <TouchableOpacity
        style={[
          isSelected ? styles.selectedContainer : styles.container,
          isNotSelected && styles.opacity,
        ]}
        onPress={handlePress}>
        <Text style={isSelected ? styles.selectedTitle : styles.title}>
          {title}
        </Text>
        <View style={styles.markerIcon}>
          <Marker
            width={32}
            height={32}
            fill={
              color !== 'white'
                ? color || (isSelected ? '#FFFFFF' : '#000000')
                : isSelected
                ? '#FFFFFF'
                : '#000000'
            }
          />
        </View>
      </TouchableOpacity>
    );
  },
);

export default Item;
