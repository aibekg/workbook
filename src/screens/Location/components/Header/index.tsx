import React, {FC} from 'react';
import {Text, View} from 'react-native';

import {styles} from './styles';
import {THeaderProps} from './types';

const Header: FC<THeaderProps> = React.memo(({title, style}) => {
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
});

export default Header;
