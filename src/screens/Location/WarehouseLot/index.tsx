import React, {useCallback, useState} from 'react';
import {
  Keyboard,
  Platform,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Toast from 'react-native-toast-message';
import {useNavigation, useRoute} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUpdateWarehouseLot} from '@hooks/useUpdateWarehouseLot';
import Marker from '@icons/marker.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import Header from '@screens/Location/components/Header';
import {
  bottomSpace,
  statusBarHeight,
  useKeyboardVisibility,
} from '@utils/deviceHelpers';

import {styles} from '../styles';

import {styles as lotStyles} from './styles';

const WarehouseLot = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.LocationLot>>();

  const route = useRoute<TNavigationRouteProp<ERootStackRoutes.LocationLot>>();

  const {updateWarehouseLot} = useUpdateWarehouseLot();

  const [lotNumber, setLotNumber] = useState<string>('');

  const handlePress = useCallback(async () => {
    try {
      if (
        route?.params?.unit?.inBoundId &&
        lotNumber &&
        route?.params?.unit?.id &&
        route?.params?.warehouseArea?.id
      ) {
        const response = await updateWarehouseLot({
          inBoundId: route?.params?.unit?.inBoundId,
          warehouseAreaId: route?.params?.warehouseArea?.id,
          unitId: route?.params?.unit?.id,
          lotNumber,
        });
        if (response) {
          navigation.navigate(ERootStackRoutes.Units);
          setTimeout(() => {
            navigation.navigate(ERootStackRoutes.Unit, {
              unitId: route?.params?.unit?.id,
            });
          }, 600);
        } else {
          Toast.show({
            type: 'error',
            text1: 'Something wrong',
            text2: 'Please try again later',
            visibilityTime: 4000,
            autoHide: true,
            topOffset: statusBarHeight + 16,
          });
        }
      }
    } catch (e) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: 'Please try again later',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
  }, [updateWarehouseLot, lotNumber, navigation, route?.params]);

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="LOT NUMBER" />
      <TouchableOpacity
        activeOpacity={1}
        onPress={Keyboard.dismiss}
        style={lotStyles.viewContainer}>
        <View style={lotStyles.inputContainer}>
          <TextInput
            value={lotNumber}
            onChangeText={setLotNumber}
            autoFocus={true}
            autoComplete="off"
            autoCapitalize="none"
            autoCorrect={false}
            blurOnSubmit={true}
            keyboardType="number-pad"
            style={lotStyles.input}
          />
        </View>
        {route?.params?.warehouseArea?.name ? (
          <>
            <Text style={lotStyles.areaName}>
              {route?.params?.warehouseArea?.name}
            </Text>
            <Marker
              width={32}
              height={32}
              fill={
                route?.params?.warehouseArea?.color !== 'white'
                  ? route?.params?.warehouseArea?.color || '#212121'
                  : '#212121'
              }
            />
          </>
        ) : null}
      </TouchableOpacity>
      {isKeyboardVisible ? null : (
        <View style={lotStyles.buttonLayout}>
          <View style={lotStyles.buttonContainer}>
            <BackButton<ERootStackRoutes.AddUnitYear> />
            <Button
              disabled={
                !route?.params?.unit?.inBoundId ||
                !lotNumber ||
                !route?.params?.unit?.id ||
                !route?.params?.warehouseArea?.id
              }
              hideWhenDisabled={true}
              buttonStyle={styles.button}
              text="Confirm"
              onPress={handlePress}
            />
          </View>
        </View>
      )}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default WarehouseLot;
