import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  viewContainer: {
    width: '100%',
    paddingBottom: 24,
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  input: {
    fontSize: 56,
    fontWeight: '400',
    textAlign: 'center',
    paddingVertical: 0,
    paddingHorizontal: 0,
    width: '100%',
    color: '#212121',
  },
  buttonLayout: {
    width: '100%',
    paddingHorizontal: 16,
  },
  buttonContainer: {
    borderTopWidth: 1,
    borderTopColor: '#21212126',
    width: '100%',
    paddingTop: 16,
    flexDirection: 'row',
    paddingBottom: bottomSpace + 16,
  },
  areaName: {
    fontSize: 24,
    fontWeight: '400',
    color: '#212121',
    marginTop: 16,
    marginBottom: 16,
  },
});
