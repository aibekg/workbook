export type TProfileForm = {
  userFirstName: string;
  userLastName: string;
};

export type TProfileField = {
  name: keyof TProfileForm;
  placeholder: string;
  label: string;
};
