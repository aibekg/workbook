import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1F24',
  },
  headerLeft: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRight: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    overflow: 'hidden',
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  scroll: {
    width: '100%',
    flex: 1,
  },
  scrollContent: {
    width: '100%',
    paddingBottom: 88 + bottomSpace,
  },
  profileContent: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    zIndex: 9,
    paddingBottom: 20,
    borderRadius: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
  },
  inputContainer: {
    width: '48%',
  },
  placeInput: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#d0d7e0',
    color: '#646C7A',
  },
  block: {
    paddingHorizontal: 12,
  },
  buttonContainer: {
    width: '100%',
    position: 'absolute',
    bottom: bottomSpace + 16,
    paddingHorizontal: 12,
  },
  button: {
    marginHorizontal: 40,
  },
});
