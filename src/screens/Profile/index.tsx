import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  Keyboard,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {shallowEqual, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useFormik} from 'formik';

import Button from '@components/Button';
import Header from '@components/Header';
import Input from '@components/Input';
import SelectAvatar from '@components/SelectAvatar';
import {useActions} from '@hooks/useActions';
import {useSaveImages} from '@hooks/useSaveImages';
import {useUpdateUserProfile} from '@hooks/useUpdateUserProfile';
import LogoutSvg from '@icons/logout.svg';
import MenuSVG from '@icons/menu.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import {TProfileField, TProfileForm} from '@screens/Profile/types';
import {selectUser} from '@store/auth/selectors';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {schema} from './schema';
import {styles} from './styles';

const Profile = () => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Profile>>();

  const {setAuth} = useActions();
  const user = useSelector(selectUser, shallowEqual);

  const {
    updateUserProfile,
    isLoading: isUpdateUserLoading,
    error,
    setError,
  } = useUpdateUserProfile();
  const {saveImages} = useSaveImages();

  const [imgPath, setImgPath] = useState<string | null>(user?.photo || null);
  const [imgBase64, setImgBase64] = useState<string | null>(null);

  const {handleChange, handleSubmit, handleBlur, errors, values} =
    useFormik<TProfileForm>({
      initialValues: {
        userFirstName: user?.userFirstName || '',
        userLastName: user?.userLastName || '',
      },
      onSubmit: async input => {
        Keyboard.dismiss();

        if (imgPath === null && user?.photo) {
          return await updateUserProfile({
            input,
            photo: `delete ${user?.photo}`,
          });
        }

        if (imgPath !== user?.photo && imgBase64) {
          const photo = await saveImages({image: imgBase64});
          return await updateUserProfile({input, photo});
        }

        await updateUserProfile({input});
      },
      validationSchema: schema,
    });

  useEffect(() => {
    setImgPath(user?.photo || null);
  }, [user?.photo]);

  const profileFields: TProfileField[] = useMemo(
    () => [
      {
        name: 'userFirstName',
        label: 'First Name',
        placeholder: 'Enter Firstname',
      },
      {name: 'userLastName', label: 'Last Name', placeholder: 'Enter Lastname'},
    ],
    [],
  );

  const disabled = useMemo(
    () =>
      !(
        values.userLastName !== user?.userLastName ||
        values.userFirstName !== user?.userFirstName ||
        imgPath !== user?.photo
      ) || isUpdateUserLoading,
    [
      imgPath,
      isUpdateUserLoading,
      user?.photo,
      user?.userFirstName,
      user?.userLastName,
      values.userFirstName,
      values.userLastName,
    ],
  );

  const handleChangeText = useCallback(
    (name: keyof TProfileForm) => (value: string) => {
      setError('');
      handleChange(name)(value);
    },
    [setError, handleChange],
  );

  const onLogout = useCallback(() => {
    setAuth(null, null);
  }, [setAuth]);

  const onUnit = useCallback(() => {
    navigation.replace(ERootStackRoutes.Units);
  }, [navigation]);

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <Header
        title="My Profile"
        headerLeft={
          <TouchableOpacity
            onPress={onUnit}
            hitSlop={{top: 8, bottom: 8, left: 8, right: 8}}
            style={styles.headerLeft}>
            <MenuSVG fill="#FFFFFF" />
          </TouchableOpacity>
        }
        headerRight={
          <TouchableOpacity
            onPress={onLogout}
            hitSlop={{top: 24, bottom: 24, left: 24, right: 24}}
            style={styles.headerRight}>
            <LogoutSvg width={20} height={20} fill="#FFFFFF" />
          </TouchableOpacity>
        }
      />
      <View style={styles.content}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollContent}
          style={styles.scroll}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={Keyboard.dismiss}
            style={styles.profileContent}>
            <SelectAvatar
              uri={imgPath}
              setUri={setImgPath}
              base64={imgBase64}
              setBase64={setImgBase64}
            />
            <View style={styles.row}>
              {profileFields.map(({name, label, placeholder}) => (
                <Input
                  key={name}
                  onChangeText={handleChangeText(name)}
                  onBlur={handleBlur(name)}
                  value={values[name]}
                  containerStyle={styles.inputContainer}
                  placeholder={placeholder}
                  label={label}
                  error={errors[name] ? 'Required' : undefined}
                />
              ))}
            </View>
            <View style={styles.block}>
              <Input
                value={user?.login}
                editable={false}
                style={styles.placeInput}
                label="Login"
              />
              <Input
                value={user?.warehouse?.name || ''}
                editable={false}
                style={styles.placeInput}
                label="Warehouse"
              />
            </View>
          </TouchableOpacity>
          {/* <HistoryOfActivity /> */}
        </ScrollView>
        {isKeyboardVisible ? null : (
          <View style={styles.buttonContainer}>
            <Button
              error={error}
              disabled={disabled}
              buttonStyle={styles.button}
              text={
                error
                  ? error
                  : isUpdateUserLoading
                  ? 'Updating...'
                  : 'Apply Changes'
              }
              onPress={handleSubmit}
            />
          </View>
        )}
      </View>
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default Profile;
