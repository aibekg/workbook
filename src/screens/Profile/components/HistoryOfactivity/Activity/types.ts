export type TActivityProps = {
  title: string;
  imgUri: string;
  time: string;
  to: string;
  last?: boolean;
};
