import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 16,
  },
  border: {
    borderBottomWidth: 1,
    borderColor: 'rgba(210,210,210,0.62)',
  },
  img: {
    width: 24,
    height: 24,
    borderRadius: 100,
    marginRight: 8,
  },
  title: {
    fontSize: 14,
    fontWeight: '700',
  },
  to: {
    fontWeight: '400',
  },
  time: {
    marginTop: 4,
    fontSize: 10,
    color: '#646C7A',
  },
});
