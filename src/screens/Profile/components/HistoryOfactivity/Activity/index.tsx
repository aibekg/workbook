import React, {FC} from 'react';
import {Image, Text, View} from 'react-native';

import {styles} from './styles';
import {TActivityProps} from './types';

const Activity: FC<TActivityProps> = ({title, imgUri, to, time, last}) => {
  return (
    <View style={[styles.container, !last && styles.border]}>
      <Image
        style={styles.img}
        source={{
          uri: imgUri,
        }}
      />
      <View>
        <Text style={styles.title}>
          {title}
          <Text style={styles.to}>{to}</Text>
        </Text>
        <Text style={styles.time}>{time}</Text>
      </View>
    </View>
  );
};

export default Activity;
