import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingTop: 48,
    paddingBottom: 16,
    backgroundColor: '#F1F1F1',
    marginTop: -24,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  title: {
    fontSize: 12,
    textTransform: 'uppercase',
    fontWeight: '700',
    color: '#000000',
    opacity: 0.5,
  },
  list: {
    marginTop: 8,
  },
});
