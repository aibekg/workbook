import * as Yup from 'yup';

export const schema = Yup.object().shape({
  userFirstName: Yup.string().required().min(2).max(20),
  userLastName: Yup.string().required().min(2).max(20),
});
