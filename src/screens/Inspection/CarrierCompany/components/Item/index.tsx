import React, {FC, useCallback, useMemo} from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';

import {TItemProps} from '@screens/Inspection/CarrierCompany/components/Item/types';

import {styles} from './styles';

const Item: FC<TItemProps> = React.memo(
  ({title, value, onPress, image, selected}) => {
    const handlePress = useCallback(() => {
      onPress && onPress(value);
    }, [onPress, value]);

    const isSelected = useMemo(
      () => !!selected && selected?.id === value?.id,
      [selected, value],
    );

    const isNotSelected = useMemo(
      () => !!selected && selected?.id !== value?.id,
      [selected, value],
    );

    return (
      <TouchableOpacity
        style={[
          isSelected ? styles.selectedContainer : styles.container,
          isNotSelected && styles.opacity,
        ]}
        onPress={handlePress}>
        {image ? (
          <Image style={styles.image} source={image} resizeMode="contain" />
        ) : null}
        <Text style={isSelected ? styles.selectedTitle : styles.title}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  },
);

export default Item;
