import {ImageSourcePropType} from 'react-native';

import {CarrierCompany} from '../../../../../generated/graphql';

export type TItemProps = {
  title: string;
  value: CarrierCompany;
  selected?: CarrierCompany | null;
  image?: ImageSourcePropType;
  onPress?: (item: CarrierCompany) => void;
};
