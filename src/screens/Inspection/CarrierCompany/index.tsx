import React, {useCallback, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItemInfo,
  Platform,
  StatusBar,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import HeaderSearch from '@components/HeaderSearch';
import {useCarrierCompanies} from '@hooks/useCarrierCompanies';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/Inspection/CarrierCompany/components/Item';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {CarrierCompany} from '../../../generated/graphql';

import {styles} from './styles';

const CarrierCompanyScreen = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.CarrierCompany>>();

  const [searchEnabled, setSearchEnabled] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const searchToggle = useCallback(() => {
    if (searchEnabled) {
      setSearch('');
    }
    setSearchEnabled(!searchEnabled);
  }, [searchEnabled]);

  const {
    carrierCompanies,
    isLoading,
    isPaginating,
    getCarrierCompanies,
    selectedCarrierCompany,
    setSelectedCarrierCompany,
  } = useCarrierCompanies(search);

  const handlePress = useCallback(() => {
    if (selectedCarrierCompany?.id && selectedCarrierCompany?.name) {
      navigation.navigate({
        name: ERootStackRoutes.CarrierDriver,
        params: {
          carrierCompanyId: selectedCarrierCompany?.id,
          carrierCompanyName: selectedCarrierCompany?.name,
        },
      });
    }
  }, [selectedCarrierCompany, navigation]);

  const keyExtractor = useCallback(
    (item: CarrierCompany) => item.id.toString(),
    [],
  );

  const getMore = useCallback(
    () => getCarrierCompanies(),
    [getCarrierCompanies],
  );

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<CarrierCompany>) => {
      return (
        <Item
          selected={selectedCarrierCompany}
          title={item.name!}
          value={item}
          onPress={setSelectedCarrierCompany}
        />
      );
    },
    [selectedCarrierCompany, setSelectedCarrierCompany],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#1E1F24"
      />
      <Header title="CARRIER" search={true} onSearch={searchToggle} />
      <FlatList
        keyExtractor={keyExtractor}
        data={carrierCompanies}
        style={styles.paddedList}
        removeClippedSubviews={true}
        contentContainerStyle={styles.paddingContent}
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        onEndReached={getMore}
        onEndReachedThreshold={0.1}
        ListEmptyComponent={
          isLoading && !isPaginating ? <ActivityIndicator size="small" /> : null
        }
        ListFooterComponent={
          isPaginating ? <ActivityIndicator size="small" /> : null
        }
      />
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitMake> />
          <Button
            hideWhenDisabled={true}
            disabled={!selectedCarrierCompany}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      )}
      {searchEnabled ? (
        <HeaderSearch
          search={search}
          setSearch={setSearch}
          onClose={searchToggle}
        />
      ) : null}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default CarrierCompanyScreen;
