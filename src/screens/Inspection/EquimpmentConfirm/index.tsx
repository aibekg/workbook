import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  Image,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {useActionSheet} from '@expo/react-native-action-sheet';
import {RouteProp, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import {useActions} from '@hooks/useActions';
import BangSvg from '@icons/bang.svg';
import CirclePlusSvg from '@icons/circle-plus.svg';
import RulerSvg from '@icons/ruler.svg';
import XCircleSvg from '@icons/x-circle.svg';
import {ERootStackRoutes, TRootStackParamList} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import {TPhoto} from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet/types';
import SelectDamageSizeBottomSheet from '@screens/Inspection/EquimpmentConfirm/components/SelectDamageSizeBottomSheet';
import SelectDamageTypeBottomSheet from '@screens/Inspection/EquimpmentConfirm/components/SelectDamageTypeBottomSheet';
import {selectEquipments} from '@store/unit/selectors';
import {TDamageType, TEquipmentTitle} from '@store/unit/types';
import {pickDocument} from '@utils/imagePickerHelpers';

import {styles} from './styles';

export type TEquipment = {
  title: TEquipmentTitle;
  damageType: TDamageType | null;
  damageSize: string | null;
};

const EquipmentConfirm = () => {
  const [damageTypeBSheetIndex, setDamageTypeBSheetIndex] = useState(0);
  const [damageSizeBSheetIndex, setDamageSizeBSheetIndex] = useState(-1);
  const [equipmentIndex, setEquipmentIndex] = useState(0);
  const [photo, setPhoto] = useState<null | TPhoto>(null);
  const {params} =
    useRoute<
      RouteProp<TRootStackParamList, ERootStackRoutes.EquipmentConfirm>
    >();
  const [equipment, setEquipment] = useState<TEquipment>({
    title: params?.selectedEquipments[equipmentIndex],
    damageType: null,
    damageSize: null,
  });
  const equipments = useSelector(selectEquipments);
  const {showActionSheetWithOptions} = useActionSheet();
  const {addEquipment} = useActions();

  const label = useMemo(
    () =>
      !equipment.damageType
        ? 'Please describe damage'
        : !equipment.damageSize
        ? 'What size is it?'
        : null,
    [equipment],
  );

  const isSaved = useCallback(
    (title: TEquipmentTitle) => equipments.find(item => item.title === title),
    [equipments],
  );

  useEffect(() => {
    if (
      isSaved(equipment.title) &&
      params.selectedEquipments.length - 1 !== equipmentIndex
    ) {
      setEquipment(prev => ({
        ...prev,
        title: params.selectedEquipments[equipmentIndex + 1],
      }));
      setEquipmentIndex(prev => prev + 1);
    }
  }, [equipment.title, equipmentIndex, isSaved, params.selectedEquipments]);

  const handDamageTypePress = useCallback(() => {
    if (damageSizeBSheetIndex !== 0 && damageTypeBSheetIndex !== 0) {
      setDamageTypeBSheetIndex(0);
    }
  }, [damageSizeBSheetIndex, damageTypeBSheetIndex]);

  const handDamageSizePress = useCallback(() => {
    if (damageSizeBSheetIndex !== 0 && damageTypeBSheetIndex !== 0) {
      setDamageSizeBSheetIndex(0);
    }
  }, [damageSizeBSheetIndex, damageTypeBSheetIndex]);

  const handlePressAddPhoto = useCallback(async () => {
    try {
      const pickerResponse = await pickDocument(showActionSheetWithOptions);
      if (pickerResponse.data) {
        setPhoto(pickerResponse);
      }
    } catch (e) {}
  }, [showActionSheetWithOptions]);

  const handleCancel = useCallback(() => {
    setPhoto(null);
  }, [setPhoto]);

  const handlePressAddNote = useCallback(() => {}, []);

  const handlePressContinue = useCallback(() => {
    if (equipment.damageType !== null && equipment.title) {
      addEquipment({
        damageType: equipment.damageType,
        damageSize: equipment.damageSize,
        title: equipment.title,
        photo,
      });
      if (params.selectedEquipments.length - 1 !== equipmentIndex) {
        setPhoto(null);
        setEquipment({
          title: params.selectedEquipments[equipmentIndex + 1],
          damageType: null,
          damageSize: null,
        });
        setDamageTypeBSheetIndex(0);
        setEquipmentIndex(prev => prev + 1);
      } else {
        // TODO: We need to update data here
      }
    }
  }, [
    addEquipment,
    equipment.damageSize,
    equipment.damageType,
    equipment.title,
    equipmentIndex,
    params.selectedEquipments,
    photo,
  ]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="EQUIPMENT" search={false} />
      <View style={styles.header}>
        <Text style={styles.title}>{equipment.title}</Text>
        <View style={styles.circle}>
          <BangSvg />
        </View>
      </View>
      <View style={styles.content}>
        {equipment.damageType ? (
          <TouchableOpacity onPress={handDamageTypePress} style={styles.card}>
            <View style={styles.circle}>
              <Text style={styles.circleText}>
                {equipment.damageType[0].toUpperCase()}
              </Text>
            </View>
            <Text style={styles.cardText}>{equipment.damageType}</Text>
          </TouchableOpacity>
        ) : null}
        {equipment.damageSize ? (
          <TouchableOpacity onPress={handDamageSizePress} style={styles.card}>
            <RulerSvg />
            <Text style={styles.cardText}>{equipment.damageSize}</Text>
          </TouchableOpacity>
        ) : null}
        {!photo ? (
          <TouchableOpacity
            onPress={handlePressAddPhoto}
            style={styles.addPhoto}>
            <CirclePlusSvg fill="#000000" />
            <Text style={styles.addPhotoText}>Add Photo</Text>
          </TouchableOpacity>
        ) : (
          <View style={[styles.photoContainer]}>
            <Image style={styles.photo} source={{uri: photo.path}} />
            <XCircleSvg
              style={styles.cancelIcon}
              onPress={handleCancel}
              fill="#000000"
            />
          </View>
        )}
        <TouchableOpacity onPress={handlePressAddNote} style={styles.addPhoto}>
          <CirclePlusSvg fill="#000000" />
          <Text style={styles.addPhotoText}>Add Note</Text>
        </TouchableOpacity>
        <Text style={styles.text}>{label}</Text>
      </View>
      <View style={styles.footer}>
        <Button text="Confirm and Continue" onPress={handlePressContinue} />
      </View>
      <SelectDamageTypeBottomSheet
        setDamageSizeBSheetIndex={setDamageSizeBSheetIndex}
        equipment={equipment}
        setEquipment={setEquipment}
        index={damageTypeBSheetIndex}
        setIndex={setDamageTypeBSheetIndex}
      />
      <SelectDamageSizeBottomSheet
        index={damageSizeBSheetIndex}
        setIndex={setDamageSizeBSheetIndex}
        equipment={equipment}
        setEquipment={setEquipment}
      />
    </View>
  );
};

export default EquipmentConfirm;
