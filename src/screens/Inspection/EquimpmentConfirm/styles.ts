import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 32,
    fontWeight: '700',
    color: '#212121',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 24,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#d0d0d0',
  },
  circle: {
    width: 32,
    height: 32,
    borderRadius: 32,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FA354D',
  },
  text: {
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'center',
    marginVertical: 28,
    color: '#010101',
    opacity: 0.5,
  },
  content: {
    padding: 16,
  },
  cardText: {
    fontSize: 28,
    color: '#3D3D3D',
    marginLeft: 12,
    textTransform: 'capitalize',
  },
  card: {
    padding: 16,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    marginVertical: 12,
  },
  circleText: {
    fontSize: 18,
    fontWeight: '700',
    color: '#FFFFFF',
  },
  addPhoto: {
    height: 120,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 20,
  },
  addPhotoText: {
    color: '#000000',
    fontWeight: '700',
    marginLeft: 8,
  },
  photoContainer: {
    position: 'relative',
    marginVertical: 20,
  },
  photo: {
    height: 120,
    width: '100%',
    borderRadius: 20,
  },
  cancelIcon: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: deviceWidth,
    padding: 32,
  },
});
