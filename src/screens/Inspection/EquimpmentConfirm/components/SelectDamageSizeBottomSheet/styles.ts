import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  background: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#383838',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 16.0,
    elevation: 24,
  },
  container: {
    padding: 20,
    position: 'relative',
    flex: 1,
  },
  bShTitle: {
    fontSize: 12,
    color: '#2121218F',
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  thumb: {
    width: 20,
    height: 20,
    borderRadius: 20,
    backgroundColor: 'red',
  },
  rail: {
    width: '100%',
    height: 1,
    backgroundColor: 'grey',
  },
  railSelected: {
    height: 1,
    backgroundColor: 'red',
  },
  slider: {
    marginVertical: 24,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: deviceWidth,
    padding: 32,
  },
});
