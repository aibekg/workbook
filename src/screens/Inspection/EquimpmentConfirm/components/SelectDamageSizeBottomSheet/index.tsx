import React, {
  Dispatch,
  SetStateAction,
  useCallback,
  useMemo,
  useRef,
  useState,
} from 'react';
import {Text, View} from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';
import RangeSlider from 'rn-range-slider';

import Button from '@components/Button';
import {TEquipment} from '@screens/Inspection/EquimpmentConfirm';

import {styles} from './styles';

export type Props = {
  index: number;
  setIndex: Dispatch<SetStateAction<number>>;
  equipment: TEquipment;
  setEquipment: Dispatch<SetStateAction<TEquipment>>;
};

const SelectDamageSizeBottomSheet = ({
  index,
  setIndex,
  equipment,
  setEquipment,
}: Props) => {
  const bottomSheetRef = useRef<BottomSheet>(null);
  const [damageSize, setDamageSize] = useState({low: 0, high: 3});
  const snapPoints = useMemo(() => ['70%'], []);

  const handleBottomSheetIndexChange = useCallback(
    (idx: number) => {
      setIndex(idx);
    },
    [setIndex],
  );

  const handlePress = useCallback(() => {
    setEquipment(prev => ({
      ...prev,
      damageSize: `${damageSize.low}-${damageSize.high}`,
    }));
    bottomSheetRef.current?.close();
  }, [damageSize.high, damageSize.low, setEquipment]);

  const renderThumb = useCallback(() => <View style={styles.thumb} />, []);
  const renderRail = useCallback(() => <View style={styles.rail} />, []);

  const renderRailSelected = useCallback(
    () => <View style={styles.railSelected} />,
    [],
  );

  const handleValueChange = useCallback((low: number, high: number) => {
    setDamageSize({low, high});
  }, []);

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={index}
      backgroundStyle={styles.background}
      enablePanDownToClose={!!equipment.damageSize}
      onChange={handleBottomSheetIndexChange}
      keyboardBehavior="extend"
      snapPoints={snapPoints}>
      <View style={styles.container}>
        <Text style={styles.bShTitle}>Size of Damage</Text>
        <RangeSlider
          style={styles.slider}
          min={0}
          max={10}
          step={1}
          floatingLabel
          high={damageSize.high}
          low={0}
          renderThumb={renderThumb}
          renderRail={renderRail}
          renderRailSelected={renderRailSelected}
          onValueChanged={handleValueChange}
        />
        <View style={styles.footer}>
          <Button text="Confirm and Continue" onPress={handlePress} />
        </View>
      </View>
    </BottomSheet>
  );
};

export default SelectDamageSizeBottomSheet;
