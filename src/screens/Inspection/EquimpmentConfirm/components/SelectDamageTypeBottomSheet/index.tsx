import React, {
  Dispatch,
  SetStateAction,
  useCallback,
  useMemo,
  useRef,
} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';

import {styles} from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet/styles';
import {TEquipment} from '@screens/Inspection/EquimpmentConfirm';
import {TDamageType} from '@store/unit/types';

export type Props = {
  index: number;
  setIndex: Dispatch<SetStateAction<number>>;
  equipment: TEquipment;
  setEquipment: Dispatch<SetStateAction<TEquipment>>;
  setDamageSizeBSheetIndex: Dispatch<SetStateAction<number>>;
};

const damageTypes: TDamageType[] = [
  'scratch',
  'ding',
  'dent',
  'crack',
  'rub',
  'missing',
];

const SelectDamageTypeBottomSheet = ({
  index,
  setIndex,
  equipment,
  setEquipment,
  setDamageSizeBSheetIndex,
}: Props) => {
  const bottomSheetRef = useRef<BottomSheet>(null);
  const snapPoints = useMemo(() => ['70%'], []);

  const handleBottomSheetIndexChange = useCallback(
    (idx: number) => {
      setIndex(idx);
    },
    [setIndex],
  );

  const handlePress = useCallback(
    (value: TDamageType) => () => {
      setEquipment(prev => ({...prev, damageType: value}));
      bottomSheetRef.current?.close();
      if (!equipment.damageSize && value !== 'missing') {
        setDamageSizeBSheetIndex(0);
      }
      if (value === 'missing') {
        setEquipment(prev => ({...prev, damageSize: null}));
      }
    },
    [equipment.damageSize, setDamageSizeBSheetIndex, setEquipment],
  );

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={index}
      backgroundStyle={styles.background}
      enablePanDownToClose={!!equipment.damageType}
      onChange={handleBottomSheetIndexChange}
      keyboardBehavior="extend"
      snapPoints={snapPoints}>
      <View style={styles.container}>
        {damageTypes.map(item => (
          <TouchableOpacity
            key={item}
            onPress={handlePress(item)}
            style={styles.item}>
            <Text style={styles.itemText}>{item}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </BottomSheet>
  );
};

export default SelectDamageTypeBottomSheet;
