import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  paddedList: {
    width: '100%',
    paddingHorizontal: 16,
    flex: 1,
  },
  paddingContent: {
    width: '100%',
    paddingTop: 0,
    paddingBottom: 96 + bottomSpace,
  },
  buttonContainer: {
    left: 16,
    right: 16,
    flexDirection: 'row',
    position: 'absolute',
    bottom: bottomSpace + 16,
  },
  button: {
    flex: 1,
    marginHorizontal: 0,
  },
  line: {
    height: 1,
    backgroundColor: '#c5c5c5',
    opacity: 0.5,
    marginVertical: 24,
  },
});
