import React, {useCallback, useState} from 'react';
import {Platform, ScrollView, StatusBar, View} from 'react-native';
import {useActionSheet} from '@expo/react-native-action-sheet';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import SelectKeyCount from '@screens/Inspection/KeysAddition/components/SelectKeyCount';
import {pickDocument} from '@utils/imagePickerHelpers';

import {styles} from './styles';

const KeysAddition = () => {
  const [manualKeysCount, setManualKeysCount] = useState(1);
  const [remoteKeysCount, setRemoteKeysCount] = useState(0);
  const {showActionSheetWithOptions} = useActionSheet();
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.KeysAddition>>();

  const handlePressMakePhoto = useCallback(async () => {
    try {
      const pickerResponse = await pickDocument(showActionSheetWithOptions, {
        cropperCircleOverlay: false,
        cropping: false,
        height: 400,
        width: 400,
      });
      if (pickerResponse.data) {
        navigation.replace(ERootStackRoutes.KeysConfirm, {
          photo: {
            path: pickerResponse.path,
            data: pickerResponse.data,
          },
          manualKeysCount,
          remoteKeysCount,
        });
      }
    } catch (e) {}
  }, [
    manualKeysCount,
    navigation,
    remoteKeysCount,
    showActionSheetWithOptions,
  ]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="NUMBER OF KEYS" search={false} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.paddedList}
        contentContainerStyle={styles.paddingContent}>
        <SelectKeyCount
          data={[0, 1, 2, 3]}
          title="Manual Keys"
          count={manualKeysCount}
          setCount={setManualKeysCount}
        />
        <View style={styles.line} />
        <SelectKeyCount
          data={[0, 1, 2]}
          title="Remote Keys"
          count={remoteKeysCount}
          setCount={setRemoteKeysCount}
        />
      </ScrollView>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.KeysAddition> />
        <Button
          hideWhenDisabled={true}
          disabled={!manualKeysCount && !remoteKeysCount}
          buttonStyle={styles.button}
          text="Make Photo"
          onPress={handlePressMakePhoto}
        />
      </View>
    </View>
  );
};

export default KeysAddition;
