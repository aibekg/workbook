import {Dispatch, SetStateAction} from 'react';

export type TSelectKeyCountProps = {
  title: 'Manual Keys' | 'Remote Keys';
  count: number;
  setCount: Dispatch<SetStateAction<number>>;
  data: number[];
};
