import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 12,
    marginTop: 4,
  },
  item: {
    width: 88,
    height: 88,
    marginVertical: 8,
    borderRadius: 88,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeItem: {
    backgroundColor: '#CDFF48',
  },
  itemText: {
    fontSize: 56,
    fontWeight: '400',
    color: '#BBC3CF',
  },
  zeroItem: {
    backgroundColor: '#e8e8e8',
  },
  activeItemText: {
    color: '#212121',
  },
  title: {
    fontSize: 24,
    fontWeight: '400',
    color: '#212121',
    marginVertical: 24,
  },
});
