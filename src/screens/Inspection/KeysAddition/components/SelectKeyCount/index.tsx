import React, {FC, memo, useCallback, useRef} from 'react';
import {Text, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import ManualKey from '@icons/key.svg';
import RemoteKey from '@icons/remote-key.svg';
import {deviceWidth} from '@utils/deviceHelpers';

import {styles} from './styles';
import {TSelectKeyCountProps} from './types';

const SelectKeyCount: FC<TSelectKeyCountProps> = memo(
  ({title, count, setCount, data}) => {
    const carouselRef = useRef(null);

    const renderItem = useCallback(
      (item: number) => (
        <View
          style={[
            styles.item,
            item === count && styles.activeItem,
            item === 0 && item === count && styles.zeroItem,
          ]}>
          <Text
            style={[
              styles.itemText,
              item === count && styles.activeItemText,
              item === count && item === 0 && styles.itemText,
            ]}>
            {item}
          </Text>
        </View>
      ),
      [count],
    );

    return (
      <View style={styles.container}>
        <Carousel<number>
          firstItem={count}
          ref={carouselRef}
          onSnapToItem={slideIndex => setCount(data[slideIndex])}
          layoutCardOffset={3}
          data={data}
          renderItem={({item}) => renderItem(item)}
          itemWidth={88}
          sliderWidth={deviceWidth}
        />
        <Text style={styles.title}>{title}</Text>
        {title === 'Manual Keys' ? (
          <ManualKey width={24} height={24} fill="#646C7A" />
        ) : (
          <RemoteKey width={24} height={24} fill="#646C7A" />
        )}
      </View>
    );
  },
);

export default SelectKeyCount;
