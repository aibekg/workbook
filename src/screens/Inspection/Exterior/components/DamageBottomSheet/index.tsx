import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Image,
  LayoutChangeEvent,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import KeyboardSpacer from 'react-native-keyboard-spacer';
// @ts-ignore
import {Picker} from 'react-native-wheel-pick';
import {USER_PHOTO_URL} from '@env';
import {useActionSheet} from '@expo/react-native-action-sheet';
import BottomSheet from '@gorhom/bottom-sheet';

import {useSaveImages} from '@hooks/useSaveImages';
import CameraSvg from '@icons/camera.svg';
import AddSvg from '@icons/plus-circle.svg';
import {
  damageTypes,
  severities,
  severitiesPercentage,
} from '@screens/Inspection/Exterior/constants';
import {bottomSpace} from '@utils/deviceHelpers';
import {pickDocument} from '@utils/imagePickerHelpers';

import {Damage} from '../../../../../generated/graphql';

import {styles} from './styles';

export type TDamageBottomSheetProps = {
  index: number;
  setIndex: Dispatch<SetStateAction<number>>;
  onRemoveDamage: () => void;
  selectedDamage: Damage | null;
  onChangeDamageType: (type: string) => void;
  onChangeDamageDetail: (severity: string) => void;
  onChangePhoto: (photo: string) => void;
  onChangeNote: (note: string) => void;
};

const DamageBottomSheet: FC<TDamageBottomSheetProps> = ({
  index,
  setIndex,
  selectedDamage,
  onRemoveDamage,
  onChangeDamageDetail,
  onChangeDamageType,
  onChangeNote,
  onChangePhoto,
}) => {
  const bottomSheetRef = useRef<BottomSheet>(null);

  const [contentHeight, setContentHeight] = useState<number>(0);
  const [selectedPickerType, setSelectedPickerType] = useState<number>(-1);
  const [selectedPhotoBase64, setSelectedPhotoBase64] = useState<string>('');

  const snapPoints = useMemo(
    () => [contentHeight || '35%', '90%'],
    [contentHeight],
  );

  useEffect(() => {
    if (index === -1) {
      setSelectedPickerType(-1);
    }
  }, [index]);

  const {showActionSheetWithOptions} = useActionSheet();

  const handleBottomSheetIndexChange = useCallback(
    (idx: number) => {
      setIndex(idx);
    },
    [setIndex],
  );

  const {saveImages, isLoading: isPhotoUploading} = useSaveImages();

  const onSelectPhoto = useCallback(async () => {
    try {
      const pickerResponse = await pickDocument(showActionSheetWithOptions, {
        cropping: false,
      });
      if (pickerResponse.data) {
        setSelectedPhotoBase64(pickerResponse.data);
        const photoSaved = await saveImages({image: pickerResponse.data});
        if (photoSaved) {
          onChangePhoto(photoSaved);
          setSelectedPhotoBase64('');
        }
      }
    } catch (e) {}
  }, [saveImages, onChangePhoto, showActionSheetWithOptions]);

  console.log('selectedDamage', selectedDamage);

  const onLayout = (e: LayoutChangeEvent) => {
    if (e?.nativeEvent?.layout?.height) {
      setContentHeight(e.nativeEvent.layout.height + getBottomSpace() + 56);
    }
  };

  if (!selectedDamage?.damageType) {
    return null;
  }
  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={index}
      backgroundStyle={styles.background}
      enablePanDownToClose={true}
      onChange={handleBottomSheetIndexChange}
      keyboardBehavior={'extend'}
      handleIndicatorStyle={styles.handle}
      snapPoints={snapPoints}>
      <View onLayout={onLayout} style={styles.container}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.textButton}
            onPress={onRemoveDamage}>
            <Text style={styles.textButtonText}>Remove</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.pickerContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.picketButton}
            onPress={() => setSelectedPickerType(0)}>
            <View style={styles.damageItemTypeContainer}>
              <Text style={styles.damageItemType}>
                {selectedDamage?.damageType[0].toUpperCase()}
              </Text>
            </View>
            <Text
              style={
                styles.damageItemText
              }>{`${selectedDamage?.damageType}`}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.picketButtonNoPadding}
            onPress={() => setSelectedPickerType(1)}>
            <View
              style={[
                styles.damageItemPercent,
                !!selectedDamage?.detail &&
                  !!severitiesPercentage[selectedDamage?.detail] && {
                    width: severitiesPercentage[selectedDamage?.detail],
                  },
              ]}
            />
            <Text style={styles.damageItemSeverityText}>{`${
              selectedDamage?.detail || ''
            }`}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.picketButton}
            disabled={isPhotoUploading}
            onPress={onSelectPhoto}>
            {selectedDamage?.photo || selectedPhotoBase64 ? (
              <View style={styles.damageImageContainer}>
                <Image
                  style={styles.damageImage}
                  source={{
                    uri:
                      (selectedDamage?.photo
                        ? USER_PHOTO_URL + selectedDamage?.photo
                        : '') || selectedPhotoBase64,
                  }}
                  resizeMode="cover"
                />
              </View>
            ) : null}
            <View style={styles.cameraButton}>
              <CameraSvg width={24} height={24} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.picketButton}
            onPress={() => setSelectedPickerType(3)}>
            <AddSvg
              width={24}
              height={24}
              stroke="#999999"
              style={styles.noteIcon}
            />
            <Text style={styles.noteText}>{'Add Note'}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.pickerContent}>
          {selectedPickerType === -1 ? null : selectedPickerType === 0 ? (
            <Picker
              style={styles.picker}
              itemStyle={styles.pickerItem}
              selectedValue={selectedDamage?.damageType}
              pickerData={damageTypes}
              onValueChange={onChangeDamageType}
            />
          ) : selectedPickerType === 1 ? (
            <Picker
              style={styles.picker}
              itemStyle={styles.pickerItem}
              selectedValue={selectedDamage?.detail}
              pickerData={severities}
              onValueChange={onChangeDamageDetail}
            />
          ) : selectedPickerType === 2 ? null : selectedPickerType === 3 ? (
            <View style={styles.inputContainer}>
              <TextInput
                autoFocus={true}
                autoCorrect={false}
                autoComplete="off"
                autoCapitalize="none"
                style={styles.input}
                multiline={true}
                blurOnSubmit={true}
                onChangeText={onChangeNote}
                value={selectedDamage?.note || ''}
              />
              {Platform.OS === 'ios' && (
                <KeyboardSpacer topSpacing={-bottomSpace} />
              )}
            </View>
          ) : null}
        </View>
      </View>
    </BottomSheet>
  );
};

export default DamageBottomSheet;
