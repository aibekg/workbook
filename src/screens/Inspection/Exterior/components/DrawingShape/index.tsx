import React from 'react';
import {Path} from 'react-native-svg';

import {catmullRomFitting} from '@utils/vectorHelpers';

type DrawingShapeProps = {
  strokeWidth: number;
  stroke?: string;
  points: [number, number][];
};

const DrawingShape = ({points, stroke, strokeWidth}: DrawingShapeProps) => {
  const updatedDrawingPoints = catmullRomFitting(
    points.map(p => ({x: p[0], y: p[1]})),
    0.5,
  );
  if (updatedDrawingPoints) {
    return (
      <Path
        d={updatedDrawingPoints}
        strokeLinecap="round"
        strokeLinejoin="round"
        fill="none"
        stroke={stroke}
        strokeWidth={strokeWidth}
      />
    );
  }
  return null;
};

export default DrawingShape;
