import React from 'react';
import {G, Path} from 'react-native-svg';

import {fromPathToArray, svgToObject} from '@utils/vectorHelpers';

import {ShapeIdType, SVGPathPart} from '../../../../../types/svgTypes';

type SVGSchemeProps = {
  pathStrokeWidth: number;
  activeColor: string;
  inactiveColor: string;
  selectedParts: ShapeIdType[];
  svgString?: string | null;
  onPressPart?: (part: ShapeIdType) => void;
};

const SVGScheme = (props: SVGSchemeProps) => {
  const parsePathAttributes = (paths: SVGPathPart[]): string => {
    const pathStrings = paths.map(p => {
      switch (p.kind) {
        case 'Z':
          return 'Z';
        case 'T':
        case 'L':
        case 'M':
          return `${p.kind}${p.points[0].x} ${p.points[0].y}`;
        case 'C':
        case 'Q':
        case 'S':
          const pointsString = p.points
            .map(point => point.x + ',' + point.y)
            .join(' ');
          return `${p.kind}${pointsString}`;
        case 'A':
          return `${p.kind} ${p.points[0].x} ${p.points[0].y} ${p.points[1].x} ${p.points[1].y} ${p.points[2].x} ${p.points[2].y} ${p.points[3].x}`;
        default:
          return '';
      }
    });

    return pathStrings.join(' ');
  };

  const {
    svgString,
    selectedParts,
    pathStrokeWidth,
    activeColor,
    inactiveColor,
    onPressPart,
  } = props;

  if (!svgString) {
    return null;
  }

  const svgObject = svgToObject(svgString);
  const svgPaths = (svgObject?.paths || []).map(
    (item: {d: string; id: string}) => fromPathToArray(item),
  );
  return (
    <G>
      {svgPaths.map(
        (path: {id: string; paths: SVGPathPart[]}, pathIndex: number) => {
          return (
            <Path
              key={`${path.id}-${pathIndex}`}
              onPressIn={onPressPart ? () => onPressPart(path.id) : undefined}
              d={parsePathAttributes(path.paths)}
              strokeLinecap="round"
              strokeLinejoin="round"
              fill="none"
              stroke={
                (selectedParts || []).indexOf(path.id) !== -1
                  ? activeColor
                  : inactiveColor
              }
              strokeWidth={pathStrokeWidth}
            />
          );
        },
      )}
    </G>
  );
};

export default SVGScheme;
