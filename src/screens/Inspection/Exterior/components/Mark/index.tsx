import React from 'react';
import {Circle, G, Text} from 'react-native-svg';

type MarkProps = {
  x: number | string;
  y: number | string;
  r: number;
  zoom?: number;
  rotation?: number;
  fillColor?: string;
  strokeColor?: string;
  textColor?: string;
  strokeWidth?: number;
  type?: string | null;
  showAllText?: boolean;
};

const Mark = ({
  x,
  y,
  r,
  zoom,
  fillColor,
  textColor,
  type,
  strokeWidth,
  strokeColor,
  rotation,
  showAllText,
}: MarkProps) => {
  const z = zoom || 1;
  return (
    <G x={x} y={y} rotate={rotation}>
      <Circle
        cx={0}
        cy={0}
        r={r / z}
        fill={fillColor ? fillColor : 'none'}
        stroke={strokeColor ? strokeColor : 'none'}
        strokeWidth={strokeWidth}
      />
      {!!type && (
        <G y={0.35 * (r / z)}>
          <Text
            fill={textColor || '#303030'}
            fontWeight="bold"
            fontSize={(r * 1.1) / z}
            x={0}
            y={0}
            textAnchor="middle">
            {showAllText ? type : type[0].toUpperCase()}
          </Text>
        </G>
      )}
    </G>
  );
};

export default Mark;
