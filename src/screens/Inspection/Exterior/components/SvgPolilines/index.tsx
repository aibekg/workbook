import React from 'react';
import {G} from 'react-native-svg';

import DrawingShape from '@screens/Inspection/Exterior/components/DrawingShape';
import {getShapeDamageCoordinatesFromSvg} from '@utils/vectorHelpers';

import {Damage, UnitExterior} from '../../../../../generated/graphql';

type SVGPolylineType = {
  strokeColor?: string;
  activeStrokeColor?: string;
  damageStrokeWidth: number;
  activeDamage?: Damage | null;
  exteriorItem?: UnitExterior | null;
};

const SVGPolylines = (props: SVGPolylineType) => {
  const renderActiveDamage = (damage: Damage) => {
    // const {damageStrokeWidth, activeStrokeColor} = props;
    switch (damage.shape) {
      case 'Line': {
        const coordinates = getShapeDamageCoordinatesFromSvg(damage?.svg);
        return (
          <DrawingShape
            key={damage?.id}
            points={coordinates.map(item => [item.x, item.y])}
            stroke="#FF453A"
            strokeWidth={2}
          />
        );
      }
      default:
        return null;
    }
  };

  const {exteriorItem, activeDamage} = props;
  const damages = (exteriorItem?.damages || []).filter(d =>
    activeDamage ? d?.id !== activeDamage.id : true,
  );
  return (
    <G>
      {damages.length > 0 &&
        damages.map(damage => {
          switch (damage?.shape) {
            case 'Line': {
              const coordinates = getShapeDamageCoordinatesFromSvg(damage?.svg);
              return (
                <DrawingShape
                  key={damage?.id}
                  points={coordinates.map(item => [item.x, item.y])}
                  stroke="#FF453A"
                  strokeWidth={2}
                />
              );
            }
            default:
              return null;
          }
        })}
      {!!activeDamage && renderActiveDamage(activeDamage)}
    </G>
  );
};

export default SVGPolylines;
