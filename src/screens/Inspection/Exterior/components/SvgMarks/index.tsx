import React from 'react';

import Mark from '@screens/Inspection/Exterior/components/Mark';
import {
  getMarkCoords,
  getPointDamageCoordinatesFromSvg,
  getShapeDamageCoordinatesFromSvg,
} from '@utils/vectorHelpers';

import {Damage, UnitExterior} from '../../../../../generated/graphql';

type SVGMarksType = {
  radius: number;
  activeDamage?: Damage | null;
  exteriorItem?: UnitExterior | null;
  zoom: number;
};

const SVGMarks = (props: SVGMarksType) => {
  const renderActiveDamage = (damage: Damage) => {
    const {zoom, radius} = props;
    let x = 0;
    let y = 0;
    switch (damage.shape) {
      case 'Line':
        {
          const coordinates = getShapeDamageCoordinatesFromSvg(damage?.svg);
          const coords = getMarkCoords(
            coordinates.map(item => [item.x, item.y]),
          );
          x = coords.x;
          y = coords.y;
        }
        break;
      case 'Point':
        {
          const coordinates = getPointDamageCoordinatesFromSvg(damage?.svg);
          x = coordinates.x;
          y = coordinates.y - radius;
        }
        break;
    }
    return (
      <Mark
        x={x}
        y={y}
        type={damage.damageType}
        zoom={zoom}
        r={radius}
        fillColor="#FF453A"
        textColor="#FFFFFF"
      />
    );
  };

  const {exteriorItem, activeDamage, zoom, radius} = props;
  const damages = (exteriorItem?.damages || []).filter(d =>
    activeDamage ? d?.id !== activeDamage.id : true,
  );

  return (
    <>
      {(damages || []).map(damage => {
        let x = 0;
        let y = 0;
        switch (damage?.shape) {
          case 'Line':
            {
              const coordinates = getShapeDamageCoordinatesFromSvg(damage?.svg);
              const coords = getMarkCoords(
                coordinates.map(item => [item.x, item.y]),
              );
              x = coords.x;
              y = coords.y;
            }
            break;
          case 'Point':
            {
              const coordinates = getPointDamageCoordinatesFromSvg(damage?.svg);
              x = coordinates.x;
              y = coordinates.y - radius;
            }
            break;
        }
        return (
          <Mark
            key={damage?.id}
            type={damage?.damageType}
            x={x}
            y={y}
            zoom={zoom}
            r={radius}
            fillColor="#FF453A"
            textColor="#FFFFFF"
          />
        );
      })}
      {!!activeDamage && renderActiveDamage(activeDamage)}
    </>
  );
};

export default SVGMarks;
