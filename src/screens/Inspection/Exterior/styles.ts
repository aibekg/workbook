import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  camera: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000000',
    zIndex: 0,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  content: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  damageContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sideContainer: {
    width: 72,
    height: 72,
    marginHorizontal: 8,
    borderColor: '#FFFFFF',
    borderWidth: 3,
    borderRadius: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sideView: {
    width: 56,
    height: 56,
    borderWidth: 2,
    backgroundColor: 'rgba(0,0,0,0.6)',
    borderColor: 'rgba(255,255,255,0.4)',
    borderRadius: 28,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sideImage: {
    width: 48,
    height: 48,
  },
  damageScroll: {
    width: '100%',
    height: 48,
    maxHeight: 48,
    marginBottom: bottomSpace + 16,
    marginTop: 16,
  },
  damageScrollContent: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 48,
    paddingHorizontal: 8,
  },
  damageItemContainer: {
    height: 48,
    marginHorizontal: 8,
    paddingHorizontal: 12,
    minWidth: 156,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#333333',
    borderRadius: 8,
    overflow: 'hidden',
  },
  damageItemTypeContainer: {
    width: 28,
    height: 28,
    backgroundColor: '#FF453A',
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  damageItemType: {
    fontWeight: '700',
    fontSize: 14,
    color: '#FFFFFF',
  },
  damageItemText: {
    marginLeft: 8,
    fontWeight: '700',
    fontSize: 16,
    color: '#FFFFFF',
  },
});
