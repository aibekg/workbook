import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {
  FlatList,
  Image,
  ListRenderItemInfo,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Orientation from 'react-native-orientation';
import {Camera, useCameraDevices} from 'react-native-vision-camera';
import {useRoute} from '@react-navigation/native';

import {DamageDrawView} from '@components/DamageDrawView';
import {ModalHandle} from '@components/ModalHandle';
import {ERootStackRoutes, TNavigationRouteProp} from '@routes/types';
import DamageBottomSheet from '@screens/Inspection/Exterior/components/DamageBottomSheet';
import {
  backSvg,
  frontSvg,
  leftSvg,
  rightSvg,
  topSvg,
} from '@screens/Inspection/Exterior/constants';
import {EExteriorSide} from '@screens/Inspection/ExteriorSide/types';
import {deviceWidth} from '@utils/deviceHelpers';
import {svgToObject} from '@utils/vectorHelpers';

import {Damage} from '../../../generated/graphql';

import {styles} from './styles';

const getSvgString = (side: EExteriorSide) => {
  switch (side) {
    case EExteriorSide.LEFT:
      return leftSvg;
    case EExteriorSide.RIGHT:
      return rightSvg;
    case EExteriorSide.FRONT:
      return frontSvg;
    case EExteriorSide.BACK:
      return backSvg;
    case EExteriorSide.TOP:
      return topSvg;
    default:
      return null;
  }
};

const ExteriorInspection = () => {
  const route =
    useRoute<TNavigationRouteProp<ERootStackRoutes.ExteriorInspection>>();

  const camera = useRef<Camera>(null);

  const [hasPermission, setHasPermission] = React.useState(false);
  const [askedPermission, setAskedPermission] = useState(false);
  const [selectedSidePhoto, setSelectedSidePhoto] = useState<string>('');
  const [damages, setDamages] = useState<Damage[]>([]);
  const [selectedDamage, setSelectedDamage] = useState<Damage | null>(null);
  const [damagePanelIndex, setDamagePanelIndex] = useState<number>(-1);

  const svgString = useMemo(() => {
    if (
      route?.params?.side !== EExteriorSide.BACK_LEFT &&
      route?.params?.side !== EExteriorSide.BACK_RIGHT &&
      route?.params?.side !== EExteriorSide.FRONT_LEFT &&
      route?.params?.side !== EExteriorSide.FRONT_RIGHT
    ) {
      return getSvgString(route?.params?.side);
    }
    return null;
  }, [route?.params?.side]);

  const svgOptions = useMemo(
    () => (svgString ? svgToObject(svgString) : null),
    [svgString],
  );

  const exteriorItem = useMemo(
    () => ({
      svg: svgString,
      damages,
    }),
    [svgString, damages],
  );

  const onAddDamage = useCallback(
    (damage: Damage) => {
      setDamages(damages.concat(damage));
    },
    [damages],
  );

  useEffect(() => {
    Orientation.unlockAllOrientations();
    return () => {
      Orientation.lockToPortrait();
    };
  }, []);

  useEffect(() => {
    (async () => {
      try {
        const status = await Camera.requestCameraPermission();
        setHasPermission(status === 'authorized');
        setAskedPermission(true);
      } catch (e) {}
    })();
  }, []);

  const devices = useCameraDevices();

  const onRemoveDamage = useCallback(() => {
    setDamages(damages.filter(item => item?.id !== selectedDamage?.id));
    setSelectedDamage(null);
  }, [damages, selectedDamage]);

  const onChangeDamageDetail = useCallback(
    (detail: string) => {
      if (selectedDamage) {
        const newSelectedDamage = {
          ...selectedDamage,
          detail,
        };
        setSelectedDamage(newSelectedDamage);
        setDamages(
          damages
            .filter(item => item?.id !== selectedDamage?.id)
            .concat([newSelectedDamage]),
        );
      }
    },
    [damages, selectedDamage],
  );

  const onChangeDamageType = useCallback(
    (damageType: string) => {
      if (selectedDamage) {
        const newSelectedDamage = {
          ...selectedDamage,
          damageType,
        };
        setSelectedDamage(newSelectedDamage);
        setDamages(
          damages
            .filter(item => item?.id !== selectedDamage?.id)
            .concat([newSelectedDamage]),
        );
      }
    },
    [damages, selectedDamage],
  );

  const onChangePhoto = useCallback(
    (photo: string) => {
      if (selectedDamage) {
        const newSelectedDamage = {
          ...selectedDamage,
          photo,
        };
        setSelectedDamage(newSelectedDamage);
        setDamages(
          damages
            .filter(item => item?.id !== selectedDamage?.id)
            .concat([newSelectedDamage]),
        );
      }
    },
    [damages, selectedDamage],
  );

  const onChangeNote = useCallback(
    (note: string) => {
      if (selectedDamage) {
        const newSelectedDamage = {
          ...selectedDamage,
          note,
        };
        setSelectedDamage(newSelectedDamage);
        setDamages(
          damages
            .filter(item => item?.id !== selectedDamage?.id)
            .concat([newSelectedDamage]),
        );
      }
    },
    [damages, selectedDamage],
  );

  console.log('selectedDamage', selectedDamage);

  const onPressDamageItem = useCallback((damageItem: Damage) => {
    setSelectedDamage(damageItem);
    setDamagePanelIndex(0);
  }, []);

  useEffect(() => {
    if (damagePanelIndex === -1) {
      setSelectedDamage(null);
    }
  }, [damagePanelIndex]);

  const renderDamageItem = useCallback(
    ({item: damageItem}: ListRenderItemInfo<Damage>) => {
      if (!damageItem?.damageType) {
        return null;
      }
      return (
        <TouchableOpacity
          key={damageItem?.id}
          activeOpacity={0.8}
          onPress={() => onPressDamageItem(damageItem)}
          style={styles.damageItemContainer}>
          <View style={styles.damageItemTypeContainer}>
            <Text style={styles.damageItemType}>
              {damageItem?.damageType[0].toUpperCase()}
            </Text>
          </View>
          <Text style={styles.damageItemText}>{`${damageItem?.damageType}  ${
            damageItem?.detail || ''
          }`}</Text>
        </TouchableOpacity>
      );
    },
    [onPressDamageItem],
  );

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'light-content'} backgroundColor="#000000" />
      <ModalHandle showCloseButton={true} />
      {selectedSidePhoto ? (
        <View style={styles.camera}>
          <Image
            style={styles.image}
            source={{uri: selectedSidePhoto}}
            resizeMode="cover"
          />
        </View>
      ) : askedPermission && hasPermission && devices.back ? (
        <Camera
          ref={camera}
          style={styles.camera}
          device={devices.back}
          orientation="portrait"
          isActive={true}
          photo={true}
        />
      ) : (
        <View style={styles.camera} />
      )}
      <View style={styles.content}>
        <View style={styles.damageContainer}>
          <DamageDrawView
            disabledDraw={false}
            exteriorItem={exteriorItem}
            svgHeight={svgOptions?.height || deviceWidth}
            svgWidth={svgOptions?.width || deviceWidth}
            onSelectDamage={onPressDamageItem}
            onAddDamage={onAddDamage}
            highlightedDamage={selectedDamage}
          />
        </View>
        {damages.length > 0 ? (
          <FlatList<Damage>
            horizontal={true}
            data={damages}
            renderItem={renderDamageItem}
            removeClippedSubviews={true}
            showsHorizontalScrollIndicator={false}
            style={styles.damageScroll}
            contentContainerStyle={styles.damageScrollContent}
          />
        ) : null}
      </View>
      <DamageBottomSheet
        index={damagePanelIndex}
        setIndex={setDamagePanelIndex}
        onRemoveDamage={onRemoveDamage}
        selectedDamage={selectedDamage}
        onChangeDamageDetail={onChangeDamageDetail}
        onChangeDamageType={onChangeDamageType}
        onChangeNote={onChangeNote}
        onChangePhoto={onChangePhoto}
      />
    </View>
  );
};

export default ExteriorInspection;
