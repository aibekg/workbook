import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  scroll: {
    flex: 1,
    width: '100%',
  },
  content: {
    width: '100%',
    paddingHorizontal: 4,
    paddingBottom: bottomSpace + 8,
    flexDirection: 'column',
    alignItems: 'center',
  },
  headerTitle: {
    width: 200,
    marginTop: 8,
    marginBottom: 16,
    fontWeight: '400',
    fontSize: 16,
    opacity: 0.5,
    color: '#000000',
    textAlign: 'center',
  },
});
