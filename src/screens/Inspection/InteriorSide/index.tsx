import React, {useCallback} from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  StatusBar,
  Text,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/Inspection/InteriorSide/components/Header';
import SideItem from '@screens/Inspection/InteriorSide/components/SideItem';
import {capitalizeWord, enumToHumanReadable} from '@utils/stringHelpers';

import {styles} from './styles';
import {EInteriorSide} from './types';

const exteriorSides = Object.keys(EInteriorSide);

const InteriorSide = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.InteriorSide>>();

  const onPressSide = useCallback(
    (side: EInteriorSide) => {
      navigation.navigate(ERootStackRoutes.InteriorInspection, {
        side,
        showCamera: true,
      });
    },
    [navigation],
  );

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<string>) => {
      return (
        <SideItem
          key={item}
          side={item as EInteriorSide}
          title={capitalizeWord(enumToHumanReadable(item))}
          onPress={() => onPressSide(item as EInteriorSide)}
        />
      );
    },
    [onPressSide],
  );

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="EXTERIOR INSPECTION" />
      <FlatList<string>
        data={exteriorSides}
        numColumns={2}
        ListHeaderComponent={
          <Text style={styles.headerTitle}>
            {'Pick a Vehicle Side to start making photos'}
          </Text>
        }
        showsVerticalScrollIndicator={false}
        style={styles.scroll}
        contentContainerStyle={styles.content}
        renderItem={renderItem}
      />
    </View>
  );
};

export default InteriorSide;
