import React, {FC, useMemo} from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';

import {TSideItemProps} from '@screens/Inspection/InteriorSide/components/SideItem/types';
import {EInteriorSide} from '@screens/Inspection/InteriorSide/types';

import {styles} from './styles';

const getSideImage = (side: EInteriorSide) => {
  switch (side) {
    default:
      return null;
  }
};

const SideItem: FC<TSideItemProps> = ({image, onPress, side, title}) => {
  const sideImage = useMemo(() => getSideImage(side), [side]);
  if (!sideImage) {
    return null;
  }
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={styles.container}>
      {image ? (
        <Image
          source={{uri: image}}
          style={styles.fullImage}
          resizeMode="cover"
        />
      ) : (
        <>
          <Image source={sideImage} style={styles.image} resizeMode="center" />
          <Text style={styles.title}>{title}</Text>
        </>
      )}
    </TouchableOpacity>
  );
};

export default SideItem;
