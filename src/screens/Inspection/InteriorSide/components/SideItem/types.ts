import {EInteriorSide} from '@screens/Inspection/InteriorSide/types';

export type TSideItemProps = {
  side: EInteriorSide;
  image?: string;
  title: string;
  onPress?: () => void;
};
