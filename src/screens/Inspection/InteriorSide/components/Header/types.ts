import {StyleProp, ViewStyle} from 'react-native';

export type THeaderProps = {
  title: string;
  style?: StyleProp<ViewStyle>;
};
