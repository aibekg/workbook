import React, {Dispatch, FC, SetStateAction, useCallback, useMemo} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import CheckedSvg from '@icons/checked.svg';

import {styles} from './styles';

export type TDocumentValue = 'title' | 'billOfSale';

export type TDocumentItemProps = {
  title: string;
  selected?: boolean;
  selectedDocuments: TDocumentValue[];
  setSelectedDocuments: Dispatch<SetStateAction<TDocumentValue[]>>;
  value: TDocumentValue;
};

const DocumentItem: FC<TDocumentItemProps> = ({
  title,
  selected,
  selectedDocuments,
  setSelectedDocuments,
  value,
}) => {
  const isSelected = useMemo(
    () => !!selectedDocuments.find(item => item === value),
    [selectedDocuments, value],
  );

  const handlePress = useCallback(() => {
    if (isSelected) {
      setSelectedDocuments(prev => prev.filter(item => item !== value));
    } else {
      setSelectedDocuments(prev => [...prev, value]);
    }
  }, [isSelected, setSelectedDocuments, value]);

  return (
    <TouchableOpacity onPress={handlePress} style={styles.container}>
      <View style={[styles.circle, isSelected && styles.selectedCircle]}>
        <CheckedSvg fill={isSelected ? '#CDFF48' : '#f3f3f3'} />
      </View>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default DocumentItem;
