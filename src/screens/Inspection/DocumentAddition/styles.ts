import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    textAlign: 'center',
    color: '#000',
    fontSize: 16,
    opacity: 0.5,
    marginVertical: 24,
  },
  list: {
    padding: 8,
    paddingHorizontal: 24,
  },
  buttonContainer: {
    left: 16,
    right: 16,
    flexDirection: 'row',
    position: 'absolute',
    bottom: bottomSpace + 16,
  },
  button: {
    flex: 1,
    marginHorizontal: 0,
  },
});
