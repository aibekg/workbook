import React, {useCallback, useState} from 'react';
import {Platform, StatusBar, Text, View} from 'react-native';
import {useActionSheet} from '@expo/react-native-action-sheet';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import {TPhoto} from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet/types';
import {pickDocument} from '@utils/imagePickerHelpers';

import DocumentItem, {TDocumentValue} from './components/DocumentItem';
import {styles} from './styles';

export type TDocumentPhotos = {
  key: TDocumentValue;
  photo: TPhoto;
};

const DocumentAddition = () => {
  const [selectedDocuments, setSelectedDocuments] = useState<TDocumentValue[]>(
    [],
  );
  const {showActionSheetWithOptions} = useActionSheet();
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.DocumentAddition>>();

  const handlePressMakePhoto = useCallback(async () => {
    try {
      let photos: TDocumentPhotos[] = [];
      for (const item of selectedDocuments) {
        const pickerResponse = await pickDocument(
          showActionSheetWithOptions,
          {
            cropperCircleOverlay: false,
            cropping: false,
            width: 400,
            height: 400,
          },
          item === 'title'
            ? 'Title Document'
            : item === 'billOfSale'
            ? 'Bill of Sale Document'
            : undefined,
        );
        if (pickerResponse.data) {
          photos.push({key: item, photo: pickerResponse});
        }
      }
      navigation.navigate(ERootStackRoutes.DocumentConfirm, {photos});
    } catch (e) {}
  }, [navigation, selectedDocuments, showActionSheetWithOptions]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="DOCUMENTS" search={false} />
      <Text style={styles.text}>
        {'Select documents you are \n going to attach'}
      </Text>
      <View style={styles.list}>
        <DocumentItem
          value="title"
          selectedDocuments={selectedDocuments}
          setSelectedDocuments={setSelectedDocuments}
          title="Title"
        />
        <DocumentItem
          value="billOfSale"
          selectedDocuments={selectedDocuments}
          setSelectedDocuments={setSelectedDocuments}
          title="Bill of Sale"
        />
      </View>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.KeysAddition> />
        <Button
          disabled={!selectedDocuments.length}
          hideWhenDisabled={true}
          buttonStyle={styles.button}
          text="Make Photo"
          onPress={handlePressMakePhoto}
        />
      </View>
    </View>
  );
};

export default DocumentAddition;
