import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  text: {
    textAlign: 'center',
    color: '#000',
    fontSize: 16,
    opacity: 0.5,
    marginVertical: 24,
  },
  list: {
    marginHorizontal: 28,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: deviceWidth,
    padding: 32,
  },
});
