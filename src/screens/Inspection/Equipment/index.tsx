import React, {useCallback, useEffect, useState} from 'react';
import {Platform, StatusBar, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import Button from '@components/Button';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import {selectEquipments} from '@store/unit/selectors';
import {TEquipmentTitle} from '@store/unit/types';

import EquipmentItem from './components/Item';
import {styles} from './styles';

const equipmentsData: TEquipmentTitle[] = [
  'Right Side Detail',
  'Left Side Mirror',
  'Spare Tire',
  'Front Mirror',
  'DVD Player',
  'Wiper Blades',
  'Radio',
];

const Equipment = () => {
  const [selectedValues, setSelectedValues] = useState<TEquipmentTitle[]>([]);
  const equipments = useSelector(selectEquipments);

  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.Equipment>>();

  const handlePress = useCallback(() => {
    navigation.navigate(ERootStackRoutes.EquipmentConfirm, {
      selectedEquipments: selectedValues,
    });
  }, [navigation, selectedValues]);

  useEffect(() => {
    selectedValues.map(item => {
      if (equipments.find(value => value.title === item)) {
        setSelectedValues(prev => prev.filter(val => val !== item));
      }
    });
  }, [equipments, selectedValues]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="EQUIPMENT" search={false} />
      <Text style={styles.text}>
        {'Select Equipment installed \non the vehicle'}
      </Text>
      <View style={styles.list}>
        {equipmentsData.map(item => (
          <EquipmentItem
            key={item}
            value={item}
            selectedValues={selectedValues}
            setSelectedValues={setSelectedValues}
          />
        ))}
      </View>
      <View style={styles.footer}>
        <Button
          disabled={!selectedValues.length}
          text="Continue"
          onPress={handlePress}
        />
      </View>
    </View>
  );
};

export default Equipment;
