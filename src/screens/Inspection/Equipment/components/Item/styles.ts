import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: 12,
    opacity: 0.4,
  },
  selectedContainer: {
    opacity: 1,
  },
  title: {
    fontSize: 28,
    color: '#212121',
    marginLeft: 12,
  },
  circle: {
    width: 32,
    height: 32,
    borderRadius: 32,
    borderWidth: 2,
    borderColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedCircle: {
    backgroundColor: '#000000',
  },
  selectedDamagedCircle: {
    backgroundColor: '#FA354D',
    borderWidth: 0,
  },
});
