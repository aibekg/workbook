import React, {
  Dispatch,
  FC,
  memo,
  SetStateAction,
  useCallback,
  useMemo,
} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';

import {useActions} from '@hooks/useActions';
import BangSvg from '@icons/bang.svg';
import CheckedSvg from '@icons/checked.svg';
import {selectEquipments} from '@store/unit/selectors';
import {TEquipmentTitle} from '@store/unit/types';

import {styles} from './styles';

export type TEquipmentProps = {
  title?: string;
  value: TEquipmentTitle;
  selectedValues: TEquipmentTitle[];
  setSelectedValues: Dispatch<SetStateAction<TEquipmentTitle[]>>;
  damaged?: boolean;
};

const EquipmentItem: FC<TEquipmentProps> = memo(
  ({value, setSelectedValues, selectedValues, title, damaged}) => {
    const equipments = useSelector(selectEquipments);
    const {removeEquipment} = useActions();
    const isSelected = useMemo(
      () => !!selectedValues.find(item => item === value),
      [selectedValues, value],
    );

    const isSaved = useMemo(
      () => !!equipments.find(item => item.title === value),
      [equipments, value],
    );

    const handlePress = useCallback(() => {
      if (isSelected) {
        setSelectedValues(prev => prev.filter(item => item !== value));
      } else {
        setSelectedValues(prev => [...prev, value]);
      }
      if (isSaved) {
        removeEquipment(value);
      }
    }, [isSaved, isSelected, removeEquipment, setSelectedValues, value]);

    return (
      <TouchableOpacity
        onPress={handlePress}
        style={[
          styles.container,
          (isSelected || isSaved) && styles.selectedContainer,
        ]}>
        <View
          style={[
            styles.circle,
            (isSelected || isSaved) && styles.selectedCircle,
            ((isSaved && isSelected) || isSaved) &&
              styles.selectedDamagedCircle,
          ]}>
          {isSelected || isSaved ? (
            isSaved ? (
              <BangSvg />
            ) : (
              <CheckedSvg fill="#CDFF48" />
            )
          ) : null}
        </View>
        <Text style={styles.title}>{title || value}</Text>
      </TouchableOpacity>
    );
  },
);

export default EquipmentItem;
