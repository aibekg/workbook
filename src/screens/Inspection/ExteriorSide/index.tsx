import React, {useCallback} from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  StatusBar,
  Text,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/Inspection/ExteriorSide/components/Header';
import SideItem from '@screens/Inspection/ExteriorSide/components/SideItem';
import {capitalizeWord, enumToHumanReadable} from '@utils/stringHelpers';

import {styles} from './styles';
import {EExteriorSide} from './types';

const exteriorSides = Object.keys(EExteriorSide);

const ExteriorSide = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.ExteriorSide>>();

  const onPressSide = useCallback(
    (side: EExteriorSide) => {
      navigation.navigate(ERootStackRoutes.ExteriorInspection, {
        side,
        showCamera:
          side !== EExteriorSide.BACK_LEFT &&
          side !== EExteriorSide.BACK_RIGHT &&
          side !== EExteriorSide.FRONT_LEFT &&
          side !== EExteriorSide.FRONT_RIGHT,
      });
    },
    [navigation],
  );

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<string>) => {
      return (
        <SideItem
          key={item}
          side={item as EExteriorSide}
          title={capitalizeWord(enumToHumanReadable(item))}
          onPress={() => onPressSide(item as EExteriorSide)}
        />
      );
    },
    [onPressSide],
  );

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="EXTERIOR INSPECTION" />
      <FlatList<string>
        data={exteriorSides}
        numColumns={2}
        ListHeaderComponent={
          <Text style={styles.headerTitle}>
            {'Pick a Vehicle Side to start making photos'}
          </Text>
        }
        showsVerticalScrollIndicator={false}
        style={styles.scroll}
        contentContainerStyle={styles.content}
        renderItem={renderItem}
      />
    </View>
  );
};

export default ExteriorSide;
