import React, {FC, useCallback} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import XSvg from '@icons/x-mark.svg';

import {styles} from './styles';
import {THeaderProps} from './types';

const Header: FC<THeaderProps> = React.memo(({title, style}) => {
  const navigation = useNavigation();
  const onBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity
        hitSlop={{top: 24, bottom: 24, left: 24, right: 24}}
        onPress={onBack}>
        <XSvg fill="#212121" width={24} height={24} />
      </TouchableOpacity>
    </View>
  );
});

export default Header;
