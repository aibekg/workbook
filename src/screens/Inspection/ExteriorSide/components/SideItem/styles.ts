import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    width: deviceWidth * 0.5 - 24,
    margin: 8,
    padding: 24,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 170,
    borderRadius: 12,
    backgroundColor: '#000000',
  },
  image: {
    width: 128,
    height: 80,
  },
  title: {
    width: '100%',
    color: '#FFFFFF',
    marginTop: 16,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '700',
  },
  fullImage: {
    height: 170,
    width: '100%',
    flex: 1,
  },
});
