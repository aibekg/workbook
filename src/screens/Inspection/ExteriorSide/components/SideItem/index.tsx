import React, {FC, useMemo} from 'react';
import {Image, Text, TouchableOpacity} from 'react-native';

import {TSideItemProps} from '@screens/Inspection/ExteriorSide/components/SideItem/types';
import {EExteriorSide} from '@screens/Inspection/ExteriorSide/types';

import {styles} from './styles';

const getSideImage = (side: EExteriorSide) => {
  switch (side) {
    case EExteriorSide.BACK:
      return require('@icons/exterior_back.png');
    case EExteriorSide.BACK_LEFT:
      return require('@icons/exterior_back_left.png');
    case EExteriorSide.BACK_RIGHT:
      return require('@icons/exterior_back_right.png');
    case EExteriorSide.FRONT:
      return require('@icons/exterior_front.png');
    case EExteriorSide.FRONT_LEFT:
      return require('@icons/exterior_front_left.png');
    case EExteriorSide.FRONT_RIGHT:
      return require('@icons/exterior_front_right.png');
    case EExteriorSide.LEFT:
      return require('@icons/exterior_left.png');
    case EExteriorSide.RIGHT:
      return require('@icons/exterior_right.png');
    case EExteriorSide.TOP:
      return require('@icons/exterior_top.png');
    default:
      return null;
  }
};

const SideItem: FC<TSideItemProps> = ({image, onPress, side, title}) => {
  const sideImage = useMemo(() => getSideImage(side), [side]);
  if (!sideImage) {
    return null;
  }
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={styles.container}>
      {image ? (
        <Image
          source={{uri: image}}
          style={styles.fullImage}
          resizeMode="cover"
        />
      ) : (
        <>
          <Image source={sideImage} style={styles.image} resizeMode="center" />
          <Text style={styles.title}>{title}</Text>
        </>
      )}
    </TouchableOpacity>
  );
};

export default SideItem;
