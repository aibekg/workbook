import {EExteriorSide} from '@screens/Inspection/ExteriorSide/types';

export type TSideItemProps = {
  side: EExteriorSide;
  image?: string;
  title: string;
  onPress?: () => void;
};
