import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  documentImage: {
    borderRadius: 12,
    overflow: 'hidden',
    marginTop: 12,
    height: 312,
    resizeMode: 'cover',
  },
  content: {
    paddingHorizontal: 12,
    paddingBottom: 172 + bottomSpace,
  },
  scroll: {
    width: '100%',
    flex: 1,
  },
  footer: {
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
    backgroundColor: '#F1F1F1',
    paddingTop: 16,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingBottom: bottomSpace + 16,
  },
  confirmButton: {
    marginHorizontal: 48,
  },
  finishBtn: {
    marginHorizontal: 0,
    backgroundColor: 'transparent',
    marginTop: 8,
  },
  finishBtnText: {
    color: '#1E1F24',
  },
});
