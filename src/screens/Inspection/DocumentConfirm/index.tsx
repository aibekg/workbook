import React, {useCallback} from 'react';
import {Image, Platform, ScrollView, StatusBar, View} from 'react-native';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import {useSaveImages} from '@hooks/useSaveImages';
import {useUpdateUnitDocument} from '@hooks/useUpdateUnitDocument';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import BaseCard from '@screens/Unit/components/BaseCard';
import {selectInspectionUnit} from '@store/unit/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';

import {styles} from './styles';

const DocumentConfirm = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.DocumentConfirm>>();
  const {params} =
    useRoute<
      RouteProp<TRootStackParamList, ERootStackRoutes.DocumentConfirm>
    >();

  const {updateUnitDocument, isLoading} = useUpdateUnitDocument();
  const {saveImages, isLoading: isPhotoUploading} = useSaveImages();

  const inspectionUnit = useSelector(selectInspectionUnit);

  const handlePressConfirm = useCallback(async () => {
    try {
      if (inspectionUnit?.id) {
        for (const item of params.photos) {
          const photoUrl = await saveImages({image: item.photo.data});
          if (photoUrl) {
            await updateUnitDocument({
              unitId: inspectionUnit?.id,
              photo: photoUrl,
              name: item.key,
              type: item.key,
            });
            navigation.navigate(ERootStackRoutes.Environment);
          }
        }
      }
    } catch (e) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: 'Please try again',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
  }, [
    navigation,
    inspectionUnit?.id,
    params.photos,
    saveImages,
    updateUnitDocument,
  ]);

  const handlePressStartOver = useCallback(() => {
    navigation.navigate(ERootStackRoutes.DocumentAddition);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="DOCUMENTS" search={false} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scroll}
        contentContainerStyle={styles.content}>
        {params.photos.map(document => (
          <BaseCard
            key={document.key}
            title={
              document.key === 'title'
                ? 'Title'
                : document.key === 'billOfSale'
                ? 'Bill of Sale'
                : null
            }
            content={
              <Image
                style={styles.documentImage}
                source={{
                  uri: document.photo.path,
                }}
              />
            }
          />
        ))}
      </ScrollView>
      <View style={styles.footer}>
        <Button
          disabled={isLoading || isPhotoUploading}
          buttonStyle={styles.confirmButton}
          text="Confirm & Next"
          onPress={handlePressConfirm}
        />
        <Button
          textStyle={styles.finishBtnText}
          buttonStyle={styles.finishBtn}
          text="Start Over"
          onPress={handlePressStartOver}
        />
      </View>
    </View>
  );
};

export default DocumentConfirm;
