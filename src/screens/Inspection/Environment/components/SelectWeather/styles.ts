import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 12,
  },
  weather: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: bottomSpace + 128,
  },
  name: {
    fontSize: 28,
    color: '#212121',
    marginVertical: 20,
    textAlign: 'center',
  },
  icon: {
    width: 128,
    height: 128,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeIcon: {
    backgroundColor: '#CDFF48',
  },
});
