import React, {Dispatch, FC, SetStateAction, useCallback} from 'react';
import {Text, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import {EWeatherKey, TWeather} from '@screens/Inspection/Environment/types';
import {deviceWidth} from '@utils/deviceHelpers';

import {styles} from './styles';

export type TSelectWeatherProps = {
  selectedWeatherKey: EWeatherKey;
  setSelectedWeatherKey: Dispatch<SetStateAction<EWeatherKey>>;
  data: TWeather[];
};

const SelectWeather: FC<TSelectWeatherProps> = ({
  data,
  selectedWeatherKey,
  setSelectedWeatherKey,
}) => {
  const renderItem = useCallback(
    ({Icon, name, key}: TWeather) => (
      <View style={styles.weather}>
        <View
          style={[
            styles.icon,
            key === selectedWeatherKey && styles.activeIcon,
          ]}>
          <Icon fill={key === selectedWeatherKey ? '#000000' : '#c3cbd5'} />
        </View>
        <Text style={styles.name}>{name}</Text>
      </View>
    ),
    [selectedWeatherKey],
  );

  return (
    <View style={styles.container}>
      <Carousel<TWeather>
        firstItem={data.findIndex(item => item.key === selectedWeatherKey)}
        onSnapToItem={slideIndex => setSelectedWeatherKey(data[slideIndex].key)}
        layoutCardOffset={3}
        data={data}
        renderItem={({item}) => renderItem(item)}
        itemWidth={144}
        sliderWidth={deviceWidth}
      />
    </View>
  );
};

export default SelectWeather;
