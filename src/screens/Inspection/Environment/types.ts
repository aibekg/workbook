import {FC} from 'react';
import {SvgProps} from 'react-native-svg';

export type TWeather = {
  key: EWeatherKey;
  name: string;
  Icon: FC<SvgProps>;
};

export enum EWeatherKey {
  RAIN = 'RAIN',
  SUN = 'SUN',
  SNOW = 'SNOW',
  FOG = 'FOG',
  THUNDER_STORM = 'THUNDER_STORM',
  SLEET = 'SLEET',
}
