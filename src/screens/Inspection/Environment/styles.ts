import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    left: 16,
    right: 16,
    position: 'absolute',
    bottom: bottomSpace + 16,
  },
  button: {
    flex: 1,
    marginHorizontal: 0,
  },
  buttonsContainer: {
    flexDirection: 'row',
  },
  locationContainer: {
    paddingVertical: 32,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#d3d3d3',
  },
  location: {
    marginLeft: 8,
    color: '#646C7A',
    fontSize: 12,
  },
});
