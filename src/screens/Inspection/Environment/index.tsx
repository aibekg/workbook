import React, {useCallback, useState} from 'react';
import {Platform, StatusBar, Text, View} from 'react-native';
import {NetworkInfo} from 'react-native-network-info';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUnitEnvironment} from '@hooks/useUnitEnvironment';
import FogSvg from '@icons/fog.svg';
import LocationSvg from '@icons/location.svg';
import RainySvg from '@icons/rainy.svg';
import SleetSvg from '@icons/sleet.svg';
import SnowySvg from '@icons/snowy.svg';
import SunnySvg from '@icons/sunny.svg';
import ThunderstormSvg from '@icons/thunderstorm.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import SelectWeather from '@screens/Inspection/Environment/components/SelectWeather';
import {selectInspectionUnit} from '@store/unit/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';

import {styles} from './styles';
import {EWeatherKey, TWeather} from './types';

const weathers: TWeather[] = [
  {
    key: EWeatherKey.SUN,
    name: 'Sunny',
    Icon: SunnySvg,
  },
  {
    key: EWeatherKey.RAIN,
    name: 'Rainy',
    Icon: RainySvg,
  },
  {
    key: EWeatherKey.SNOW,
    name: 'Snowy',
    Icon: SnowySvg,
  },
  {
    key: EWeatherKey.FOG,
    name: 'Fog',
    Icon: FogSvg,
  },
  {
    key: EWeatherKey.SLEET,
    name: 'Sleet',
    Icon: SleetSvg,
  },
  {
    key: EWeatherKey.THUNDER_STORM,
    name: 'Thunder',
    Icon: ThunderstormSvg,
  },
];

const Environment = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.Environment>>();

  const [selectedWeatherKey, setSelectedWeatherKey] = useState(weathers[0].key);
  const {addUnitEnvironment} = useUnitEnvironment();
  const inspectionUnit = useSelector(selectInspectionUnit);

  const handlePress = useCallback(async () => {
    try {
      if (inspectionUnit?.id) {
        const frontIp = await NetworkInfo.getIPV4Address();
        await addUnitEnvironment({
          input: {
            unitId: inspectionUnit?.id,
            weather: selectedWeatherKey,
          },
          frontIp,
        });
        navigation.navigate(ERootStackRoutes.ExteriorSide);
      }
    } catch (e) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: 'Please try again',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
  }, [navigation, addUnitEnvironment, inspectionUnit?.id, selectedWeatherKey]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="ENVIRONMENT" search={false} />
      <SelectWeather
        selectedWeatherKey={selectedWeatherKey}
        setSelectedWeatherKey={setSelectedWeatherKey}
        data={weathers}
      />
      <View style={styles.footer}>
        {inspectionUnit?.wareHouseAreaLot?.warehouseArea?.name ? (
          <View style={styles.locationContainer}>
            <LocationSvg fill={'#a3acb2'} />
            <Text
              style={
                styles.location
              }>{`${inspectionUnit?.wareHouseAreaLot?.warehouseArea?.name}`}</Text>
          </View>
        ) : null}
        <View style={styles.buttonsContainer}>
          <BackButton<ERootStackRoutes.KeysAddition> />
          <Button
            hideWhenDisabled={true}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      </View>
    </View>
  );
};

export default Environment;
