import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  list: {
    width: '100%',
    paddingHorizontal: 8,
    flex: 1,
  },
  content: {
    width: '100%',
    paddingBottom: 96 + bottomSpace,
  },
  paddedList: {
    width: '100%',
    paddingHorizontal: 16,
    flex: 1,
  },
  paddingContent: {
    width: '100%',
    paddingTop: 32,
    paddingBottom: 96 + bottomSpace,
  },
  selectText: {
    fontSize: 28,
    color: '#212121',
    fontWeight: 'bold',
    paddingHorizontal: 28,
    marginBottom: 16,
  },
  buttonContainer: {
    left: 16,
    right: 16,
    flexDirection: 'row',
    position: 'absolute',
    bottom: bottomSpace + 16,
  },
  button: {
    flex: 1,
    marginHorizontal: 0,
  },
  addButton: {
    flex: 1,
    marginHorizontal: 0,
    backgroundColor: '#FFFFFF',
    borderColor: '#21212126',
    borderWidth: 1,
  },
  addButtonText: {
    color: '#212121',
  },
});
