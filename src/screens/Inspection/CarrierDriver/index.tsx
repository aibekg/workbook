import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItemInfo,
  Platform,
  StatusBar,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import HeaderSearch from '@components/HeaderSearch';
import {useCarrierDrivers} from '@hooks/useCarrierDrivers';
import {useUpdateUnitCarrierDriver} from '@hooks/useUpdateUnitCarrierDriver';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import AddNewDriverBottomSheet from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet';
import Item from '@screens/Inspection/CarrierDriver/components/Item';
import {selectInspectionUnit} from '@store/unit/selectors';
import {
  bottomSpace,
  statusBarHeight,
  useKeyboardVisibility,
} from '@utils/deviceHelpers';

import {CarrierDriver} from '../../../generated/graphql';

import {styles} from './styles';

const CarrierDriverScreen = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.CarrierDriver>>();
  const route =
    useRoute<TNavigationRouteProp<ERootStackRoutes.CarrierDriver>>();

  const [searchEnabled, setSearchEnabled] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const prevIndex = useRef(-1);
  const [index, setIndex] = useState(-1);

  const searchToggle = useCallback(() => {
    if (searchEnabled) {
      setSearch('');
    }
    setSearchEnabled(!searchEnabled);
  }, [searchEnabled]);

  const {
    refresh,
    carrierDrivers,
    isLoading,
    isPaginating,
    getCarrierDrivers,
    selectedCarrierDriver,
    setSelectedCarrierDriver,
  } = useCarrierDrivers(route?.params?.carrierCompanyId, search);

  const {updateUnitCarrierDriver} = useUpdateUnitCarrierDriver();

  const inspectionUnit = useSelector(selectInspectionUnit);

  useEffect(() => {
    if (prevIndex.current !== index && index === -1) {
      refresh();
      prevIndex.current = index;
    }
  }, [index, refresh]);

  const handlePress = useCallback(async () => {
    if (selectedCarrierDriver) {
      try {
        if (
          inspectionUnit?.id &&
          selectedCarrierDriver?.id &&
          selectedCarrierDriver?.name
        ) {
          const unit = await updateUnitCarrierDriver({
            carrirerDriverId: selectedCarrierDriver?.id,
            unitId: inspectionUnit?.id,
          });
          if (unit?.id) {
            navigation.navigate(ERootStackRoutes.KeysAddition);
          }
        }
      } catch (e) {
        Toast.show({
          type: 'error',
          text1: 'Something wrong',
          text2: 'Please try again',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: statusBarHeight + 16,
        });
      }
    } else {
      setIndex(0);
    }
  }, [
    navigation,
    selectedCarrierDriver,
    inspectionUnit,
    updateUnitCarrierDriver,
  ]);

  const keyExtractor = useCallback(
    (item: CarrierDriver) => item.id.toString(),
    [],
  );

  const getMore = useCallback(() => getCarrierDrivers(), [getCarrierDrivers]);

  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<CarrierDriver>) => {
      return (
        <Item
          selected={selectedCarrierDriver}
          title={item.name!}
          value={item}
          onPress={setSelectedCarrierDriver}
        />
      );
    },
    [selectedCarrierDriver, setSelectedCarrierDriver],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#1E1F24"
      />
      <Header title="DRIVER" search={true} onSearch={searchToggle} />
      <FlatList
        keyExtractor={keyExtractor}
        data={carrierDrivers}
        removeClippedSubviews={true}
        style={styles.paddedList}
        contentContainerStyle={styles.paddingContent}
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        onEndReached={getMore}
        onEndReachedThreshold={0.1}
        ListEmptyComponent={
          isLoading && !isPaginating ? <ActivityIndicator size="small" /> : null
        }
        ListFooterComponent={
          isPaginating ? <ActivityIndicator size="small" /> : null
        }
      />
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitMake> />
          <Button
            buttonStyle={
              selectedCarrierDriver ? styles.button : styles.addButton
            }
            textStyle={selectedCarrierDriver ? undefined : styles.addButtonText}
            text={selectedCarrierDriver ? 'Confirm' : 'Add new Driver'}
            onPress={handlePress}
          />
        </View>
      )}
      {searchEnabled ? (
        <HeaderSearch
          search={search}
          setSearch={setSearch}
          onClose={searchToggle}
        />
      ) : null}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
      <AddNewDriverBottomSheet index={index} setIndex={setIndex} />
    </View>
  );
};

export default CarrierDriverScreen;
