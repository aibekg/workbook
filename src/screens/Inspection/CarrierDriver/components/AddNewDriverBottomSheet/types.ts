export type TAddNewDriverFormValues = {
  firstname: string;
  lastname: string;
  driverLicense: string;
};

export type TPhoto = {
  path: string;
  data: string;
};
