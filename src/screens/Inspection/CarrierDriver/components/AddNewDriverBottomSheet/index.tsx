import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useMemo,
  useRef,
  useState,
} from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import BottomSheet from '@gorhom/bottom-sheet';
import {useFormik} from 'formik';

import Button from '@components/Button';
import Input from '@components/Input';
import {useSaveImages} from '@hooks/useSaveImages';
import {useUpdateCarrierDriver} from '@hooks/useUpdateCarrierDriver';
import {bottomSpace} from '@utils/deviceHelpers';

import Photo from './components/Photo';
import {addNewDriverSchema} from './schema';
import {styles} from './styles';
import {TAddNewDriverFormValues, TPhoto} from './types';

export type TAddNewDriverBottomSheetProps = {
  index: number;
  setIndex: Dispatch<SetStateAction<number>>;
};

export type TFiled = {
  name: keyof TAddNewDriverFormValues;
};

const AddNewDriverBottomSheet: FC<TAddNewDriverBottomSheetProps> = ({
  index,
  setIndex,
}) => {
  const snapPoints = useMemo(() => ['85%'], []);
  const bottomSheetRef = useRef<BottomSheet>(null);
  const [photo, setPhoto] = useState<TPhoto | null>(null);
  const {updateCarrierDriver, isLoading, error} = useUpdateCarrierDriver();
  const {saveImages} = useSaveImages();

  const {handleChange, handleSubmit, handleBlur, values, errors, resetForm} =
    useFormik<TAddNewDriverFormValues>({
      initialValues: {
        firstname: '',
        lastname: '',
        driverLicense: '',
      },
      validateOnBlur: true,
      onSubmit: async data => {
        try {
          const input = {
            name: `${data.firstname} ${data.lastname}`,
            licenceNumber: data.driverLicense,
          };

          if (photo) {
            const photoUrl = await saveImages({image: photo.data});
            await updateCarrierDriver({input, photo: photoUrl});
          } else {
            await updateCarrierDriver({input});
          }
          resetFields();
          setIndex(-1);
        } catch (e) {}
      },
      validationSchema: addNewDriverSchema,
    });

  const resetFields = useCallback(() => {
    bottomSheetRef.current?.close();
    resetForm();
  }, [resetForm]);

  const fields: TFiled[] = useMemo(
    () => [{name: 'firstname'}, {name: 'lastname'}, {name: 'driverLicense'}],
    [],
  );

  const handleBottomSheetIndexChange = useCallback(
    (idx: number) => {
      setIndex(idx);
    },
    [setIndex],
  );

  const renderPlaceholder = useCallback((name: string) => {
    return `Enter ${name
      .split(/(?=[A-Z])/g)
      .map(item => item.charAt(0).toUpperCase() + item.slice(1))
      .join(' ')}`;
  }, []);

  const handleTextChange = useCallback(
    (name: keyof TAddNewDriverFormValues) => (value: string) => {
      handleChange(name)(value);
    },
    [handleChange],
  );

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={index}
      backgroundStyle={styles.background}
      enablePanDownToClose
      onChange={handleBottomSheetIndexChange}
      keyboardBehavior={'extend'}
      snapPoints={snapPoints}>
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.scrollContent}
        showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>New Driver</Text>
        <View style={styles.content}>
          {fields.map(({name}) => (
            <Input
              key={name}
              value={values[name]}
              onChangeText={handleTextChange(name)}
              onBlur={handleBlur(name)}
              placeholder={renderPlaceholder(name)}
              error={errors[name]}
              label={name}
            />
          ))}
          <Photo photo={photo} setPhoto={setPhoto} />
        </View>
        {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
      </ScrollView>
      <Button
        error={error}
        disabled={isLoading}
        onPress={handleSubmit}
        text="Continue"
        buttonStyle={styles.button}
      />
    </BottomSheet>
  );
};

export default AddNewDriverBottomSheet;
