import React, {Dispatch, FC, SetStateAction, useCallback} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {useActionSheet} from '@expo/react-native-action-sheet';

import CirclePlusSvg from '@icons/circle-plus.svg';
import XCircleSvg from '@icons/x-circle.svg';
import {pickDocument} from '@utils/imagePickerHelpers';

import {TPhoto} from '../../types';

import {styles} from './styles';

export type TPhotoProps = {
  photo: TPhoto | null;
  setPhoto: Dispatch<SetStateAction<TPhoto | null>>;
};

const Photo: FC<TPhotoProps> = ({photo, setPhoto}) => {
  const {showActionSheetWithOptions} = useActionSheet();

  const handleCancel = useCallback(() => {
    setPhoto(null);
  }, [setPhoto]);

  const handlePressAddPhoto = useCallback(async () => {
    try {
      const pickerResponse = await pickDocument(showActionSheetWithOptions);
      if (pickerResponse.data) {
        setPhoto(pickerResponse);
      }
    } catch (e) {}
  }, [showActionSheetWithOptions]);

  return photo ? (
    <View style={[styles.photoContainer]}>
      <Image style={styles.photo} source={{uri: photo.path}} />
      <XCircleSvg
        style={styles.cancelIcon}
        onPress={handleCancel}
        fill="#000000"
      />
    </View>
  ) : (
    <TouchableOpacity onPress={handlePressAddPhoto} style={styles.addPhoto}>
      <CirclePlusSvg fill="#000000" />
      <Text style={styles.addPhotoText}>Add Photo</Text>
    </TouchableOpacity>
  );
};

export default Photo;
