import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  photoContainer: {
    position: 'relative',
    marginVertical: 20,
  },
  photo: {
    height: 180,
    width: '100%',
    borderRadius: 20,
  },
  cancelIcon: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  addPhoto: {
    height: 180,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 20,
  },
  addPhotoText: {
    color: '#000000',
    fontWeight: '700',
    marginLeft: 8,
  },
});
