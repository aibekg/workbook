import * as Yup from 'yup';

export const addNewDriverSchema = Yup.object().shape({
  firstname: Yup.string().required().min(2).max(20),
  lastname: Yup.string().required().min(2).max(20),
  driverLicense: Yup.string().required(),
});
