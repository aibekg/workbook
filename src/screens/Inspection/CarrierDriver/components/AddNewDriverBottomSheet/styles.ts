import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  background: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#383838',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 16.0,
    elevation: 24,
  },
  container: {
    flex: 1,
  },
  content: {
    padding: 8,
  },
  button: {
    position: 'absolute',
    left: 56,
    right: 56,
    bottom: bottomSpace + 16,
  },
  title: {
    fontSize: 28,
    fontWeight: '700',
    marginTop: 8,
    marginHorizontal: 20,
  },
  scroll: {
    flex: 1,
    width: '100%',
  },
  scrollContent: {
    paddingBottom: bottomSpace + 88,
    width: '100%',
  },
});
