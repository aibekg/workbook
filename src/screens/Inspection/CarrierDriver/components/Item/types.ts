import {ImageSourcePropType} from 'react-native';

import {CarrierDriver} from '../../../../../generated/graphql';

export type TItemProps = {
  title: string;
  value: CarrierDriver;
  selected?: CarrierDriver | null;
  image?: ImageSourcePropType;
  onPress?: (item: CarrierDriver) => void;
};
