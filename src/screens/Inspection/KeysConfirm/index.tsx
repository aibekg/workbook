import React, {useCallback} from 'react';
import {Platform, ScrollView, StatusBar, Text, View} from 'react-native';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import {useSaveImages} from '@hooks/useSaveImages';
import {useUpdateUnitKeys} from '@hooks/useUpdateUnitKeys';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Keys from '@screens/Unit/components/Keys';
import {selectInspectionUnit} from '@store/unit/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';

import {styles} from './styles';

const KeysConfirm = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.KeysConfirm>>();
  const {
    params: {manualKeysCount, remoteKeysCount, photo},
  } = useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.KeysConfirm>>();

  const {updateUnitKeys, isLoading} = useUpdateUnitKeys();
  const {saveImages, isLoading: isPhotoUploading} = useSaveImages();

  const inspectionUnit = useSelector(selectInspectionUnit);

  const handlePressConfirm = useCallback(async () => {
    try {
      if (inspectionUnit?.id) {
        const imagePath = await saveImages({image: photo.data});
        if (imagePath) {
          await updateUnitKeys({
            manualKeyCount: manualKeysCount,
            remoteKeyCount: remoteKeysCount,
            photo: imagePath,
            unitId: inspectionUnit?.id,
          });
          navigation.navigate(ERootStackRoutes.DocumentAddition);
        }
      }
    } catch (e) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: 'Please try again',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
  }, [
    navigation,
    inspectionUnit?.id,
    manualKeysCount,
    photo.data,
    remoteKeysCount,
    saveImages,
    updateUnitKeys,
  ]);

  const handlePressStartOver = useCallback(() => {
    navigation.navigate(ERootStackRoutes.KeysAddition);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#000000"
      />
      <Header title="NUMBER OF KEYS" search={false} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.content}
        style={styles.scroll}>
        <Keys
          localData
          data={{
            manualKeyCount: manualKeysCount,
            remoteKeyCount: remoteKeysCount,
            photo: photo.path,
          }}
        />
        <Text style={styles.text}>
          {
            'Please check the information above \nand confirm if everything is \ncorrect'
          }
        </Text>
      </ScrollView>
      <View style={styles.footer}>
        <Button
          disabled={isLoading || isPhotoUploading}
          buttonStyle={styles.confirmButton}
          text="Confirm & Next"
          onPress={handlePressConfirm}
        />
        <Button
          textStyle={styles.finishBtnText}
          buttonStyle={styles.finishBtn}
          text="Start Over"
          onPress={handlePressStartOver}
        />
      </View>
    </View>
  );
};

export default KeysConfirm;
