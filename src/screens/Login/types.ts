export type TLoginForm = {
  login: string;
  password: string;
};

export type TField = {
  name: keyof TLoginForm;
  placeholder: string;
};
