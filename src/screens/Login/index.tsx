import React, {MutableRefObject, useCallback, useMemo, useRef} from 'react';
import {
  Image,
  Keyboard,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useFormik} from 'formik';

import Button from '@components/Button';
import Input from '@components/Input';
import {useLogin} from '@hooks/useLogin';
import LogoSvg from '@icons/logo.svg';
import {bottomSpace} from '@utils/deviceHelpers';

import {schema} from './schema';
import {styles} from './styles';
import {TField, TLoginForm} from './types';

const Login = () => {
  const scrollRef = useRef<ScrollView>();
  const {login, isLoading, error, setError} = useLogin();

  const {handleChange, handleSubmit, handleBlur, isValid, values} =
    useFormik<TLoginForm>({
      initialValues: {
        login: __DEV__ ? '1233210' : '',
        password: __DEV__ ? '123456Cc' : '',
      },
      onSubmit: login,
      validationSchema: schema,
    });

  const loginFields: TField[] = useMemo(
    () => [
      {name: 'login', placeholder: 'Enter Login'},
      {name: 'password', placeholder: 'Enter Password'},
    ],
    [],
  );

  const handleChangeText = useCallback(
    (name: keyof TLoginForm) => (value: string) => {
      setError('');
      /^[A-Za-z0-9_]*$/.test(value) && handleChange(name)(value);
    },
    [setError, handleChange],
  );

  const onFocusInput = useCallback(() => {
    setTimeout(() => scrollRef.current?.scrollToEnd({animated: true}), 600);
  }, []);

  return (
    <View style={styles.screen}>
      <Image
        resizeMode="contain"
        source={require('@icons/shadow_top.png')}
        style={styles.topShadow}
      />
      <ScrollView
        ref={scrollRef as MutableRefObject<ScrollView>}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        bounces={false}
        style={styles.container}
        contentContainerStyle={styles.content}>
        <View style={styles.logoBlock}>
          <LogoSvg {...styles.logoIcon} fill="#4B5375" />
        </View>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.touchContainer}
          onPress={Keyboard.dismiss}>
          <View style={styles.formBlock}>
            <Text style={styles.title}>{'Enter Your\nCredentials'}</Text>
            <View style={styles.form}>
              {loginFields.map(({name, placeholder}) => (
                <Input
                  key={name}
                  placeholder={placeholder}
                  onChangeText={handleChangeText(name)}
                  onBlur={handleBlur(name)}
                  onFocus={onFocusInput}
                  value={values[name]}
                  label={name}
                  style={error ? styles.errorInput : undefined}
                  secureTextEntry={name === 'password'}
                  blurOnSubmit={true}
                />
              ))}
              <Button
                buttonStyle={error ? styles.errorButton : styles.button}
                textStyle={error ? styles.errorText : undefined}
                disabled={
                  error
                    ? false
                    : !values.login ||
                      !values.password ||
                      !isValid ||
                      !!error ||
                      isLoading
                }
                text={error ? error : isLoading ? 'Signing In...' : 'Sign In'}
                onPress={error ? undefined : handleSubmit}
              />
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default Login;
