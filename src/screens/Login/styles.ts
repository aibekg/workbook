import {StyleSheet} from 'react-native';

import {bottomSpace, deviceHeight, deviceRatio} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#16171B',
  },
  container: {
    flex: 1,
    width: '100%',
  },
  content: {
    width: '100%',
    minHeight: deviceHeight,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  touchContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    width: '100%',
  },
  logoBlock: {
    width: '100%',
    marginBottom: deviceRatio > 1.9 ? 112 : 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoIcon: {
    width: 124,
    height: 124,
  },
  topShadow: {
    top: 0,
    width: 186,
    height: 278,
    alignSelf: 'center',
    position: 'absolute',
  },
  formBlock: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 24,
    borderTopLeftRadius: 24,
    paddingHorizontal: 12,
    paddingBottom: 16 + bottomSpace,
  },
  title: {
    color: '#212121',
    fontSize: 28,
    fontWeight: '700',
    textAlign: 'left',
    lineHeight: 32,
    marginTop: 24,
    marginLeft: 16,
  },
  form: {
    marginTop: 8,
  },
  button: {
    marginHorizontal: 40,
    marginTop: 32,
  },
  errorButton: {
    backgroundColor: 'transparent',
    marginHorizontal: 24,
    marginTop: 32,
  },
  errorText: {
    color: '#8e8f91',
  },
  errorInput: {
    backgroundColor: '#FFEBEE',
    color: '#FA354D',
  },
});
