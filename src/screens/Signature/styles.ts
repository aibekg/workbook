import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: deviceWidth,
    flex: 1,
  },
  signature: {
    flex: 1,
  },
  reset: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    zIndex: 1000,
  },
  check: {
    width: 88,
    height: 60,
    borderRadius: 32,
    backgroundColor: '#CDFF48',
    position: 'absolute',
    left: 20,
    bottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancel: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
});
