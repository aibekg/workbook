import React, {useCallback, useEffect, useRef} from 'react';
import {StatusBar, TouchableOpacity, View} from 'react-native';
import Orientation from 'react-native-orientation';
import SignatureCapture, {
  SaveEventParams,
} from 'react-native-signature-capture';
import {RouteProp, useRoute} from '@react-navigation/native';

import CheckCircleSvg from '@icons/check-circle.svg';
import ResetSvg from '@icons/reset.svg';
import XSvg from '@icons/x-mark.svg';
import {ERootStackRoutes, TRootStackParamList} from '@routes/types';

import {styles} from './styles';

const Signature = () => {
  const {params} =
    useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.Signature>>();
  const signatureRef = useRef<SignatureCapture | null>(null);

  useEffect(() => {
    Orientation.lockToLandscape();
    return () => {
      Orientation.lockToPortrait();
    };
  }, []);

  const handleReset = useCallback(() => {
    signatureRef.current?.resetImage();
  }, []);

  const handleSave = useCallback(() => {
    signatureRef.current?.saveImage();
  }, []);

  const handleSaveEvent = useCallback(
    (event: SaveEventParams) => {
      params.onScan(event.encoded);
    },
    [params],
  );

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <SignatureCapture
        style={styles.signature}
        ref={signatureRef}
        saveImageFileInExtStorage={false}
        showNativeButtons={false}
        onSaveEvent={handleSaveEvent}
        showTitleLabel={false}
        // @ts-ignore
        backgroundColor="#000000"
        strokeColor="#ffffff"
        minStrokeWidth={4}
        maxStrokeWidth={4}
        viewMode={'portrait'}
      />
      <TouchableOpacity onPress={handleReset} style={styles.reset}>
        <ResetSvg fill="#FFFFFF" />
      </TouchableOpacity>
      <TouchableOpacity onPress={handleSave} style={styles.check}>
        <CheckCircleSvg fill="#000000" />
      </TouchableOpacity>
      <XSvg style={styles.cancel} fill="#FFFFFF" />
    </View>
  );
};

export default Signature;
