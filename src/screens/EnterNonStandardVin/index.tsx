import React, {useCallback, useState} from 'react';
import {
  Keyboard,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import Input from '@components/Input';
import {useActions} from '@hooks/useActions';
import {useVinData} from '@hooks/useVinData';
import ScanSvg from '@icons/scan-black.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import {bottomSpace} from '@utils/deviceHelpers';

import {Unit} from '../../generated/graphql';

import {styles} from './styles';

const EnterNonStandardVin = () => {
  const {setNewUnitDataFiled, clearUnitDataFiled} = useActions();

  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.EnterNonStandardVin>>();

  const route =
    useRoute<
      RouteProp<TRootStackParamList, ERootStackRoutes.EnterNonStandardVin>
    >();

  const [vinNumber, setVinNumber] = useState<string>(route?.params?.vin || '');

  const {getVinData} = useVinData();

  const handleContinue = useCallback(async () => {
    const vinDataResponse = await getVinData({
      vin: vinNumber,
      photo: route?.params?.vinPhoto || null,
    });
    clearUnitDataFiled();
    setNewUnitDataFiled({
      type: 'vin',
      value: vinNumber || null,
    });
    if (vinDataResponse?.unit) {
      setNewUnitDataFiled({
        type: 'year',
        value: vinDataResponse?.unit?.year || null,
      });
      setNewUnitDataFiled({
        type: 'manufacturer',
        value: vinDataResponse?.unit?.manufacturer || null,
      });
      setNewUnitDataFiled({
        type: 'model',
        value: vinDataResponse?.unit?.model || null,
      });
      setNewUnitDataFiled({
        type: 'bodyId',
        value: vinDataResponse?.unit?.body?.id || null,
      });
      setNewUnitDataFiled({
        type: 'colorId',
        value: vinDataResponse?.unit?.color?.id || null,
      });
      setTimeout(() => {
        if (vinDataResponse?.unit) {
          navigation.navigate({
            name: ERootStackRoutes.ReceiveUnitConfirm,
            params: {
              unit: vinDataResponse?.unit as Unit,
            },
          });
        }
      }, 600);
    } else {
      if (vinDataResponse?.vinData) {
        if (vinDataResponse?.vinData?.manufacturer) {
          setNewUnitDataFiled({
            type: 'manufacturer',
            value: vinDataResponse?.vinData?.manufacturer || null,
          });
        }
        if (vinDataResponse?.vinData?.year) {
          setNewUnitDataFiled({
            type: 'year',
            value: vinDataResponse?.vinData?.year || null,
          });
        }
      }
      setTimeout(() => {
        navigation.navigate({
          name: ERootStackRoutes.AddUnitCommodity,
          params: undefined,
        });
      }, 600);
    }
  }, [
    route?.params,
    clearUnitDataFiled,
    setNewUnitDataFiled,
    vinNumber,
    getVinData,
    navigation,
  ]);

  const handleCancel = useCallback(() => {
    navigation.replace(ERootStackRoutes.CodeScanner);
  }, [navigation]);

  const handleChange = useCallback((value: string) => {
    if (!value || (value.length <= 17 && /^[a-z0-9]+$/i.test(value))) {
      setVinNumber(value.toUpperCase());
    }
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#1E1F24"
      />
      <View style={styles.header}>
        <Text style={styles.title}>Enter Non Standard VIN</Text>
        <TouchableOpacity
          hitSlop={{top: 8, bottom: 8, left: 8, right: 8}}
          onPress={handleCancel}>
          <ScanSvg width={24} height={24} />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        activeOpacity={1}
        onPress={Keyboard.dismiss}
        style={styles.container}>
        <View style={styles.inputWrapper}>
          <View style={styles.inputContainer}>
            <Input
              autoFocus
              placeholder="Enter Number"
              placeholderTextColor="#E2E5EA"
              keyboardType={
                Platform.OS === 'ios' ? 'default' : 'visible-password'
              }
              maxLength={17}
              onChangeText={handleChange}
              value={vinNumber}
              style={styles.input}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            disabled={!vinNumber}
            text="Continue"
            buttonStyle={styles.button}
            errorStyle={styles.errorButton}
            onPress={handleContinue}
          />
        </View>
        {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
      </TouchableOpacity>
    </View>
  );
};

export default EnterNonStandardVin;
