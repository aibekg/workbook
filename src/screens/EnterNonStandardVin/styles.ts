import {StyleSheet} from 'react-native';

import {bottomSpace, statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
  },
  header: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: statusBarHeight + 16,
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 12,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#212121',
  },
  inputWrapper: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 24,
  },
  inputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  input: {
    width: '100%',
    backgroundColor: 'transparent',
    textAlign: 'center',
    color: '#212121',
    fontSize: 26,
    fontWeight: '700',
    letterSpacing: -0.5,
    textTransform: 'uppercase',
  },
  buttonContainer: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: bottomSpace + 16,
  },
  button: {
    marginHorizontal: 32,
  },
  errorButton: {
    backgroundColor: 'transparent',
    marginHorizontal: 0,
  },
  errorText: {
    color: '#8e8f91',
  },
});
