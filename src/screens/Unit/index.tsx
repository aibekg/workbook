import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  NativeScrollEvent,
  NativeSyntheticEvent,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {MenuProvider} from 'react-native-popup-menu';
import {USER_PHOTO_URL} from '@env';
import {
  RouteProp,
  useIsFocused,
  useNavigation,
  useRoute,
} from '@react-navigation/native';

import Button from '@components/Button';
import {ModalHandle} from '@components/ModalHandle';
import {useActions} from '@hooks/useActions';
import useUnitById from '@hooks/useUnitById';
import CarSvg from '@icons/car-photo.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import Documents from '@screens/Unit/components/Documents';
import UnitInfo from '@screens/Unit/components/Info';
import Inspection from '@screens/Unit/components/Inspection';
import Keys from '@screens/Unit/components/Keys';
import Overview from '@screens/Unit/components/Overview';
import {UnitStatus} from '@screens/Units/types';

import {styles} from './styles';

const Unit = () => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Unit>>();
  const {
    params: {unitId},
  } = useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.Unit>>();

  const {unit, getUnitById, isLoading} = useUnitById();

  const isFocused = useIsFocused();
  useEffect(() => {
    if (isFocused && unitId) {
      getUnitById({id: unitId});
    }
  }, [isFocused, unitId, getUnitById]);

  const [currentTabIndex, setCurrentTabIndex] = useState<number>(0);

  const handleTabPress = useCallback(
    (tabIndex: number) => () => {
      setCurrentTabIndex(tabIndex);
    },
    [],
  );

  const {setInspectionUnit} = useActions();

  const routes = useMemo(
    () => [
      {key: 'overview', title: 'Overview'},
      {key: 'inspection', title: 'Inspection'},
      {key: 'documents', title: 'Documents'},
      {key: 'keySet', title: 'Key Set'},
    ],
    [],
  );

  const tabScene = useMemo(() => {
    switch (currentTabIndex) {
      case 0:
        return <Overview unit={unit} />;
      case 1:
        return <Inspection />;
      case 2:
        return <Documents unit={unit} />;
      case 3:
        return <Keys data={unit?.keys} localData={false} />;
      default:
        return <Overview unit={unit} />;
    }
  }, [currentTabIndex, unit]);

  const handlePress = useCallback(() => {
    switch (unit?.status) {
      case UnitStatus.LOT_MISSING:
        navigation.goBack();
        setTimeout(() => {
          navigation.navigate(ERootStackRoutes.LocationArea, {
            unit,
          });
        }, 600);
        break;
      case UnitStatus.INSPECTION_PENDING:
        if (unit?.vinNumber) {
          setInspectionUnit({
            value: unit,
          });
          setTimeout(() => {
            navigation.goBack();
            setTimeout(() => {
              navigation.navigate(ERootStackRoutes.CarrierCompany);
            }, 600);
          }, 300);
        } else {
          navigation.goBack();
          setTimeout(() => {
            navigation.navigate(ERootStackRoutes.CodeScanner);
          }, 600);
        }
        break;
      default:
        if (unit?.id && !unit?.wareHouseAreaLot?.id) {
          navigation.goBack();
          setTimeout(() => {
            navigation.navigate(ERootStackRoutes.LocationArea, {
              unit,
            });
          }, 600);
        }
        break;
    }
  }, [setInspectionUnit, navigation, unit]);

  const footerButtonText = useMemo(() => {
    switch (unit?.status) {
      case UnitStatus.LOT_MISSING:
        return 'Assign Lot Number';
      case UnitStatus.INSPECTION_PENDING:
        if (unit?.vinNumber) {
          return 'Start Inspection';
        } else {
          return 'Scan VIN';
        }
      default:
        if (!unit?.wareHouseAreaLot?.id) {
          return 'Assign Lot Number';
        }
        return '';
    }
  }, [unit]);

  const [showCarImage, setShowCarImage] = useState<boolean>(false);
  const onScroll = useCallback(
    (scrollEvent: NativeSyntheticEvent<NativeScrollEvent>) => {
      if (scrollEvent.nativeEvent.contentOffset.y >= 280) {
        if (!showCarImage) {
          setShowCarImage(true);
        }
      } else {
        if (showCarImage) {
          setShowCarImage(false);
        }
      }
    },
    [showCarImage],
  );

  return (
    <MenuProvider
      style={styles.screen}
      skipInstanceCheck
      customStyles={{backdrop: styles.backdrop}}>
      <View style={styles.wrapper}>
        <ModalHandle showCloseButton={true} />
        {isLoading && !unit ? (
          <View style={styles.loading}>
            <ActivityIndicator size="small" />
          </View>
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={isLoading && !!unit}
                onRefresh={() => getUnitById({id: unitId})}
                tintColor="#2A2A2A"
              />
            }
            nestedScrollEnabled={true}
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[1]}
            scrollEventThrottle={16}
            onScroll={onScroll}
            style={styles.container}
            contentContainerStyle={styles.scrollContent}>
            {unit?.photo?.length ? (
              <Image
                style={styles.image}
                source={{uri: USER_PHOTO_URL + unit.photo[0] || ''}}
              />
            ) : (
              <View style={styles.image}>
                <CarSvg />
              </View>
            )}
            <UnitInfo unit={unit} showCarImage={showCarImage} />
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.tabsContent}
              style={styles.tabs}>
              {routes.map((route, routeIndex) => (
                <TouchableOpacity
                  onPress={handleTabPress(routeIndex)}
                  key={route.key}
                  style={[
                    styles.tab,
                    currentTabIndex === routeIndex && styles.activeTab,
                  ]}>
                  <Text
                    style={[
                      styles.label,
                      currentTabIndex === routeIndex && styles.activeLabel,
                    ]}>
                    {route.title}
                  </Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
            <TouchableOpacity activeOpacity={1} style={styles.scene}>
              {tabScene}
            </TouchableOpacity>
          </ScrollView>
        )}
        {!!unit && footerButtonText ? (
          <View style={styles.footer}>
            <Button text={footerButtonText} onPress={handlePress} />
          </View>
        ) : null}
      </View>
    </MenuProvider>
  );
};

export default Unit;
