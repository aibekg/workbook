import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: '100%',
  },
  wrapper: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabs: {
    width: '100%',
    backgroundColor: 'white',
  },
  tabsContent: {
    flexDirection: 'row',
    paddingHorizontal: 8,
  },
  scene: {
    width: '100%',
    paddingHorizontal: 8,
    flexDirection: 'column',
    backgroundColor: '#F1F1F1',
  },
  image: {
    height: 280,
    backgroundColor: '#CDFF48',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#F1F1F1',
  },
  scrollContent: {
    width: '100%',
    backgroundColor: '#F1F1F1',
    paddingBottom: bottomSpace + 80,
  },
  footer: {
    position: 'absolute',
    bottom: bottomSpace + 16,
    paddingHorizontal: 48,
    width: '100%',
  },
  loaderWrapper: {
    position: 'absolute',
    top: -50,
    left: 0,
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    zIndex: 10,
  },
  loading: {
    width: 96,
    height: 96,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 16,
    position: 'absolute',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12,
    elevation: 8,
  },
  backdrop: {
    backgroundColor: '#00000080',
    opacity: 1,
  },
  tab: {
    flex: 1,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 2,
    marginHorizontal: 8,
    borderColor: 'transparent',
  },
  activeTab: {
    borderBottomWidth: 2,
    borderColor: '#CDFF48',
  },
  label: {
    color: '#000000',
    fontSize: 14,
    fontWeight: '400',
  },
  activeLabel: {
    fontWeight: '500',
  },
});
