import {Platform, StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  info: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 16,
    paddingTop: Platform.OS === 'ios' ? 24 : 16,
    paddingBottom: 16,
    flexDirection: 'row',
  },
  title: {
    color: '#000000',
    fontSize: 28,
    fontWeight: '400',
  },
  color: {
    width: 48,
    height: 48,
    borderRadius: 24,
    backgroundColor: '#d2d2d2',
    marginRight: -24,
  },
  vinNumber: {
    fontSize: 18,
    fontWeight: '700',
    color: '#000000',
    marginTop: 4,
  },
  type: {
    backgroundColor: '#000000',
    width: 48,
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carImage: {
    backgroundColor: '#000000',
    width: 48,
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 48,
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CDFF48',
  },
  leftContent: {
    flexDirection: 'column',
    flex: 1,
  },
  rightContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    minWidth: 96,
  },
  rightContentWithCarImage: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    minWidth: 96,
    marginLeft: 8,
    marginRight: Platform.OS === 'ios' ? 0 : 56,
  },
});
