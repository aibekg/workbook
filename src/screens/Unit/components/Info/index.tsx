import React, {FC, useMemo} from 'react';
import {Image, Text, View} from 'react-native';
import {SvgProps} from 'react-native-svg';
import {USER_PHOTO_URL} from '@env';

import CarSvg from '@icons/car-photo.svg';
import Convertible from '@icons/convertible.svg';
import Coupe from '@icons/coupe.svg';
import Hatchback from '@icons/hatchback.svg';
import SedanSvg from '@icons/sedan.svg';
import Sedan from '@icons/sedan.svg';
import SUV from '@icons/suv.svg';
import Truck from '@icons/truck_body.svg';
import Van from '@icons/van.svg';
import {getColorString} from '@utils/stringHelpers';

import {Unit} from '../../../../generated/graphql';

import {styles} from './styles';

export type TUnitInfoProps = {
  unit: Unit | null;
  showCarImage?: boolean;
};

const UnitInfo: FC<TUnitInfoProps> = ({showCarImage, unit}) => {
  const bodyIcons: React.FC<SvgProps>[] = useMemo(
    () => [Sedan, Hatchback, SUV, Truck, Convertible, Van, Coupe],
    [],
  );

  const BodySVG = useMemo(
    () =>
      unit?.body?.id && bodyIcons[unit?.body?.id - 1]
        ? bodyIcons[(unit?.body?.id - 1) % bodyIcons.length]
        : SedanSvg,
    [unit?.body?.id, bodyIcons],
  );

  return (
    <View style={styles.info}>
      <View style={styles.leftContent}>
        <Text style={styles.title}>{`${unit?.year || ''} ${
          unit?.manufacturer || ''
        } ${unit?.model || ''}`}</Text>
        <Text selectable={true} style={styles.vinNumber}>
          {unit?.vinNumber}
        </Text>
      </View>
      <View
        style={
          showCarImage ? styles.rightContentWithCarImage : styles.rightContent
        }>
        <View
          style={[
            styles.color,
            {
              backgroundColor: getColorString(
                unit?.color?.value?.toLowerCase() || 'grey',
              ),
            },
            unit?.color?.value === 'white' && {
              borderColor: '#212121',
              borderWidth: 1,
            },
          ]}
        />
        <View style={[styles.type, showCarImage && {marginRight: -24}]}>
          <BodySVG stroke="#CDFF48" strokeWidth={2} width={40} />
        </View>
        {showCarImage ? (
          <View style={styles.carImage}>
            {unit?.photo?.length ? (
              <Image
                style={styles.image}
                source={{uri: USER_PHOTO_URL + unit.photo[0] || ''}}
              />
            ) : (
              <View style={styles.image}>
                <CarSvg strokeWidth={4} width={40} height={40} />
              </View>
            )}
          </View>
        ) : null}
      </View>
    </View>
  );
};

export default UnitInfo;
