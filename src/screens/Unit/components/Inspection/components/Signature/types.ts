import {TBaseCard} from '@screens/Unit/components/BaseCard';

export type TSignatureProps = Omit<TBaseCard, 'content' | 'icon' | 'text'> & {
  data: TSignatureData;
};

export type TSignatureData = {
  photo: unknown;
  name: string;
  date: string;
};
