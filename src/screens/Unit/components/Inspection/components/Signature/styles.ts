import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
  },
  photo: {
    height: 200,
    marginTop: 8,
    width: '100%',
  },
  data: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 8,
  },
  circle: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CDFF48',
    borderRadius: 24,
    marginRight: 12,
  },
  circleText: {
    fontSize: 8,
    fontWeight: '700',
  },
  name: {
    fontSize: 14,
    fontWeight: '700',
    color: '#1E1F24',
  },
  label: {
    fontWeight: '400',
  },
  date: {
    marginTop: 2,
    fontSize: 10,
    color: '#646C7A',
  },
  noSignature: {
    marginTop: 8,
    height: 72,
    borderRadius: 12,
    backgroundColor: '#FA354D',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
