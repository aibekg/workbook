import React, {FC, useMemo} from 'react';
import {Image, Text, View} from 'react-native';

import SubtractSvg from '@icons/subtract.svg';
import BaseCard from '@screens/Unit/components/BaseCard';

import {styles} from './styles';
import {TSignatureProps} from './types';

const Signature: FC<TSignatureProps> = ({title, data}) => {
  const RenderContent = useMemo(() => {
    return (
      <View style={styles.container}>
        {data.photo ? (
          <Image style={styles.photo} source={data.photo} />
        ) : (
          <View style={styles.noSignature}>
            <SubtractSvg />
          </View>
        )}
        <View style={styles.data}>
          <View style={styles.circle}>
            <Text style={styles.circleText}>
              {data.name.replace(/[a-z]|\s/g, '')}
            </Text>
          </View>
          <View>
            <Text style={styles.name}>
              {data.name}
              <Text style={styles.label}> Signed Inspection</Text>
            </Text>
            <Text style={styles.date}>{data.date}</Text>
          </View>
        </View>
      </View>
    );
  }, [data]);

  return <BaseCard title={title} content={RenderContent} />;
};

export default Signature;
