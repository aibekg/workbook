import React, {FC, useMemo} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';

import BaseCard from '@screens/Unit/components/BaseCard';

import {styles} from './styles';
import {TDamageReportViewProps} from './types';

const DamageReportView: FC<TDamageReportViewProps> = ({
  title,
  damages,
  photos,
  svg,
}) => {
  const RenderContent = useMemo(() => {
    return (
      <View style={styles.container}>
        {photos ? (
          <ScrollView
            nestedScrollEnabled={true}
            removeClippedSubviews={true}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            style={styles.scroll}
            contentContainerStyle={styles.scrollContent}>
            {photos.map((photoItem, photoIndex) => (
              <TouchableOpacity
                activeOpacity={1}
                style={[
                  styles.carImageContainer,
                  photoIndex !== 0 && styles.carImageMargin,
                ]}>
                <Image
                  source={{uri: USER_PHOTO_URL + photoItem}}
                  resizeMode="cover"
                  style={styles.carImage}
                />
              </TouchableOpacity>
            ))}
          </ScrollView>
        ) : null}
        {svg ? <View style={styles.carContainer}>{svg}</View> : null}
        <View style={styles.damages}>
          {damages.map((damage, index) => (
            <View
              key={index}
              style={[
                styles.damage,
                index === damages.length - 1 && styles.lastDamage,
              ]}>
              <View style={styles.circle}>
                <Text style={styles.circleText}>
                  {damage.type[0].toUpperCase()}
                </Text>
              </View>
              <Text style={styles.title}>{damage.title}</Text>
              <Text style={styles.type}>{damage.detail || ''}</Text>
            </View>
          ))}
        </View>
      </View>
    );
  }, [photos, damages, svg]);

  return <BaseCard title={title} content={RenderContent} />;
};

export default DamageReportView;
