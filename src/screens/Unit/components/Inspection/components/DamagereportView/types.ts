import {TBaseCard} from '@screens/Unit/components/BaseCard';

export type TDamageReportViewProps = Omit<
  TBaseCard,
  'content' | 'icon' | 'text'
> & {
  photos?: string[];
  damages: TDamage[];
  svg?: JSX.Element;
};

export type TDamage = {
  title: string;
  type: string;
  detail?: string | null;
};
