import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
  },
  photo: {
    marginTop: 12,
    height: 502,
    resizeMode: 'contain',
  },
  damages: {
    marginTop: 16,
  },
  damage: {
    flexDirection: 'row',
    paddingVertical: 12,
    borderBottomWidth: 1,
    alignItems: 'center',
    borderColor: '#0000000F',
  },
  lastDamage: {
    borderBottomWidth: 0,
  },
  circle: {
    width: 32,
    height: 32,
    borderRadius: 32,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  circleText: {
    fontWeight: '700',
    color: '#FFFFFF',
    fontSize: 18,
  },
  title: {
    marginLeft: 12,
    fontSize: 18,
    color: '#212121',
  },
  type: {
    marginLeft: 'auto',
    fontSize: 12,
    fontWeight: '700',
    color: '#212121',
    opacity: 0.7,
  },
  carContainer: {
    height: 540,
    marginTop: 8,
    borderRadius: 12,
    backgroundColor: '#000000',
  },
  carImageContainer: {
    width: 124,
    height: 124,
    borderRadius: 12,
    overflow: 'hidden',
  },
  carImageMargin: {
    width: 132,
    paddingLeft: 8,
  },
  carImage: {
    width: 124,
    height: 124,
    backgroundColor: '#000000',
    borderRadius: 12,
  },
  scroll: {
    width: '100%',
  },
  scrollContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
