import React, {useMemo} from 'react';
import {View} from 'react-native';

import {EExteriorSide} from '@screens/Inspection/ExteriorSide/types';
import {EInteriorSide} from '@screens/Inspection/InteriorSide/types';
import {TDamage} from '@screens/Unit/components/Inspection/components/DamagereportView/types';
import Back from '@screens/Unit/components/Inspection/svg-components/Back';
import Body from '@screens/Unit/components/Inspection/svg-components/Body';
import Front from '@screens/Unit/components/Inspection/svg-components/Front';
import Left from '@screens/Unit/components/Inspection/svg-components/Left';
import LeftBackDoor from '@screens/Unit/components/Inspection/svg-components/LeftBackDoor';
import LeftFrontDoor from '@screens/Unit/components/Inspection/svg-components/LeftFrontDoor';
import Right from '@screens/Unit/components/Inspection/svg-components/Right';
import RightBackDoor from '@screens/Unit/components/Inspection/svg-components/RightBackDoor';
import RightFrontDoor from '@screens/Unit/components/Inspection/svg-components/RightFrontDoor';
import Top from '@screens/Unit/components/Inspection/svg-components/Top';
import {capitalizeWord, enumToHumanReadable} from '@utils/stringHelpers';

import {Unit} from '../../../../generated/graphql';

import DamageReportView from './components/DamagereportView';
import {styles} from './styles';

type Props = {
  unit?: Unit | null;
};

const Inspection = ({unit}: Props) => {
  const ExteriorSvg = useMemo(() => {
    return (
      <>
        <Front
          damageCount={
            (
              (unit?.exteriors || []).find(
                item => item?.side === EExteriorSide.FRONT,
              )?.damages || []
            ).length
          }
        />
        <Left
          damageCount={
            (
              (unit?.exteriors || []).find(
                item => item?.side === EExteriorSide.LEFT,
              )?.damages || []
            ).length
          }
        />
        <Right
          damageCount={
            (
              (unit?.exteriors || []).find(
                item => item?.side === EExteriorSide.RIGHT,
              )?.damages || []
            ).length
          }
        />
        <Top
          damageCount={
            (
              (unit?.exteriors || []).find(
                item => item?.side === EExteriorSide.TOP,
              )?.damages || []
            ).length
          }
        />
        <Back
          damageCount={
            (
              (unit?.exteriors || []).find(
                item => item?.side === EExteriorSide.BACK,
              )?.damages || []
            ).length
          }
        />
      </>
    );
  }, [unit?.exteriors]);

  const InteriorSvg = useMemo(
    () => (
      <>
        <LeftFrontDoor
          damageCount={
            (
              (unit?.interiors || []).find(
                item => item?.side === EInteriorSide.LEFT_FRONT,
              )?.damages || []
            ).length
          }
        />
        <LeftBackDoor
          damageCount={
            (
              (unit?.interiors || []).find(
                item => item?.side === EInteriorSide.LEFT_BACK,
              )?.damages || []
            ).length
          }
        />
        <Body
          damageCount={
            (
              (unit?.interiors || []).find(
                item => item?.side === EInteriorSide.BODY,
              )?.damages || []
            ).length
          }
        />
        <RightFrontDoor
          damageCount={
            (
              (unit?.interiors || []).find(
                item => item?.side === EInteriorSide.RIGHT_BACK,
              )?.damages || []
            ).length
          }
        />
        <RightBackDoor
          damageCount={
            (
              (unit?.interiors || []).find(
                item => item?.side === EInteriorSide.RIGHT_BACK,
              )?.damages || []
            ).length
          }
        />
      </>
    ),
    [unit?.interiors],
  );

  const exteriorDamages = useMemo(() => {
    const damages: TDamage[] = [];
    (unit?.exteriors || []).forEach(exteriorItem => {
      (exteriorItem?.damages || []).forEach(damageItem => {
        if (damageItem) {
          damages.push({
            title: capitalizeWord(
              enumToHumanReadable(exteriorItem?.side || ''),
            ),
            detail: damageItem?.detail || '',
            type: damageItem?.damageType || '',
          });
        }
      });
    });
    return damages;
  }, [unit?.exteriors]);

  const interiorDamages = useMemo(() => {
    const damages: TDamage[] = [];
    (unit?.interiors || []).forEach(interiorItem => {
      (interiorItem?.damages || []).forEach(damageItem => {
        if (damageItem) {
          damages.push({
            title: capitalizeWord(
              enumToHumanReadable(interiorItem?.side || ''),
            ),
            detail: damageItem?.detail || '',
            type: damageItem?.damageType || '',
          });
        }
      });
    });
    return damages;
  }, [unit?.interiors]);

  const equipDamages = useMemo(() => {
    const damages: TDamage[] = [];
    (unit?.equipments || []).forEach(interiorItem => {
      (interiorItem?.damages || []).forEach(damageItem => {
        if (damageItem) {
          damages.push({
            title: damageItem?.detail || '',
            type: damageItem?.damageType || '',
          });
        }
      });
    });
    return damages;
  }, [unit?.equipments]);

  const exteriorPhotos = useMemo(() => {
    return (unit?.exteriors || []).map(item => item?.photo!);
  }, [unit?.exteriors]);

  const interiorPhotos = useMemo(() => {
    return (unit?.interiors || []).map(item => item?.photo!);
  }, [unit?.interiors]);

  const equipPhotos = useMemo(() => {
    return (unit?.equipments || []).map(item => item?.photo!);
  }, [unit?.equipments]);

  return (
    <View style={styles.container}>
      <DamageReportView
        svg={ExteriorSvg}
        photos={exteriorPhotos}
        damages={exteriorDamages}
        title="Exterior"
      />
      <DamageReportView
        svg={InteriorSvg}
        photos={interiorPhotos}
        damages={interiorDamages}
        title="Interior"
      />
      {equipDamages.length > 0 || equipPhotos.length > 0 ? (
        <DamageReportView
          photos={equipPhotos}
          damages={equipDamages}
          title="Equipments"
        />
      ) : null}
      {/* <Signature data={noSignatureData} title="Driver Signature" /> */}
      {/* <Signature data={noSignatureData} title="Driver Signature" /> */}
    </View>
  );
};

export default Inspection;
