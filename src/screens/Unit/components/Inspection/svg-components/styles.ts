import {StyleSheet} from 'react-native';

import {deviceWidth} from '@utils/deviceHelpers';

const containerWidth = deviceWidth - 48;
const containerHeight = 540;

export const styles = StyleSheet.create({
  front: {
    position: 'absolute',
    left: containerWidth / 2 - 70,
    top: 20,
  },
  back: {
    position: 'absolute',
    left: containerWidth / 2 - 76,
    bottom: 20,
  },
  left: {
    position: 'absolute',
    left: 20,
    top: containerHeight / 2 - 128,
  },
  right: {
    position: 'absolute',
    right: 20,
    top: containerHeight / 2 - 128,
  },
  top: {
    position: 'absolute',
    top: containerHeight / 2 - 20,
    left: containerWidth / 2 - 44,
  },
  body: {
    position: 'absolute',
    top: containerHeight / 2 - 211,
    left: containerWidth / 2 - 87,
  },
  leftFrontDoor: {
    position: 'absolute',
    left: 12,
    top: containerHeight / 2 - 44,
  },
  leftBackDoor: {
    position: 'absolute',
    left: 12,
    top: containerHeight / 2 + 44,
  },
  rightBackDoor: {
    position: 'absolute',
    right: 12,
    top: containerHeight / 2 + 44,
  },
  rightFrontDoor: {
    position: 'absolute',
    right: 12,
    top: containerHeight / 2 - 44,
  },
});
