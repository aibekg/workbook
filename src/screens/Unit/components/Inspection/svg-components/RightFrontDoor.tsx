import React from 'react';
import {Path, Svg} from 'react-native-svg';

import Mark from '@screens/Inspection/Exterior/components/Mark';

import {styles} from './styles';

type Props = {
  damageCount?: number | null;
};

const paths =
  '["M33.6738 0.700195C59.7469 0.700195 64.9542 74.1649 64.9542 74.1649H33.6738C33.6738 74.1649 33.6738 22.3139 33.6738 0.700195Z", "M33.6739 0.700439H1V65.982C1 70.4956 4.66712 74.1651 9.17766 74.1651H33.6372V0.700439H33.6739Z", "M25.0196 6.09558C27.0732 4.04063 28.1734 7.89367 28.1734 7.89367C28.1734 7.89367 20.0691 18.0584 20.0691 22.4618C20.0691 26.8653 20.0691 41.617 20.0691 41.617C20.0691 41.617 15.9619 42.7545 15.9619 38.7914C15.9619 34.8283 15.9619 26.7919 15.9619 22.4618C15.9986 15.4163 22.9661 8.15054 25.0196 6.09558Z", "M28.1729 19.4898V14.6826"]';

const LeftBackDoor = ({damageCount}: Props) => {
  return (
    <Svg style={styles.rightFrontDoor} width={68} height={76}>
      {JSON.parse(paths).map((path: string) => (
        <Path
          key={path}
          d={path}
          stroke={damageCount ? '#FFFFFF' : 'rgba(255,255,255,0.4)'}
          strokeMiterlimit="10"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      ))}
      {damageCount ? (
        <Mark
          type={`${damageCount}`}
          x={33}
          y={38}
          zoom={1}
          r={18}
          fillColor="#FF453A"
          textColor="#FFFFFF"
          showAllText={true}
        />
      ) : null}
    </Svg>
  );
};

export default LeftBackDoor;
