import React from 'react';
import {Path, Svg} from 'react-native-svg';

import Mark from '@screens/Inspection/Exterior/components/Mark';

import {styles} from './styles';

type Props = {
  damageCount?: number | null;
};

const paths =
  '["M32.2804 0.700439C6.20729 0.700439 1 74.1651 1 74.1651H32.2804C32.2804 74.1651 32.2804 22.3142 32.2804 0.700439Z", "M32.2803 0.700195H64.9542V65.9818C64.9542 70.4953 61.2871 74.1649 56.7765 74.1649H32.2803V0.700195Z", "M40.935 6.09558C38.8814 4.04063 37.7812 7.89367 37.7812 7.89367C37.7812 7.89367 45.8856 18.0584 45.8856 22.4618C45.8856 26.8653 45.8856 41.617 45.8856 41.617C45.8856 41.617 49.9927 42.7545 49.9927 38.7914C49.9927 34.8283 49.9927 26.7919 49.9927 22.4618C49.956 15.4163 42.9885 8.15054 40.935 6.09558Z", "M37.781 19.4898V14.6826"]';

const LeftFrontDoor = ({damageCount}: Props) => {
  return (
    <Svg style={styles.leftFrontDoor} width={66} height={76}>
      {JSON.parse(paths).map((path: string, index: number) => (
        <Path
          key={path}
          d={path}
          stroke={damageCount ? '#FFFFFF' : 'rgba(255,255,255,0.4)'}
          strokeMiterlimit="10"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      ))}
      {damageCount ? (
        <Mark
          type={`${damageCount}`}
          x={33}
          y={38}
          zoom={1}
          r={18}
          fillColor="#FF453A"
          textColor="#FFFFFF"
          showAllText={true}
        />
      ) : null}
    </Svg>
  );
};

export default LeftFrontDoor;
