import React from 'react';
import {Path, Svg} from 'react-native-svg';

import Mark from '@screens/Inspection/Exterior/components/Mark';

import {styles} from './styles';

type Props = {
  damageCount?: number | null;
};

const paths =
  '["M33.6738 74.7303C59.7469 74.7303 64.9542 1.26562 64.9542 1.26562H33.6738C33.6738 1.26562 33.6738 53.1166 33.6738 74.7303Z", "M33.6739 1.26562H1V57.6301C1 67.0609 8.66426 74.7303 18.0887 74.7303H33.6739V1.26562Z", "M25.0194 6.65979C27.073 4.60484 28.1731 8.45787 28.1731 8.45787C28.1731 8.45787 20.0688 18.6226 20.0688 23.026C20.0688 27.4295 20.0688 42.1812 20.0688 42.1812C20.0688 42.1812 15.9617 43.3187 15.9617 39.3556C15.9617 35.3925 15.9617 27.3561 15.9617 23.026C15.9983 15.9805 22.9658 8.71475 25.0194 6.65979Z", "M28.1729 20.0542V15.2471"]';

const RightBackDoor = ({damageCount}: Props) => {
  return (
    <Svg style={styles.rightBackDoor} width={66} height={76}>
      {JSON.parse(paths).map((path: string, index: number) => (
        <Path
          key={path}
          d={path}
          stroke={damageCount ? '#FFFFFF' : 'rgba(255,255,255,0.4)'}
          strokeMiterlimit="10"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      ))}
      {damageCount ? (
        <Mark
          type={`${damageCount}`}
          x={33}
          y={38}
          zoom={1}
          r={18}
          fillColor="#FF453A"
          textColor="#FFFFFF"
          showAllText={true}
        />
      ) : null}
    </Svg>
  );
};

export default RightBackDoor;
