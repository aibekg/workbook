import React from 'react';
import {Path, Svg} from 'react-native-svg';

import Mark from '@screens/Inspection/Exterior/components/Mark';

import {styles} from './styles';

type Props = {
  damageCount?: number | null;
};

const paths =
  '["M32.2804 74.7303C6.20729 74.7303 1 1.26562 1 1.26562H32.2804C32.2804 1.26562 32.2804 53.1166 32.2804 74.7303Z", "M32.2803 1.26562H64.9542V57.6301C64.9542 67.0609 57.2899 74.7303 47.8655 74.7303H32.2803V1.26562Z", "M40.9347 6.65979C38.8811 4.60484 37.781 8.45787 37.781 8.45787C37.781 8.45787 45.8853 18.6226 45.8853 23.026C45.8853 27.4295 45.8853 42.1812 45.8853 42.1812C45.8853 42.1812 49.9925 43.3187 49.9925 39.3556C49.9925 35.3925 49.9925 27.3561 49.9925 23.026C49.9558 15.9805 42.9883 8.71475 40.9347 6.65979Z", "M37.781 20.0545V15.2473"]';

const LeftBackDoor = ({damageCount}: Props) => {
  return (
    <Svg style={styles.leftBackDoor} width={66} height={76}>
      {JSON.parse(paths).map((path: string, index: number) => (
        <Path
          key={path}
          d={path}
          stroke={damageCount ? '#FFFFFF' : 'rgba(255,255,255,0.4)'}
          strokeMiterlimit="10"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      ))}
      {damageCount ? (
        <Mark
          type={`${damageCount}`}
          x={33}
          y={38}
          zoom={1}
          r={18}
          fillColor="#FF453A"
          textColor="#FFFFFF"
          showAllText={true}
        />
      ) : null}
    </Svg>
  );
};

export default LeftBackDoor;
