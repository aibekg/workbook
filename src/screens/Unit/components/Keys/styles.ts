import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {},
  row: {
    flexDirection: 'row',
  },
  key: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  count: {
    fontSize: 48,
    fontWeight: '400',
    color: '#1E1F24',
    marginRight: 12,
  },
  text: {
    color: '#606060',
    marginTop: 4,
    fontSize: 14,
  },
  photo: {
    borderRadius: 12,
    overflow: 'hidden',
    marginTop: 8,
    height: 180,
    resizeMode: 'cover',
  },
  noDataYet: {
    paddingVertical: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    textAlign: 'center',
    color: '#646C7A',
    opacity: 0.5,
  },
});
