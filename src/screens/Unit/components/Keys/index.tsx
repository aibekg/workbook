import React, {FC, useMemo} from 'react';
import {Image, Text, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';

import KeySvg from '@icons/key.svg';
import RemoteKeySvg from '@icons/remote-key.svg';
import BaseCard from '@screens/Unit/components/BaseCard';

import {Keys as TKeys} from '../../../../generated/graphql';

import {styles} from './styles';

export type TKeysProps = {
  data?: Partial<TKeys> | null;
  localData?: boolean;
};

const Keys: FC<TKeysProps> = ({data, localData = false}) => {
  const RenderContent = useMemo(() => {
    return data ? (
      <View style={styles.container}>
        <View style={styles.row}>
          <View style={styles.key}>
            <Text style={styles.count}>{data.manualKeyCount}</Text>
            <View>
              <KeySvg fill="#606060" />
              <Text style={styles.text}>Manual Keys</Text>
            </View>
          </View>
          <View style={styles.key}>
            <Text style={styles.count}>{data.remoteKeyCount}</Text>
            <View>
              <RemoteKeySvg fill="#606060" />
              <Text style={styles.text}>Remote Key</Text>
            </View>
          </View>
        </View>
        <Image
          style={styles.photo}
          source={{
            uri: !localData ? USER_PHOTO_URL + data.photo : data.photo!,
          }}
        />
      </View>
    ) : null;
  }, [data, localData]);

  return data ? (
    <BaseCard title="Keys" content={RenderContent} />
  ) : (
    <View style={styles.noDataYet}>
      <Text style={styles.noDataText}>
        {'We don’t have any data \n on this item yet'}
      </Text>
    </View>
  );
};

export default Keys;
