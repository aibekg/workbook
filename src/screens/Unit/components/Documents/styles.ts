import {Platform, StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
  },
  documentImage: {
    borderRadius: 12,
    overflow: 'hidden',
    height: 312,
    resizeMode: 'cover',
  },
  documentImageTouch: {
    marginTop: 8,
  },
  popup: {
    marginTop: Platform.OS === 'ios' ? -60 : 0,
  },
  noDataYet: {
    paddingVertical: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    textAlign: 'center',
    color: '#646C7A',
    opacity: 0.5,
  },
  imageModal: {
    backgroundColor: '#000000',
  },
  icon: {
    top: 12,
  },
});
