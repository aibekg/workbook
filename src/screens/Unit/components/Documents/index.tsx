import React, {useCallback, useState} from 'react';
import {
  Image,
  Linking,
  Share,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {USER_PHOTO_URL} from '@env';
// @ts-ignore
import {ImageZoom} from '@likashefqet/react-native-image-zoom';
import {Document, Unit} from 'src/generated/graphql';

import Menu from '@components/Menu';
import Modal from '@components/Modal';
import CircleMenuSvg from '@icons/circle-menu.svg';
import BaseCard from '@screens/Unit/components/BaseCard';

import {styles} from './styles';

const options = [
  {
    key: 'open',
    text: 'Open Document',
  },
  {
    key: 'download',
    text: 'Download',
  },
  {
    key: 'share',
    text: 'Share',
  },
];

type Props = {
  unit: Unit | null;
};

const Documents = ({unit}: Props) => {
  const [documentPhoto, setDocumentPhoto] = useState<string>('');
  const [documentModalVisible, setDocumentModalVisible] =
    useState<boolean>(false);
  const closeDocumentModal = useCallback(() => {
    setDocumentModalVisible(false);
    setTimeout(() => setDocumentPhoto(''), 600);
  }, []);

  const handleSelect = useCallback(
    (document?: Document | null) => (value: string) => {
      switch (value) {
        case 'open':
          if (document?.photo) {
            setDocumentPhoto(document?.photo);
            setDocumentModalVisible(true);
          }
          break;
        case 'download':
          if (document?.photo) {
            Linking.openURL(USER_PHOTO_URL + document?.photo)
              .then(undefined)
              .catch(undefined);
          }
          break;
        case 'share':
          if (document?.photo) {
            Share.share({
              title: `Document: ${document?.type || ''}`,
              url: USER_PHOTO_URL + document?.photo,
            })
              .then(undefined)
              .catch(undefined);
          }
          break;
        default:
          break;
      }
    },
    [],
  );

  return unit?.documents?.length ? (
    <View style={styles.container}>
      {unit.documents.map(document => (
        <BaseCard
          key={document?.id}
          title={document?.name?.split(/(?=[A-Z])/).join(' ') || ''}
          iconStyle={styles.icon}
          icon={
            <Menu
              style={styles.popup}
              title="Actions With Title"
              onSelect={handleSelect(document)}
              trigger={<CircleMenuSvg stroke="#0F172A" />}
              options={options}
            />
          }
          content={
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => handleSelect(document)('open')}
              style={styles.documentImageTouch}>
              <Image
                style={styles.documentImage}
                source={{
                  uri: USER_PHOTO_URL + document?.photo,
                }}
              />
            </TouchableOpacity>
          }
        />
      ))}
      <Modal
        isVisible={documentModalVisible}
        close={closeDocumentModal}
        contentStyle={styles.imageModal}>
        <ImageZoom uri={USER_PHOTO_URL + documentPhoto} />
      </Modal>
    </View>
  ) : (
    <View style={styles.noDataYet}>
      <Text style={styles.noDataText}>
        {'We don’t have any data \n on this item yet'}
      </Text>
    </View>
  );
};

export default Documents;
