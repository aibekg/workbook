import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
  },
  content: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    position: 'relative',
  },
  noDataYet: {
    paddingVertical: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    textAlign: 'center',
    color: '#646C7A',
    opacity: 0.5,
  },
  block: {
    flexDirection: 'row',
  },
  blockItem: {
    flex: 1,
  },
  blockRightItem: {
    marginRight: 8,
    flex: 1,
  },
  blockLeftItem: {
    marginLeft: 8,
    flex: 1,
  },
  gap: {flex: 0.1},
  photo: {
    width: 36,
    height: 36,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CDFF48',
  },
  capital: {
    fontSize: 12,
    fontWeight: '600',
    color: '#000000',
    textAlign: 'center',
  },
});
