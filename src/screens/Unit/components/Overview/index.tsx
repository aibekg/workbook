import React, {useMemo} from 'react';
import {Image, Text, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';

import LocationSvg from '@icons/location.svg';
import NonOperableSvg from '@icons/non-operable.svg';
import NonRolableSvg from '@icons/non-rolable.svg';
import SteeringWheelSvg from '@icons/steering-wheel.svg';
import UserSvg from '@icons/user.svg';
import BaseCard from '@screens/Unit/components/BaseCard';

import {Unit} from '../../../../generated/graphql';

import Message from './components/Message';
import {styles} from './styles';

type Props = {
  unit: Unit | null;
};

const getConditionIcon = (conditionId: number) => {
  switch (conditionId) {
    case 1:
      return <SteeringWheelSvg width={24} height={24} stroke="#1E1F24" />;
    case 2:
      return <NonOperableSvg width={24} height={24} stroke="#1E1F24" />;
    case 3:
      return <NonRolableSvg width={24} height={24} stroke="#1E1F24" />;
    default:
      return <SteeringWheelSvg width={24} height={24} stroke="#1E1F24" />;
  }
};

const Overview = ({unit}: Props) => {
  const isEmpty = useMemo(() => {
    return (
      !unit?.condition &&
      !unit?.wareHouseAreaLot?.id &&
      !unit?.carrierDriver &&
      !unit?.customer
    );
  }, [
    unit?.carrierDriver,
    unit?.condition,
    unit?.customer,
    unit?.wareHouseAreaLot?.id,
  ]);

  return (
    <View style={styles.container}>
      {isEmpty ? (
        <View style={styles.noDataYet}>
          <Text style={styles.noDataText}>
            {'We don’t have any data \n on this item yet'}
          </Text>
        </View>
      ) : (
        <View style={styles.content}>
          {!unit?.wareHouseAreaLot?.id ? (
            <Message message="Los is Missing for this Item" />
          ) : null}
          <View style={styles.block}>
            {unit?.condition?.id ? (
              <BaseCard
                style={
                  unit?.wareHouseAreaLot?.warehouseArea?.name
                    ? styles.blockRightItem
                    : styles.blockItem
                }
                title="Condition"
                text={unit.condition?.name || ''}
                icon={getConditionIcon(unit?.condition?.id)}
              />
            ) : null}
            {unit?.wareHouseAreaLot?.warehouseArea?.name ? (
              <BaseCard
                style={
                  unit?.condition?.id ? styles.blockLeftItem : styles.blockItem
                }
                title="Location"
                text={unit?.wareHouseAreaLot?.warehouseArea?.name}
                icon={<LocationSvg fill="#1E1F24" />}
              />
            ) : null}
          </View>
          {unit?.carrierDriver ? (
            <BaseCard
              title="Carrier"
              text={unit.carrierDriver.name}
              icon={
                unit?.carrierDriver?.photo ? (
                  <Image
                    style={styles.photo}
                    source={{
                      uri: USER_PHOTO_URL + unit.carrierDriver.photo || '',
                    }}
                  />
                ) : (
                  <View style={styles.photo}>
                    <UserSvg width={36} />
                  </View>
                )
              }
            />
          ) : null}
          {unit?.customer?.name ? (
            <BaseCard
              title="Customer"
              text={unit?.customer?.name}
              icon={
                <View style={styles.photo}>
                  <Text style={styles.capital}>
                    {unit?.customer?.name[0].toUpperCase()}
                  </Text>
                </View>
              }
            />
          ) : null}
        </View>
      )}
    </View>
  );
};

export default Overview;
