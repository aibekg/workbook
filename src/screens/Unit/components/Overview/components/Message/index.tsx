import React, {FC} from 'react';
import {Text, View} from 'react-native';

import LocationSvg from '@icons/location.svg';

import {styles} from './styles';

export type TMessageProps = {
  type?: 'error' | 'success' | 'warning';
  message?: string;
};

const Message: FC<TMessageProps> = ({message}) => {
  return (
    <View style={styles.container}>
      <LocationSvg style={styles.icon} fill="#000000" />
      <Text style={[styles.text]}>{message}</Text>
    </View>
  );
};

export default Message;
