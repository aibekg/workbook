import React from 'react';
import {Text, View} from 'react-native';

import Activity from '@screens/Profile/components/HistoryOfactivity/Activity';

import {styles} from './styles';

const Activities = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Activities</Text>
      <Activity
        title="Added photo to "
        imgUri="https://images.unsplash.com/photo-1605559424843-9e4c228bf1c2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWVyY2VkZXMlMjBjYXJ8ZW58MHx8MHx8&w=1000&q=80"
        time="10.10.22 2:49 pm"
        to="WAUZZZ8883499222"
      />
      <Activity
        last
        title="Added photo to "
        imgUri="https://images.unsplash.com/photo-1605559424843-9e4c228bf1c2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWVyY2VkZXMlMjBjYXJ8ZW58MHx8MHx8&w=1000&q=80"
        time="10.10.22 2:49 pm"
        to="WAUZZZ8883499222"
      />
    </View>
  );
};

export default Activities;
