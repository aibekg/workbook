import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {padding: 16},
  title: {
    fontSize: 12,
    textTransform: 'uppercase',
    fontWeight: '700',
    color: '#000000',
    opacity: 0.5,
  },
});
