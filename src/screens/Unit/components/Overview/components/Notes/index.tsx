import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';

import CircleAddSvg from '@icons/circle-add.svg';
import BaseCard from '@screens/Unit/components/BaseCard';

import {styles} from './styles';

const Notes = () => {
  const notes = true;

  return (
    <BaseCard
      title="Notes"
      icon={notes ? <CircleAddSvg stroke="#777b88" /> : null}
      content={
        !notes ? (
          <TouchableOpacity style={styles.addNote}>
            <CircleAddSvg stroke="#777b88" />
            <Text style={styles.addNoteText}>Add Note</Text>
          </TouchableOpacity>
        ) : (
          <View>
            <View style={styles.note}>
              <Text style={styles.noteTitle}>
                The car has a scratch near right mirror
              </Text>
              <View style={styles.content}>
                <Image
                  style={styles.photo}
                  source={{
                    uri: 'https://www.dmarge.com/wp-content/uploads/2021/01/dwayne-the-rock--480x320.jpg',
                  }}
                />
                <Text style={styles.name}>Kaiya Dokidis</Text>
                <Text style={styles.time}>10.10.22 2:49 pm </Text>
              </View>
            </View>
            <View style={[styles.note, styles.lastNode]}>
              <Text style={styles.noteTitle}>
                The car has a scratch near right mirror
              </Text>
              <View style={styles.content}>
                <Image
                  style={styles.photo}
                  source={{
                    uri: 'https://www.dmarge.com/wp-content/uploads/2021/01/dwayne-the-rock--480x320.jpg',
                  }}
                />
                <Text style={styles.name}>Kaiya Dokidis</Text>
                <Text style={styles.time}>10.10.22 2:49 pm </Text>
              </View>
            </View>
          </View>
        )
      }
    />
  );
};

export default Notes;
