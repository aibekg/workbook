import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  note: {
    borderColor: '#d0d0d0',
    paddingVertical: 12,
    borderBottomWidth: 1,
  },
  lastNode: {
    borderBottomWidth: 0,
  },
  addNote: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  addNoteText: {
    marginLeft: 8,
    fontSize: 16,
    fontWeight: '700',
    color: '#777b88',
  },
  noteTitle: {
    color: '#1E1F24',
    fontSize: 16,
    fontWeight: '400',
  },
  content: {
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  time: {
    position: 'absolute',
    right: 8,
    bottom: 8,
    fontSize: 10,
    fontWeight: '400',
    color: '#646C7A',
  },
  photo: {
    width: 24,
    height: 24,
    borderRadius: 100,
  },
  name: {
    marginLeft: 8,
    color: '#646C7A',
    fontSize: 12,
    fontWeight: '400',
  },
});
