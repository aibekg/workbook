import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderRadius: 12,
    marginVertical: 8,
  },
  iconContainer: {
    position: 'absolute',
    top: 16,
    right: 12,
  },
  header: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  title: {
    color: '#000000',
    opacity: 0.5,
    fontWeight: '700',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  content: {
    marginTop: 8,
  },
  text: {
    fontSize: 18,
    fontWeight: '400',
    color: '#1E1F24',
  },
});
