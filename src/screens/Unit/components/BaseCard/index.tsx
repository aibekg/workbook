import React, {FC, ReactNode} from 'react';
import {StyleProp, Text, TouchableOpacity, View, ViewStyle} from 'react-native';

import {styles} from './styles';

export type TBaseCard = {
  style?: StyleProp<ViewStyle>;
  title: string | null;
  icon?: ReactNode;
  text?: string | null;
  content?: ReactNode;
  iconStyle?: StyleProp<ViewStyle>;
};

const BaseCard: FC<TBaseCard> = ({
  style,
  title,
  icon,
  iconStyle,
  text,
  content,
}) => {
  return (
    <TouchableOpacity activeOpacity={1} style={[styles.container, style]}>
      <View style={styles.header}>
        {title ? <Text style={styles.title}>{title}</Text> : null}
      </View>
      <View style={styles.content}>
        {text ? <Text style={styles.text}>{text}</Text> : null}
        {content}
      </View>
      <View style={[styles.iconContainer, iconStyle]}>{icon}</View>
    </TouchableOpacity>
  );
};

export default BaseCard;
