import {TVehicleCondition} from '@screens/ReceiveUnitConfirm';

export type TVehicleBottomSheetProps = {
  index: number;
  setIndex: (index: number) => void;
  selectedVehicleCondition: TVehicleCondition | null;
  setSelectedVehicleCondition: (value: TVehicleCondition) => void;
};
