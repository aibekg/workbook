import React, {FC, useCallback, useMemo, useRef, useState} from 'react';
import {LayoutChangeEvent, Text, View} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import BottomSheet from '@gorhom/bottom-sheet';

import Button from '@components/Button';
import NonOperableSvg from '@icons/non-operable.svg';
import NonRolableSvg from '@icons/non-rolable.svg';
import SteeringWheelSvg from '@icons/steering-wheel.svg';
import {TVehicleCondition} from '@screens/ReceiveUnitConfirm';
import Card from '@screens/ReceiveUnitConfirm/components/Card';
import {deviceHeight, deviceRatio} from '@utils/deviceHelpers';

import {styles} from './styles';
import {TVehicleBottomSheetProps} from './types';

const VehicleConditionBottomSheet: FC<TVehicleBottomSheetProps> = ({
  index,
  setIndex,
  selectedVehicleCondition,
  setSelectedVehicleCondition,
}) => {
  const [snapPoints, setSnapPoints] = useState<number[]>([
    deviceRatio <= 1.9 ? deviceHeight * 0.75 : deviceHeight * 0.65,
  ]);
  const bottomSheetRef = useRef<BottomSheet>(null);
  const [selectingVehicleCondition, setSelectingVehicleCondition] =
    useState<TVehicleCondition | null>(selectedVehicleCondition);
  const handleChange = useCallback(
    (idx: number) => {
      setIndex(idx);
    },
    [setIndex],
  );

  const vehicleConditions = useMemo(
    () => [
      {
        id: 1,
        title: 'Operable',
        description: 'Starts and drive',
        Icon: SteeringWheelSvg,
      },
      {
        id: 2,
        title: 'Not Operable',
        description: 'Vehicle doesn’t start',
        Icon: NonOperableSvg,
      },
      {
        id: 3,
        title: 'Not Rollable',
        description: 'Can’t move without assistance',
        Icon: NonRolableSvg,
      },
    ],
    [],
  );

  const handleVehicleConditionPress = useCallback(
    (val: TVehicleCondition) => () => {
      setSelectingVehicleCondition(val);
    },
    [],
  );

  const handleConfirm = useCallback(() => {
    if (selectingVehicleCondition) {
      setSelectedVehicleCondition(selectingVehicleCondition);
      bottomSheetRef.current?.close();
    }
  }, [selectingVehicleCondition, setSelectedVehicleCondition]);

  const onLayout = (e: LayoutChangeEvent) => {
    setSnapPoints([
      getBottomSpace() + 48 + e.nativeEvent.layout.height ||
        (deviceRatio <= 1.9 ? deviceHeight * 0.75 : deviceHeight * 0.65),
    ]);
  };

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={index}
      backgroundStyle={styles.container}
      enablePanDownToClose={!!selectedVehicleCondition}
      onChange={handleChange}
      snapPoints={snapPoints}>
      <View onLayout={onLayout} style={styles.modalContent}>
        <View style={styles.content}>
          <Text style={styles.title}>Choose the Vehicle Condition</Text>
        </View>
        <View style={styles.list}>
          {vehicleConditions.map(item => (
            <Card
              key={item.id}
              onPress={handleVehicleConditionPress(item)}
              active={item.id === selectingVehicleCondition?.id}
              style={styles.item}
              icon={
                <View
                  style={[
                    styles.type,
                    item.id !== selectingVehicleCondition?.id &&
                      styles.notActiveType,
                  ]}>
                  <item.Icon
                    width={28}
                    height={28}
                    fill={
                      selectingVehicleCondition?.id === item.id
                        ? '#CDFF48'
                        : '#1E1F24'
                    }
                  />
                </View>
              }
              title={item.title}
              description={item.description}
            />
          ))}
        </View>
        <View style={styles.footer}>
          <Button
            disabled={!selectingVehicleCondition}
            text="Confirm"
            onPress={handleConfirm}
          />
        </View>
      </View>
    </BottomSheet>
  );
};

export default VehicleConditionBottomSheet;
