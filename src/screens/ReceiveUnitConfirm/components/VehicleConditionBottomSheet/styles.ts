import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#383838',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 16.0,
    elevation: 24,
  },
  modalContent: {
    flexDirection: 'column',
    width: '100%',
  },
  content: {
    flexDirection: 'column',
    width: '100%',
    padding: 8,
  },
  title: {
    color: '#212121',
    fontWeight: '700',
    fontSize: 12,
    opacity: 0.7,
    textTransform: 'uppercase',
    marginLeft: 8,
  },
  type: {
    backgroundColor: '#000000',
    width: 48,
    height: 48,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    marginTop: 20,
  },
  item: {
    marginTop: 0,
  },
  notActiveType: {
    backgroundColor: '#F8F8F8',
  },
  footer: {
    marginTop: 32,
    paddingHorizontal: 40,
    width: '100%',
  },
});
