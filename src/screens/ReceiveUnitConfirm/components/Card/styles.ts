import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingVertical: 12,
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 8,
    marginTop: 20,
  },
  column: {
    flex: 1,
    marginLeft: 8,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F24',
  },
  description: {
    marginTop: 4,
    color: '#000000',
    opacity: 0.5,
  },
  activeContainer: {
    backgroundColor: '#CDFF48',
  },
});
