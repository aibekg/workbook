import React, {FC} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {styles} from './styles';
import {TCardProps} from './types';

const Card: FC<TCardProps> = ({
  icon,
  title,
  description,
  flex,
  onPress,
  style,
  active,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        {flex},
        style,
        active && styles.activeContainer,
      ]}>
      {icon}
      <View style={styles.column}>
        <Text style={styles.title}>{title}</Text>
        {description ? (
          <Text style={styles.description}>{description}</Text>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

export default Card;
