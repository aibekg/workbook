import {ReactNode} from 'react';
import {StyleProp, ViewStyle} from 'react-native';

export type TCardProps = {
  icon: ReactNode;
  title: string;
  description?: string;
  flex?: number;
  onPress?: () => void;
  style?: StyleProp<ViewStyle>;
  active?: boolean;
};
