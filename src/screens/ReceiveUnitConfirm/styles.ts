import {StyleSheet} from 'react-native';

import {bottomSpace, deviceWidth, statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
    width: '100%',
  },
  scroll: {
    flex: 1,
    width: '100%',
  },
  content: {
    paddingHorizontal: 12,
    paddingTop: 20 + statusBarHeight,
    width: '100%',
  },
  logo: {
    width: 80,
    height: 80,
    borderRadius: 80,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    width: '100%',
    paddingHorizontal: 8,
  },
  header: {
    width: '100%',
    borderRadius: 12,
    backgroundColor: '#1E1F24',
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  name: {
    fontSize: 28,
    color: '#FFFFFF',
  },
  vinNumber: {
    marginTop: 16,
    fontSize: 18,
    color: '#FFFFFF',
  },
  vinNumberBold: {
    fontWeight: '600',
  },
  vinNumberGreen: {
    color: '#CDFF48',
  },
  blockRow: {
    flexDirection: 'row',
  },
  type: {
    backgroundColor: '#000000',
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  color: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#d2d2d2',
  },
  missing: {
    borderWidth: 1,
    borderColor: '#FA354D',
  },
  info: {
    color: '#000000',
    marginBottom: 24,
    opacity: 0.5,
    paddingHorizontal: 40,
    textAlign: 'center',
    lineHeight: 24,
    width: '100%',
  },
  footer: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    width: deviceWidth,
    backgroundColor: '#F1F1F1',
    marginTop: 48,
    paddingTop: 16,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingBottom: bottomSpace + 16,
  },
  confirmButton: {
    marginHorizontal: 48,
  },
  finishBtn: {
    marginHorizontal: 0,
    backgroundColor: 'transparent',
    marginTop: 8,
  },
  finishBtnText: {
    color: '#1E1F24',
  },
});
