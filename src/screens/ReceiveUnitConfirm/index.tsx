import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {ScrollView, StatusBar, Text, View} from 'react-native';
import {SvgProps} from 'react-native-svg';
import {useSelector} from 'react-redux';
import {
  CommonActions,
  RouteProp,
  useNavigation,
  useRoute,
} from '@react-navigation/native';

import Button from '@components/Button';
import {useActions} from '@hooks/useActions';
import {useReceiveUnit} from '@hooks/useReceiveUnit';
import Convertible from '@icons/convertible.svg';
import Coupe from '@icons/coupe.svg';
import Hatchback from '@icons/hatchback.svg';
import NonOperableSvg from '@icons/non-operable.svg';
import NonRolableSvg from '@icons/non-rolable.svg';
import SedanSvg from '@icons/sedan.svg';
import Sedan from '@icons/sedan.svg';
import SteeringWheelSvg from '@icons/steering-wheel.svg';
import SUV from '@icons/suv.svg';
import Truck from '@icons/truck_body.svg';
import Van from '@icons/van.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import VehicleConditionBottomSheet from '@screens/ReceiveUnitConfirm/components/VehicleConditionBottomSheet';
import {selectNewUnitData} from '@store/unit/selectors';
import {
  capitalizeWord,
  getColorString,
  validateVin,
} from '@utils/stringHelpers';

import {Unit} from '../../generated/graphql';

import Card from './components/Card';
import {styles} from './styles';

export type TVehicleCondition = {
  id: number;
  title: string;
  description: string;
  Icon: React.FC<SvgProps>;
};

const getConditionIcon = (conditionId: number) => {
  switch (conditionId) {
    case 1:
      return SteeringWheelSvg;
    case 2:
      return NonOperableSvg;
    case 3:
      return NonRolableSvg;
    default:
      return SteeringWheelSvg;
  }
};

const ReceiveUnitConfirm = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.ReceiveUnitConfirm>>();

  const {params} =
    useRoute<
      RouteProp<TRootStackParamList, ERootStackRoutes.ReceiveUnitConfirm>
    >();

  const {clearUnitDataFiled, setInspectionUnit} = useActions();

  const [index, setIndex] = useState(0);
  const [selectedVehicleCondition, setSelectedVehicleCondition] =
    useState<TVehicleCondition | null>(null);

  const {colorId, bodyId, model, manufacturer, year, vin} =
    useSelector(selectNewUnitData);

  const {receiveUnit} = useReceiveUnit();

  useEffect(() => {
    if (params?.unit?.condition?.id) {
      setSelectedVehicleCondition({
        id: params?.unit?.condition?.id,
        title: params?.unit?.condition?.name || '',
        description: '',
        Icon: getConditionIcon(params?.unit?.condition?.id),
      });
    }
  }, [params?.unit?.condition]);

  const handlePressConfirm = useCallback(async () => {
    if (params?.unit?.id && colorId && bodyId && selectedVehicleCondition?.id) {
      try {
        const unit = await receiveUnit({
          id: params?.unit?.id,
          conditionId: selectedVehicleCondition?.id,
          colorId,
          bodyId,
        });
        if (unit?.id) {
          setInspectionUnit({
            value: unit as Unit,
          });
          setTimeout(() => {
            navigation.dispatch(
              CommonActions.reset({
                index: 1,
                routes: [
                  {name: ERootStackRoutes.Units},
                  {
                    name: ERootStackRoutes.CarrierCompany,
                  },
                ],
              }),
            );
          }, 300);
          setTimeout(() => {
            clearUnitDataFiled();
          }, 900);
        }
      } catch (e) {}
    }
  }, [
    colorId,
    bodyId,
    clearUnitDataFiled,
    setInspectionUnit,
    navigation,
    params?.unit,
    receiveUnit,
    selectedVehicleCondition,
  ]);

  const handleFinishPress = useCallback(async () => {
    if (params?.unit?.id && colorId && bodyId && selectedVehicleCondition?.id) {
      try {
        const unit = await receiveUnit({
          id: params?.unit?.id,
          conditionId: selectedVehicleCondition?.id,
          colorId,
          bodyId,
        });
        if (unit?.id) {
          navigation.navigate(ERootStackRoutes.ReceiveUnit, {
            unitManufacturer: manufacturer,
            unitModel: model,
            unitYear: year,
          });
          setTimeout(() => {
            clearUnitDataFiled();
          }, 600);
        }
      } catch (e) {}
    }
  }, [
    colorId,
    bodyId,
    year,
    model,
    manufacturer,
    clearUnitDataFiled,
    navigation,
    params?.unit,
    receiveUnit,
    selectedVehicleCondition,
  ]);

  const handleChooseVehiclePress = useCallback(() => {
    setIndex(0);
  }, []);

  const handleChooseBody = useCallback(() => {
    setIndex(-1);
    if (vin) {
      navigation.push(ERootStackRoutes.AddUnitBody, {
        independent: true,
        vin,
      });
    }
  }, [vin, navigation]);

  const handleChooseColor = useCallback(() => {
    setIndex(-1);
    if (vin) {
      navigation.push(ERootStackRoutes.AddUnitColor, {
        independent: true,
        vin,
      });
    }
  }, [vin, navigation]);

  const bodyIcons: React.FC<SvgProps>[] = useMemo(
    () => [Sedan, Hatchback, SUV, Truck, Convertible, Van, Coupe],
    [],
  );

  const BodySVG = useMemo(
    () =>
      bodyId && bodyIcons[bodyId - 1]
        ? bodyIcons[(bodyId - 1) % bodyIcons.length]
        : SedanSvg,
    [bodyId, bodyIcons],
  );

  const body = useMemo(() => params?.unit?.body?.name, [params?.unit]);
  const color = useMemo(() => params?.unit?.color?.value, [params?.unit]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scroll}
        contentContainerStyle={styles.content}>
        <View style={styles.headerContainer}>
          <View style={styles.header}>
            <Text
              style={styles.name}>{`${year} ${manufacturer} ${model}`}</Text>
            {vin ? (
              validateVin(vin, true) ? (
                <Text style={styles.vinNumber}>
                  {vin?.slice(0, vin?.length - 7)}
                  <Text style={styles.vinNumberBold}>
                    {vin?.slice(vin?.length - 7, vin?.length)}
                  </Text>
                </Text>
              ) : (
                <Text style={styles.vinNumber}>{vin}</Text>
              )
            ) : null}
          </View>
        </View>
        <View style={styles.blockRow}>
          <Card
            flex={1}
            onPress={handleChooseBody}
            icon={
              <View style={[styles.type]}>
                <BodySVG
                  stroke="#CDFF48"
                  width={32}
                  height={32}
                  strokeWidth={2}
                />
              </View>
            }
            title={body || '-'}
            style={!body ? styles.missing : undefined}
          />
          <Card
            flex={1}
            onPress={handleChooseColor}
            icon={
              <View
                style={[
                  styles.color,
                  !!color && {
                    backgroundColor:
                      color.toLowerCase() === 'other'
                        ? undefined
                        : getColorString(color.toLowerCase()),
                  },
                  color === 'white' && {
                    borderWidth: 1,
                    borderColor: '#212121',
                  },
                ]}
              />
            }
            title={color ? capitalizeWord(color) : '-'}
            style={!color ? styles.missing : undefined}
          />
        </View>
        {selectedVehicleCondition ? (
          <Card
            icon={
              <View style={styles.type}>
                <selectedVehicleCondition.Icon
                  width={28}
                  height={28}
                  fill="#CDFF48"
                />
              </View>
            }
            title={selectedVehicleCondition?.title}
            description={selectedVehicleCondition?.description}
            onPress={handleChooseVehiclePress}
          />
        ) : null}
      </ScrollView>
      <View style={styles.footer}>
        <Text style={styles.info}>
          {
            'Please check the information above and confirm if everything is correct'
          }
        </Text>
        <Button
          disabled={
            !params?.unit ||
            !colorId ||
            !bodyId ||
            !selectedVehicleCondition?.id
          }
          buttonStyle={styles.confirmButton}
          text="Confirm & Start Inspection"
          onPress={handlePressConfirm}
        />
        <Button
          disabled={
            !params?.unit ||
            !colorId ||
            !bodyId ||
            !selectedVehicleCondition?.id
          }
          textStyle={styles.finishBtnText}
          buttonStyle={styles.finishBtn}
          text="Mark as Received and Finish"
          onPress={handleFinishPress}
        />
      </View>
      <VehicleConditionBottomSheet
        selectedVehicleCondition={selectedVehicleCondition}
        setSelectedVehicleCondition={setSelectedVehicleCondition}
        setIndex={setIndex}
        index={index}
      />
    </View>
  );
};

export default ReceiveUnitConfirm;
