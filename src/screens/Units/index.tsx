import React, {useCallback, useMemo, useState} from 'react';
import {
  RefreshControl,
  SectionList,
  SectionListRenderItemInfo,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import groupBy from 'lodash/groupBy';

import Header from '@components/Header';
import useInBoundUnits, {
  THiddenData,
  TInBoundUnits,
} from '@hooks/useInBoundUnits';
import QueueListSvg from '@icons/queue-list.svg';
import ScanSvg from '@icons/scan.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import BottomTab from '@screens/Units/components/BottomTab';
import HeaderTitle from '@screens/Units/components/HeaderTitle';
import MoreItems from '@screens/Units/components/MoreItems';
import UnitItem from '@screens/Units/components/UnitItem';
import {enumToHumanReadable} from '@utils/stringHelpers';

import {Unit} from '../../generated/graphql';

import {styles} from './styles';
import {InBoundStatus, TRenderSectionProps} from './types';

const Units = () => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Units>>();

  const {inBoundUnits, isLoading, getInBoundUnits} = useInBoundUnits();

  const [inBoundSelectedView, setInBoundSelectedView] = useState<
    'show-units' | 'group-by-carriers'
  >('show-units');

  const inBoundData = useMemo(() => {
    switch (inBoundSelectedView) {
      case 'show-units':
        return inBoundUnits;
      case 'group-by-carriers':
        const inBoundUnitsFlat: Unit[] = inBoundUnits
          .map(item => item.data as Unit[])
          .flat();
        const inBoundHiddenUnitsFlat: Unit[] = inBoundUnits
          .map(
            item =>
              item.hiddenData
                .map(hiddenItem => hiddenItem.data)
                .flat() as Unit[],
          )
          .flat();
        const grouped = groupBy(
          inBoundUnitsFlat,
          unitData => unitData?.carrierDriver?.name || 'No Carrier Driver',
        );
        const groupedHidden = groupBy(
          inBoundHiddenUnitsFlat,
          unitData => unitData?.carrierDriver?.name || 'No Carrier Driver',
        );
        return Object.keys(grouped).map(key => ({
          status: key,
          data: (grouped[key] || []).concat(groupedHidden[key] || []),
          hiddenData: [],
        }));
    }
  }, [inBoundSelectedView, inBoundUnits]);

  const onVin = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.CodeScanner,
      params: undefined,
    });
  }, [navigation]);

  const handleShowMoreUnits = useCallback(
    (hiddenData: THiddenData[]) => () => {
      navigation.navigate(ERootStackRoutes.MoreUnits, {
        hiddenData,
      });
    },
    [navigation],
  );

  const keyExtractor = useCallback(
    (item: Unit, index: number) => index.toString(),
    [],
  );

  const renderSectionHeader = useCallback(
    ({section: {data, status}}: TRenderSectionProps) =>
      data.length ? (
        <View
          style={[
            styles.sectionHeader,
            status === InBoundStatus.IN_PROGRESS && styles.inProgressHeader,
          ]}>
          <Text
            style={[
              styles.sectionTitle,
              status === InBoundStatus.IN_PROGRESS && styles.inProgressTitle,
            ]}>
            {enumToHumanReadable(status)}
          </Text>
        </View>
      ) : null,
    [],
  );

  const renderSectionFooter = useCallback(
    ({section: {hiddenData}}: TRenderSectionProps) =>
      hiddenData.length ? (
        <MoreItems
          onPress={handleShowMoreUnits(hiddenData)}
          hiddenData={hiddenData}
        />
      ) : null,
    [handleShowMoreUnits],
  );

  const ListEmptyComponent = useMemo(
    () => (
      <View style={styles.empty}>
        <QueueListSvg />
        <Text style={styles.emptyText}>No items in this list yet</Text>
      </View>
    ),
    [],
  );

  const renderItem = useCallback(
    ({item, section}: SectionListRenderItemInfo<Unit, TInBoundUnits>) => (
      <UnitItem
        style={styles.unitItem}
        showStatus={section.status === InBoundStatus.IN_PROGRESS}
        unit={item}
      />
    ),
    [],
  );

  return (
    <View style={styles.container}>
      <Header
        title={<HeaderTitle onViewSelected={setInBoundSelectedView} />}
        headerRight={
          <TouchableOpacity
            onPress={onVin}
            hitSlop={styles.hitStop}
            style={styles.headerRight}>
            <ScanSvg width={24} height={24} />
          </TouchableOpacity>
        }
      />
      <View style={styles.content}>
        <SectionList<Unit, TInBoundUnits>
          sections={inBoundData}
          showsVerticalScrollIndicator={false}
          keyExtractor={keyExtractor}
          stickySectionHeadersEnabled={true}
          refreshControl={
            <RefreshControl
              refreshing={isLoading}
              onRefresh={getInBoundUnits}
              tintColor="#2A2A2A"
            />
          }
          renderItem={renderItem}
          style={styles.sectionList}
          contentContainerStyle={styles.sectionContent}
          renderSectionHeader={renderSectionHeader}
          renderSectionFooter={renderSectionFooter}
          ListEmptyComponent={ListEmptyComponent}
        />
        <BottomTab />
      </View>
    </View>
  );
};

export default Units;
