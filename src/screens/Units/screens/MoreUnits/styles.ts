import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#f3f3f3',
    borderTopRightRadius: 32,
    borderTopLeftRadius: 32,
  },
  title: {
    paddingHorizontal: 12,
    fontSize: 28,
    fontWeight: '400',
    color: '#212121',
  },
  sectionHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 40,
    paddingBottom: 8,
    paddingHorizontal: 12,
    backgroundColor: '#f3f3f3',
  },
  sectionTitle: {
    color: '#1E1F24',
    fontSize: 16,
    fontWeight: '700',
    marginLeft: 12,
  },
  sectionPhoto: {
    width: 36,
    height: 36,
    borderRadius: 100,
  },
  list: {
    flex: 1,
    width: '100%',
  },
  listContent: {
    width: '100%',
    paddingTop: 40,
    paddingHorizontal: 12,
    paddingBottom: bottomSpace + 16,
  },
});
