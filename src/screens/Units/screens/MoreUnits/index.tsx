import React from 'react';
import {Image, SectionList, Text, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';
import {RouteProp, useRoute} from '@react-navigation/native';

import {ModalHandle} from '@components/ModalHandle';
import {THiddenData} from '@hooks/useInBoundUnits';
import {ERootStackRoutes, TRootStackParamList} from '@routes/types';
import UnitItem from '@screens/Units/components/UnitItem';

import {Unit} from '../../../../generated/graphql';

import {styles} from './styles';

const MoreUnits = () => {
  const {
    params: {hiddenData},
  } = useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.MoreUnits>>();

  return (
    <View style={styles.container}>
      <ModalHandle />
      <SectionList<Unit, THiddenData>
        sections={hiddenData}
        showsVerticalScrollIndicator={false}
        style={styles.list}
        contentContainerStyle={styles.listContent}
        ListHeaderComponent={
          <Text style={styles.title}>{'Items in Progress'}</Text>
        }
        keyExtractor={index => index.id}
        renderItem={({item}) => <UnitItem unit={item} />}
        renderSectionHeader={({section: {user}}) => (
          <View style={styles.sectionHeader}>
            <Image
              source={{uri: user.photo ? USER_PHOTO_URL + user.photo : ''}}
              style={styles.sectionPhoto}
            />
            <Text style={styles.sectionTitle}>
              {`${user.userFirstName} ${user.userLastName}`.trim()}
            </Text>
          </View>
        )}
      />
    </View>
  );
};

export default MoreUnits;
