import React, {FC, useCallback, useMemo} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';
import {useNavigation} from '@react-navigation/native';

import carPlaceholder from '@icons/car_placeholder.png';
import ErrorSvg from '@icons/error.svg';
import SuccessSvg from '@icons/success.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import {TUnitItemProps} from '@screens/Units/components/UnitItem/types';
import {getTimeAgo} from '@utils/dateHelpers';

import {styles} from './styles';

const UnitItem: FC<TUnitItemProps> = ({unit, showStatus, style}) => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Units>>();

  const handlePress = useCallback(() => {
    navigation.navigate(ERootStackRoutes.Unit, {
      unitId: unit.id,
    });
  }, [navigation, unit]);

  const checkStatus = useMemo(
    () => unit.status !== 'LOT_MISSING',
    [unit.status],
  );

  return (
    <TouchableOpacity onPress={handlePress} style={[styles.container, style]}>
      <View style={styles.content}>
        {unit.photo?.length ? (
          <Image
            resizeMode="cover"
            style={styles.photo}
            source={{
              uri: USER_PHOTO_URL + unit.photo[0],
            }}
          />
        ) : (
          <Image
            resizeMode="cover"
            source={carPlaceholder}
            style={styles.photo}
          />
        )}
        <View style={styles.block}>
          <View style={styles.unitTextContainer}>
            <Text style={styles.title} ellipsizeMode="tail" numberOfLines={1}>
              {unit.vinNumber.slice(-7) || 'No Information'}
            </Text>
            {unit.manufacturer && unit.model && unit.year ? (
              <Text
                style={styles.model}
                numberOfLines={1}>{` '${`${unit.year}`.slice(-2)} ${
                unit.manufacturer
              } ${unit.model}`}</Text>
            ) : null}
          </View>
          <View style={styles.unitTimeContainer}>
            <Text style={styles.subTitle}>
              {unit.carrierDriver?.name || ''}
            </Text>
            {unit.createdAt ? (
              <Text style={styles.time}>{getTimeAgo(unit.createdAt)}</Text>
            ) : null}
          </View>
        </View>
      </View>
      {showStatus ? (
        <View style={styles.status}>
          <View style={styles.statusBlock}>
            {checkStatus ? <SuccessSvg /> : <ErrorSvg />}
            <Text
              style={[
                styles.statusTitle,
                checkStatus ? styles.statusGreen : styles.statusRed,
              ]}>
              {unit.status}
            </Text>
          </View>
        </View>
      ) : null}
      {/*<Text style={styles.timeSpent}>Time spent 23:35</Text>*/}
    </TouchableOpacity>
  );
};

export default UnitItem;
