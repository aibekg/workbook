import {StyleProp, ViewStyle} from 'react-native';

import {Unit} from '../../../../generated/graphql';

export type TUnitItemProps = {
  unit: Unit;
  showStatus?: boolean;
  style?: StyleProp<ViewStyle>;
};
