import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    backgroundColor: '#FFFFFF',
    marginVertical: 8,
    overflow: 'hidden',
  },
  photo: {
    width: 64,
    height: 64,
    borderRadius: 12,
    marginRight: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  content: {
    flexDirection: 'row',
    padding: 8,
  },
  block: {
    flex: 1,
    paddingRight: 8,
    justifyContent: 'center',
  },
  unitTextContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  unitTimeContainer: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  title: {
    color: '#000000',
    fontWeight: '700',
    fontSize: 18,
    lineHeight: 22,
    textTransform: 'uppercase',
  },
  model: {
    color: '#646C7A',
    flex: 1,
    fontSize: 16,
    lineHeight: 22,
    fontWeight: '400',
  },
  subTitle: {
    color: '#646C7A',
    fontSize: 14,
    fontWeight: '400',
    opacity: 0.5,
  },
  time: {
    marginLeft: 8,
    fontWeight: '400',
    color: '#646C7A',
    fontSize: 14,
    opacity: 0.5,
  },
  status: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    backgroundColor: '#1E1F24',
  },
  statusBlock: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  statusTitle: {
    color: '#1E1F24',
    fontSize: 12,
    fontWeight: '700',
    marginLeft: 4,
  },
  statusGreen: {
    color: '#CDFF48',
  },
  statusRed: {
    color: '#FA354D',
  },
  timeSpent: {
    color: '#FFFFFF',
    fontSize: 12,
    fontWeight: '400',
  },
});
