import React, {useCallback, useMemo, useState} from 'react';
import {TouchableOpacity, View} from 'react-native';

import ArrowDownSvg from '@icons/arrow-down-on-square.svg';
import ArrowUpSvg from '@icons/arrow-up-on-square.svg';
import LocationSvg from '@icons/location.svg';
import TruckSvg from '@icons/truck_tab.svg';

import {styles} from './styles';

const BottomTab = () => {
  const [activeTab, setActiveTab] = useState(0);
  const icons = useMemo(
    () => [
      {id: 0, Icon: ArrowDownSvg},
      {id: 1, Icon: LocationSvg},
      {id: 2, Icon: ArrowUpSvg},
      {id: 3, Icon: TruckSvg},
    ],
    [],
  );

  const handlePress = useCallback(
    (id: number) => () => {
      setActiveTab(id);
    },
    [],
  );

  return (
    <View style={styles.container}>
      <View style={styles.tabsContainer}>
        {icons.map(({Icon, id}) => (
          <TouchableOpacity
            onPress={handlePress(id)}
            disabled={id !== 0}
            style={[
              styles.icon,
              id === 0 && styles.firstIcon,
              id === icons.length - 1 && styles.lastIcon,
              id !== 0 && styles.disabled,
            ]}
            key={id}>
            <Icon fill={activeTab === id ? '#CDFF48' : '#BCBBC8'} />
          </TouchableOpacity>
        ))}
      </View>
      {/*<TouchableOpacity style={styles.bell}>
        <BellSvg />
      </TouchableOpacity>*/}
    </View>
  );
};

export default BottomTab;
