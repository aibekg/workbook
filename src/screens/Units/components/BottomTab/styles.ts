import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: bottomSpace + 16,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 48,
    flexDirection: 'row',
  },
  tabsContainer: {
    borderRadius: 30,
    paddingHorizontal: 4,
    backgroundColor: '#1E1F24',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    width: 56,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  firstIcon: {
    marginLeft: 0,
  },
  lastIcon: {
    marginRight: 0,
  },
  disabled: {
    opacity: 0.5,
  },
  bell: {
    width: 52,
    height: 52,
    borderRadius: 100,
    backgroundColor: '#FA354D',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 12,
  },
});
