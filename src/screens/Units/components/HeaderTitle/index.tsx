import React, {useCallback} from 'react';
import {Text, View} from 'react-native';

import Menu from '@components/Menu';
import ArrowDownSvg from '@icons/arrow-down.svg';

import {styles} from './styles';

const options = [
  {
    key: 'show-units',
    text: 'Show Units',
  },
  {
    key: 'group-by-carriers',
    text: 'Group by Carriers',
  },
];

type Props = {
  onViewSelected: (view: 'show-units' | 'group-by-carriers') => void;
};

const HeaderTitle = ({onViewSelected}: Props) => {
  const handleSelect = useCallback(
    (value: string) => {
      onViewSelected(value as 'show-units' | 'group-by-carriers');
    },
    [onViewSelected],
  );
  return (
    <Menu
      title="CHANGE VIEW"
      onSelect={handleSelect}
      style={styles.popup}
      trigger={
        <View style={styles.container}>
          <Text style={styles.title}>In-Bound Units</Text>
          <ArrowDownSvg />
        </View>
      }
      options={options}
    />
  );
};

export default HeaderTitle;
