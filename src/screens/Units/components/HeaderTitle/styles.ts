import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  title: {
    color: '#1E1F24',
    fontSize: 14,
    fontWeight: '700',
    marginRight: 4,
  },
  container: {
    backgroundColor: '#CDFF48',
    paddingVertical: 8,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
  },
  popup: {
    marginLeft: -48,
  },
});
