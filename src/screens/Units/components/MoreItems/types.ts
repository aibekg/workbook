import {THiddenData} from '@hooks/useInBoundUnits';

export type TMoreItemProps = {
  hiddenData: THiddenData[];
  onPress: () => void;
};
