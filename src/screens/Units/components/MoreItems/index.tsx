import React, {FC, useMemo} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {USER_PHOTO_URL} from '@env';

import carPlaceholder from '@icons/car_placeholder.png';
import {TMoreItemProps} from '@screens/Units/components/MoreItems/types';

import {styles} from './styles';

const MoreItems: FC<TMoreItemProps> = ({hiddenData, onPress}) => {
  const images = useMemo(() => {
    const imageArray: any[] = (hiddenData || []).map(item =>
      (item.data || []).map(dataItem =>
        dataItem.photo && (dataItem.photo || []).length > 0
          ? {uri: USER_PHOTO_URL + dataItem.photo[0]}
          : carPlaceholder,
      ),
    );
    return [].concat.apply([], imageArray);
  }, [hiddenData]);

  const dataLength = useMemo(() => {
    return (hiddenData || []).reduce((acc: number, currentItem) => {
      return acc + (currentItem.data || []).length;
    }, 0);
  }, [hiddenData]);

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.images}>
        {images.map((img, index) => (
          <Image
            key={index}
            resizeMode="cover"
            style={[styles.image, index === 0 && styles.firstImage]}
            source={img}
          />
        ))}
      </View>
      <Text style={styles.title}>
        {`${dataLength} ${dataLength > 1 ? 'Moves are' : 'Move is'}
        Processing by Teammates`}
      </Text>
    </TouchableOpacity>
  );
};

export default MoreItems;
