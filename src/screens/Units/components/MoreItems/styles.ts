import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1E1F24',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    margin: 8,
    borderRadius: 12,
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 8,
    borderWidth: 1.5,
    backgroundColor: '#FFFFFF',
    borderColor: '#FFFFFF',
    marginLeft: -24,
  },
  firstImage: {
    marginLeft: 0,
  },
  images: {
    flexDirection: 'row',
  },
  title: {
    color: '#CDFF48',
    fontSize: 12,
    fontWeight: '700',
    marginLeft: 12,
  },
});
