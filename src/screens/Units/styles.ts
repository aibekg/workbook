import {StyleSheet} from 'react-native';

import {bottomSpace, deviceHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#1E1F24',
  },
  content: {
    flex: 1,
    overflow: 'hidden',
    backgroundColor: '#f3f3f3',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    position: 'relative',
  },
  sectionList: {
    flex: 1,
    width: '100%',
  },
  sectionContent: {
    width: '100%',
    flexDirection: 'column',
    paddingBottom: bottomSpace + 88,
  },
  headerRight: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sectionHeader: {
    paddingVertical: 16,
    backgroundColor: '#f3f3f3',
  },
  sectionTitle: {
    color: '#1E1F24',
    textAlign: 'center',
    fontWeight: '700',
    opacity: 0.7,
    textTransform: 'uppercase',
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    height: deviceHeight - 200,
  },
  emptyText: {
    marginTop: 16,
    color: '#646C7A',
    fontSize: 16,
    fontWeight: '400',
    opacity: 0.5,
  },
  inProgressHeader: {
    backgroundColor: '#1E1F24',
  },
  inProgressTitle: {
    color: '#CDFF48',
  },
  unitItem: {
    margin: 8,
  },
  hitStop: {top: 24, bottom: 24, left: 24, right: 24},
  activityIndicator: {
    marginTop: 20,
  },
});
