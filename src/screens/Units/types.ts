import {TInBoundUnits} from '@hooks/useInBoundUnits';

export type TRenderSectionProps = {
  section: TInBoundUnits;
};

export type TUnit = {
  title: string;
  model: string;
  status?: boolean;
};

export enum InBoundStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  EXPECTED = 'EXPECTED',
  RECEIVED = 'RECEIVED',
  INSPECTION_PENDING = 'INSPECTION_PENDING',
}

export enum UnitStatus {
  'INSPECTION_PENDING' = 'INSPECTION_PENDING',
  'RECEIVED' = 'RECEIVED',
  'REPORTING' = 'REPORTING',
  'EQUIPMENT_INSPECTION' = 'EQUIPMENT_INSPECTION',
  'EXTERIOR_INSPECTION' = 'EXTERIOR_INSPECTION',
  'INTERIOR_INSPECTION' = 'INTERIOR_INSPECTION',
  'LOT_MISSING' = 'LOT_MISSING',
  'EXPECTED' = 'EXPECTED',
}
