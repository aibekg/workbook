import React, {useCallback} from 'react';
import {Image, StatusBar, Text, View} from 'react-native';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import CarSvg from '@icons/car-photo.svg';
import XSvg from '@icons/x-mark.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';

import {RECEIVED_BACKGROUND} from '../../assets/images';

import {styles} from './styles';

const ReceiveUnit = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.ReceiveUnit>>();

  const {params} =
    useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.ReceiveUnit>>();

  const handleGoToInBoundPress = useCallback(() => {
    navigation.navigate(ERootStackRoutes.Units);
  }, [navigation]);

  const handleCancelPress = useCallback(() => {
    navigation.navigate(ERootStackRoutes.Units);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#1E1F24" />
      <Image style={styles.background} source={RECEIVED_BACKGROUND} />
      <XSvg
        style={styles.cancelIcon}
        onPress={handleCancelPress}
        fill="#FFFFFF"
      />
      <View style={styles.content}>
        <View style={styles.photo}>
          <CarSvg
            style={styles.car}
            width={152}
            height={152}
            strokeWidth={2.5}
          />
        </View>
        <Text style={styles.title}>Item Received</Text>
        {params?.unitYear && params?.unitManufacturer && params?.unitModel ? (
          <Text style={styles.description}>
            {`${params?.unitYear} ${params?.unitManufacturer} ${params?.unitModel} \n has been set as received`}
          </Text>
        ) : null}
      </View>
      <View style={styles.footer}>
        <Button
          buttonStyle={styles.button}
          textStyle={styles.buttonText}
          text="Go to In-Bound List"
          onPress={handleGoToInBoundPress}
        />
      </View>
    </View>
  );
};

export default ReceiveUnit;
