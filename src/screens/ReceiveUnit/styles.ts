import {StyleSheet} from 'react-native';

import {
  bottomSpace,
  deviceHeight,
  deviceWidth,
  statusBarHeight,
} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1E1E',
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: deviceWidth,
    height: deviceHeight,
  },
  cancelIcon: {
    position: 'absolute',
    top: statusBarHeight + 16,
    right: 20,
  },
  content: {
    alignItems: 'center',
    paddingBottom: 140,
  },
  photo: {
    width: 128,
    height: 128,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: '#CDFF48',
    overflow: 'hidden',
  },
  title: {
    color: '#FFFFFF',
    fontSize: 32,
    fontWeight: '700',
    marginTop: 48,
  },
  description: {
    color: '#FFFFFF',
    opacity: 0.5,
    textAlign: 'center',
    fontSize: 16,
    marginTop: 24,
  },
  footer: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    width: deviceWidth,
    marginTop: 48,
    paddingBottom: bottomSpace + 16,
    paddingHorizontal: 40,
  },
  button: {
    backgroundColor: '#FFFFFF',
  },
  buttonText: {
    color: '#000000',
  },
  car: {
    marginRight: -32,
  },
});
