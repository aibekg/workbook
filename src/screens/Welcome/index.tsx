import React, {useCallback, useMemo, useState} from 'react';
import {Keyboard, ScrollView, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useFormik} from 'formik';

import Button from '@components/Button';
import Input from '@components/Input';
import SelectAvatar from '@components/SelectAvatar';
import {useSaveImages} from '@hooks/useSaveImages';
import {useUpdateUserProfile} from '@hooks/useUpdateUserProfile';
import {TProfileForm} from '@screens/Profile/types';
import {selectUser} from '@store/auth/selectors';

import {schema} from './schema';
import {styles} from './styles';
import {TWelcomeField, TWelcomeForm} from './types';

const Welcome = () => {
  const user = useSelector(selectUser);

  const {
    updateUserProfile,
    isLoading: isUpdateUserLoading,
    error,
    setError,
  } = useUpdateUserProfile();
  const {saveImages} = useSaveImages();

  const [imgPath, setImgPath] = useState<string | null>(user?.photo || null);
  const [imgBase64, setImgBase64] = useState<string | null>(null);

  const {handleChange, handleSubmit, handleBlur, errors, values} =
    useFormik<TWelcomeForm>({
      initialValues: {
        userFirstName: user?.userFirstName || '',
        userLastName: user?.userLastName || '',
      },
      onSubmit: async input => {
        Keyboard.dismiss();

        if (imgPath === null && user?.photo) {
          return await updateUserProfile({
            input,
            photo: `delete ${user?.photo}`,
          });
        }

        if (imgPath !== user?.photo && imgBase64) {
          const photo = await saveImages({image: imgBase64});
          return await updateUserProfile({input, photo});
        }

        await updateUserProfile({input});
      },
      validationSchema: schema,
    });

  const welcomeFields: TWelcomeField[] = useMemo(
    () => [
      {
        name: 'userFirstName',
        label: 'First Name',
        placeholder: 'Enter Firstname',
      },
      {name: 'userLastName', label: 'Last Name', placeholder: 'Enter Lastname'},
    ],
    [],
  );

  const disabled = useMemo(
    () =>
      !(
        values.userLastName !== user?.userLastName ||
        values.userFirstName !== user?.userFirstName ||
        imgPath !== user?.photo
      ) || isUpdateUserLoading,
    [
      imgPath,
      isUpdateUserLoading,
      user?.photo,
      user?.userFirstName,
      user?.userLastName,
      values.userFirstName,
      values.userLastName,
    ],
  );

  const handleChangeText = useCallback(
    (name: keyof TProfileForm) => (value: string) => {
      setError('');
      handleChange(name)(value);
    },
    [setError, handleChange],
  );

  return (
    <View style={styles.container}>
      <View style={styles.block}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          style={styles.scroll}
          contentContainerStyle={styles.content}
          showsVerticalScrollIndicator={false}>
          <Text style={styles.title}>Welcome!</Text>
          <Text style={styles.subTitle}>Please fill out your profile</Text>
          {welcomeFields.map(({name, label, placeholder}) => (
            <Input
              key={name}
              placeholder={placeholder}
              onChangeText={handleChangeText(name)}
              onBlur={handleBlur(name)}
              value={values[name]}
              label={label}
              error={errors[name]}
            />
          ))}
          <SelectAvatar
            uri={imgPath}
            setUri={setImgPath}
            base64={imgBase64}
            setBase64={setImgBase64}
          />
        </ScrollView>
        <View style={styles.footer}>
          <Button
            error={error}
            disabled={disabled}
            text="Confirm"
            onPress={handleSubmit}
          />
        </View>
      </View>
    </View>
  );
};

export default Welcome;
