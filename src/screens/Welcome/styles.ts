import {StyleSheet} from 'react-native';

import {bottomSpace, statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1E1E1E',
    flex: 1,
  },
  block: {
    backgroundColor: '#FFFFFF',
    marginTop: statusBarHeight + 16,
    borderTopRightRadius: 24,
    borderTopLeftRadius: 24,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingBottom: bottomSpace + 16,
  },
  scroll: {
    flex: 1,
  },
  content: {
    paddingHorizontal: 12,
  },
  title: {
    marginTop: 28,
    marginLeft: 16,
    fontSize: 32,
    color: '#212121',
    fontWeight: '700',
  },
  subTitle: {
    marginVertical: 16,
    fontSize: 18,
    marginLeft: 16,
    color: '#212121',
    fontWeight: '400',
  },
  footer: {
    marginTop: 24,
    width: '100%',
    paddingHorizontal: 24,
    justifyContent: 'center',
  },
});
