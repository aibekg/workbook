export type TWelcomeForm = {
  userFirstName: string;
  userLastName: string;
};

export type TWelcomeField = {
  name: keyof TWelcomeForm;
  placeholder: string;
  label: string;
};
