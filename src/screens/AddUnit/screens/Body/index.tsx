import React, {useCallback} from 'react';
import {ScrollView, StatusBar, View} from 'react-native';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUnitBodies} from '@hooks/useUnitBodies';
import {useUpdateUnit} from '@hooks/useUpdateUnit';
import Convertible from '@icons/convertible.svg';
import Coupe from '@icons/coupe.svg';
import Hatchback from '@icons/hatchback.svg';
import Sedan from '@icons/sedan.svg';
import SUV from '@icons/suv.svg';
import Truck from '@icons/truck_body.svg';
import Van from '@icons/van.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/AddUnit/screens/Body/components/Item';
import {selectNewUnitData} from '@store/unit/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';

import {Unit} from '../../../../generated/graphql';
import {styles} from '../../styles';

const bodyIcons: any[] = [
  Sedan,
  Hatchback,
  SUV,
  Truck,
  Convertible,
  Van,
  Coupe,
];

const Body = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitBody>>();
  const route = useRoute<TNavigationRouteProp<ERootStackRoutes.AddUnitBody>>();

  const {bodyId} = useSelector(selectNewUnitData);

  const {bodies} = useUnitBodies();

  const {updateUnit} = useUpdateUnit();

  const handlePress = useCallback(async () => {
    if (route?.params?.independent) {
      try {
        const unit = await updateUnit({
          input: {
            vinNumber: route?.params?.vin,
            bodyId,
          },
        });
        if (unit?.id) {
          navigation.navigate({
            name: ERootStackRoutes.ReceiveUnitConfirm,
            params: {
              unit: unit as Unit,
            },
          });
        } else {
          Toast.show({
            type: 'error',
            text1: 'Something wrong',
            text2: 'Please try again later',
            visibilityTime: 4000,
            autoHide: true,
            topOffset: statusBarHeight + 16,
          });
        }
      } catch (e) {
        Toast.show({
          type: 'error',
          text1: 'Something wrong',
          text2: 'Please try again later',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: statusBarHeight + 16,
        });
      }
    } else {
      navigation.navigate({
        name: ERootStackRoutes.AddUnitColor,
        params: undefined,
      });
    }
  }, [navigation, bodyId, updateUnit, route?.params]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header search={false} title="BODY TYPE" />
      <ScrollView
        style={styles.list}
        contentContainerStyle={styles.content}
        showsVerticalScrollIndicator={false}>
        {bodies.map((item, index: number) => (
          <Item
            key={item.id}
            Icon={bodyIcons[index]}
            selected={bodyId}
            title={item.name!}
            value={item.id!}
          />
        ))}
      </ScrollView>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.AddUnitBody> />
        <Button
          disabled={!bodyId}
          hideWhenDisabled={true}
          buttonStyle={styles.button}
          text="Confirm"
          onPress={handlePress}
        />
      </View>
    </View>
  );
};

export default Body;
