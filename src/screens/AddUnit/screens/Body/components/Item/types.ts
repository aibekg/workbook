import React from 'react';
import {SvgProps} from 'react-native-svg';

export type TBodyItemProps = {
  title: string;
  value: number;
  selected?: number | null;
  Icon: React.FC<SvgProps>;
  onPress?: () => void;
};
