import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#90909040',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 28,
    borderRadius: 12,
    marginVertical: 10,
  },
  selectedContainer: {
    borderWidth: 1,
    backgroundColor: '#1E1F24',
    borderColor: '#90909040',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 28,
    borderRadius: 12,
    marginVertical: 10,
  },
  title: {
    flex: 1,
    marginRight: 16,
    fontSize: 28,
    fontWeight: '400',
    color: '#212121',
  },
  selectedTitle: {
    flex: 1,
    marginRight: 16,
    fontSize: 28,
    fontWeight: '400',
    color: '#FFFFFF',
  },
  opacity: {
    opacity: 0.5,
  },
});
