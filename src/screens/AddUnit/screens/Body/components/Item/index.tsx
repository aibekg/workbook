import React, {FC, useCallback, useMemo} from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {useActions} from '@hooks/useActions';

import {styles} from './styles';
import {TBodyItemProps} from './types';

const Item: FC<TBodyItemProps> = React.memo(
  ({title, selected, Icon, value, onPress}) => {
    const {setNewUnitDataFiled} = useActions();

    const handlePress = useCallback(() => {
      setNewUnitDataFiled({type: 'bodyId', value});
      onPress && onPress();
    }, [onPress, value, setNewUnitDataFiled]);

    const isSelected = useMemo(
      () => !!selected && selected === value,
      [selected, value],
    );

    const isNotSelected = useMemo(
      () => !!selected && selected !== value,
      [selected, value],
    );

    return (
      <TouchableOpacity
        style={[
          isSelected ? styles.selectedContainer : styles.container,
          isNotSelected && styles.opacity,
        ]}
        onPress={handlePress}>
        <Text style={isSelected ? styles.selectedTitle : styles.title}>
          {title}
        </Text>
        <Icon stroke={isSelected ? '#CDFF48' : '#212121'} />
      </TouchableOpacity>
    );
  },
);

export default Item;
