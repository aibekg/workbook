import React, {useCallback} from 'react';
import {ScrollView, StatusBar, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUnitTypes} from '@hooks/useUnitTypes';
import Car from '@icons/car.svg';
import Moto from '@icons/motorcycle.svg';
import Truck from '@icons/truck.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import {styles} from '@screens/AddUnit/styles';
import {selectNewUnitData} from '@store/unit/selectors';

import AddUnitHeader from './../../components/Header';
import Item from './components/Item';

const commodityTypeIcons: any[] = [Car, Moto, Truck];

const CommodityType = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitCommodity>>();

  const {commodityTypeId} = useSelector(selectNewUnitData);

  const {types} = useUnitTypes();

  const handlePress = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.AddUnitYear,
      params: undefined,
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <AddUnitHeader title="TYPE" search={false} />
      <ScrollView
        style={styles.list}
        contentContainerStyle={styles.content}
        showsVerticalScrollIndicator={false}>
        {types.map(({name, id}) => (
          <Item
            value={id!}
            Icon={commodityTypeIcons[id! - 1]}
            selected={commodityTypeId}
            key={id}
            title={name as string}
            disabled={id !== 1}
          />
        ))}
      </ScrollView>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.AddUnitCommodityType> />
        <Button
          disabled={!commodityTypeId}
          hideWhenDisabled={true}
          buttonStyle={styles.button}
          text="Confirm"
          onPress={handlePress}
        />
      </View>
    </View>
  );
};

export default CommodityType;
