import React from 'react';
import {SvgProps} from 'react-native-svg';

export type TCommodityItemProps = {
  title: string;
  value: number;
  selected?: number | null;
  Icon: React.FC<SvgProps>;
  onPress?: () => void;
  disabled?: boolean;
};
