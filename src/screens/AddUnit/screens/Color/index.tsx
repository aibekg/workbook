import React, {useCallback} from 'react';
import {ScrollView, StatusBar, View} from 'react-native';
import Toast from 'react-native-toast-message';
import {useSelector} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUnitColors} from '@hooks/useUnitColors';
import {useUpdateUnit} from '@hooks/useUpdateUnit';
import otherColorImage from '@icons/other_color.png';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/AddUnit/components/Item';
import {selectNewUnitData} from '@store/unit/selectors';
import {statusBarHeight} from '@utils/deviceHelpers';
import {capitalizeWord, getColorString} from '@utils/stringHelpers';

import {Unit} from '../../../../generated/graphql';
import {styles} from '../../styles';

const Color = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitColor>>();
  const route = useRoute<TNavigationRouteProp<ERootStackRoutes.AddUnitColor>>();

  const {colors} = useUnitColors();

  const {
    colorId,
    bodyId,
    manufacturer,
    commodityId,
    commodityTypeId,
    model,
    year,
    vin,
  } = useSelector(selectNewUnitData);
  const {updateUnit} = useUpdateUnit();

  const handlePress = useCallback(async () => {
    try {
      const unit = await updateUnit({
        input: route?.params?.independent
          ? {
              vinNumber: route?.params?.vin,
              colorId,
            }
          : {
              colorId,
              bodyId,
              comodityId: commodityId,
              comodityTypeId: commodityTypeId,
              manufacturer,
              model,
              vinNumber: vin,
              year,
            },
      });
      if (unit?.id) {
        navigation.navigate({
          name: ERootStackRoutes.ReceiveUnitConfirm,
          params: {
            unit: unit as Unit,
          },
        });
      } else {
        Toast.show({
          type: 'error',
          text1: 'Something wrong',
          text2: 'Please try again later',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: statusBarHeight + 16,
        });
      }
    } catch (e) {
      Toast.show({
        type: 'error',
        text1: 'Something wrong',
        text2: 'Please try again later',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: statusBarHeight + 16,
      });
    }
  }, [
    route?.params,
    navigation,
    colorId,
    bodyId,
    vin,
    updateUnit,
    manufacturer,
    commodityId,
    commodityTypeId,
    model,
    year,
  ]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header search={false} title="COLOR" />
      <ScrollView
        style={styles.paddedList}
        contentContainerStyle={styles.paddingContent}
        showsVerticalScrollIndicator={false}>
        {colors.map(item => (
          <Item
            key={item.id}
            type="colorId"
            image={
              item?.value?.toLowerCase() === 'other'
                ? otherColorImage
                : undefined
            }
            Prefix={
              item?.value?.toLowerCase() === 'other'
                ? undefined
                : getColorString(item?.value?.toLowerCase())
            }
            selected={colorId}
            title={capitalizeWord(item.value)}
            value={item.id}
          />
        ))}
      </ScrollView>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.AddUnitColor> />
        <Button
          disabled={!colorId}
          hideWhenDisabled={true}
          buttonStyle={styles.button}
          text="Confirm"
          onPress={handlePress}
        />
      </View>
    </View>
  );
};

export default Color;
