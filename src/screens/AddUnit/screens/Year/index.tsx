import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {Platform, ScrollView, StatusBar, View} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import HeaderSearch from '@components/HeaderSearch';
import {useUnitYears} from '@hooks/useUnitYears';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/AddUnit/components/Item';
import {selectNewUnitData} from '@store/unit/selectors';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {styles} from '../../styles';

const Year = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitYear>>();

  const {year} = useSelector(selectNewUnitData);

  const {years} = useUnitYears();

  const [searchEnabled, setSearchEnabled] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const searchToggle = useCallback(() => {
    if (searchEnabled) {
      setSearch('');
    }
    setSearchEnabled(!searchEnabled);
  }, [searchEnabled]);
  useEffect(() => {
    if (year) {
      setSearch('');
      setSearchEnabled(false);
    }
  }, [year]);

  const handlePress = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.AddUnitMake,
      params: undefined,
    });
  }, [navigation]);

  const filteredYears = useMemo(
    () => (search ? years.filter(item => `${item}`.includes(search)) : years),
    [search, years],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="YEAR" search={true} onSearch={searchToggle} />
      <ScrollView
        style={styles.paddedList}
        keyboardShouldPersistTaps="always"
        contentContainerStyle={styles.paddingContent}
        showsVerticalScrollIndicator={false}>
        {filteredYears.map(item => (
          <Item
            type="year"
            key={item}
            selected={year}
            title={`${item}`}
            value={item}
          />
        ))}
      </ScrollView>
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitYear> />
          <Button
            disabled={!year}
            hideWhenDisabled={true}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      )}
      {searchEnabled ? (
        <HeaderSearch
          search={search}
          setSearch={setSearch}
          onClose={searchToggle}
        />
      ) : null}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default Year;
