import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Platform,
  StatusBar,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import HeaderSearch from '@components/HeaderSearch';
import {useUnitManufacturers} from '@hooks/useUnitManufacturers';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/AddUnit/components/Item';
import {selectNewUnitData} from '@store/unit/selectors';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {styles} from '../../styles';

const Make = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitMake>>();

  const {manufacturer} = useSelector(selectNewUnitData);

  const [searchEnabled, setSearchEnabled] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const searchToggle = useCallback(() => {
    if (searchEnabled) {
      setSearch('');
    }
    setSearchEnabled(!searchEnabled);
  }, [searchEnabled]);
  useEffect(() => {
    if (manufacturer) {
      setSearch('');
      setSearchEnabled(false);
    }
  }, [manufacturer]);

  const {manufacturers, isLoading, isPaginating, getUnitManufacturers} =
    useUnitManufacturers(search);

  const handlePress = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.AddUnitModel,
      params: undefined,
    });
  }, [navigation]);

  const keyExtractor = useCallback((item: string) => item.toString(), []);

  const getMoreManufacturers = useCallback(
    () => getUnitManufacturers(),
    [getUnitManufacturers],
  );

  const renderItem = useCallback(
    ({item}: any) => {
      return (
        <Item
          type="manufacturer"
          selected={manufacturer}
          title={item}
          value={item}
        />
      );
    },
    [manufacturer],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="MAKE" search={true} onSearch={searchToggle} />
      <FlatList
        keyExtractor={keyExtractor}
        data={manufacturers}
        style={styles.paddedList}
        removeClippedSubviews={true}
        contentContainerStyle={styles.paddingContent}
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        onEndReached={getMoreManufacturers}
        onEndReachedThreshold={0.1}
        ListEmptyComponent={
          isLoading && !isPaginating ? <ActivityIndicator size="small" /> : null
        }
        ListFooterComponent={
          isPaginating ? <ActivityIndicator size="small" /> : null
        }
      />
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitMake> />
          <Button
            disabled={!manufacturer}
            hideWhenDisabled={true}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      )}
      {searchEnabled ? (
        <HeaderSearch
          search={search}
          setSearch={setSearch}
          onClose={searchToggle}
        />
      ) : null}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default Make;
