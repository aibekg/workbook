import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Platform,
  StatusBar,
  Text,
  View,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import HeaderSearch from '@components/HeaderSearch';
import {useUnitModels} from '@hooks/useUnitModels';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Header from '@screens/AddUnit/components/Header';
import Item from '@screens/AddUnit/components/Item';
import {selectNewUnitData} from '@store/unit/selectors';
import {bottomSpace, useKeyboardVisibility} from '@utils/deviceHelpers';

import {styles} from '../../styles';

const Model = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitMake>>();

  const {manufacturer, model} = useSelector(selectNewUnitData);

  const [searchEnabled, setSearchEnabled] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const searchToggle = useCallback(() => {
    if (searchEnabled) {
      setSearch('');
    }
    setSearchEnabled(!searchEnabled);
  }, [searchEnabled]);
  useEffect(() => {
    if (model) {
      setSearch('');
      setSearchEnabled(false);
    }
  }, [model]);

  const {models, getUnitModels, isLoading, isPaginating} =
    useUnitModels(search);

  const handlePress = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.AddUnitBody,
      params: undefined,
    });
  }, [navigation]);

  const keyExtractor = useCallback((item: string) => item.toString(), []);

  const getMoreModels = useCallback(() => getUnitModels(), [getUnitModels]);

  const renderItem = useCallback(
    ({item}: any) => (
      <Item
        type="model"
        selected={model}
        exactMatch={true}
        title={item}
        value={item}
      />
    ),
    [model],
  );

  const {isKeyboardVisible} = useKeyboardVisibility();

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <Header title="MODEL" search={true} onSearch={searchToggle} />
      {!searchEnabled && manufacturer ? (
        <Text style={styles.selectText}>{manufacturer}</Text>
      ) : null}
      <FlatList
        keyExtractor={keyExtractor}
        data={models}
        style={styles.paddedList}
        removeClippedSubviews={true}
        contentContainerStyle={styles.paddingContent}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        renderItem={renderItem}
        onEndReached={getMoreModels}
        onEndReachedThreshold={0.1}
        ListEmptyComponent={
          isLoading && !isPaginating ? <ActivityIndicator size="small" /> : null
        }
        ListFooterComponent={
          isPaginating ? <ActivityIndicator size="small" /> : null
        }
      />
      {isKeyboardVisible ? null : (
        <View style={styles.buttonContainer}>
          <BackButton<ERootStackRoutes.AddUnitModel> />
          <Button
            disabled={!model}
            hideWhenDisabled={true}
            buttonStyle={styles.button}
            text="Confirm"
            onPress={handlePress}
          />
        </View>
      )}
      {searchEnabled ? (
        <HeaderSearch
          search={search}
          setSearch={setSearch}
          onClose={searchToggle}
        />
      ) : null}
      {Platform.OS === 'ios' && <KeyboardSpacer topSpacing={-bottomSpace} />}
    </View>
  );
};

export default Model;
