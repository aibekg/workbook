import React from 'react';
import {SvgProps} from 'react-native-svg';

export type TItem = {
  title: string;
  Icon: React.FC<SvgProps>;
  value: number;
};
