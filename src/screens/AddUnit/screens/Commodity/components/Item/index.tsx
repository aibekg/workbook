import React, {FC, useCallback, useMemo} from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {useActions} from '@hooks/useActions';

import {styles} from './styles';
import {TCommodityItemProps} from './types';

const Item: FC<TCommodityItemProps> = React.memo(
  ({title, selected, Icon, disabled, value, onPress}) => {
    const {setNewUnitDataFiled} = useActions();

    const handlePress = useCallback(() => {
      setNewUnitDataFiled({type: 'commodityId', value});
      onPress && onPress();
    }, [onPress, value, setNewUnitDataFiled]);

    const isSelected = useMemo(
      () => !!selected && selected === value,
      [selected, value],
    );

    const isNotSelected = useMemo(
      () => !!selected && selected !== value,
      [selected, value],
    );

    return (
      <TouchableOpacity
        disabled={disabled}
        style={[
          isSelected ? styles.selectedContainer : styles.container,
          isNotSelected && styles.opacity,
        ]}
        onPress={handlePress}>
        <Text style={isSelected ? styles.selectedTitle : styles.title}>
          {title}
        </Text>
        <Icon fill={isSelected ? '#CDFF48' : '#212121'} />
      </TouchableOpacity>
    );
  },
);

export default Item;
