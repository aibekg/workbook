import React, {useCallback} from 'react';
import {ScrollView, StatusBar, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import BackButton from '@components/BackButton';
import Button from '@components/Button';
import {useUnitComodities} from '@hooks/useUnitComodities';
import Boats from '@icons/comodity_boats.svg';
import Cars from '@icons/comodity_cars.svg';
import DryCargo from '@icons/comodity_dry.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import Item from '@screens/AddUnit/screens/Commodity/components/Item';
import {styles} from '@screens/AddUnit/styles';
import {selectNewUnitData} from '@store/unit/selectors';

import AddUnitHeader from './../../components/Header';

const commodityIcons: any[] = [Cars, Boats, DryCargo];

const Commodity = () => {
  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.AddUnitCommodity>>();

  const {commodityId} = useSelector(selectNewUnitData);

  const {commodities} = useUnitComodities();

  const handlePress = useCallback(() => {
    navigation.navigate({
      name: ERootStackRoutes.AddUnitCommodityType,
      params: undefined,
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#FFFFFF" />
      <AddUnitHeader title="COMMODITY" search={false} />
      <ScrollView
        style={styles.list}
        contentContainerStyle={styles.content}
        showsVerticalScrollIndicator={false}>
        {commodities.map(({name, id}) => (
          <Item
            value={id!}
            Icon={commodityIcons[id! - 1]}
            selected={commodityId}
            key={id}
            title={name as string}
            disabled={id !== 1}
          />
        ))}
      </ScrollView>
      <View style={styles.buttonContainer}>
        <BackButton<ERootStackRoutes.AddUnitCommodity> />
        <Button
          disabled={!commodityId}
          hideWhenDisabled={true}
          buttonStyle={styles.button}
          text="Confirm"
          onPress={handlePress}
        />
      </View>
    </View>
  );
};

export default Commodity;
