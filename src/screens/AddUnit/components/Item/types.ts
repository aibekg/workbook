import React from 'react';
import {ImageSourcePropType} from 'react-native';
import {SvgProps} from 'react-native-svg';

import {TNewUnitData} from '@store/unit/types';

export type TItemProps = {
  title: string;
  value: string | number;
  selected?: string | number | null;
  exactMatch?: boolean;
  Prefix?: React.FC<SvgProps> | string;
  image?: ImageSourcePropType;
  onPress?: (item?: string | number) => void;
  type: keyof TNewUnitData;
};
