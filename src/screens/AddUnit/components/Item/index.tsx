import React, {FC, useCallback, useEffect, useMemo} from 'react';
import {Image, Keyboard, Text, TouchableOpacity, View} from 'react-native';

import {useActions} from '@hooks/useActions';
import {TItemProps} from '@screens/AddUnit/components/Item/types';

import {styles} from './styles';

const Item: FC<TItemProps> = React.memo(
  ({title, value, Prefix, onPress, image, exactMatch, selected, type}) => {
    const {setNewUnitDataFiled} = useActions();

    const handlePress = useCallback(() => {
      Keyboard.dismiss();
      setNewUnitDataFiled({type, value: value});
      onPress && onPress(value);
    }, [onPress, setNewUnitDataFiled, value, type]);

    const isSelected = useMemo(
      () =>
        !!selected &&
        (selected === value ||
          (!exactMatch &&
            typeof selected === 'string' &&
            typeof value === 'string' &&
            (selected.includes(value) || value.includes(selected)))),
      [selected, exactMatch, value],
    );

    const isNotSelected = useMemo(
      () =>
        !!selected &&
        !(
          selected === value ||
          (!exactMatch &&
            typeof selected === 'string' &&
            typeof value === 'string' &&
            (selected.includes(value) || value.includes(selected)))
        ),
      [selected, exactMatch, value],
    );

    useEffect(() => {
      if (
        !exactMatch &&
        selected &&
        value &&
        typeof selected === 'string' &&
        typeof value === 'string'
      ) {
        if (selected.includes(value)) {
          setNewUnitDataFiled({type, value: value});
        } else if (value.includes(selected)) {
          setNewUnitDataFiled({type, value: selected});
        }
      }
    }, [type, setNewUnitDataFiled, selected, value, exactMatch]);

    const colorStyle = useMemo(
      () =>
        type === 'colorId' && typeof Prefix === 'string'
          ? [
              styles.circle,
              {
                backgroundColor: Prefix,
                borderWidth:
                  Prefix.toUpperCase() === '#FFFFFF' ||
                  Prefix.toLowerCase() === 'white'
                    ? 1
                    : 0,
              },
            ]
          : undefined,
      [type, Prefix],
    );

    return (
      <TouchableOpacity
        style={[
          isSelected ? styles.selectedContainer : styles.container,
          isNotSelected && styles.opacity,
        ]}
        onPress={handlePress}>
        {image ? (
          <Image style={styles.image} source={image} resizeMode="contain" />
        ) : Prefix ? (
          type === 'colorId' && typeof Prefix === 'string' ? (
            <View style={colorStyle} />
          ) : (
            <Prefix style={styles.icon} width={32} height={32} />
          )
        ) : null}
        <Text style={isSelected ? styles.selectedTitle : styles.title}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  },
);

export default Item;
