import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingVertical: 8,
    paddingHorizontal: 12,
    alignItems: 'center',
    flexDirection: 'row',
  },
  selectedContainer: {
    borderRadius: 12,
    width: '100%',
    backgroundColor: '#1E1F24',
    paddingVertical: 8,
    paddingHorizontal: 12,
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    fontSize: 28,
    color: '#212121',
  },
  selectedTitle: {
    fontSize: 28,
    color: '#FFFFFF',
  },
  circle: {
    width: 32,
    height: 32,
    borderRadius: 16,
    marginRight: 16,
    borderColor: '#90909040',
  },
  icon: {
    marginRight: 16,
  },
  image: {
    width: 32,
    height: 32,
    marginRight: 16,
  },
  opacity: {
    opacity: 0.5,
  },
});
