import React, {FC} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import SearchSvg from '@icons/search.svg';

import {styles} from './styles';
import {TAddUnitHeaderProps} from './types';

const Header: FC<TAddUnitHeaderProps> = React.memo(
  ({title, search = true, style, onSearch}) => {
    return (
      <View style={[styles.container, style]}>
        <Text style={styles.title}>{title}</Text>
        {search ? (
          <TouchableOpacity
            hitSlop={{top: 24, bottom: 24, left: 24, right: 24}}
            onPress={onSearch}>
            <SearchSvg />
          </TouchableOpacity>
        ) : (
          <View style={styles.placeholder} />
        )}
      </View>
    );
  },
);

export default Header;
