import {StyleSheet} from 'react-native';

import {statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    paddingTop: 20 + statusBarHeight,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 28,
    paddingBottom: 20,
  },
  title: {
    fontSize: 12,
    fontWeight: '700',
    color: '#212121',
  },
  placeholder: {
    height: 20,
  },
});
