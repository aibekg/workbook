import {StyleProp, ViewStyle} from 'react-native';

export type TAddUnitHeaderProps = {
  title: string;
  search?: boolean;
  onSearch?: () => void;
  style?: StyleProp<ViewStyle>;
};
