import {StyleSheet} from 'react-native';

import {statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
  },
  header: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: statusBarHeight + 16,
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 12,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: '#212121',
  },
  inputWrapper: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 24,
  },
  inputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  input: {
    width: '100%',
    backgroundColor: 'transparent',
    textAlign: 'center',
    color: '#596376',
    paddingHorizontal: 0,
    fontSize: 26,
    letterSpacing: -0.5,
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  inputPlainText: {
    fontSize: 26,
    letterSpacing: -0.5,
    color: '#596376',
  },
  inputTextBlack: {
    fontSize: 26,
    letterSpacing: -0.5,
    color: '#212121',
  },
  inputPlace: {
    fontSize: 26,
    letterSpacing: -0.5,
    color: '#E2E5EA',
  },
  buttonContainer: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  button: {
    marginHorizontal: 32,
  },
  errorButton: {
    backgroundColor: 'transparent',
    marginHorizontal: 0,
  },
  errorText: {
    fontWeight: '400',
    color: '#8e8f91',
  },
});
