import React, {useCallback, useMemo, useState} from 'react';
import {
  Keyboard,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';

import Button from '@components/Button';
import Input from '@components/Input';
import {VinKeyboard} from '@components/VinKeyboard';
import {useActions} from '@hooks/useActions';
import {useVinData} from '@hooks/useVinData';
import ScanSvg from '@icons/scan-black.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TNavigationRouteProp,
} from '@routes/types';
import {validateVin} from '@utils/stringHelpers';

import {Unit} from '../../generated/graphql';

import {styles} from './styles';

const EnterVin = () => {
  const {setNewUnitDataFiled, clearUnitDataFiled} = useActions();

  const navigation =
    useNavigation<TNavigationProp<ERootStackRoutes.EnterVin>>();

  const route = useRoute<TNavigationRouteProp<ERootStackRoutes.EnterVin>>();

  const [vin, setVin] = useState<string>('');

  const {getVinData} = useVinData();

  const handleCancel = useCallback(() => {
    navigation.replace(ERootStackRoutes.CodeScanner);
  }, [navigation]);

  const handleChange = useCallback((value: string) => {
    setVin(value);
  }, []);

  const vinDisplay = useMemo(() => {
    const characters = vin.split('');
    const chars: any[] = [];
    characters.forEach((char: string, index: number) => {
      if (index >= characters.length - 7 && index >= 10) {
        chars.push(
          <Text key={`${char}+${index}`} style={styles.inputTextBlack}>
            {char.toUpperCase()}
          </Text>,
        );
      } else {
        chars.push(char.toUpperCase());
      }
    });
    if (chars.length < 15) {
      for (let charIndex = chars.length; charIndex < 15; charIndex++) {
        chars.push(
          <Text key={`X+${charIndex}`} style={styles.inputPlace}>
            {'X'}
          </Text>,
        );
      }
    }
    return chars;
  }, [vin]);

  const isValidVin = useMemo(() => !!vin && validateVin(vin), [vin]);

  const handleContinue = useCallback(async () => {
    if (isValidVin) {
      const vinDataResponse = await getVinData({
        vin,
        photo: route?.params?.vinPhoto || null,
      });
      clearUnitDataFiled();
      setNewUnitDataFiled({
        type: 'vin',
        value: vin || null,
      });
      if (vinDataResponse?.unit) {
        setNewUnitDataFiled({
          type: 'year',
          value: vinDataResponse?.unit?.year || null,
        });
        setNewUnitDataFiled({
          type: 'manufacturer',
          value: vinDataResponse?.unit?.manufacturer || null,
        });
        setNewUnitDataFiled({
          type: 'model',
          value: vinDataResponse?.unit?.model || null,
        });
        setNewUnitDataFiled({
          type: 'bodyId',
          value: vinDataResponse?.unit?.body?.id || null,
        });
        setNewUnitDataFiled({
          type: 'colorId',
          value: vinDataResponse?.unit?.color?.id || null,
        });
        setTimeout(() => {
          if (vinDataResponse?.unit) {
            navigation.navigate({
              name: ERootStackRoutes.ReceiveUnitConfirm,
              params: {
                unit: vinDataResponse?.unit as Unit,
              },
            });
          }
        }, 600);
      } else {
        if (vinDataResponse?.vinData) {
          if (vinDataResponse?.vinData?.manufacturer) {
            setNewUnitDataFiled({
              type: 'manufacturer',
              value: vinDataResponse?.vinData?.manufacturer || null,
            });
          }
          if (vinDataResponse?.vinData?.year) {
            setNewUnitDataFiled({
              type: 'year',
              value: vinDataResponse?.vinData?.year || null,
            });
          }
        }
        setTimeout(() => {
          navigation.navigate({
            name: ERootStackRoutes.AddUnitCommodity,
            params: undefined,
          });
        }, 600);
      }
    } else {
      navigation.replace(ERootStackRoutes.EnterNonStandardVin, {
        vin,
        vinPhoto: route?.params.vinPhoto,
      });
    }
  }, [
    route?.params,
    isValidVin,
    clearUnitDataFiled,
    setNewUnitDataFiled,
    vin,
    getVinData,
    navigation,
  ]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={Platform.OS === 'android' ? 'light-content' : 'dark-content'}
        backgroundColor="#1E1F24"
      />
      <View style={styles.header}>
        <Text style={styles.title}>Enter VIN</Text>
        <TouchableOpacity
          hitSlop={{top: 8, bottom: 8, left: 8, right: 8}}
          onPress={handleCancel}>
          <ScanSvg width={24} height={24} />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        activeOpacity={1}
        onPress={Keyboard.dismiss}
        style={styles.container}>
        <View style={styles.inputWrapper}>
          <View style={styles.inputContainer}>
            <Input
              autoFocus={false}
              editable={false}
              allowFontScaling={true}
              keyboardType={
                Platform.OS === 'ios' ? 'default' : 'visible-password'
              }
              style={styles.input}>
              <Text>{vinDisplay}</Text>
            </Input>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            text={isValidVin ? 'Continue' : 'Non Standard VIN or Other ID'}
            buttonStyle={isValidVin ? styles.button : styles.errorButton}
            textStyle={isValidVin ? undefined : styles.errorText}
            onPress={handleContinue}
          />
        </View>
        <VinKeyboard onChange={handleChange} vin={vin} />
      </TouchableOpacity>
    </View>
  );
};

export default EnterVin;
