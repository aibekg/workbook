import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';

import {THiddenData} from '@hooks/useInBoundUnits';
import {TPhoto} from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet/types';
import {TDocumentPhotos} from '@screens/Inspection/DocumentAddition';
import {EExteriorSide} from '@screens/Inspection/ExteriorSide/types';
import {EInteriorSide} from '@screens/Inspection/InteriorSide/types';
import {TEquipmentTitle} from '@store/unit/types';

import {Unit as UnitType, WarehouseArea} from '../generated/graphql';

export enum ERootStackRoutes {
  Login = 'Login',
  Profile = 'Profile',
  AddUnitCommodity = 'AddUnitCommodity',
  AddUnitCommodityType = 'AddUnitCommodityType',
  AddUnitYear = 'AddUnitYear',
  AddUnitMake = 'AddUnitMake',
  AddUnitModel = 'AddUnitModel',
  AddUnitColor = 'AddUnitColor',
  AddUnitBody = 'AddUnitBody',
  CarrierCompany = 'CarrierCompany',
  CarrierDriver = 'CarrierDriver',
  Environment = 'Environment',
  ReceiveUnitConfirm = 'ReceiveUnitConfirm',
  ReceiveUnit = 'ReceiveUnit',
  CodeScanner = 'CodeScanner',
  Welcome = 'Welcome',
  Units = 'Units',
  EnterVin = 'EnterVin',
  EnterNonStandardVin = 'EnterNonStandardVin',
  Unit = 'Unit',
  MoreUnits = 'MoreUnits',
  Signature = 'Signature',
  KeysConfirm = 'KeysConfirm',
  KeysAddition = 'KeysAddition',
  DocumentAddition = 'DocumentAddition',
  DocumentConfirm = 'DocumentConfirm',
  Equipment = 'Equipment',
  EquipmentConfirm = 'EquipmentConfirm',
  LocationArea = 'LocationArea',
  LocationLot = 'LocationLot',
  ExteriorInspection = 'ExteriorInspection',
  ExteriorSide = 'ExteriorSide',
  InteriorInspection = 'InteriorInspection',
  InteriorSide = 'InteriorSide',
}

export type TRootStackParamList = {
  [ERootStackRoutes.Login]: undefined;
  [ERootStackRoutes.Profile]: undefined;
  [ERootStackRoutes.AddUnitCommodity]: undefined;
  [ERootStackRoutes.AddUnitCommodityType]: undefined;
  [ERootStackRoutes.AddUnitYear]: undefined;
  [ERootStackRoutes.AddUnitMake]: undefined;
  [ERootStackRoutes.AddUnitModel]: undefined;
  [ERootStackRoutes.AddUnitBody]:
    | undefined
    | {
        vin: string;
        independent: boolean;
      };
  [ERootStackRoutes.AddUnitColor]:
    | undefined
    | {
        vin: string;
        independent: boolean;
      };
  [ERootStackRoutes.CodeScanner]: undefined;
  [ERootStackRoutes.Welcome]: undefined;
  [ERootStackRoutes.Units]: undefined;
  [ERootStackRoutes.EnterVin]: {vinPhoto: string | null};
  [ERootStackRoutes.EnterNonStandardVin]: {
    vin: string;
    vinPhoto: string | null;
  };
  [ERootStackRoutes.Unit]: {
    unitId: string;
  };
  [ERootStackRoutes.MoreUnits]: {
    hiddenData: THiddenData[];
  };
  [ERootStackRoutes.CarrierCompany]: undefined;
  [ERootStackRoutes.CarrierDriver]: {
    carrierCompanyName: string;
    carrierCompanyId: string;
  };
  [ERootStackRoutes.Environment]: undefined;
  [ERootStackRoutes.ReceiveUnitConfirm]: {
    unit: UnitType;
  };
  [ERootStackRoutes.ReceiveUnit]: {
    unitManufacturer: string | null;
    unitModel: string | null;
    unitYear: number | null;
  };
  [ERootStackRoutes.KeysConfirm]: {
    photo: TPhoto;
    manualKeysCount: number;
    remoteKeysCount: number;
  };
  [ERootStackRoutes.KeysAddition]: undefined;
  [ERootStackRoutes.DocumentAddition]: undefined;
  [ERootStackRoutes.DocumentConfirm]: {
    photos: TDocumentPhotos[];
  };
  [ERootStackRoutes.Signature]: {
    onScan: (signatureBase64: string) => void;
  };
  [ERootStackRoutes.Equipment]: undefined;
  [ERootStackRoutes.EquipmentConfirm]: {
    selectedEquipments: TEquipmentTitle[];
  };
  [ERootStackRoutes.LocationArea]: {
    unit: UnitType;
  };
  [ERootStackRoutes.LocationLot]: {
    unit: UnitType;
    warehouseArea: WarehouseArea;
  };
  [ERootStackRoutes.ExteriorInspection]: {
    side: EExteriorSide;
    showCamera: boolean;
  };
  [ERootStackRoutes.ExteriorSide]: undefined;
  [ERootStackRoutes.InteriorInspection]: {
    side: EInteriorSide;
    showCamera: boolean;
  };
  [ERootStackRoutes.InteriorSide]: undefined;
};

export type TNavigationProp<RouteName extends keyof TRootStackParamList> =
  NativeStackNavigationProp<TRootStackParamList, RouteName>;

export type TNavigationRouteProp<RouteName extends keyof TRootStackParamList> =
  RouteProp<TRootStackParamList, RouteName>;
