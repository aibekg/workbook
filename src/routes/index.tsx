import {StackNavigationOptions} from '@react-navigation/stack';

import {ERootStackRoutes} from '@routes/types';
import Body from '@screens/AddUnit/screens/Body';
import Color from '@screens/AddUnit/screens/Color';
import Commodity from '@screens/AddUnit/screens/Commodity';
import CommodityType from '@screens/AddUnit/screens/CommodityType';
import Make from '@screens/AddUnit/screens/Make';
import Model from '@screens/AddUnit/screens/Model';
import Year from '@screens/AddUnit/screens/Year';
import CodeScanner from '@screens/CodeScanner';
import EnterNonStandardVin from '@screens/EnterNonStandardVin';
import EnterVin from '@screens/EnterVin';
import Carrier from '@screens/Inspection/CarrierCompany';
import Driver from '@screens/Inspection/CarrierDriver';
import DocumentAddition from '@screens/Inspection/DocumentAddition';
import DocumentConfirm from '@screens/Inspection/DocumentConfirm';
import Environment from '@screens/Inspection/Environment';
import EquipmentConfirm from '@screens/Inspection/EquimpmentConfirm';
import Equipment from '@screens/Inspection/Equipment';
import ExteriorInspection from '@screens/Inspection/Exterior';
import ExteriorSide from '@screens/Inspection/ExteriorSide';
import KeysAddition from '@screens/Inspection/KeysAddition';
import KeysConfirm from '@screens/Inspection/KeysConfirm';
import WarehouseArea from '@screens/Location/WarehouseArea';
import WarehouseLot from '@screens/Location/WarehouseLot';
import Login from '@screens/Login';
import Profile from '@screens/Profile';
import ReceiveUnit from '@screens/ReceiveUnit';
import ReceiveUnitConfirm from '@screens/ReceiveUnitConfirm';
import Signature from '@screens/Signature';
import Unit from '@screens/Unit';
import Units from '@screens/Units';
import MoreUnits from '@screens/Units/screens/MoreUnits';
import Welcome from '@screens/Welcome';

export const AUTH_STACK_SCREENS = [
  {name: ERootStackRoutes.Login, component: Login},
];

const forFade = ({current}: any) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

const modalScreenOptions = {
  gestureEnabled: true,
  presentation: 'modal',
  animation: 'fade_from_bottom',
};

export const APP_STACK_SCREENS = [
  {
    name: ERootStackRoutes.Units,
    component: Units,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Profile,
    component: Profile,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Welcome,
    component: Welcome,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitCommodity,
    component: Commodity,
  },
  {
    name: ERootStackRoutes.AddUnitCommodityType,
    component: CommodityType,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitMake,
    component: Make,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitColor,
    component: Color,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitModel,
    component: Model,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitYear,
    component: Year,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.AddUnitBody,
    component: Body,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.CodeScanner,
    component: CodeScanner,
    options: {
      animationEnabled: true,
      gestureEnabled: false,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.EnterVin,
    component: EnterVin,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.EnterNonStandardVin,
    component: EnterNonStandardVin,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.CarrierCompany,
    component: Carrier,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.CarrierDriver,
    component: Driver,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.ReceiveUnitConfirm,
    component: ReceiveUnitConfirm,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.ReceiveUnit,
    component: ReceiveUnit,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Environment,
    component: Environment,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Signature,
    component: Signature,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Unit,
    component: Unit,
    options: modalScreenOptions,
  },
  {
    name: ERootStackRoutes.MoreUnits,
    component: MoreUnits,
    options: modalScreenOptions,
  },
  {
    name: ERootStackRoutes.KeysConfirm,
    component: KeysConfirm,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.KeysAddition,
    component: KeysAddition,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.DocumentAddition,
    component: DocumentAddition,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.DocumentConfirm,
    component: DocumentConfirm,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.LocationArea,
    component: WarehouseArea,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.LocationLot,
    component: WarehouseLot,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.ExteriorSide,
    component: ExteriorSide,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.ExteriorInspection,
    component: ExteriorInspection,
    options: {
      gestureEnabled: false,
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.Equipment,
    component: Equipment,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
  {
    name: ERootStackRoutes.EquipmentConfirm,
    component: EquipmentConfirm,
    options: {
      animationEnabled: true,
      cardStyleInterpolator: forFade,
    } as StackNavigationOptions,
  },
];
