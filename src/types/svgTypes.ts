// @ts-ignore
import * as I from 'kld-intersections';

export type ShapeIdType = string | null | undefined;

export type SVGPathPart = {
  kind: string;
  points: {x: number; y: number}[];
};

export type GeneratedAffineShape = {
  id: ShapeIdType;
  shape: I.AffineShape;
  includes: ShapeIdType[];
};
