declare module '@env' {
  export const GRAPHQL_ENDPOINT: string;
  export const ENCRYPTION_KEY: string;
  export const USER_PHOTO_URL: string;
}

declare module '*.svg' {
  import React from 'react';
  import {SvgProps} from 'react-native-svg';
  const content: React.FC<SvgProps>;
  export default content;
}

declare module '*.png' {}

// declare module 'react-native-keyboard-spacer' {
//   import KeyboardSpacer from 'react-native-keyboard-spacer';
//   export default KeyboardSpacer;
// }
