import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  goBackButton: {
    marginRight: 12,
    backgroundColor: '#FFFFFF',
    width: 96,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 28,
    borderColor: '#21212126',
    padding: 20,
  },
});
