import React, {useCallback} from 'react';
import {TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import ArrowLeftSvg from '@icons/arrow-left.svg';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';

import {styles} from './styles';
import {TFooterProps} from './types';

const BackButton = <T extends ERootStackRoutes>({style}: TFooterProps) => {
  const navigation = useNavigation<TNavigationProp<T>>();

  const handleBackPress = useCallback(() => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
  }, [navigation]);

  return (
    <TouchableOpacity
      onPress={handleBackPress}
      style={[styles.goBackButton, style]}>
      <ArrowLeftSvg fill="#212121" />
    </TouchableOpacity>
  );
};

export default React.memo(BackButton) as typeof BackButton;
