import {ReactNode} from 'react';
import {StyleProp, ViewStyle} from 'react-native';

export type TFooterProps = {
  children?: ReactNode;
  style?: StyleProp<ViewStyle>;
};
