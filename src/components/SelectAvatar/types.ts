import {Dispatch, SetStateAction} from 'react';
import {ImageStyle, StyleProp, ViewStyle} from 'react-native';

export type TSelectAvatar = {
  uri: string | null;
  setUri: Dispatch<SetStateAction<string | null>>;
  base64: string | null;
  setBase64: Dispatch<SetStateAction<string | null>>;
  style?: StyleProp<ViewStyle>;
  imageStyle?: StyleProp<ImageStyle>;
};
