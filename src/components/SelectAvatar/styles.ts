import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  imageContainer: {
    height: 132,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    backgroundColor: '#CDFF48',
    borderRadius: 12,
    paddingHorizontal: 12,
  },
  addPhoto: {
    width: '100%',
    borderStyle: 'dashed',
    borderColor: '#1E1F24',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 12,
    height: 98,
  },
  addPhotoText: {
    color: '#000000',
    fontWeight: '700',
    marginLeft: 8,
  },
  image: {
    width: 112,
    height: 112,
    borderRadius: 56,
  },
  imageTouch: {
    width: 112,
    height: 112,
  },
  cancelIcon: {
    position: 'absolute',
    right: 12,
    top: 12,
  },
});
