import React, {FC, useCallback} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';
import {USER_PHOTO_URL} from '@env';
import {useActionSheet} from '@expo/react-native-action-sheet';

import CirclePlusSvg from '@icons/circle-plus.svg';
import XCircleSvg from '@icons/x-circle.svg';
import {selectUser} from '@store/auth/selectors';
import {pickDocument} from '@utils/imagePickerHelpers';

import {styles} from './styles';
import {TSelectAvatar} from './types';

const SelectAvatar: FC<TSelectAvatar> = ({
  uri,
  setUri,
  style,
  imageStyle,
  setBase64,
}) => {
  const user = useSelector(selectUser);

  const {showActionSheetWithOptions} = useActionSheet();

  const handlePressAddPhoto = useCallback(async () => {
    try {
      const pickerResponse = await pickDocument(showActionSheetWithOptions);
      if (pickerResponse.data) {
        setBase64(pickerResponse.data);
      }
      setUri(pickerResponse.path);
    } catch (e) {}
  }, [showActionSheetWithOptions, setBase64, setUri]);

  const handleDeletePhoto = useCallback(() => {
    setUri(null);
    setBase64(null);
  }, [setBase64, setUri]);

  return uri ? (
    <View style={[styles.imageContainer, style]}>
      <TouchableOpacity
        onPress={handlePressAddPhoto}
        style={[styles.imageTouch, imageStyle]}>
        <Image
          style={styles.image}
          source={{uri: user?.photo === uri ? USER_PHOTO_URL + uri : uri}}
        />
      </TouchableOpacity>
      <XCircleSvg
        style={styles.cancelIcon}
        onPress={handleDeletePhoto}
        fill="#000000"
      />
    </View>
  ) : (
    <View style={[styles.imageContainer, style]}>
      <TouchableOpacity onPress={handlePressAddPhoto} style={styles.addPhoto}>
        <CirclePlusSvg fill="#000000" />
        <Text style={styles.addPhotoText}>Add Photo</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SelectAvatar;
