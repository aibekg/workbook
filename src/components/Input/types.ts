import {StyleProp, TextInputProps, ViewStyle} from 'react-native';

export type TInput = TextInputProps & {
  label?: string;
  error?: string;
  containerStyle?: StyleProp<ViewStyle>;
};
