import React, {FC} from 'react';
import {Text, TextInput, View} from 'react-native';

import {TInput} from '@components/Input/types';

import {styles} from './styles';

const Input: FC<TInput> = ({
  label,
  value,
  error,
  containerStyle,
  style,
  ...props
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      {label ? <Text style={styles.label}>{label}</Text> : null}
      <TextInput
        value={value}
        placeholderTextColor="#b6b6b6"
        style={[styles.input, style, !!error && styles.errorInput]}
        {...props}
      />
      {error ? <Text style={styles.error}>{error}</Text> : null}
    </View>
  );
};

export default Input;
