import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 24,
    position: 'relative',
  },
  input: {
    backgroundColor: '#F1F1F1',
    color: '#000000',
    paddingVertical: 20,
    paddingHorizontal: 16,
    fontWeight: '400',
    fontSize: 16,
    borderRadius: 8,
  },
  label: {
    textTransform: 'uppercase',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 10,
    marginBottom: 8,
    marginLeft: 16,
    color: '#212121',
  },
  errorInput: {
    backgroundColor: '#FFEBEE',
    color: '#FA354D',
  },
  error: {
    marginTop: 4,
    color: '#FA354D',
    fontSize: 12,
    marginLeft: 16,
  },
});
