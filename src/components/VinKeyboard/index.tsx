import * as React from 'react';
import {useCallback} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {styles} from '@components/VinKeyboard/styles';

export type KeyboardKey =
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '0'
  | 'W'
  | 'E'
  | 'R'
  | 'T'
  | 'Y'
  | 'U'
  | 'P'
  | 'A'
  | 'S'
  | 'D'
  | 'F'
  | 'G'
  | 'H'
  | 'J'
  | 'K'
  | 'L'
  | 'Z'
  | 'X'
  | 'C'
  | 'V'
  | 'B'
  | 'N'
  | 'M';

const keyboardKeys: KeyboardKey[] = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '0',
  'W',
  'E',
  'R',
  'T',
  'Y',
  'U',
  'P',
  'A',
  'S',
  'D',
  'F',
  'G',
  'H',
  'J',
  'K',
  'L',
  'Z',
  'X',
  'C',
  'V',
  'B',
  'N',
  'M',
];

type KeyboardButtonProps = {
  onPress: () => void;
  disabled: boolean;
  label: string | number;
};

const KeyboardButton = React.memo(
  ({disabled, onPress, label}: KeyboardButtonProps) => (
    <TouchableOpacity
      activeOpacity={0.8}
      disabled={disabled}
      onPress={onPress}
      style={[styles.keyboardButtonContainer, disabled && styles.disabled]}>
      <Text style={styles.keyboardText}>{label}</Text>
    </TouchableOpacity>
  ),
);

type KeyboardVinProps = {
  onHeight?: (height: number) => void;
  onChange: (vin: string) => void;
  maxLength?: number;
  vin: string;
};

export const VinKeyboard = ({
  vin,
  onChange,
  onHeight,
  maxLength,
}: KeyboardVinProps) => {
  const addKey = useCallback(
    (key: KeyboardKey) => {
      if (vin.length < (maxLength || 17)) {
        onChange(vin + key);
      }
    },
    [vin, maxLength, onChange],
  );

  const deleteKey = useCallback(() => {
    const newVin = vin.slice(0, vin.length - 1);
    onChange(newVin);
  }, [vin, onChange]);

  const isKeyEnabled = useCallback(
    (key: KeyboardKey): boolean => {
      if (vin.length === 8) {
        return !(+key >= 0 || key === 'X');
      } else if (vin.length >= 11) {
        return !(+key >= 0);
      } else {
        return false;
      }
    },
    [vin],
  );

  return (
    <View
      style={styles.container}
      onLayout={e => onHeight && onHeight(e.nativeEvent.layout.height)}>
      <View style={styles.keyRows}>
        <View style={styles.keyRow}>
          {keyboardKeys.slice(0, 10).map(key => (
            <KeyboardButton
              key={key}
              onPress={() => addKey(key)}
              label={key}
              disabled={isKeyEnabled(key)}
            />
          ))}
        </View>
        <View style={styles.keyRow}>
          {keyboardKeys.slice(10, 20).map(key => (
            <KeyboardButton
              key={key}
              onPress={() => addKey(key)}
              label={key}
              disabled={isKeyEnabled(key)}
            />
          ))}
        </View>
        <View style={styles.keyRow}>
          <View style={styles.keyFlexHalf} />
          {keyboardKeys.slice(20, 29).map(key => (
            <KeyboardButton
              key={key}
              onPress={() => addKey(key)}
              label={key}
              disabled={isKeyEnabled(key)}
            />
          ))}
          <View style={styles.keyFlexHalf} />
        </View>
        <View style={styles.keyRow}>
          <View style={styles.closeContainer} />
          <View style={styles.keyFlexHalfHalf} />
          {keyboardKeys.slice(29, keyboardKeys.length).map(key => (
            <KeyboardButton
              key={key}
              onPress={() => addKey(key)}
              label={key}
              disabled={isKeyEnabled(key)}
            />
          ))}
          <View style={styles.keyFlexHalfHalf} />
          <TouchableOpacity onPress={deleteKey} style={styles.deleteContainer}>
            <Text style={styles.deleteText}>{'Delete'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
