import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  keyboardButtonContainer: {
    borderRadius: 4,
    flex: 1,
    height: 40,
    backgroundColor: '#1E1F24',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 3,
  },
  keyboardText: {
    fontSize: 22,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  container: {
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
    overflow: 'hidden',
    width: '100%',
    flexDirection: 'column',
  },
  keyRows: {
    justifyContent: 'space-between',
    backgroundColor: '#1E1F24',
    padding: 4,
    paddingTop: 8,
    paddingBottom: bottomSpace + 4,
  },
  keyRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 8,
  },
  keyFlexHalf: {
    flex: 0.5,
  },
  keyFlexHalfHalf: {
    flex: 0.25,
  },
  closeContainer: {
    borderRadius: 4,
    width: 80,
    height: 40,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8,
    marginRight: 4,
  },
  closeText: {
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  deleteContainer: {
    borderRadius: 4,
    width: 80,
    height: 40,
    backgroundColor: '#596376',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
    marginLeft: 4,
  },
  deleteText: {
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  disabled: {opacity: 0.5},
});
