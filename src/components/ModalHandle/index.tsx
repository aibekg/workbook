import React, {useCallback} from 'react';
import {Platform, StyleSheet, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import XSvg from '@icons/x-mark.svg';
import {statusBarHeight} from '@utils/deviceHelpers';

type Props = {
  showCloseButton?: boolean;
  onClose?: () => void;
};

export const ModalHandle = ({showCloseButton, onClose}: Props) => {
  const navigation = useNavigation();

  const handleClosePress = useCallback(() => {
    if (showCloseButton && onClose) {
      onClose();
    } else {
      navigation.goBack();
    }
  }, [navigation, showCloseButton, onClose]);

  if (showCloseButton || Platform.OS === 'android') {
    return (
      <TouchableOpacity
        onPress={handleClosePress}
        activeOpacity={0.8}
        style={styles.closeContainer}>
        <XSvg fill="#000000" />
      </TouchableOpacity>
    );
  }
  return (
    <View style={styles.header}>
      <View style={styles.swipe} />
    </View>
  );
};

export const styles = StyleSheet.create({
  closeContainer: {
    width: 48,
    height: 48,
    borderRadius: 24,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    elevation: 8,
    top: statusBarHeight + 16,
    right: 16,
    position: 'absolute',
    zIndex: 9999,
  },
  header: {
    position: 'absolute',
    zIndex: 9999,
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    paddingTop: 8,
    height: 56,
  },
  swipe: {
    width: 132,
    height: 5,
    borderRadius: 20,
    backgroundColor: '#000000',
  },
});
