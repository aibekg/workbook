import React, {FC} from 'react';
import {TouchableOpacity, View} from 'react-native';

import {THeaderSearch} from '@components/HeaderSearch/types';
import Input from '@components/Input';
import CrossSVG from '@icons/cross.svg';

import {styles} from './styles';

const HeaderSearch: FC<THeaderSearch> = React.memo(
  ({search, setSearch, containerStyle, placeholder, onClose}) => {
    return (
      <View style={[styles.container, containerStyle]}>
        <View style={styles.inputIconContainer}>
          <Input
            value={search}
            onChangeText={setSearch}
            containerStyle={styles.inputContainer}
            style={styles.input}
            autoFocus={true}
            autoCapitalize="none"
            autoComplete="off"
            placeholder={placeholder || 'Search...'}
          />
          <TouchableOpacity
            hitSlop={{top: 24, bottom: 24, left: 24, right: 24}}
            style={styles.closeButton}
            onPress={onClose}>
            <CrossSVG fill="#212121" />
          </TouchableOpacity>
        </View>
      </View>
    );
  },
);

export default HeaderSearch;
