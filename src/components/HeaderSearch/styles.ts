import {StyleSheet} from 'react-native';

import {bottomSpace} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  container: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: -8,
    },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 8,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingTop: 16,
    paddingBottom: bottomSpace + 16,
  },
  input: {
    backgroundColor: '#FFFFFF',
    borderWidth: 0,
    fontSize: 24,
    paddingVertical: 12,
    borderColor: '#d0d7e0',
  },
  inputIconContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputContainer: {
    marginTop: 0,
  },
  closeButton: {
    position: 'absolute',
    right: 12,
  },
});
