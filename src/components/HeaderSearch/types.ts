export type THeaderSearch = {
  containerStyle?: any;
  search: string;
  placeholder?: string;
  setSearch: (v: string) => void;
  onClose: () => void;
};
