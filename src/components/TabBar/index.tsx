import React, {useCallback, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {styles} from './styles';
import {TTabBarProps} from './types';

function TabBar({routes, scenes}: TTabBarProps) {
  const [currentTab, setCurrentTab] = useState<string>(routes[0].key || '');

  const handleTabPress = useCallback(
    (key: string) => () => {
      setCurrentTab(key);
    },
    [],
  );

  return (
    <View>
      <View style={styles.tabs}>
        {routes.map(route => (
          <TouchableOpacity
            onPress={handleTabPress(route.key)}
            key={route.key}
            style={[styles.tab, currentTab === route.key && styles.activeTab]}>
            <Text
              style={[
                styles.label,
                currentTab === route.key && styles.activeLabel,
              ]}>
              {route.title}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <View style={styles.scene}>
        {scenes[currentTab] ? scenes[currentTab] : null}
      </View>
    </View>
  );
}

export default TabBar;
