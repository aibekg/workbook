export type TRoute = {
  key: string;
  title: string;
};

export type TScenes = Record<string, JSX.Element>;

export type TTabBarProps = {
  routes: TRoute[];
  scenes: TScenes;
};
