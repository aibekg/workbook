import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  tabs: {
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  tab: {
    flex: 1,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: 'transparent',
  },
  activeTab: {
    borderBottomWidth: 2,
    borderColor: '#CDFF48',
  },
  label: {
    color: '#000000',
    fontSize: 14,
    fontWeight: '400',
  },
  activeLabel: {
    fontWeight: '500',
  },
  scene: {
    minHeight: 400,
    backgroundColor: '#F1F1F1',
  },
});
