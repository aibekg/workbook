import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {
  GestureResponderEvent,
  LayoutChangeEvent,
  PanResponder,
  PanResponderGestureState,
  PanResponderInstance,
  StyleSheet,
  View,
} from 'react-native';
import Orientation from 'react-native-orientation';
import Svg, {G} from 'react-native-svg';

import DrawingShape from '@screens/Inspection/Exterior/components/DrawingShape';
import SVGMarks from '@screens/Inspection/Exterior/components/SvgMarks';
import SVGPolylines from '@screens/Inspection/Exterior/components/SvgPolilines';
import SVGScheme from '@screens/Inspection/Exterior/components/SvgScheme';
import {statusBarHeight} from '@utils/deviceHelpers';
import {generateUUID} from '@utils/stringHelpers';
import {
  calcCenter,
  calcDistance,
  calcOffsetByZoom,
  findDamagesInArea,
  findIntersectedParts,
  fromArrayToPath,
  generateAffineShapes,
  maxOffset,
  removeOutsidePoints,
} from '@utils/vectorHelpers';

import {Damage, UnitExterior} from '../../generated/graphql';
import {GeneratedAffineShape, ShapeIdType} from '../../types/svgTypes';

type DamageDrawViewProps = {
  svgWidth: number;
  svgHeight: number;
  disabledDraw?: boolean;
  exteriorItem?: UnitExterior | null;
  highlightedDamage?: Damage | null;
  onSelectDamage?: (damage: Damage) => void;
  onAddDamage?: (damage: Damage) => void;
};

const minZoom = 1;
const maxZoom = 5;

export const DamageDrawView = ({
  disabledDraw,
  onAddDamage,
  onSelectDamage,
  exteriorItem,
  highlightedDamage,
  svgHeight,
  svgWidth,
}: DamageDrawViewProps) => {
  const generatedAffineShapes = useRef<GeneratedAffineShape[]>([]);
  useEffect(() => {
    if (
      !generatedAffineShapes.current ||
      generatedAffineShapes.current.length === 0
    ) {
      generatedAffineShapes.current = generateAffineShapes(
        exteriorItem?.svg || '',
      );
    }
  }, [exteriorItem?.svg]);

  const panResponder = useRef<PanResponderInstance>();
  const [drawingPoints, setDrawingPoints] = useState<[number, number][]>([]);
  const drawingPointsInternal = useRef<[number, number][]>([]);
  const [drawingAffectedParts, setDrawingAffectedParts] = useState<
    ShapeIdType[]
  >([]);
  const drawingAffectedPartsInternal = useRef<ShapeIdType[]>([]);
  const [layoutKnown, setLayoutKnown] = useState<boolean>(false);
  const touchState = useRef<string>('single');
  const touchStartTime = useRef<number>(0);
  const height = useRef<number>(0);
  const width = useRef<number>(0);
  const [heightState, setHeightState] = useState<number>(0);
  const [widthState, setWidthState] = useState<number>(0);
  const [zoom, setZoom] = useState<number>(1);
  const zoomInternal = useRef<number>(1);
  const [top, setTop] = useState<number>(statusBarHeight);
  const topInternal = useRef<number>(0);
  const [left, setLeft] = useState<number>(0);
  const leftInternal = useRef<number>(0);
  const [schemeScale, setSchemeScale] = useState<number>(1);
  const schemeScaleInternal = useRef<number>(1);
  const disabledDrawInternal = useRef<boolean>(disabledDraw || false);
  const initial = useRef<{
    distance: number;
    x: number;
    y: number;
    top: number;
    left: number;
    topWithoutZoom: number;
    leftWithoutZoom: number;
    zoom: number;
  } | null>(null);

  useEffect(() => {
    disabledDrawInternal.current = disabledDraw || false;
  }, [disabledDraw]);

  const onLayout = (event: LayoutChangeEvent) => {
    const newSchemeScale = Math.min(
      event.nativeEvent.layout.width / svgWidth,
      event.nativeEvent.layout.height / svgHeight,
    );
    setWidthState(event.nativeEvent.layout.width);
    width.current = event.nativeEvent.layout.width;
    setHeightState(event.nativeEvent.layout.height);
    height.current = event.nativeEvent.layout.height;
    setSchemeScale(newSchemeScale);
    schemeScaleInternal.current = newSchemeScale;
    setLayoutKnown(true);
  };

  const processPan = useCallback(
    (
      x1: number,
      y1: number,
      x2: number,
      y2: number,
      dx: number,
      dy: number,
    ) => {
      const distance = calcDistance(x1, y1, x2, y2);
      const {x, y} = calcCenter(x1, y1, x2, y2);

      if (!initial.current) {
        const offsetByZoom = calcOffsetByZoom(
          width.current,
          height.current,
          zoomInternal.current,
        );
        initial.current = {
          distance,
          x,
          y,
          top: topInternal.current,
          left: leftInternal.current,
          zoom: zoomInternal.current,
          topWithoutZoom: topInternal.current - offsetByZoom.top,
          leftWithoutZoom: leftInternal.current - offsetByZoom.left,
        };
      } else if (initial.current) {
        const touchZoom = distance / initial.current.distance;
        const calculateZoom = (
          zoomValue: number,
          maxZoomValue: number,
          minZoomValue: number,
        ) => {
          if (zoomValue >= minZoomValue && zoomValue <= maxZoomValue) {
            return zoomValue;
          }
          if (zoomValue < minZoomValue) {
            return minZoomValue;
          }
          return maxZoomValue;
        };

        const calculatedZoom = calculateZoom(
          touchZoom * initial.current.zoom,
          maxZoom,
          minZoom,
        );

        const offsetByZoom = calcOffsetByZoom(
          width.current,
          height.current,
          calculatedZoom,
        );
        const zoomedLeft =
          initial.current.leftWithoutZoom * touchZoom + offsetByZoom.left;
        const zoomedTop =
          initial.current.topWithoutZoom * touchZoom + offsetByZoom.top;

        const leftValue = zoomedLeft + (dx * calculatedZoom) / 2;
        const topValue = zoomedTop + (dy * calculatedZoom) / 2;
        setLeft(
          leftValue > 0
            ? 0
            : maxOffset(
                leftValue,
                width.current,
                width.current * calculatedZoom,
              ),
        );
        leftInternal.current =
          leftValue > 0
            ? 0
            : maxOffset(
                leftValue,
                width.current,
                width.current * calculatedZoom,
              );
        setTop(
          topValue > 0
            ? 0
            : maxOffset(
                topValue,
                height.current,
                height.current * calculatedZoom,
              ),
        );
        topInternal.current =
          topValue > 0
            ? 0
            : maxOffset(
                topValue,
                height.current,
                height.current * calculatedZoom,
              );
        setZoom(calculatedZoom);
        zoomInternal.current = calculatedZoom;
      }
    },
    [],
  );

  useEffect(() => {
    const onChange = (orientation?: string) => {
      if (orientation === 'LANDSCAPE') {
        setLeft(0);
        leftInternal.current = 0;
        setTop(0);
        topInternal.current = 0;
      } else {
        setLeft(0);
        leftInternal.current = 0;
        setTop(statusBarHeight || 0);
        topInternal.current = statusBarHeight || 0;
      }
      if (width.current || height.current) {
        onLayout({
          nativeEvent: {
            layout: {
              width: width.current,
              height: height.current,
            },
          },
        } as LayoutChangeEvent);
        setTimeout(() => processPan(240, 240, 180, 180, 10, 10), 500);
      }
    };
    Orientation.addOrientationListener(onChange);
    return () => {
      Orientation.removeOrientationListener(onChange);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [processPan, svgWidth, svgHeight]);

  const onResponderStart = useCallback((e: GestureResponderEvent) => {
    touchStartTime.current = Date.now();
    touchState.current =
      e.nativeEvent.touches.length > 1 ? 'multi' : touchState.current;
  }, []);

  const onResponderMove = useCallback(
    (e: GestureResponderEvent, gestureState: PanResponderGestureState) => {
      const {touches} = e.nativeEvent;
      switch (touchState.current) {
        case 'single':
          if (Math.abs(gestureState.dx) > 5 || Math.abs(gestureState.dy) > 5) {
            touchState.current = 'singleMove';
          }
          break;
        case 'singleMove':
          if (!disabledDrawInternal.current) {
            const rZoom = +zoomInternal.current.toFixed(2);
            const x =
              (Math.floor(gestureState.moveX) - leftInternal.current) /
              rZoom /
              schemeScaleInternal.current;
            const y =
              (Math.floor(gestureState.moveY) - topInternal.current) /
              rZoom /
              schemeScaleInternal.current;
            const lastCoords =
              drawingPointsInternal.current[
                drawingPointsInternal.current.length - 1
              ];

            if (
              !drawingPointsInternal.current ||
              drawingPointsInternal.current.length === 0
            ) {
              setDrawingPoints([[x, y]]);
              drawingPointsInternal.current = [[x, y]];
            } else if (
              Math.abs(x - lastCoords[0]) >
                10 / rZoom / schemeScaleInternal.current ||
              Math.abs(y - lastCoords[1]) >
                10 / rZoom / schemeScaleInternal.current
            ) {
              setDrawingPoints([...drawingPointsInternal.current, [x, y]]);
              drawingPointsInternal.current = [
                ...drawingPointsInternal.current,
                [x, y],
              ];
              setDrawingAffectedParts(
                findIntersectedParts(
                  [...drawingPointsInternal.current, [x, y]],
                  generatedAffineShapes.current,
                ),
              );
              drawingAffectedPartsInternal.current = findIntersectedParts(
                [...drawingPointsInternal.current, [x, y]],
                generatedAffineShapes.current,
              );
            }
          }
          break;
        case 'multi':
          if (touches.length > 1) {
            processPan(
              touches[0].pageX,
              touches[0].pageY,
              touches[1].pageX,
              touches[1].pageY,
              gestureState.dx,
              gestureState.dy,
            );
          }
          break;
        default:
          break;
      }
    },
    [processPan],
  );

  const onResponderEnd = useCallback(
    (e: GestureResponderEvent, gestureState: PanResponderGestureState) => {
      const timestamp = Date.now();
      switch (touchState.current) {
        case 'single':
          if (
            timestamp - touchStartTime.current >= 600 &&
            !disabledDrawInternal.current
          ) {
            const x =
              (gestureState.x0 - leftInternal.current) /
              zoomInternal.current /
              schemeScaleInternal.current;
            const y =
              (gestureState.y0 - topInternal.current) /
              zoomInternal.current /
              schemeScaleInternal.current;
            const affectedParts = findIntersectedParts(
              [[x, y]],
              generatedAffineShapes.current,
            );
            if (onAddDamage && affectedParts.length > 0) {
              onAddDamage({
                id: generateUUID(),
                shape: 'Point',
                damageType: 'Dent',
                detail: '0–1"',
                svg: fromArrayToPath(height.current, width.current, [[x, y]]),
                affectedParts: affectedParts as string[],
              });
            }
          } else if (onSelectDamage) {
            const selectedDamages = findDamagesInArea(
              e.nativeEvent.pageX,
              e.nativeEvent.pageY,
              zoomInternal.current,
              leftInternal.current,
              topInternal.current,
              schemeScaleInternal.current,
              exteriorItem?.damages || [],
            );
            if (selectedDamages.length > 0 && selectedDamages[0]) {
              onSelectDamage(selectedDamages[0]);
            }
          }
          break;
        case 'singleMove':
          if (
            drawingAffectedPartsInternal.current.length > 0 &&
            !disabledDrawInternal.current
          ) {
            const lastCoords =
              drawingPointsInternal.current[
                drawingPointsInternal.current.length - 1
              ];
            const firstCoords = drawingPointsInternal.current[0];
            const points =
              Math.abs(lastCoords[0] - firstCoords[0]) <
                20 / zoomInternal.current / schemeScaleInternal.current &&
              Math.abs(lastCoords[1] - firstCoords[1]) <
                20 / zoomInternal.current / schemeScaleInternal.current
                ? [...drawingPointsInternal.current, firstCoords]
                : drawingPointsInternal.current;

            const updatedPoints = removeOutsidePoints(
              points,
              generatedAffineShapes.current.filter(
                s => drawingAffectedPartsInternal.current.indexOf(s.id) !== -1,
              ),
            );

            if (onAddDamage && updatedPoints.length > 0) {
              onAddDamage({
                id: generateUUID(),
                shape: 'Line',
                damageType: 'Scratch',
                detail: '1–3"',
                svg: fromArrayToPath(
                  height.current,
                  width.current,
                  updatedPoints,
                ),
                affectedParts: drawingAffectedPartsInternal.current as string[],
              });
            }
          }
          setDrawingPoints([]);
          drawingPointsInternal.current = [];
          setDrawingAffectedParts([]);
          drawingAffectedPartsInternal.current = [];
          break;
        default:
          break;
      }
    },
    [exteriorItem?.damages, onAddDamage, onSelectDamage],
  );

  const onResponderRelease = useCallback(() => {
    touchState.current = 'single';
    initial.current = null;
  }, []);

  useEffect(() => {
    panResponder.current = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderStart: onResponderStart,
      onPanResponderMove: onResponderMove,
      onPanResponderTerminationRequest: () => true,
      onPanResponderRelease: onResponderRelease,
      onShouldBlockNativeResponder: () => true,
      onPanResponderEnd: onResponderEnd,
    });
  }, [onResponderEnd, onResponderMove, onResponderRelease, onResponderStart]);

  const selectedParts = useMemo(
    () =>
      highlightedDamage
        ? [
            ...(exteriorItem?.damages
              ?.map(item => item?.affectedParts || [])
              ?.flat() || []),
            ...drawingAffectedParts,
            ...(highlightedDamage?.affectedParts || []),
          ]
        : [
            ...(exteriorItem?.damages
              ?.map(item => item?.affectedParts || [])
              ?.flat() || []),
            ...drawingAffectedParts,
          ],
    [drawingAffectedParts, exteriorItem?.damages, highlightedDamage],
  );

  return (
    <View
      style={styles.container}
      {...(panResponder.current
        ? panResponder.current?.panHandlers
        : undefined)}
      onLayout={onLayout}>
      {layoutKnown && (
        <Svg height={heightState} width={widthState}>
          <G x={left} y={top} scale={zoom}>
            <G scale={schemeScale}>
              <SVGScheme
                activeColor="#FFFFFF"
                inactiveColor="#FFFFFF80"
                pathStrokeWidth={1.5 / schemeScale}
                svgString={exteriorItem?.svg}
                selectedParts={selectedParts || []}
              />
              {drawingPoints.length > 0 && (
                <DrawingShape
                  points={drawingPoints}
                  stroke="#FF453A"
                  strokeWidth={2 / schemeScale}
                />
              )}
              <SVGPolylines
                strokeColor="#FFFFFF"
                activeStrokeColor="#FF453A"
                exteriorItem={exteriorItem}
                activeDamage={highlightedDamage}
                damageStrokeWidth={2 / schemeScale}
              />
              <SVGMarks
                exteriorItem={exteriorItem}
                activeDamage={highlightedDamage}
                radius={12 / schemeScale}
                zoom={zoom}
              />
            </G>
          </G>
        </Svg>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
