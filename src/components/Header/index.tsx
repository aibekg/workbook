import React, {FC, useCallback, useMemo} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';
import {USER_PHOTO_URL} from '@env';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

import {THeader} from '@components/Header/types';
import Menu from '@components/Menu';
import {useActions} from '@hooks/useActions';
import SettingsSvg from '@icons/settings.svg';
import UserSvg from '@icons/user.svg';
import {
  ERootStackRoutes,
  TNavigationProp,
  TRootStackParamList,
} from '@routes/types';
import {selectUser} from '@store/auth/selectors';

import {styles} from './styles';

const Header: FC<THeader> = React.memo(
  ({title, hideRight, hideLeft, headerLeft, headerRight}) => {
    const user = useSelector(selectUser);
    const {setAuth} = useActions();
    const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Units>>();
    const {name} =
      useRoute<RouteProp<TRootStackParamList, ERootStackRoutes.Profile>>();

    const onSettings = useCallback(() => {
      navigation.replace(ERootStackRoutes.Profile);
    }, [navigation]);

    const handleSelect = useCallback(
      (value: string) => {
        switch (value) {
          case 'profile':
            navigation.replace(ERootStackRoutes.Profile);
            break;
          case 'logout':
            setAuth(null, null);
            break;
        }
      },
      [navigation, setAuth],
    );

    const headerActions = useMemo(() => {
      return [
        {
          text: 'My Profile',
          key: 'profile',
          disabled: name === ERootStackRoutes.Profile,
        },
        {
          text: 'Logout',
          key: 'logout',
        },
      ];
    }, [name]);

    const renderPhoto = useCallback(
      (size: number = 64, isHeader?: boolean) => {
        return user?.photo ? (
          <Image
            style={[
              styles.avatar,
              {width: size, height: size},
              isHeader && styles.marginBottom,
            ]}
            source={{
              uri: USER_PHOTO_URL + user?.photo,
            }}
          />
        ) : (
          <View
            style={[
              styles.avatar,
              styles.borderRadiusZero,
              {width: size, height: size},
              isHeader && styles.marginBottom,
            ]}>
            <UserSvg width={size} />
          </View>
        );
      },
      [user?.photo],
    );

    return (
      <View style={styles.header}>
        {hideLeft ? (
          <View style={styles.headerLeft} />
        ) : (
          headerLeft || (
            <Menu
              header={renderPhoto(64, true)}
              title={`${user?.userFirstName || ''} ${
                user?.userLastName || ''
              }`.trim()}
              onSelect={handleSelect}
              trigger={renderPhoto(32)}
              options={headerActions}
            />
          )
        )}
        {typeof title === 'string' ? (
          <Text style={styles.headerTitle}>{title || ''}</Text>
        ) : (
          title
        )}
        {hideRight ? (
          <View style={styles.headerRight} />
        ) : (
          headerRight || (
            <TouchableOpacity
              onPress={onSettings}
              hitSlop={{top: 24, bottom: 24, left: 24, right: 24}}
              style={styles.headerRight}>
              <SettingsSvg stroke="#FFFFFF" />
            </TouchableOpacity>
          )
        )}
      </View>
    );
  },
);

export default Header;
