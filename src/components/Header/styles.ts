import {StyleSheet} from 'react-native';

import {statusBarHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  header: {
    backgroundColor: '#1E1F24',
    paddingTop: 20 + statusBarHeight,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  headerTitle: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '700',
  },
  headerLeft: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRight: {
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#CDFF48',
    borderColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  marginBottom: {
    marginBottom: 12,
  },
  borderRadiusZero: {
    borderWidth: 0,
  },
});
