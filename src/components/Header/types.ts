import {ReactElement, ReactNode} from 'react';

export type THeader = {
  title: string | ReactNode;
  headerLeft?: ReactElement;
  headerRight?: ReactElement;
  hideRight?: boolean;
  hideLeft?: boolean;
};
