import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: 0,
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
  },
});
