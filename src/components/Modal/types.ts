import {ModalProps} from 'react-native-modal';

export type TModalProps = Partial<ModalProps> & {
  close?: () => void;
  contentStyle?: any;
};
