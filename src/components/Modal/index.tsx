import React, {FC, useEffect} from 'react';
import {View} from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import RNModal from 'react-native-modal';

import {ModalHandle} from '@components/ModalHandle';

import {styles} from './styles';
import {TModalProps} from './types';

const Modal: FC<TModalProps> = ({
  close,
  contentStyle,
  children,
  ...restProps
}) => {
  useEffect(() => {
    if (restProps?.isVisible) {
      ReactNativeHapticFeedback.trigger('impactMedium', {
        enableVibrateFallback: false,
        ignoreAndroidSystemSettings: false,
      });
    }
  }, [restProps?.isVisible]);
  return (
    <RNModal
      {...restProps}
      style={styles.modal}
      propagateSwipe={true}
      onBackdropPress={close}
      onBackButtonPress={close}
      backdropTransitionOutTiming={0}
      useNativeDriverForBackdrop={true}
      hideModalContentWhileAnimating={true}
      useNativeDriver={true}
      backdropColor="#000000"
      backdropOpacity={0.7}>
      <ModalHandle showCloseButton={true} onClose={close} />
      <View style={[styles.contentWrapper, contentStyle]}>{children}</View>
    </RNModal>
  );
};

export default Modal;
