import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  button: {
    padding: 20,
    backgroundColor: '#1E1F24',
    borderRadius: 28,
  },
  disabledButton: {
    backgroundColor: '#8e8f91',
  },
  text: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  errorButton: {
    backgroundColor: 'transparent',
  },
  errorText: {
    color: '#8e8f91',
  },
});
