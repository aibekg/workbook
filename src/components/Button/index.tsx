import React, {FC} from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {TButton} from '@components/Button/types';

import {styles} from './styles';

const Button: FC<TButton> = React.memo(
  ({
    text,
    onPress,
    buttonStyle,
    textStyle,
    disabled,
    hideWhenDisabled,
    error,
    errorStyle,
  }) => {
    if (hideWhenDisabled && disabled) {
      return null;
    }
    return (
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled || !!error}
        style={[
          styles.button,
          buttonStyle,
          disabled && styles.disabledButton,
          !!error && [styles.errorButton, errorStyle],
        ]}>
        <Text style={[styles.text, textStyle, !!error && styles.errorText]}>
          {text}
        </Text>
      </TouchableOpacity>
    );
  },
);

export default Button;
