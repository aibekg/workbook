import {StyleProp, TextStyle, ViewStyle} from 'react-native';

export type TButton = {
  text: string;
  onPress?: () => void;
  buttonStyle?: StyleProp<ViewStyle>;
  errorStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  disabled?: boolean;
  hideWhenDisabled?: boolean;
  error?: string | null;
};
