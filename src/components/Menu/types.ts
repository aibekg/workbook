import {ReactNode} from 'react';
import {StyleProp, ViewStyle} from 'react-native';

export type TMenuProps = {
  onSelect: (value: string) => void;
  trigger: ReactNode;
  options: TMenuOption[];
  header?: ReactNode;
  title?: string;
  style?: StyleProp<ViewStyle>;
};

export type TMenuOption = {
  text: string;
  key: string;
  disabled?: boolean;
};
