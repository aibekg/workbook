import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 16,
    width: 240,
    position: 'absolute',
  },
  title: {
    textTransform: 'uppercase',
    opacity: 0.7,
    color: '#1E1F24',
    fontSize: 10,
    fontWeight: '800',
    marginTop: 8,
    marginBottom: 16,
  },
  option: {
    paddingHorizontal: 0,
    paddingVertical: 8,
    zIndex: 1000,
  },
  optionText: {
    fontSize: 18,
    color: '#1E1F24',
    fontWeight: '700',
    zIndex: 1000,
  },
  disabledText: {
    opacity: 0.5,
  },
});
