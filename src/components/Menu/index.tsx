import React, {FC} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {
  Menu as RNMenu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from 'react-native-popup-menu';

import {styles} from './styles';
import {TMenuProps} from './types';

const Menu: FC<TMenuProps> = ({
  onSelect,
  trigger,
  options,
  title,
  style,
  header,
}) => {
  return (
    <RNMenu onSelect={onSelect}>
      <MenuTrigger customStyles={{TriggerTouchableComponent: TouchableOpacity}}>
        {trigger}
      </MenuTrigger>
      <MenuOptions optionsContainerStyle={[styles.container, style]}>
        {header}
        <Text style={styles.title}>{title}</Text>
        {options.map(({text, key, disabled}) => (
          <MenuOption
            disabled={disabled}
            style={styles.option}
            key={key}
            customStyles={{OptionTouchableComponent: TouchableOpacity}}
            value={key}>
            <Text style={[styles.optionText, disabled && styles.disabledText]}>
              {text}
            </Text>
          </MenuOption>
        ))}
      </MenuOptions>
    </RNMenu>
  );
};

export default Menu;
