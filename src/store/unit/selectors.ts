import {createSelector} from '@reduxjs/toolkit';

import {RootState, UnitState} from '@store/index';

const selectedState = (state: RootState): UnitState => state.unit;

export const selectNewUnitData = createSelector(
  selectedState,
  state => state.newInitData,
);

export const selectInspectionUnit = createSelector(
  selectedState,
  state => state.inspectionUnit,
);

export const selectEquipments = createSelector(
  selectedState,
  state => state.equipments,
);
