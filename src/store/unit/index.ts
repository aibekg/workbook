import {
  addEquipment,
  removeEquipment,
  setInspectionUnit,
  setNewUnitDataFiled,
  TUnitAction,
} from './actions';
import {EUnitActionType, TUnitState} from './types';

const initialState: TUnitState = {
  newInitData: {
    colorId: null,
    manufacturer: null,
    commodityId: null,
    commodityTypeId: null,
    year: null,
    model: null,
    vin: null,
    bodyId: null,
  },
  inspectionUnit: {
    id: undefined,
  },
  equipments: [],
};

export default function UnitReducer(
  state = initialState,
  action: TUnitAction,
): TUnitState {
  switch (action.type) {
    case EUnitActionType.SET_NEW_UNIT_DATA_FIELD:
      return {
        ...state,
        newInitData: {
          ...state.newInitData,
          [(action as ReturnType<typeof setNewUnitDataFiled>).payload.type]: (
            action as ReturnType<typeof setNewUnitDataFiled>
          ).payload.value,
        },
      };
    case EUnitActionType.CLEAR_UNIT_DATA_FIELD:
      return {...state, newInitData: initialState.newInitData};
    case EUnitActionType.SET_INSPECTION_UNIT:
      return {
        ...state,
        inspectionUnit: {
          ...state.inspectionUnit,
          ...((action as ReturnType<typeof setInspectionUnit>).payload.value ||
            {}),
        },
      };
    case EUnitActionType.ADD_EQUIPMENT:
      return {
        ...state,
        equipments: [
          ...state.equipments,
          (action as ReturnType<typeof addEquipment>).payload,
        ],
      };
    case EUnitActionType.REMOVE_EQUIPMENT:
      return {
        ...state,
        equipments: state.equipments.filter(
          item =>
            item.title !==
            (action as ReturnType<typeof removeEquipment>).payload,
        ),
      };
    default:
      return state;
  }
}
