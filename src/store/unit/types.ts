import {TPhoto} from '@screens/Inspection/CarrierDriver/components/AddNewDriverBottomSheet/types';

import {Unit} from '../../generated/graphql';

export type TUnitState = {
  newInitData: TNewUnitData;
  inspectionUnit: TInspectionUnit;
  equipments: TEquipment[];
};

export type TEquipment = {
  title: TEquipmentTitle;
  damageType: TDamageType;
  damageSize?: string | null;
  photo: TPhoto | null;
};

export type TEquipmentTitle =
  | 'Right Side Detail'
  | 'Left Side Mirror'
  | 'Spare Tire'
  | 'Front Mirror'
  | 'DVD Player'
  | 'Wiper Blades'
  | 'Radio';

export type TDamageType =
  | 'scratch'
  | 'ding'
  | 'dent'
  | 'crack'
  | 'rub'
  | 'missing';

export type TInspectionUnit = Partial<Unit>;

export type TNewUnitData = {
  manufacturer: TNewUnitValue<string>;
  commodityId: TNewUnitValue<number>;
  commodityTypeId: TNewUnitValue<number>;
  year: TNewUnitValue<number>;
  model: TNewUnitValue<string>;
  colorId: TNewUnitValue<number>;
  bodyId: TNewUnitValue<number>;
  vin: TNewUnitValue<string>;
};

export type TNewUnitValue<T> = T | null;

export type TSetNewUnitDataField = {
  type: keyof TNewUnitData;
  value: string | number | null;
};

export type TSetInspection = {
  value: TInspectionUnit | null;
};

export enum EUnitActionType {
  SET_NEW_UNIT_DATA_FIELD = 'SET_NEW_UNIT_DATA_FIELD',
  CLEAR_UNIT_DATA_FIELD = 'CLEAR_UNIT_DATA_FIELD',
  SET_INSPECTION_UNIT = 'SET_INSPECTION_UNIT',
  ADD_EQUIPMENT = 'ADD_EQUIPMENT',
  REMOVE_EQUIPMENT = 'REMOVE_EQUIPMENT',
}
