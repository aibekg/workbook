import {
  EUnitActionType,
  TEquipment,
  TEquipmentTitle,
  TSetInspection,
  TSetNewUnitDataField,
} from './types';

export const setNewUnitDataFiled = (payload: TSetNewUnitDataField) => ({
  type: EUnitActionType.SET_NEW_UNIT_DATA_FIELD,
  payload,
});

export const setInspectionUnit = (payload: TSetInspection) => ({
  type: EUnitActionType.SET_INSPECTION_UNIT,
  payload,
});

export const clearUnitDataFiled = () => ({
  type: EUnitActionType.CLEAR_UNIT_DATA_FIELD,
});

export const addEquipment = (payload: TEquipment) => ({
  type: EUnitActionType.ADD_EQUIPMENT,
  payload,
});

export const removeEquipment = (payload: TEquipmentTitle) => ({
  type: EUnitActionType.REMOVE_EQUIPMENT,
  payload,
});

export type TUnitAction =
  | ReturnType<typeof setNewUnitDataFiled>
  | ReturnType<typeof setInspectionUnit>
  | ReturnType<typeof clearUnitDataFiled>
  | ReturnType<typeof addEquipment>;
