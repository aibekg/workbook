import {createSelector} from '@reduxjs/toolkit';

import {AuthState, RootState} from '../index';

const selectedState = (state: RootState): AuthState => state.auth;

export const selectAccessToken = createSelector(
  selectedState,
  state => state.accessToken,
);

export const selectUser = createSelector(selectedState, state => state.user);
