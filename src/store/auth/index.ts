import {EAuthActionType, TAuthAction, TAuthState} from './types';

const initialState: TAuthState = {
  accessToken: null,
};

export default function authReducer(
  state = initialState,
  action: TAuthAction,
): TAuthState {
  switch (action.type) {
    case EAuthActionType.SET_AUTH:
      return {
        ...state,
        accessToken: action.payload.accessToken,
        user: action.payload.user,
      };
    case EAuthActionType.SET_USER:
      return {
        ...state,
        user: action.payload.user,
      };
    default:
      return state;
  }
}
