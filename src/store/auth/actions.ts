import {EAuthActionType, TSetAuth, TSetUser} from '@store/auth/types';

import {User, UserItemFragment} from '../../generated/graphql';

export const setAuth = (
  accessToken: string | null,
  user: User | null,
): TSetAuth => ({
  type: EAuthActionType.SET_AUTH,
  payload: {
    accessToken,
    user,
  },
});

export const setUser = (user: UserItemFragment): TSetUser => ({
  type: EAuthActionType.SET_USER,
  payload: {
    user,
  },
});
