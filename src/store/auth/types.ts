import {setAuth, setUser} from '@store/auth/actions';

import {UserItemFragment} from '../../generated/graphql';

export type TAuthState = {
  accessToken: string | null;
  user?: UserItemFragment | null;
};

export enum EAuthActionType {
  SET_AUTH = 'SET_AUTH',
  SET_USER = 'SET_USER',
}

export type TSetAuth = {
  type: EAuthActionType.SET_AUTH;
  payload: {
    accessToken: string | null;
    user: UserItemFragment | null;
  };
};

export type TSetUser = {
  type: EAuthActionType.SET_USER;
  payload: {
    user: UserItemFragment;
  };
};

export type TAuthAction = ReturnType<typeof setAuth | typeof setUser>;
