import {MMKV} from 'react-native-mmkv';
import {ENCRYPTION_KEY} from '@env';
import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist';

import authReducer from './auth';
import unitReducer from './unit';

const authStorage = new MMKV({
  id: 'auth-storage',
  encryptionKey: ENCRYPTION_KEY,
});

const MMKVStorage = (storage: MMKV) => ({
  setItem: async (key: string, value: any) => {
    storage.set(key, value);
    return Promise.resolve(true);
  },
  getItem: async (key: string) => {
    const secureStorageValue = storage.getString(key);
    return Promise.resolve(secureStorageValue);
  },
  removeItem: async (key: string) => {
    storage.delete(key);
    return Promise.resolve();
  },
});

const authPersistConfig = {
  key: 'auth',
  storage: MMKVStorage(authStorage),
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, authReducer),
  unit: unitReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof rootReducer>;

export type AuthState = ReturnType<typeof authReducer>;

export type UnitState = ReturnType<typeof unitReducer>;
