import * as AuthActions from '../store/auth/actions';
import * as UnitActions from '../store/unit/actions';

export default {
  ...AuthActions,
  ...UnitActions,
};
