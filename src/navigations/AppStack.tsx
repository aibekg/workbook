import React from 'react';
import {useSelector} from 'react-redux';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {useAppInit} from '@hooks/useAppInit';
import {APP_STACK_SCREENS, AUTH_STACK_SCREENS} from '@routes/index';
import {ERootStackRoutes} from '@routes/types';
import {selectAccessToken} from '@store/auth/selectors';

const Stack = createNativeStackNavigator();

const AppStack = () => {
  const isAuthorized = useSelector(selectAccessToken);
  useAppInit();
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={ERootStackRoutes.DocumentAddition}>
      {!isAuthorized
        ? AUTH_STACK_SCREENS.map(screen => (
            <Stack.Screen
              key={screen.name}
              name={screen.name}
              component={screen.component}
            />
          ))
        : APP_STACK_SCREENS.map(screen => (
            <Stack.Screen
              key={screen.name}
              name={screen.name}
              component={screen.component}
              // @ts-ignore
              options={screen.options}
            />
          ))}
    </Stack.Navigator>
  );
};

export default AppStack;
