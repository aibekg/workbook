import {useCallback, useState} from 'react';
import {Keyboard} from 'react-native';

import {useActions} from '@hooks/useActions';

import {
  LoginMutationVariables,
  useLoginMutation,
  User,
} from '../generated/graphql';

export const useLogin = () => {
  const {setAuth} = useActions();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [loginMutation] = useLoginMutation();

  const login = useCallback(
    async (variables: LoginMutationVariables) => {
      try {
        Keyboard.dismiss();
        setIsLoading(true);
        const response = await loginMutation({
          variables,
        });
        if (response?.data?.login?.token && response?.data?.login?.user) {
          setAuth(
            response?.data?.login?.token,
            response?.data?.login?.user as User,
          );
        }
        /* setTimeout(() => {
          if (!response?.data?.login?.user?.photo) {
            navigation.navigate({
              name: ERootStackRoutes.Welcome,
              params: undefined,
            });
          }
        }, 600); */
      } catch (e) {
        setError('Incorrect Credentials');
      } finally {
        setIsLoading(false);
      }
    },
    [setAuth, loginMutation],
  );

  return {
    login,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
