import {useEffect} from 'react';
import {useSelector} from 'react-redux';

import {useCurrentUser} from '@hooks/useCurrentUser';
import {selectAccessToken} from '@store/auth/selectors';

export const useAppInit = () => {
  const isAuthorized = useSelector(selectAccessToken);
  const getCurrentUser = useCurrentUser();

  useEffect(() => {
    if (isAuthorized) {
      getCurrentUser();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
