import {useCallback, useState} from 'react';

import {
  UpdateUnitDocumentMutationVariables,
  useUpdateUnitDocumentMutation,
} from '../generated/graphql';

export const useUpdateUnitDocument = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateUnitDocumentMutation] = useUpdateUnitDocumentMutation();

  const updateUnitDocument = useCallback(
    async (variables: UpdateUnitDocumentMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateUnitDocumentMutation({
          variables,
        });
        return response?.data?.updateUnitDocument || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateUnitDocumentMutation],
  );

  return {
    updateUnitDocument,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
