import {useCallback} from 'react';
import {
  UserRegisterMutationVariables,
  useUserRegisterMutation,
} from 'src/generated/graphql';

export const useRegister = () => {
  const [registerRequest] = useUserRegisterMutation();

  return useCallback(
    async (variables: UserRegisterMutationVariables) => {
      const response = await registerRequest({
        variables,
      });
      return response?.data || null;
    },
    [registerRequest],
  );
};
