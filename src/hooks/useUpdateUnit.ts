import {useCallback, useState} from 'react';

import {
  UpdateUnitMutationVariables,
  useUpdateUnitMutation,
} from '../generated/graphql';

export const useUpdateUnit = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateUnitMutation] = useUpdateUnitMutation();

  const updateUnit = useCallback(
    async (variables: UpdateUnitMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateUnitMutation({
          variables,
        });
        return response?.data?.updateUnit || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateUnitMutation],
  );

  return {
    updateUnit,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
