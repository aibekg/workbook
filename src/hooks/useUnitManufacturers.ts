import {useCallback, useEffect, useRef, useState} from 'react';

import {
  UnitManufacturersQueryVariables,
  useUnitManufacturersLazyQuery,
} from '../generated/graphql';

export const useUnitManufacturers = (search: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isPaginating, setIsPaginating] = useState<boolean>(false);
  const isLoadingInternal = useRef<boolean>(false);
  const [error, setError] = useState<string>('');
  const [manufacturers, setManufacturers] = useState<string[]>([]);
  const offset = useRef<number>(0);
  const noMorePagination = useRef<boolean>(false);

  const [unitManufacturersQuery] = useUnitManufacturersLazyQuery();

  const getUnitManufacturers = useCallback(
    async (variables?: UnitManufacturersQueryVariables) => {
      if (
        !isLoadingInternal.current &&
        (offset.current > 0 ? !noMorePagination.current : true)
      ) {
        try {
          isLoadingInternal.current = true;
          setIsLoading(true);
          if (offset.current > 0) {
            setIsPaginating(true);
          }
          const response = await unitManufacturersQuery({
            variables: {
              ...variables,
              search,
              offset: offset.current,
              limit: 40,
            },
          });
          const unitManufacturers = response?.data?.unitManufacturers;
          if (unitManufacturers) {
            if (!unitManufacturers.length) {
              noMorePagination.current = true;
            }
            setManufacturers(prev => {
              const newUnitManufacturers = [
                ...prev,
                ...(unitManufacturers as string[]),
              ];
              return newUnitManufacturers.filter((item, pos) => {
                return newUnitManufacturers.indexOf(item) === pos;
              });
            });
            offset.current = offset.current + 40;
          }
          return unitManufacturers || [];
        } catch (e) {
          setError('No Manufacturers');
          return null;
        } finally {
          setIsLoading(false);
          setIsPaginating(false);
          isLoadingInternal.current = false;
        }
      }
    },
    [search, unitManufacturersQuery],
  );

  useEffect(() => {
    if (search) {
      setManufacturers([]);
    }
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
  }, [search]);

  useEffect(() => {
    getUnitManufacturers();
  }, [getUnitManufacturers]);

  return {
    getUnitManufacturers,
    isLoading,
    setIsLoading,
    isPaginating,
    error,
    setError,
    manufacturers,
  };
};
