import {useCallback, useEffect, useState} from 'react';
import orderBy from 'lodash/orderBy';

import {Body, useUnitBodiesLazyQuery} from '../generated/graphql';

export const useUnitBodies = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [bodies, setBodies] = useState<Body[]>([]);

  const [unitBodiesQuery] = useUnitBodiesLazyQuery();

  const getUnitBodies = useCallback(async () => {
    try {
      setIsLoading(true);
      const response = await unitBodiesQuery();
      return orderBy(response?.data?.unitBodies, ['id']) || [];
    } catch (e) {
      setError('No Colors');
      return null;
    } finally {
      setIsLoading(false);
    }
  }, [unitBodiesQuery]);

  useEffect(() => {
    getUnitBodies().then(data => {
      setBodies(data as Body[]);
    });
  }, [getUnitBodies]);

  return {
    getUnitBodies,
    isLoading,
    setIsLoading,
    error,
    setError,
    bodies,
  };
};
