import {useCallback, useState} from 'react';

import {
  SaveImagesMutationVariables,
  useSaveImagesMutation,
} from '../generated/graphql';

export const useSaveImages = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [saveImageMutation] = useSaveImagesMutation();

  const saveImages = useCallback(
    async (variables: SaveImagesMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await saveImageMutation({
          variables,
        });
        return response?.data?.saveImages || null;
      } catch (e) {
        setError('Failed to upload image');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [saveImageMutation],
  );

  return {
    saveImages,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
