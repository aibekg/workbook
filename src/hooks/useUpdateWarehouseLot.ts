import {useCallback, useState} from 'react';

import {
  UpdateWarehouseLotMutationVariables,
  useUpdateWarehouseLotMutation,
} from '../generated/graphql';

export const useUpdateWarehouseLot = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateWarehouseLotMutation] = useUpdateWarehouseLotMutation();

  const updateWarehouseLot = useCallback(
    async (variables: UpdateWarehouseLotMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateWarehouseLotMutation({
          variables,
        });
        return response?.data?.updateInBoundWarehouseAreaId || false;
      } catch (e) {
        setError('No WarehouseAreas');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateWarehouseLotMutation],
  );

  return {
    updateWarehouseLot,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
