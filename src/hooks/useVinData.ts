import {useCallback, useState} from 'react';

import {useVinDataLazyQuery, VinDataQueryVariables} from '../generated/graphql';

export const useVinData = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [vinDataQuery] = useVinDataLazyQuery();

  const getVinData = useCallback(
    async (variables: VinDataQueryVariables) => {
      try {
        setIsLoading(true);
        const response = await vinDataQuery({
          variables,
        });
        return response?.data?.vinData || null;
      } catch (e) {
        setError('Invalid VIN');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [vinDataQuery],
  );

  return {
    getVinData,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
