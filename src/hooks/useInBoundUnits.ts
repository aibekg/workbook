import {useCallback, useEffect, useRef, useState} from 'react';
import groupBy from 'lodash/groupBy';

import {
  InBoundUnitsQueryVariables,
  Unit,
  useInBoundUnitsLazyQuery,
  User,
} from '../generated/graphql';

export type TInBoundUnits = {
  status: string;
  data: Unit[];
  hiddenData: THiddenData[];
};

export type THiddenData = {
  user: User;
  data: Unit[];
};

const useInBoundUnits = () => {
  const [getInBoundUnitsQuery] = useInBoundUnitsLazyQuery();
  const isLoadingInternal = useRef<boolean>(false);
  const [isLoading, setIsLoading] = useState(false);
  const [inBoundUnits, setInBoundUnits] = useState<TInBoundUnits[]>([]);
  const [error] = useState<null | string>(null);

  const getInBoundUnits = useCallback(
    async (variables?: InBoundUnitsQueryVariables) => {
      if (!isLoadingInternal.current) {
        try {
          isLoadingInternal.current = true;
          setIsLoading(true);
          const response = await getInBoundUnitsQuery({
            variables,
            fetchPolicy: 'no-cache',
          });
          const inBoundUnitsData = response.data?.inBoundUnits;

          if (inBoundUnitsData?.length) {
            const groupedData = inBoundUnitsData.map(inBoundUnit => {
              const groupedHiddenData = groupBy(
                inBoundUnit?.hiddenData,
                data => data?.user?.id,
              );
              const hiddenData = Object.keys(groupedHiddenData).map(id => ({
                user: inBoundUnit?.hiddenData?.find(
                  data => data!.user!.id === id,
                )?.user,
                data: groupedHiddenData[id],
              }));
              return {
                ...inBoundUnit,
                hiddenData,
              };
            });

            const groupedInBoundData = groupBy(
              groupedData,
              data => data?.status,
            );
            const inBoundData = Object.keys(groupedInBoundData).map(
              (statusKey: string) =>
                ({
                  data: groupedInBoundData[statusKey]
                    .map(item => item.data)
                    .flat(),
                  hiddenData: groupedInBoundData[statusKey]
                    .map(item => item.hiddenData)
                    .flat(),
                  status: statusKey,
                } as TInBoundUnits),
            );

            setInBoundUnits(
              inBoundData.filter(
                item => item.data.length > 0 || item.hiddenData.length > 0,
              ) as TInBoundUnits[],
            );
          }
          return inBoundUnitsData || [];
        } catch (e) {
        } finally {
          isLoadingInternal.current = false;
          setIsLoading(false);
        }
      }
    },
    [getInBoundUnitsQuery],
  );

  useEffect(() => {
    getInBoundUnits();
  }, [getInBoundUnits]);

  return {
    getInBoundUnits,
    inBoundUnits,
    isLoading,
    error,
  };
};

export default useInBoundUnits;
