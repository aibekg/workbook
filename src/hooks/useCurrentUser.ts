import {useCallback} from 'react';

import {useActions} from '@hooks/useActions';

import {useCurrentUserLazyQuery} from '../generated/graphql';

export const useCurrentUser = () => {
  const [getCurrentUserQuery] = useCurrentUserLazyQuery();
  const {setAuth, setUser} = useActions();

  return useCallback(() => {
    getCurrentUserQuery()
      .then(response => {
        if (response.error) {
          setAuth(null, null);
        } else if (response?.data?.currentUser) {
          setUser(response?.data?.currentUser);
        }
      })
      .catch(() => {
        setAuth(null, null);
      });
  }, [setAuth, setUser, getCurrentUserQuery]);
};
