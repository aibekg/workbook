import {useCallback, useEffect, useState} from 'react';

import {useWarehouseAreaLazyQuery, WarehouseArea} from '../generated/graphql';

export const useWarehouseAreas = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [warehouseAreas, setWarehouseAreas] = useState<WarehouseArea[]>([]);
  const [selectedWarehouseArea, setSelectedWarehouseArea] =
    useState<WarehouseArea | null>(null);

  const [unitWarehouseAreasQuery] = useWarehouseAreaLazyQuery();

  const getUnitWarehouseAreas = useCallback(async () => {
    try {
      setIsLoading(true);
      const response = await unitWarehouseAreasQuery();
      return response?.data?.warehouseAreaByWarehouseId || [];
    } catch (e) {
      setError('No WarehouseAreas');
      return null;
    } finally {
      setIsLoading(false);
    }
  }, [unitWarehouseAreasQuery]);

  useEffect(() => {
    getUnitWarehouseAreas().then(data => {
      setWarehouseAreas(data as WarehouseArea[]);
    });
  }, [getUnitWarehouseAreas]);

  return {
    selectedWarehouseArea,
    setSelectedWarehouseArea,
    getUnitWarehouseAreas,
    isLoading,
    setIsLoading,
    error,
    setError,
    warehouseAreas,
  };
};
