import {useCallback, useState} from 'react';

import {UnitStatus} from '@screens/Units/types';

import {
  Unit,
  UnitByIdQueryVariables,
  useUnitByIdLazyQuery,
} from '../generated/graphql';

export enum EUnitErrors {
  LotMissing = 'Lot is Missing for this Item',
}

const useUnitById = () => {
  const [getUnitByIdQuery] = useUnitByIdLazyQuery();
  const [error, setError] = useState<EUnitErrors | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [unit, setUnit] = useState<null | Unit>(null);

  const getUnitById = useCallback(
    async (variables: UnitByIdQueryVariables) => {
      try {
        setIsLoading(true);
        const response = await getUnitByIdQuery({
          variables,
          fetchPolicy: 'no-cache',
        });
        const unitData = response.data?.unitById;

        if (unitData) {
          if (unitData?.status === UnitStatus.LOT_MISSING) {
            setError(EUnitErrors.LotMissing);
          }
          setUnit(unitData as Unit);
        }
      } catch (e) {
      } finally {
        setIsLoading(false);
      }
    },
    [getUnitByIdQuery],
  );

  return {
    getUnitById,
    unit,
    isLoading,
    setIsLoading,
    setError,
    error,
  };
};

export default useUnitById;
