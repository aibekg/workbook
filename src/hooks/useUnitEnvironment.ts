import {useCallback, useState} from 'react';

import {
  AddUnitEnvironmentMutationVariables,
  useAddUnitEnvironmentMutation,
} from '../generated/graphql';

export const useUnitEnvironment = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [addUnitEnvironmentMutation] = useAddUnitEnvironmentMutation();

  const addUnitEnvironment = useCallback(
    async (variables: AddUnitEnvironmentMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await addUnitEnvironmentMutation({
          variables,
        });
        return response?.data?.addEnvironment || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [addUnitEnvironmentMutation],
  );

  return {
    addUnitEnvironment,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
