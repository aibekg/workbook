import {useCallback, useState} from 'react';

import {
  UpdateUnitKeysMutationVariables,
  useUpdateUnitKeysMutation,
} from '../generated/graphql';

export const useUpdateUnitKeys = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateUnitKeysMutation] = useUpdateUnitKeysMutation();

  const updateUnitKeys = useCallback(
    async (variables: UpdateUnitKeysMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateUnitKeysMutation({
          variables,
        });
        return response?.data?.updateUnitKeys || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateUnitKeysMutation],
  );

  return {
    updateUnitKeys,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
