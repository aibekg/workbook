import {useCallback, useEffect, useRef, useState} from 'react';

import {
  CarrierDriver,
  CarrierDriversQueryVariables,
  useCarrierDriversLazyQuery,
} from '../generated/graphql';

export const useCarrierDrivers = (
  carrierCompanyId?: string,
  search?: string,
) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isPaginating, setIsPaginating] = useState<boolean>(false);
  const isLoadingInternal = useRef<boolean>(false);
  const [error, setError] = useState<string>('');
  const [carrierDrivers, setCarrierDrivers] = useState<CarrierDriver[]>([]);
  const offset = useRef<number>(0);
  const noMorePagination = useRef<boolean>(false);
  const [selectedCarrierDriver, setSelectedCarrierDriver] =
    useState<CarrierDriver | null>(null);

  const [carrierDriversQuery] = useCarrierDriversLazyQuery();

  const getCarrierDrivers = useCallback(
    async (variables?: CarrierDriversQueryVariables) => {
      if (
        carrierCompanyId &&
        !isLoadingInternal.current &&
        (offset.current > 0 ? !noMorePagination.current : true)
      ) {
        try {
          isLoadingInternal.current = true;
          setIsLoading(true);
          if (offset.current > 0) {
            setIsPaginating(true);
          }
          const response = await carrierDriversQuery({
            variables: {
              ...variables,
              carrierCompanyId,
              search,
              offset: offset.current,
              limit: 40,
            },
          });
          const carrierDriversData = response?.data?.carrierDrivers;
          if (carrierDriversData) {
            if (!carrierDriversData.length) {
              noMorePagination.current = true;
            }
            setCarrierDrivers(prev => {
              const newCarrierDrivers = [
                ...prev,
                ...(carrierDriversData as CarrierDriver[]),
              ];
              return newCarrierDrivers.filter((item, pos) => {
                return (
                  newCarrierDrivers.findIndex(
                    subItem => item.id === subItem.id,
                  ) === pos
                );
              });
            });
            offset.current = offset.current + 40;
          }
          return carrierDriversData || [];
        } catch (e) {
          setError('No CarrierDrivers');
          return null;
        } finally {
          setIsLoading(false);
          setIsPaginating(false);
          isLoadingInternal.current = false;
        }
      }
    },
    [carrierCompanyId, search, carrierDriversQuery],
  );

  useEffect(() => {
    setCarrierDrivers([]);
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
  }, [search]);

  useEffect(() => {
    getCarrierDrivers();
  }, [getCarrierDrivers]);

  const refresh = useCallback(() => {
    setCarrierDrivers([]);
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
    getCarrierDrivers();
  }, [getCarrierDrivers]);

  return {
    getCarrierDrivers,
    isLoading,
    setIsLoading,
    isPaginating,
    error,
    setError,
    carrierDrivers,
    selectedCarrierDriver,
    setSelectedCarrierDriver,
    refresh,
  };
};
