import {useCallback, useEffect, useRef, useState} from 'react';

import {
  CarrierCompaniesQueryVariables,
  CarrierCompany,
  useCarrierCompaniesLazyQuery,
} from '../generated/graphql';

export const useCarrierCompanies = (search: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isPaginating, setIsPaginating] = useState<boolean>(false);
  const isLoadingInternal = useRef<boolean>(false);
  const [error, setError] = useState<string>('');
  const [carrierCompanies, setCarrierCompanies] = useState<CarrierCompany[]>(
    [],
  );
  const offset = useRef<number>(0);
  const noMorePagination = useRef<boolean>(false);
  const [selectedCarrierCompany, setSelectedCarrierCompany] =
    useState<CarrierCompany | null>(null);

  const [carrierCompaniesQuery] = useCarrierCompaniesLazyQuery();

  const getCarrierCompanies = useCallback(
    async (variables?: CarrierCompaniesQueryVariables) => {
      if (
        !isLoadingInternal.current &&
        (offset.current > 0 ? !noMorePagination.current : true)
      ) {
        try {
          isLoadingInternal.current = true;
          setIsLoading(true);
          if (offset.current > 0) {
            setIsPaginating(true);
          }
          const response = await carrierCompaniesQuery({
            variables: {
              ...variables,
              search,
              offset: offset.current,
              limit: 40,
            },
          });
          const carrierCompaniesData = response?.data?.carrierCompanies;
          if (carrierCompaniesData) {
            if (!carrierCompaniesData.length) {
              noMorePagination.current = true;
            }
            setCarrierCompanies(prev => {
              const newCarrierCompanies = [
                ...prev,
                ...(carrierCompaniesData as CarrierCompany[]),
              ];
              return newCarrierCompanies.filter((item, pos) => {
                return (
                  newCarrierCompanies.findIndex(
                    subItem => item.id === subItem.id,
                  ) === pos
                );
              });
            });
            offset.current = offset.current + 40;
          }
          return carrierCompaniesData || [];
        } catch (e) {
          setError('No CarrierCompanies');
          return null;
        } finally {
          setIsLoading(false);
          setIsPaginating(false);
          isLoadingInternal.current = false;
        }
      }
    },
    [search, carrierCompaniesQuery],
  );

  useEffect(() => {
    setCarrierCompanies([]);
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
  }, [search]);

  useEffect(() => {
    getCarrierCompanies();
  }, [getCarrierCompanies]);

  return {
    getCarrierCompanies,
    isLoading,
    setIsLoading,
    isPaginating,
    error,
    setError,
    carrierCompanies,
    selectedCarrierCompany,
    setSelectedCarrierCompany,
  };
};
