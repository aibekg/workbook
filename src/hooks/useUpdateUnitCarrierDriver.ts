import {useCallback, useState} from 'react';

import {
  UpdateUnitCarrierDriverMutationVariables,
  useUpdateUnitCarrierDriverMutation,
} from '../generated/graphql';

export const useUpdateUnitCarrierDriver = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateUnitCarrierDriverMutation] =
    useUpdateUnitCarrierDriverMutation();

  const updateUnitCarrierDriver = useCallback(
    async (variables: UpdateUnitCarrierDriverMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateUnitCarrierDriverMutation({
          variables,
        });
        return response?.data?.updateUnitCarrierDriver || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateUnitCarrierDriverMutation],
  );

  return {
    updateUnitCarrierDriver,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
