import {useCallback, useEffect, useState} from 'react';

import {
  UnitExterior,
  UnitExteriorsQueryVariables,
  useUnitExteriorsLazyQuery,
} from '../generated/graphql';

export const useUnitExteriors = (unitId?: string | null) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [exteriors, setExteriors] = useState<UnitExterior[]>([]);

  const [unitExteriorsQuery] = useUnitExteriorsLazyQuery();

  const getUnitExteriors = useCallback(
    async (variables: UnitExteriorsQueryVariables) => {
      try {
        setIsLoading(true);
        const response = await unitExteriorsQuery({
          variables,
          fetchPolicy: 'no-cache',
        });
        return response?.data?.unitExteriors || [];
      } catch (e) {
        setError('No Exteriors');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [unitExteriorsQuery],
  );

  useEffect(() => {
    if (unitId) {
      getUnitExteriors({unitId}).then(exteriorsResult => {
        setExteriors(exteriorsResult as UnitExterior[]);
      });
    }
  }, [unitId, getUnitExteriors]);

  return {
    getUnitExteriors,
    isLoading,
    setIsLoading,
    error,
    setError,
    exteriors,
    setExteriors,
  };
};
