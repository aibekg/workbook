import {useCallback, useEffect, useState} from 'react';
import orderBy from 'lodash/orderBy';

import {Comodity, useUnitComoditiesLazyQuery} from '../generated/graphql';

export const useUnitComodities = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [commodities, setCommodities] = useState<Comodity[]>([]);
  const [error, setError] = useState<string>('');

  const [unitComoditiesQuery] = useUnitComoditiesLazyQuery();

  const getUnitComodities = useCallback(async (): Promise<Comodity[]> => {
    try {
      setIsLoading(true);
      const response = await unitComoditiesQuery();
      if (response.error) {
        return [];
      }
      return (
        orderBy(response?.data?.unitComodities as Comodity[], ['id']) || []
      );
    } catch (e) {
      setError('No Comodities');
      return [];
    } finally {
      setIsLoading(false);
    }
  }, [unitComoditiesQuery]);

  useEffect(() => {
    getUnitComodities().then(data => {
      setCommodities(data as Comodity[]);
    });
  }, [getUnitComodities]);

  return {
    getUnitComodities,
    isLoading,
    setIsLoading,
    error,
    setError,
    commodities,
    setCommodities,
  };
};
