import {useCallback, useEffect, useState} from 'react';
import orderBy from 'lodash/orderBy';

import {Color, useUnitColorsLazyQuery} from '../generated/graphql';

export const useUnitColors = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [colors, setColors] = useState<Color[]>([]);

  const [unitColorsQuery] = useUnitColorsLazyQuery();

  const getUnitColors = useCallback(async () => {
    try {
      setIsLoading(true);
      const response = await unitColorsQuery();
      return orderBy(response?.data?.unitColors, ['id']) || [];
    } catch (e) {
      setError('No Colors');
      return null;
    } finally {
      setIsLoading(false);
    }
  }, [unitColorsQuery]);

  useEffect(() => {
    getUnitColors().then(data => {
      setColors(data as Color[]);
    });
  }, [getUnitColors]);

  return {
    getUnitColors,
    isLoading,
    setIsLoading,
    error,
    setError,
    colors,
  };
};
