import {useCallback, useState} from 'react';

import {
  ReceiveUnitMutationVariables,
  useReceiveUnitMutation,
} from '../generated/graphql';

export const useReceiveUnit = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [receiveUnitMutation] = useReceiveUnitMutation();

  const receiveUnit = useCallback(
    async (variables: ReceiveUnitMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await receiveUnitMutation({
          variables,
        });
        return response?.data?.receiveUnit || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [receiveUnitMutation],
  );

  return {
    receiveUnit,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
