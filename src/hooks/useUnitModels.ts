import {useCallback, useEffect, useRef, useState} from 'react';
import {useSelector} from 'react-redux';

import {selectNewUnitData} from '@store/unit/selectors';

import {
  UnitModelsQueryVariables,
  useUnitModelsLazyQuery,
} from '../generated/graphql';

export const useUnitModels = (search: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isPaginating, setIsPaginating] = useState<boolean>(false);
  const isLoadingInternal = useRef<boolean>(false);
  const [error, setError] = useState<string>('');
  const [models, setModels] = useState<string[]>([]);
  const offset = useRef<number>(0);
  const noMorePagination = useRef<boolean>(false);

  const {manufacturer} = useSelector(selectNewUnitData);

  const [unitModelsQuery] = useUnitModelsLazyQuery();

  const getUnitModels = useCallback(
    async (variables?: UnitModelsQueryVariables) => {
      if (
        manufacturer &&
        !isLoadingInternal.current &&
        (offset.current > 0 ? !noMorePagination.current : true)
      ) {
        try {
          isLoadingInternal.current = true;
          setIsLoading(true);
          if (offset.current > 0) {
            setIsPaginating(true);
          }
          const response = await unitModelsQuery({
            variables: {
              ...variables,
              search,
              manufacturer,
              offset: offset.current,
              limit: 40,
            },
          });
          const unitModels = response?.data?.unitModels;
          if (unitModels) {
            if (!unitModels.length) {
              noMorePagination.current = true;
            }
            setModels(prev => {
              const newUnitModels = [...prev, ...(unitModels as string[])];
              return newUnitModels.filter((item, pos) => {
                return newUnitModels.indexOf(item) === pos;
              });
            });
            offset.current = offset.current + 40;
          }
          return unitModels || [];
        } catch (e) {
          setError('No Models');
          return null;
        } finally {
          setIsLoading(false);
          setIsPaginating(false);
          isLoadingInternal.current = false;
        }
      }
    },
    [search, manufacturer, unitModelsQuery],
  );

  useEffect(() => {
    if (search) {
      setModels([]);
    }
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
  }, [search]);

  useEffect(() => {
    getUnitModels();
  }, [getUnitModels]);

  return {
    getUnitModels,
    isLoading,
    isPaginating,
    setIsLoading,
    error,
    setError,
    models,
  };
};
