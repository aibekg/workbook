import {useCallback, useEffect, useState} from 'react';

export const useUnitYears = () => {
  const [years, setYears] = useState<number[]>([]);

  const generateYears = useCallback((limit = 100): number[] => {
    const max = new Date().getFullYear() + 2;
    const min = max - (limit - 1);
    const yearsArray = [];

    for (let i = max; i >= min; i--) {
      yearsArray.push(i);
    }
    return yearsArray;
  }, []);

  useEffect(() => {
    setYears(generateYears());
  }, [generateYears]);

  return {years, generateYears};
};
