import {useCallback, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import orderBy from 'lodash/orderBy';

import {selectNewUnitData} from '@store/unit/selectors';

import {
  Comodity,
  ComodityType,
  useUnitTypesLazyQuery,
} from '../generated/graphql';

export const useUnitTypes = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [types, setTypes] = useState<Comodity[]>([]);
  const [error, setError] = useState<string>('');

  const {commodityId} = useSelector(selectNewUnitData);

  const [unitTypesQuery] = useUnitTypesLazyQuery();

  const getUnitTypes = useCallback(async (): Promise<ComodityType[]> => {
    if (commodityId) {
      try {
        setIsLoading(true);
        const response = await unitTypesQuery({
          variables: {
            comodityId: commodityId,
          },
        });
        if (response.error) {
          return [];
        }
        return (
          orderBy(response?.data?.unitTypes as ComodityType[], ['id']) || []
        );
      } catch (e) {
        setError('No Types');
        return [];
      } finally {
        setIsLoading(false);
      }
    }
    return [];
  }, [commodityId, unitTypesQuery]);

  useEffect(() => {
    getUnitTypes().then(data => {
      setTypes(data as Comodity[]);
    });
  }, [getUnitTypes]);

  return {
    getUnitTypes,
    isLoading,
    setIsLoading,
    error,
    setError,
    types,
    setTypes,
  };
};
