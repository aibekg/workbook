import {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';

export type TUsePaginationProps<T, K> = {
  hookReturnValues: THookReturnType<T, K>;
  search?: string;
};

export type THookReturnType<T, K> = {
  state: T[];
  setState: Dispatch<SetStateAction<T[]>>;
  getData: (variables: K) => Promise<T[]>;
  isLoading: boolean;
  setIsLoading: Dispatch<SetStateAction<boolean>>;
  error: string | null;
  setError: Dispatch<SetStateAction<string | null>>;
};

export function usePagination<T, K>({
  hookReturnValues,
  search,
}: TUsePaginationProps<T, K>) {
  const isLoadingInternal = useRef(false);
  const offset = useRef<number>(0);
  const noMorePagination = useRef<boolean>(false);
  const [isPaginating, setIsPaginating] = useState(false);
  const {getData, state, setState, isLoading, error, setIsLoading} =
    hookReturnValues;

  const getDataWithPagination = useCallback(async () => {
    if (
      !isLoadingInternal.current &&
      (offset.current > 0 ? !noMorePagination.current : true)
    ) {
      try {
        isLoadingInternal.current = true;
        if (offset.current > 0) {
          setIsPaginating(true);
        }

        const response = await getData({
          search,
          offset: offset.current,
          limit: 40,
        } as unknown as K);

        if (response) {
          if (!response.length) {
            noMorePagination.current = true;
          }
          setState(prev => {
            const newUnitData = [...prev, ...(response as T[])];
            return newUnitData.filter((item, pos) => {
              return newUnitData.indexOf(item) === pos;
            });
          });
          offset.current = offset.current + 40;
        }
        return response || [];
      } finally {
        setIsPaginating(false);
        isLoadingInternal.current = false;
      }
    }
  }, [getData, search, setState]);

  useEffect(() => {
    getDataWithPagination();
  }, [getDataWithPagination]);

  useEffect(() => {
    setState([]);
    offset.current = 0;
    noMorePagination.current = false;
    setIsLoading(true);
    setIsPaginating(false);
  }, [search, setIsLoading, setState]);

  return {
    getDataWithPagination,
    isPaginating,
    state,
    isLoading,
    error,
  };
}
