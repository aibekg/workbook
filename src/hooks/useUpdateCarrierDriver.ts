import {useCallback, useState} from 'react';

import {
  UpdateCarrierDriverMutationVariables,
  useUpdateCarrierDriverMutation,
} from '../generated/graphql';

export const useUpdateCarrierDriver = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const [updateCarrierDriverMutation] = useUpdateCarrierDriverMutation();

  const updateCarrierDriver = useCallback(
    async (variables: UpdateCarrierDriverMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateCarrierDriverMutation({
          variables,
        });
        return response?.data?.updateCarrierDriver || null;
      } catch (e) {
        setError('Invalid Input');
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [updateCarrierDriverMutation],
  );

  return {
    updateCarrierDriver,
    isLoading,
    setIsLoading,
    error,
    setError,
  };
};
