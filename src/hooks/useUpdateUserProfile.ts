import {useCallback, useState} from 'react';
import {useSelector} from 'react-redux';

import {useActions} from '@hooks/useActions';
import {selectUser} from '@store/auth/selectors';

import {
  UpdateUserProfileMutationVariables,
  UserItemFragment,
  useUpdateUserProfileMutation,
} from '../generated/graphql';

export const useUpdateUserProfile = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<null | string>(null);

  const [updateUserProfileRequest] = useUpdateUserProfileMutation();
  const {setUser} = useActions();
  const user = useSelector(selectUser);

  const updateUserProfile = useCallback(
    async (variables: UpdateUserProfileMutationVariables) => {
      try {
        setIsLoading(true);
        const response = await updateUserProfileRequest({
          variables,
        });
        if (response.data?.updateUserProfile) {
          setUser({
            ...user,
            ...(variables.input as UserItemFragment),
            photo: response.data.updateUserProfile.photo,
          });
          return response.data.updateUserProfile;
        }

        setError('Failed to update');
        if (response.errors) {
          setError(response.errors[0].message);
          return false;
        }
      } catch (e) {
        setError('Failed to update');
        return false;
      } finally {
        setIsLoading(false);
      }
    },
    [setUser, updateUserProfileRequest, user],
  );

  return {
    updateUserProfile,
    isLoading,
    error,
    setError,
  };
};
