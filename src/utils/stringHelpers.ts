function validateVinHelper(vin: string) {
  if (vin === '11111111111111111') {
    return false;
  }
  if (!vin.match('^([0-9a-hj-npr-zA-HJ-NPR-Z]{10,17})+$')) {
    return false;
  }
  const letters: any[] = [
    {k: 'A', v: 1},
    {k: 'B', v: 2},
    {k: 'C', v: 3},
    {k: 'D', v: 4},
    {k: 'E', v: 5},
    {k: 'F', v: 6},
    {k: 'G', v: 7},
    {k: 'H', v: 8},
    {k: 'J', v: 1},
    {k: 'K', v: 2},
    {k: 'L', v: 3},
    {k: 'M', v: 4},
    {k: 'N', v: 5},
    {k: 'P', v: 7},
    {k: 'R', v: 9},
    {k: 'S', v: 2},
    {k: 'T', v: 3},
    {k: 'U', v: 4},
    {k: 'V', v: 5},
    {k: 'W', v: 6},
    {k: 'X', v: 7},
    {k: 'Y', v: 8},
    {k: 'Z', v: 9},
  ];
  const weights: number[] = [
    8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2,
  ];
  const exclude: string[] = ['I', 'O', 'Q'];
  let val = 0;
  for (let idx = 0; idx < vin.length; idx++) {
    let item = vin.charAt(idx).toUpperCase();
    if (
      exclude.find(x => {
        return x === item;
      })
    ) {
      return false;
    }
    const pos =
      item.match('^[0-9]+$') != null
        ? parseInt(item)
        : letters.filter(function (letter) {
            return letter.k === item;
          })[0].v;
    val += pos * weights[idx];
  }
  const checksum = val % 11;
  return vin.charAt(8) === (checksum < 10 ? checksum.toString() : 'X');
}

export function validateVin(vin: string, validateReal?: boolean) {
  if (validateReal) {
    return validateVinHelper(vin);
  }
  return vin.length >= 12;
}

const capitalizeWordHelper = (word: string) => {
  if (word) {
    return `${word[0].toUpperCase()}${word
      .slice(1, word.length)
      .toLowerCase()}`;
  }
  return '';
};

export const capitalizeWord = (word: string) => {
  return (word || '').split(' ').map(capitalizeWordHelper).join(' ');
};

const colorMap: Record<string, string> = {
  blue: '#6299FF',
  silver: '#E7E7E7',
  grey: '#B5B5B5',
  black: '#000000',
  white: '#FFFFFF',
  red: '#FF6057',
  brown: '#876153',
  green: '#4DD862',
  other: '#934EEC',
};

export const getColorString = (color: string) => {
  return colorMap[color] || '#FFFFFF';
};

export const camelCaseToHumanReadable = (str: string) => {
  return (str || '')
    .split('_')
    .map(item =>
      (item.charAt(0).toUpperCase() + item.slice(1)).replace(/_/g, ''),
    )
    .join(' ');
};

export const enumToHumanReadable = (str: string) => {
  return (str || '')
    .split('_')
    .map(item =>
      (item.charAt(0).toUpperCase() + item.slice(1))
        .replace(/_/g, '')
        .toUpperCase(),
    )
    .join(' ');
};

export function generateUUID() {
  let d = new Date().getTime();
  let d2 =
    (typeof performance !== 'undefined' &&
      performance.now &&
      performance.now() * 1000) ||
    0;
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = Math.random() * 16;
    if (d > 0) {
      // eslint-disable-next-line no-bitwise
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      // eslint-disable-next-line no-bitwise
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    // eslint-disable-next-line no-bitwise
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
}
