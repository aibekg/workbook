import TimeAgo from 'javascript-time-ago';
import timeAgoEn from 'javascript-time-ago/locale/en.json';
import {DateTime} from 'luxon';

let timeAgo: TimeAgo | undefined;
if (!timeAgo) {
  TimeAgo.addLocale(timeAgoEn);
  timeAgo = new TimeAgo('en-US');
}

export const getTimeAgo = (time: string) => {
  if (!time) {
    return '';
  }
  return timeAgo?.format(DateTime.fromISO(time).toMillis(), 'mini-minute-now', {
    now: DateTime.now().toMillis(),
    round: 'floor',
  });
};
