// @ts-ignore
import XMLParser from 'react-xml-parser';
// @ts-ignore
import * as C from 'kld-contours';
// @ts-ignore
import * as I from 'kld-intersections';

import {chunkArray} from '@utils/arrayHelpers';
import {generateUUID} from '@utils/stringHelpers';

import {Damage} from '../generated/graphql';
import {GeneratedAffineShape, SVGPathPart} from '../types/svgTypes';

export const svgToObject = (svg: string) => {
  if (!svg) {
    return null;
  }
  const svgJson = new XMLParser().parseFromString(svg);
  return {
    height: svgJson.attributes.height,
    width: svgJson.attributes.width,
    paths: svgJson.children
      .filter((item: any) => item.name === 'path')
      .map((path: any, pathIndex: number) => ({
        d: path.attributes.d,
        id: path.attributes.id || generateUUID(),
      })),
  };
};

const SVG_PATH_COMMANDS: any = {
  M: ['x', 'y'],
  m: ['dx', 'dy'],
  H: ['x'],
  h: ['dx'],
  V: ['y'],
  v: ['dy'],
  L: ['x', 'y'],
  l: ['dx', 'dy'],
  Z: [],
  C: ['x1', 'y1', 'x2', 'y2', 'x', 'y'],
  c: ['dx1', 'dy1', 'dx2', 'dy2', 'dx', 'dy'],
  S: ['x2', 'y2', 'x', 'y'],
  s: ['dx2', 'dy2', 'dx', 'dy'],
  Q: ['x1', 'y1', 'x', 'y'],
  q: ['dx1', 'dy1', 'dx', 'dy'],
  T: ['x', 'y'],
  t: ['dx', 'dy'],
  A: ['rx', 'ry', 'rotation', 'large-arc', 'sweep', 'x', 'y'],
  a: ['rx', 'ry', 'rotation', 'large-arc', 'sweep', 'dx', 'dy'],
};

export const fromPathToArray = (path: {d: string; id: string}) => {
  const pathItems: any[] = path?.d
    .replace(/\s/g, '&')
    .split(new RegExp(`(?=[${Object.keys(SVG_PATH_COMMANDS)}\\s])`, 'g'));
  const segments: SVGPathPart[] = [];
  pathItems.forEach(pathItem => {
    let currentCommand: any = '';
    if (SVG_PATH_COMMANDS.hasOwnProperty(pathItem[0])) {
      currentCommand = pathItem[0];
    }
    if (currentCommand) {
      const pathValues = pathItem.slice(1, pathItem.length).split('&');
      const chunkedPathValues = chunkArray(pathValues, 2);
      const currentElement = {
        kind: currentCommand,
        points: chunkedPathValues.map((item: number[]) => ({
          x: Number(item[0]),
          y: Number(item[1]),
        })),
      };
      segments.push(currentElement);
    }
  });
  return {
    id: path.id,
    paths: segments,
  };
};

export const fromArrayToPath = (
  height: number,
  width: number,
  points: number[][],
) => {
  return `
    <svg height="${height}" width="${width}">
        <path d="${points.map(pointXY => `M${pointXY.join(' ')}`).join('')}" />
    </svg>
  `;
};

export const generateAffineShapes = (svgString: string) => {
  const svgObject = svgToObject(svgString);
  const svgPaths: {id: string; d: string}[] = svgObject?.paths || [];

  const getLastPoint = (path: SVGPathPart) => {
    if (!path || path.kind === 'Z') {
      return undefined;
    }
    switch (path.kind) {
      case 'A':
      case 'M':
      case 'T':
      case 'L':
      case 'H':
        return path.points[0];
      case 'C':
      case 'Q':
      case 'S':
        return path.points[path.points.length - 1];
      default:
        return null;
    }
  };

  const getSecondLastPoint = (path: SVGPathPart) => {
    if (!path) {
      return undefined;
    }
    switch (path.kind) {
      case 'Z':
      case 'A':
      case 'M':
      case 'L':
      case 'H':
        return undefined;
      case 'T':
        return path.points[0];
      case 'C':
      case 'Q':
      case 'S':
        return path.points[path.points.length - 2];
      default:
        return null;
    }
  };

  const affineShapes: I.PolygonAffineShape[] = [];

  svgPaths.map(path => {
    if (path) {
      const {paths: pathArray} = fromPathToArray(path);
      const contourPoints: I.Point2D[] = [];
      pathArray.map((command, i) => {
        const lastPoint = getLastPoint(pathArray[i - 1]);
        const secondLastPoint = getSecondLastPoint(pathArray[i - 1]);
        if (!lastPoint) {
          return;
        }
        switch (command.kind) {
          case 'Z':
            break;
          case 'A':
          case 'M':
          case 'L':
            contourPoints.push(
              new I.Point2D(command.points[0].x, command.points[0].y),
            );
            break;
          case 'H':
            contourPoints.push(new I.Point2D(command.points[0].x, 0));
            break;
          case 'C':
            const cubicBezier = new C.CubicBezier2D(
              new I.Point2D(lastPoint.x, lastPoint.y),
              new I.Point2D(command.points[0].x, command.points[0].y),
              new I.Point2D(command.points[1].x, command.points[1].y),
              new I.Point2D(command.points[2].x, command.points[2].y),
            );
            const cubicBezierPolygon = cubicBezier.toPolygon2D();
            cubicBezierPolygon.points
              .slice(1)
              .map((point: I.Point2D) => contourPoints.push(point));
            break;
          case 'Q':
            const quadraticBezier = new C.QuadraticBezier2D(
              new I.Point2D(lastPoint.x, lastPoint.y),
              new I.Point2D(command.points[0].x, command.points[0].y),
              new I.Point2D(command.points[1].x, command.points[1].y),
            );
            const quadraticBezierPolygon = quadraticBezier.toPolygon2D();
            quadraticBezierPolygon.points
              .slice(1)
              .map((point: I.Point2D) => contourPoints.push(point));
            break;
          case 'S':
            if (secondLastPoint) {
              const _sx =
                lastPoint.x - Math.abs(lastPoint.x - secondLastPoint.x);
              const _sy =
                lastPoint.y - Math.abs(lastPoint.y - secondLastPoint.y);
              const smoothCubicBezier = new C.CubicBezier2D(
                new I.Point2D(lastPoint.x, lastPoint.y),
                new I.Point2D(_sx, _sy),
                new I.Point2D(command.points[0].x, command.points[0].y),
                new I.Point2D(command.points[1].x, command.points[1].y),
              );
              const smoothCubicBezierPolygon = smoothCubicBezier.toPolygon2D();
              smoothCubicBezierPolygon.points
                .slice(1)
                .map((point: I.Point2D) => contourPoints.push(point));
            }
            break;
          case 'T':
            if (secondLastPoint) {
              const _tx =
                lastPoint.x - Math.abs(lastPoint.x - secondLastPoint.x);
              const _ty =
                lastPoint.y - Math.abs(lastPoint.y - secondLastPoint.y);
              const smoothQuadraticBezier = new C.QuadraticBezier2D(
                new I.Point2D(lastPoint.x, lastPoint.y),
                new I.Point2D(_tx, _ty),
                new I.Point2D(command.points[0].x, command.points[0].y),
              );
              const smoothQuadraticBezierPolygon =
                smoothQuadraticBezier.toPolygon2D();
              smoothQuadraticBezierPolygon.points
                .slice(1)
                .map((point: I.Point2D) => contourPoints.push(point));
            }
            break;
          default:
            break;
        }
      });

      affineShapes.push({
        id: path.id,
        shape: I.AffineShapes.polygon(contourPoints),
      });
    }
  });

  return affineShapes.map(({id, shape}) => {
    return {
      id,
      shape,
      includes: affineShapes.reduce((acc, s) => {
        return I.Intersection.includesPolygonPolygon(
          shape.args[0],
          s.shape.args[0],
        )
          ? [...acc, s.id]
          : acc;
      }, []),
    };
  }) as GeneratedAffineShape[];
};

export const findIntersectionsWithShapes = (
  polyline: [number, number][],
  generatedShapes: GeneratedAffineShape[],
) => {
  if (polyline.length === 0) {
    return [];
  }

  return generatedShapes.map(part => {
    if (polyline.length === 1) {
      const {status, points} = I.Intersection.intersect(
        I.AffineShapes.circle(new I.Point2D(polyline[0][0], polyline[0][1]), 1),
        part.shape,
      );
      return {
        ...part,
        points,
        status,
      };
    }

    const {status, points} = I.Intersection.intersect(
      I.AffineShapes.polyline(
        polyline.map(point => new I.Point2D(point[0], point[1])),
      ),
      part.shape,
    );

    return {
      ...part,
      points,
      status,
    };
  });
};

export const removeOutsidePoints = (
  polyline: [number, number][],
  shapes: GeneratedAffineShape[],
): [number, number][] => {
  const affectedParts = findIntersectionsWithShapes(polyline, shapes);
  const intersectedParts = affectedParts.filter(
    s =>
      s.points.length > 0 &&
      !affectedParts.some(p => p.includes.indexOf(s.id) !== -1),
  );

  if (affectedParts.some(p => p?.status === 'Inside')) {
    return polyline;
  }
  if (!intersectedParts.length) {
    return polyline;
  }

  const removeFromBeginning = (polylineInner: [number, number][]) => {
    const updatedPolyline: [number, number][] = [];
    let pointReplaced: boolean = false;

    for (let i = 0; i < polylineInner.length && !pointReplaced; i++) {
      const point = polylineInner[i];
      const nextPoint =
        i + 1 < polylineInner.length ? polylineInner[i + 1] : null;

      for (let j = 0; j < intersectedParts.length && !pointReplaced; j++) {
        const intersectedPart = intersectedParts[j];
        const pointInsidePart = I.Intersection.includesPolygonPoint(
          intersectedPart.shape.args[0],
          new I.Point2D(point[0], point[1]),
        );

        if (pointInsidePart) {
          polylineInner.slice(i).map(p => {
            updatedPolyline.push(p);
          });
          pointReplaced = true;
        } else {
          const nextPointInsidePart =
            !!nextPoint &&
            I.Intersection.includesPolygonPoint(
              intersectedPart.shape.args[0],
              new I.Point2D(nextPoint[0], nextPoint[1]),
            );

          if (nextPointInsidePart) {
            const polylineHead = polylineInner.slice(0, i + 2);

            const polylineHeadIntersection = findIntersectionsWithShapes(
              polylineHead,
              shapes,
            ).filter(
              s =>
                s.points.length > 0 &&
                !affectedParts.some(p => p.includes.indexOf(s.id) !== -1),
            );

            const intersectionPoint =
              polylineHeadIntersection[0] &&
              polylineHeadIntersection[0].points &&
              polylineHeadIntersection[0].points[0];

            if (!intersectionPoint) {
              console.warn('Intersection point outside polyline not found');
            } else {
              updatedPolyline.push([intersectionPoint.x, intersectionPoint.y]);
            }

            polylineInner.slice(i + 1).map(p => {
              updatedPolyline.push(p);
            });

            pointReplaced = true;
          }
        }
      }
    }
    return updatedPolyline;
  };

  return removeFromBeginning(removeFromBeginning(polyline).reverse());
};

export const findDamagesInArea = (
  x: number,
  y: number,
  zoom: number,
  left: number,
  top: number,
  schemeScale: number,
  damages: (Damage | null | undefined)[],
) => {
  const touchCircle = I.AffineShapes.circle(
    new I.Point2D(
      (x - left) / zoom / schemeScale,
      (y - top) / zoom / schemeScale,
    ),
    12 / schemeScale,
  );
  return damages.filter(damage => {
    switch (damage?.shape) {
      case 'Line':
        if (damage?.svg) {
          const polylineSvgObject = svgToObject(damage?.svg);
          const svgPaths = (polylineSvgObject?.paths || []).map(
            (item: {d: string; id: string}) => fromPathToArray(item).paths,
          );
          if (svgPaths.length > 0) {
            const polylineIntersection = I.Intersection.intersect(
              touchCircle,
              I.AffineShapes.polyline(
                svgPaths[0].map(
                  (point: any) =>
                    new I.Point2D(point.points[0].x, point.points[0].y),
                ),
              ),
            );
            return polylineIntersection?.status === 'Intersection';
          }
        }
        return false;
      case 'Point':
        if (damage?.svg) {
          const polylineSvgObject = svgToObject(damage?.svg);
          const svgPaths = (polylineSvgObject?.paths || []).map(
            (item: {d: string; id: string}) => fromPathToArray(item).paths,
          );
          if (svgPaths.length > 0) {
            const circleIntersection = I.Intersection.intersect(
              touchCircle,
              I.AffineShapes.circle(
                new I.Point2D(
                  svgPaths[0][0].points[0].x,
                  svgPaths[0][0].points[0].y,
                ),
                5,
              ),
            );
            return (
              circleIntersection?.status === 'Intersection' ||
              circleIntersection?.status === 'Inside'
            );
          }
        }
        return false;
      default:
        return false;
    }
  });
};

export const findIntersectedParts = (
  points: [number, number][],
  generatedAffineShapes: GeneratedAffineShape[],
) => {
  if (points.length === 0) {
    return [];
  }

  const affectedParts = findIntersectionsWithShapes(
    points,
    generatedAffineShapes,
  );

  return affectedParts
    .filter(part => {
      if (part?.status === 'Outside' || part?.status === 'No Intersection') {
        return false;
      }
      return !(
        part?.status === 'Inside' &&
        part.includes.some(partId => {
          const includedPart = affectedParts.find(ap => ap.id === partId);
          return includedPart && includedPart?.status === 'Inside';
        })
      );
    })
    .map(({id}) => id);
};

export function calcDistance(x1: number, y1: number, x2: number, y2: number) {
  let dx = Math.abs(x1 - x2);
  let dy = Math.abs(y1 - y2);
  return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
}

export function calcCenter(x1: number, y1: number, x2: number, y2: number) {
  function middle(p1: number, p2: number) {
    return p1 > p2 ? p1 - (p1 - p2) / 2 : p2 - (p2 - p1) / 2;
  }

  return {
    x: middle(x1, x2),
    y: middle(y1, y2),
  };
}

export function maxOffset(
  offset: number,
  windowDimension: number,
  imageDimension: number,
) {
  let max = windowDimension - imageDimension;
  if (max >= 0) {
    return 0;
  }
  return offset < max ? max : offset;
}

export function calcOffsetByZoom(width: number, height: number, zoom: number) {
  let xDiff = width * zoom - width;
  let yDiff = height * zoom - height;
  return {
    left: -xDiff / 2,
    top: -yDiff / 2,
  };
}

export function getMarkCoords(coords: [number, number][]) {
  const sortedArray: any = coords.reduce(
    // @ts-ignore
    (accumulator, currentValue) => {
      const newXArray = [...accumulator.coordsX, currentValue[0]];
      const newYArray = [...accumulator.coordsY, currentValue[1]];
      return {coordsX: newXArray, coordsY: newYArray};
    },
    {coordsX: [], coordsY: []},
  );

  const left = Math.min.apply(Math, sortedArray.coordsX);
  const top = Math.min.apply(Math, sortedArray.coordsY);
  const right = Math.max.apply(Math, sortedArray.coordsX);
  const bottom = Math.max.apply(Math, sortedArray.coordsY);

  const cx = left + (right - left) / 2;
  const cy = top + (bottom - top) / 2;
  const centerCoords = {cx, cy};

  let distance = NaN;
  return coords.reduce(
    (accumulator, currentValue) => {
      const xCatWidth = Math.abs(centerCoords.cx - currentValue[0]);
      const yCatWidth = Math.abs(centerCoords.cy - currentValue[1]);
      const hypotenuse = Math.sqrt(
        Math.pow(xCatWidth, 2) + Math.pow(yCatWidth, 2),
      );
      if (hypotenuse < distance || !distance) {
        distance = hypotenuse;
        return {x: currentValue[0], y: currentValue[1]};
      }
      return accumulator;
    },
    {x: 0, y: 0},
  );
}

export const catmullRomFitting = function (
  data: {x: number; y: number}[],
  alpha: number,
) {
  if (alpha === 0 || alpha === undefined) {
    return false;
  } else {
    let p0, p1, p2, p3, bp1, bp2, d1, d2, d3, A, B, N, M;
    let d3powA, d2powA, d3pow2A, d2pow2A, d1pow2A, d1powA;
    let d = 'M' + Math.round(data[0].x) + ',' + Math.round(data[0].y) + ' ';
    let length = data.length;
    for (let i = 0; i < length - 1; i++) {
      p0 = i === 0 ? data[0] : data[i - 1];
      p1 = data[i];
      p2 = data[i + 1];
      p3 = i + 2 < length ? data[i + 2] : p2;

      d1 = Math.sqrt(Math.pow(p0.x - p1.x, 2) + Math.pow(p0.y - p1.y, 2));
      d2 = Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
      d3 = Math.sqrt(Math.pow(p2.x - p3.x, 2) + Math.pow(p2.y - p3.y, 2));

      d3powA = Math.pow(d3, alpha);
      d3pow2A = Math.pow(d3, 2 * alpha);
      d2powA = Math.pow(d2, alpha);
      d2pow2A = Math.pow(d2, 2 * alpha);
      d1powA = Math.pow(d1, alpha);
      d1pow2A = Math.pow(d1, 2 * alpha);

      A = 2 * d1pow2A + 3 * d1powA * d2powA + d2pow2A;
      B = 2 * d3pow2A + 3 * d3powA * d2powA + d2pow2A;
      N = 3 * d1powA * (d1powA + d2powA);
      if (N > 0) {
        N = 1 / N;
      }
      M = 3 * d3powA * (d3powA + d2powA);
      if (M > 0) {
        M = 1 / M;
      }

      bp1 = {
        x: (-d2pow2A * p0.x + A * p1.x + d1pow2A * p2.x) * N,
        y: (-d2pow2A * p0.y + A * p1.y + d1pow2A * p2.y) * N,
      };

      bp2 = {
        x: (d3pow2A * p1.x + B * p2.x - d2pow2A * p3.x) * M,
        y: (d3pow2A * p1.y + B * p2.y - d2pow2A * p3.y) * M,
      };

      if (bp1.x === 0 && bp1.y === 0) {
        bp1 = p1;
      }
      if (bp2.x === 0 && bp2.y === 0) {
        bp2 = p2;
      }

      d +=
        'C' +
        Math.round(bp1.x) +
        ',' +
        Math.round(bp1.y) +
        ' ' +
        Math.round(bp2.x) +
        ',' +
        Math.round(bp2.y) +
        ' ' +
        Math.round(p2.x) +
        ',' +
        Math.round(p2.y) +
        ' ';
    }

    return d;
  }
};

export const getPointDamageCoordinatesFromSvg = (svg?: string | null) => {
  if (!svg) {
    return {
      x: 0,
      y: 0,
    };
  }
  let x: number = 0;
  let y: number = 0;
  const svgObject = svgToObject(svg);
  if ((svgObject?.paths || []).length > 0) {
    const path = svgObject?.paths[0];
    const pathArray = fromPathToArray(path);
    x = pathArray.paths[0].points[0].x;
    y = pathArray.paths[0].points[0].y;
  }
  return {
    x,
    y,
  };
};

export const getShapeDamageCoordinatesFromSvg = (svg?: string | null) => {
  if (!svg) {
    return [
      {
        x: 0,
        y: 0,
      },
    ];
  }
  const svgObject = svgToObject(svg);
  const shapeArray: {x: number; y: number}[] = [];
  (svgObject?.paths || []).forEach((path: {d: string; id: string}) => {
    const pathArray = fromPathToArray(path);
    (pathArray.paths || []).forEach(pathItem => {
      (pathItem.points || []).forEach(pointItem => {
        shapeArray.push({
          x: pointItem.x,
          y: pointItem.y,
        });
      });
    });
  });
  return shapeArray;
};
