import {useEffect, useState} from 'react';
import {Dimensions, Keyboard, Platform} from 'react-native';
import {
  getBottomSpace,
  getStatusBarHeight,
  isIphoneX,
} from 'react-native-iphone-x-helper';

export const bottomSpace = getBottomSpace();
export const statusBarHeight =
  Platform.OS === 'android' ? 0 : getStatusBarHeight() + (isIphoneX() ? 8 : 0);

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
export const deviceRatio = deviceHeight / deviceWidth;

export const useKeyboardVisibility = () => {
  const [visible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    function onKeyboardShow() {
      setVisible(true);
    }

    function onKeyboardHide() {
      setVisible(false);
    }

    const showSubscription = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyboardShow,
    );
    const hideSubscription = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyboardHide,
    );

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  return {
    isKeyboardVisible: visible,
  };
};
