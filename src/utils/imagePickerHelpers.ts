import {Keyboard} from 'react-native';
import ImagePicker, {Options} from 'react-native-image-crop-picker';
import {ActionSheetOptions} from '@expo/react-native-action-sheet/lib/typescript/types';

export const getBlobFromFileURI = async (fileUri: string) => {
  try {
    const response = await fetch(fileUri);
    return await response.blob();
  } catch (e) {
    return null;
  }
};

export const convertBlobToBase64 = async (blob: any) => {
  try {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise(resolve => {
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
      reader.onerror = () => {
        resolve(null);
      };
    });
  } catch (err) {
    return null;
  }
};

const documentCancelButtonIndex = 2;

export const pickDocument: (
  showActionSheet: (
    options: ActionSheetOptions,
    callback: (i?: number) => void | Promise<void>,
  ) => void,
  options?: Options,
  title?: string,
) => Promise<{path: string; data: string}> = async (
  showActionSheet: (
    options: ActionSheetOptions,
    callback: (i?: number) => void | Promise<void>,
  ) => void,
  options,
  title,
) => {
  Keyboard.dismiss();
  return await new Promise((resolve, reject) => {
    showActionSheet(
      {
        title,
        options: ['Take Photo', 'Choose from Gallery', 'Cancel'],
        cancelButtonIndex: documentCancelButtonIndex,
      },
      (buttonIndex: number | undefined) => {
        switch (buttonIndex) {
          case 0:
            ImagePicker.openCamera({
              cropperCircleOverlay: true,
              cropping: true,
              height: 300,
              width: 300,
              ...(options || {}),
              multiple: false,
              mediaType: 'photo',
              writeTempFile: true,
              includeBase64: true,
            })
              .then(response => {
                if (Array.isArray(response) && response.length > 0) {
                  resolve({
                    path: response[0].path || '',
                    data: `data:${response[0].mime || ''};base64,${
                      response[0].data || ''
                    }`,
                  });
                } else {
                  resolve({
                    path: response.path || '',
                    data: `data:${response.mime || ''};base64,${
                      response.data || ''
                    }`,
                  });
                }
              })
              .catch(e => reject(e));
            break;
          case 1:
            ImagePicker.openPicker({
              cropperCircleOverlay: true,
              cropping: true,
              height: 300,
              width: 300,
              ...(options || {}),
              multiple: false,
              mediaType: 'photo',
              writeTempFile: true,
              includeBase64: true,
            })
              .then(response => {
                if (Array.isArray(response) && response.length > 0) {
                  resolve({
                    path: response[0].path || '',
                    data: `data:${response[0].mime || ''};base64,${
                      response[0].data || ''
                    }`,
                  });
                } else {
                  resolve({
                    path: response.path || '',
                    data: `data:${response.mime || ''};base64,${
                      response.data || ''
                    }`,
                  });
                }
              })
              .catch(e => reject(e));
            break;
          default:
            reject(new Error('Picker cancelled'));
            break;
        }
      },
    );
  });
};
