export const chunkArray = (array: any[], perChunk: number) => {
  return array.reduce((all, one, i) => {
    const ch = Math.floor(i / perChunk);
    all[ch] = [].concat(all[ch] || [], one);
    return all;
  }, []);
};
