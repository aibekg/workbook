import React, {useCallback, useEffect} from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import Orientation from 'react-native-orientation';
import {MenuProvider} from 'react-native-popup-menu';
import Toast from 'react-native-toast-message';
import {Provider} from 'react-redux';
import {ApolloProvider} from '@apollo/client';
import {ActionSheetProvider} from '@expo/react-native-action-sheet';
import {NavigationContainer} from '@react-navigation/native';
import {PersistGate} from 'redux-persist/integration/react';

import {apolloClient, setReduxStore} from '@api/client';
import {persistor, store} from '@store/index';

import AppStack from './src/navigations/AppStack';

setReduxStore(store);

const App = () => {
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

  const onNavigationReady = useCallback(
    () => RNBootSplash.hide({fade: true}),
    [],
  );

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#1E1F24" />
      <ActionSheetProvider>
        <ApolloProvider client={apolloClient}>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <NavigationContainer onReady={onNavigationReady}>
                <GestureHandlerRootView style={styles.container}>
                  <MenuProvider customStyles={{backdrop: styles.backdrop}}>
                    <AppStack />
                  </MenuProvider>
                </GestureHandlerRootView>
              </NavigationContainer>
            </PersistGate>
          </Provider>
        </ApolloProvider>
      </ActionSheetProvider>
      <Toast />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backdrop: {
    backgroundColor: '#00000080',
    opacity: 1,
  },
});

export default App;
